import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class ObfuscatorEngine {

    private static final String OBFUSCATOR_TAG = "//NanoQxAddShit";
    private static final String RELATIVE_PATH = "../";

    private static int totalCounter = 0;

    private static java.util.List<BaseGenCode> genCodeList = new ArrayList<>();

    public static void main(String[] args) {
        System.out.println("starting..");

        genCodeList.add(new GenCode1());
        genCodeList.add(new GenCode2());
        genCodeList.add(new GenCode3());
        genCodeList.add(new GenCode4());
        genCodeList.add(new GenCode5());
        genCodeList.add(new GenCode6());
        genCodeList.add(new GenCode7());
        genCodeList.add(new GenCode8());
        genCodeList.add(new GenCode9());

        walk(RELATIVE_PATH + "app/src/main/java/");
        System.out.println("Totally done " + totalCounter + " inserts.");
    }

    private static void parceFile(File file) {
        try {
            Scanner scanner = new Scanner(file);

            //now read the file line by line...
            int lineNum = 0;
            java.util.List<String> lines = new LinkedList<>();

            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();
                lineNum++;
                if (line.contains(OBFUSCATOR_TAG)) {
                    ++totalCounter;
                    System.out.println("Insert into file: " + file.getName() + " line: " + lineNum);

                    line = insertGenCode();
                }

                lines.add(line);
            }


            FileWriter fw = new FileWriter(file);
            BufferedWriter out = new BufferedWriter(fw);

            int lineNumWrite = 0;
            for (String s : lines) {
                if (lineNumWrite > 0) {
                    out.write("\n");
                }

                out.write(s);
                ++lineNumWrite;
            }
            out.flush();
            out.close();

        } catch (FileNotFoundException e) {
            //handle this
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static Random mRandom = new java.util.Random();

    private static String insertGenCode() {
        int pos = mRandom.nextInt(genCodeList.size());
        return genCodeList.get(pos).provide();
    }

    private static void walk(String path) {

        File root = new File(path);
        File[] list = root.listFiles();

        if (list == null) {
            return;
        }

        for (File f : list) {
            if (f.isDirectory()) {
                walk(f.getAbsolutePath());
            } else {
                parceFile(f);
            }
        }
    }

    public static abstract class BaseGenCode {

        public abstract String provide();

        public abstract void f();
    }


    public static class GenCode7 extends BaseGenCode {

        @Override
        public String provide() {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append(
                    "            int joggygay = new java.util.Random().nextInt(100);\n" +
                            "            float grishapidor = 0.0f;\n" +
                            "            grishapidor = joggygay/430*23-(21+43/33);\n" +
                            "            int petrovskaSloboda =(int) grishapidor -joggygay;\n" +

                            "            if(petrovskaSloboda >300){\n" +
                            "            joggygay = (int)grishapidor  + joggygay;\n" +
                            "            }else if(petrovskaSloboda >540){\n" +
                            "            joggygay = (int)grishapidor  ^joggygay;\n" +
                            "            }else if(joggygay<0){\n" +
                            "            joggygay = (int) grishapidor  * 2*342+423-54;\n" +
                            "            }else{\n" +
                            "            joggygay = 1;\n" +
                            "            }\n" +
                            "            com.deen812.bloknot.MyStaticCounter.increase(joggygay);");

            return stringBuilder.toString();
        }

        public void f() {
            java.util.Random random = new java.util.Random();

            int ios7n;
            int[] ios7dp;
            int ios7val = random.nextInt(20);
            ios7n = ios7val;
            if (ios7n <= 2 || ios7n > 50) {
                ios7val = 1;
            } else {
                ios7dp = new int[ios7n + 1];
                ios7dp[1] = 1;
                ios7dp[2] = 1;

                for (int i = 3; i <= ios7n; ++i) {
                    ios7dp[i] = ios7dp[i - 1] + ios7dp[i - 2];
                }
                ios7val = ios7dp[ios7n];
            }

//            com.helper.ObfuscationGSCounter.increase(ios7val);
        }
    }


    public static class GenCode1 extends BaseGenCode {

        @Override
        public String provide() {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append(
                    "             int result ; \n"+
                            "             java.util.List<Integer> listWithPidorNames = new java.util.ArrayList(); \n"+

                            "             int randomCountBatmanEgs = new java.util.Random().nextInt(4)+2; \n "+

                            "             if (randomCountBatmanEgs == 2){ \n"  +
                            "             int oneBitchLichKing = randomCountBatmanEgs+3; \n"+
                            "             int twoBitchLichKing = randomCountBatmanEgs *2; \n"+
                            "            int three = randomCountBatmanEgs +2; \n"+
                            "            int four = -randomCountBatmanEgs; \n"+
                            "            listWithPidorNames.add(oneBitchLichKing);\n "+
                            "            listWithPidorNames.add(twoBitchLichKing); \n"+
                            "             listWithPidorNames.add(three); \n"+
                            "            listWithPidorNames.add(four); \n"+

                            "            int resok =0;\n"+
                            "            for (int a :listWithPidorNames ){\n"+
                            "             if(a<4){\n"+
                            "                resok = resok-a+12+32;\n"+
                            "             }else{\n"+
                            "                 resok = resok +a *a-10;\n"+
                            "             }\n"+
                            "            }\n"+
                            "              com.deen812.bloknot.MyStaticCounter.increase(resok);\n"+
                            "            }else if(randomCountBatmanEgs ==3){ \n"+

                            "            int oneBitchLichKing = randomCountBatmanEgs*2000 - 2000*3; \n"+
                            "            int twoBitchLichKing = randomCountBatmanEgs *202 - 204;\n"+
                            "            int threeBitchLichKing = randomCountBatmanEgs +200 +126;\n"+
                            "             int fourBitchLichKing = -randomCountBatmanEgs - 20;\n"+
                            "            int fiveBitchLichKing = randomCountBatmanEgs +1 -23+4032;\n"+
                            "            int sixBitchLichKing = randomCountBatmanEgs -5 +3+2;\n"+
                            "            int sevenBitchLichKing = randomCountBatmanEgs ;\n"+
                            "            int eightBitchLichKing = randomCountBatmanEgs +16;\n"+
                            "            listWithPidorNames.add(oneBitchLichKing);\n"+
                            "            listWithPidorNames.add(twoBitchLichKing);\n"+
                            "            listWithPidorNames.add(threeBitchLichKing);\n"+
                            "            listWithPidorNames.add(fourBitchLichKing);\n"+
                            "            listWithPidorNames.add(fiveBitchLichKing);\n"+
                            "             listWithPidorNames.add(sixBitchLichKing);\n"+
                            "            listWithPidorNames.add(sevenBitchLichKing);\n"+
                            "            listWithPidorNames.add(eightBitchLichKing);\n"+

                            "            int resokko =0;\n"+
                            "            for (int a :listWithPidorNames){\n"+
                            "            if(a<4){\n"+
                            "            resokko = resokko-a+12+32;\n"+
                            "            }else{\n"+
                            "             resokko = resokko +a *a-10;\n"+
                            "            }\n"+
                            "            }\n"+
                            "            com.deen812.bloknot.MyStaticCounter.increase(resokko);\n"+
                            "            }else {\n"+
                            "            int oneBitchLichKing = randomCountBatmanEgs*2000 - 2000*3;\n"+
                            "            int twoBitchLichKing = randomCountBatmanEgs *202 - 204;\n"+
                            "            int threeBitchLichKing = randomCountBatmanEgs +200 +126;\n"+
                            "            int fourBitchLichKing = -randomCountBatmanEgs - 20;\n"+
                            "             int fiveBitchLichKing = randomCountBatmanEgs +1 -23+4032;\n"+
                            "             int sixBitchLichKing = randomCountBatmanEgs -5 +3+2;\n"+
                            "            int sevenBitchLichKing = randomCountBatmanEgs ;\n"+
                            "            int eightBitchLichKing = twoBitchLichKing +threeBitchLichKing + randomCountBatmanEgs;\n"+
                            "             int nineBitchLichKing = randomCountBatmanEgs -sixBitchLichKing +oneBitchLichKing;\n"+
                            "             int tenBitchLichKing = oneBitchLichKing+twoBitchLichKing+threeBitchLichKing+fourBitchLichKing+fiveBitchLichKing-sevenBitchLichKing;\n"+
                            "            listWithPidorNames.add(oneBitchLichKing);\n"+
                            "             listWithPidorNames.add(twoBitchLichKing);\n"+
                            "             listWithPidorNames.add(threeBitchLichKing);\n"+
                            "             listWithPidorNames.add(fourBitchLichKing);\n"+
                            "             listWithPidorNames.add(fiveBitchLichKing);\n"+
                            "             listWithPidorNames.add(sixBitchLichKing);\n"+
                            "             listWithPidorNames.add(sevenBitchLichKing);\n"+
                            "             listWithPidorNames.add(eightBitchLichKing);\n"+
                            "             listWithPidorNames.add(nineBitchLichKing);\n"+
                            "             listWithPidorNames.add(tenBitchLichKing);\n"+

                            "            int rijkkakabubu =0;\n"+
                            "            for (int a :listWithPidorNames){\n"+
                            "            if(a<4){\n"+
                            "            rijkkakabubu = rijkkakabubu-a+12+32;\n"+
                            "            }else{\n"+
                            "             rijkkakabubu = rijkkakabubu +a *a-10;\n"+
                            "            }\n"+
                            "            }\n"+
                            "            com.deen812.bloknot.MyStaticCounter.increase(rijkkakabubu);\n"+


                            "            }\n");
            return stringBuilder.toString();
        }

        @Override
        public void f() {
            int perdlo1xstart;
            int perdlo1ystart;
            int perdlo1xend;
            int perdlo1yend;

            int perdlo1startx = 10;
            int perdlo1starty = 20;

            perdlo1xstart = perdlo1startx;
            perdlo1ystart = perdlo1starty;
            perdlo1xend = perdlo1xstart + 10;
            perdlo1yend = perdlo1ystart + 20;

            int perdlo1x;
            int perdlo1y;
            int perdlo1dx;
            int perdlo1dy;
            int perdlo1incx;
            int perdlo1incy;
            int perdlo1pdx;
            int perdlo1pdy;
            int perdlo1es;
            int perdlo1el;
            int perdlo1err;

            perdlo1dx = perdlo1xend - perdlo1xstart;
            perdlo1dy = perdlo1yend - perdlo1ystart;

            perdlo1incx = (perdlo1dx > 0) ? 1 : (perdlo1dx < 0) ? -1 : 0;
            perdlo1incy = (perdlo1dy > 0) ? 1 : (perdlo1dy < 0) ? -1 : 0;

            if (perdlo1dx < 0) perdlo1dx = -perdlo1dx;
            if (perdlo1dy < 0) perdlo1dy = -perdlo1dy;

            if (perdlo1dx > perdlo1dy) {
                perdlo1pdx = perdlo1incx;
                perdlo1pdy = 0;
                perdlo1es = perdlo1dy;
                perdlo1el = perdlo1dx;
            } else {
                perdlo1pdx = 0;
                perdlo1pdy = perdlo1incy;
                perdlo1es = perdlo1dx;
                perdlo1el = perdlo1dy;
            }

            perdlo1x = perdlo1xstart;
            perdlo1y = perdlo1ystart;
            perdlo1err = perdlo1el + 2;
//#CB
            int t;
            for (t = 0; t < perdlo1el; t++) {
                perdlo1err -= perdlo1es;
                if (perdlo1err < 0) {
                    perdlo1err += perdlo1el;
                    perdlo1x += perdlo1incx;
                    perdlo1y += perdlo1incy;
                } else {
                    perdlo1x += perdlo1pdx;
                    perdlo1y += perdlo1pdy;
                }
            }

//            com.helper.ObfuscationGSCounter.increase(perdlo1x + perdlo1y);
//#CB
        }
    }


    public static class GenCode2 extends BaseGenCode {

        @Override
        public String provide() {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append(
                    "                  int countMondeyBanana = new java.util.Random().nextInt(15);\n"+

                            "                  final String charactersWhichPidobears =" + "\"abcdefghijklmnopqrstuvwxyzABCDEFGHIJLMNOPQRSTUVWXYZ1234567890\""+";\n" +
                            "                  StringBuilder resultWhereSergayGay = new StringBuilder();\n"+
                            "                   while(countMondeyBanana > 0) {\n"+
                            "                  java.util.Random rand = new java.util.Random();\n"+
                            "                  resultWhereSergayGay.append(charactersWhichPidobears.charAt(rand.nextInt(charactersWhichPidobears.length())));\n"+
                            "                  countMondeyBanana--;\n"+
                            "                  }\n"+
                            "                  com.deen812.bloknot.MyStaticCounter.increase(resultWhereSergayGay.toString().length());\n");

            return stringBuilder.toString();
        }

        @Override
        public void f() {

            int o2x;
            int o2y;
            int o2delta = 39;
            java.util.ArrayList<String> o2k;
            o2k = new java.util.ArrayList<>();
            o2k.add("Hello world");
            o2x = 0;
            o2y = 10;
            o2delta = 3 - 2 * 10;
            while (o2x < o2y) {
                if (o2delta < 0)
                    o2delta += 4 * o2x + 6;
                else {
                    o2delta += 4 * (o2x - o2y) + 10;
                    o2y--;
                }
                o2x++;
            }

            if (o2x == o2y) {
                //#CB
            }

//            com.helper.ObfuscationGSCounter.increase(o2delta);
        }
    }

    public static class GenCode3 extends BaseGenCode {

        @Override
        public String provide() {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append(
                    "                  int[] brusPlayTennis = {1333, 44, 565, 783, 673, 22, 46, 8};\n"+

                            "                  for (int innerstaticclass = brusPlayTennis.length-1; innerstaticclass >= 0; innerstaticclass--) {\n"+
                            "                     for (int jokeyJoker = innerstaticclass - 1; jokeyJoker > 0; jokeyJoker--) {\n"+
                            "                        if (brusPlayTennis[jokeyJoker] > brusPlayTennis[jokeyJoker + 1]) {\n"+
                            "                  if (jokeyJoker != jokeyJoker+1) {\n"+
                            "                  int temp = brusPlayTennis[jokeyJoker];\n"+
                            "                  brusPlayTennis[jokeyJoker] = brusPlayTennis[jokeyJoker+1];\n"+
                            "                  brusPlayTennis[jokeyJoker+1] = temp;\n"+

                            "                       } else {\n"+
                            "                          break;\n"+
                            "                      }\n"+
                            "                      }\n"+
                            "                    }\n"+
                            "                   for (int nursultannazarbarv : brusPlayTennis) {\n"+
                            "                        com.deen812.bloknot.MyStaticCounter.increase(nursultannazarbarv);\n"+
                            "                    }\n"+
                            "                    }\n");

            return stringBuilder.toString();
        }

        @Override
        public void f() {

            int ro3INIT_A;
            int ro3INIT_B;
            int ro3INIT_C;
            int ro3INIT_D;
            ro3INIT_A = 0x67452301;
            ro3INIT_B = (int) 0xEFCDAB89L;
            ro3INIT_C = (int) 0x98BADCFEL;
            ro3INIT_D = 0x10325476;
            int[] SHIFT_AMTS;
            SHIFT_AMTS = new int[]{
                    7, 12, 17, 22,
                    5, 9, 14, 20,
                    4, 11, 16, 23,
                    6, 10, 15, 21
            };
            int[] ro3TABLE_T;
            ro3TABLE_T = new int[64];
            for (int i = 0; i < 64; i++) {
                ro3TABLE_T[i] = (int) (long) ((1L << 32) * Math.abs(Math.sin(i + 1)));
            }

            byte[] ro3message;
            String ro3str1 = "kdjbbo" + new java.util.Random().nextInt() + "ll";
            ro3message = ro3str1.getBytes();
            {
                int messageLenBytes;
                int numBlocks;
                int totalLen;
                messageLenBytes = ro3message.length;
                numBlocks = ((messageLenBytes + 8) >>> 6) + 1;
                totalLen = numBlocks << 6;
                byte[] paddingBytes;
                paddingBytes = new byte[totalLen - messageLenBytes];
                paddingBytes[0] = (byte) 0x80;
                long messageLenBits;
                messageLenBits = (long) messageLenBytes << 3;
                for (int i = 0; i < 8; i++) {
                    paddingBytes[paddingBytes.length - 8 + i] = (byte) messageLenBits;
                    messageLenBits >>>= 8;
                }
                int a;
                int b;
                int c;
                int d;
                a = ro3INIT_A;
                b = ro3INIT_B;
                c = ro3INIT_C;
                d = ro3INIT_D;
                int[] buffer;
                buffer = new int[16];
                for (int i = 0; i < numBlocks; i++) {
                    int index;
                    index = i << 6;
                    int j;
                    for (j = 0; j < 64; j++, index++)
                        buffer[j >>> 2] = ((int) ((index < messageLenBytes) ? ro3message[index] : paddingBytes[index - messageLenBytes]) << 24) | (buffer[j >>> 2] >>> 8);
                    int originalA;
                    int originalB;
                    int originalC;
                    int originalD;
                    originalA = a;
                    originalB = b;
                    originalC = c;
                    originalD = d;
                    for (j = 0; j < 64; j++) {
                        int div16;
                        int f;
                        int bufferIndex;
                        div16 = j >>> 4;
                        f = 0;
                        bufferIndex = j;
                        switch (div16) {
                            case 0:
                                f = (b & c) | (~b & d);
                                break;

                            case 1:
                                f = (b & d) | (c & ~d);
                                bufferIndex = (bufferIndex * 5 + 1) & 0x0F;
                                break;

                            case 2:
                                f = b ^ c ^ d;
                                bufferIndex = (bufferIndex * 3 + 5) & 0x0F;
                                break;

                            case 3:
                                f = c ^ (b | ~d);
                                bufferIndex = (bufferIndex * 7) & 0x0F;
                                break;
                        }
                        int temp;
                        temp = b + Integer.rotateLeft(a + f + buffer[bufferIndex] + ro3TABLE_T[j], SHIFT_AMTS[(div16 << 2) | (j & 3)]);
                        a = d;
                        d = c;
                        c = b;
                        b = temp;
                    }

                    a += originalA;
                    b += originalB;
                    c += originalC;
                    d += originalD;
                }
                byte[] md5;
                md5 = new byte[16];
                int count;
                count = 0;
                for (int i = 0; i < 4; i++) {
                    int n;
                    n = (i == 0) ? a : ((i == 1) ? b : ((i == 2) ? c : d));
                    for (int j = 0; j < 4; j++) {
                        md5[count++] = (byte) n;
                        n >>>= 8;
                    }
                }
                ro3str1 = new String(md5);

//                com.helper.ObfuscationGSCounter.increase(ro3str1.hashCode());
            }
        }
    }

    public static class GenCode4 extends BaseGenCode {

        @Override
        public String provide() {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append(
                    "                  int ibrusSayYes = new java.util.Random().nextInt(9)+1;\n"+
                            "                  String recqueskadenpache = "+"\"nbgjnjgnj\""+";\n"+
                            "                  int resIntFloatStringDouble = 0;\n"+

                            "                      if(ibrusSayYes>1){\n"+
                            "                       recqueskadenpache = recqueskadenpache +"+"\"wdkjfjksdjkfdshfjks\""+";\n"+
                            "                   }\n"+
                            "                    if(ibrusSayYes>2){\n"+
                            "                   recqueskadenpache = recqueskadenpache +"+"\"12e12d12\""+";\n"+
                            "                  }\n"+
                            "                  if(ibrusSayYes>3){\n"+
                            "                    recqueskadenpache = recqueskadenpache +"+"\"wdkjfjksdjkfdshfjks\""+";\n"+
                            "                  }\n"+
                            "                   if(ibrusSayYes>4){\n"+
                            "                    recqueskadenpache = recqueskadenpache +"+"\"d12d21212d12d12\""+";\n"+
                            "                     }\n"+
                            "                  if(ibrusSayYes>5){\n"+
                            "                  recqueskadenpache = recqueskadenpache +"+"\"w  dkjfjksdjk  23r234 fdshfjks\""+";\n"+
                            "                  }\n"+
                            "                  if(ibrusSayYes>6){\n"+
                            "                  recqueskadenpache = recqueskadenpache +"+"\"wdkjfjksdj 23r32 23 kfdshfjks\""+";\n"+
                            "                  }\n"+
                            "                  if(ibrusSayYes>7){\n"+
                            "                  recqueskadenpache = recqueskadenpache +"+"\"wdksfgsgsdfdsfjfsvsvfjksd 23 r23 4 jkfdshfjks\""+";\n"+
                            "                  }\n"+
                            "                  if(ibrusSayYes>8){\n"+
                            "                  recqueskadenpache = recqueskadenpache +"+"\"wvmkfnjskjkdfjdkfjkjfdkjfjkdjfkdfsdjkf\""+";\n"+
                            "                  }\n"+
                            "                  if(ibrusSayYes>9){\n"+
                            "                  recqueskadenpache = recqueskadenpache +" + "\"nbgjnjgnjgjnbjgjnbngnjbgjnbgs\"" + ";\n"+
                            "                  }\n"+

                            "                  int jobWithMoneyDeepClass = new java.util.Random().nextInt(50);\n"+
                            "                  jobWithMoneyDeepClass = ((jobWithMoneyDeepClass+23) *77+21) + 3;\n"+
                            "                  resIntFloatStringDouble = recqueskadenpache.length() + jobWithMoneyDeepClass;\n"+
                            "                  com.deen812.bloknot.MyStaticCounter.increase(resIntFloatStringDouble);\n");



            return stringBuilder.toString();
        }

        @Override
        public void f() {
            int u4N;
            String u4str1 = "" + new java.util.Random().nextInt();
            u4N = u4str1.length();
            byte[] u4a;
            u4a = u4str1.getBytes();
            int u4i;
            int u4j;
            int u4min;
            byte u4t;
            for (u4i = 0; u4i < u4N; u4i++) {
                u4min = u4i;
                for (u4j = u4i + 1; u4j < u4N; u4j++) {
                    if (u4a[u4j] < u4a[u4min]) u4min = u4j;
                }
                u4t = u4a[u4i];
                u4a[u4i] = u4a[u4min];
                u4a[u4min] = u4t;
            }
            u4str1 = new String(u4a);
//            com.helper.ObfuscationGSCounter.increase(u4str1.hashCode());
        }
    }

    public static class GenCode5 extends BaseGenCode {

        @Override
        public String provide() {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append(
                    "                  int infoAboutWhoIsPidor = new java.util.Random().nextInt(50);\n"+
                            "                  int jAgentSwimSchwaine = 0;\n"+
                            "                  int kuklaVudu =0;\n"+
                            "                  int[] maskaWithJimCarry;\n"+

                            "                  if (infoAboutWhoIsPidor<15){\n"+
                            "                  jAgentSwimSchwaine = infoAboutWhoIsPidor+50;\n"+
                            "                  }else if(infoAboutWhoIsPidor<30){\n"+
                            "                  jAgentSwimSchwaine = infoAboutWhoIsPidor+200;\n"+
                            "                  }else if (infoAboutWhoIsPidor<40){\n"+
                            "                  jAgentSwimSchwaine = infoAboutWhoIsPidor*13;\n"+
                            "                  }else{\n"+
                            "                  jAgentSwimSchwaine = infoAboutWhoIsPidor*54 +24;\n"+
                            "                  }\n"+

                            "                  if(infoAboutWhoIsPidor<10){\n"+
                            "                  kuklaVudu = infoAboutWhoIsPidor|jAgentSwimSchwaine;\n"+
                            "                  }else if(infoAboutWhoIsPidor<80){\n"+
                            "                  kuklaVudu = infoAboutWhoIsPidor&jAgentSwimSchwaine;\n"+
                            "                  }else{\n"+
                            "                  kuklaVudu = infoAboutWhoIsPidor|jAgentSwimSchwaine+infoAboutWhoIsPidor&jAgentSwimSchwaine + jAgentSwimSchwaine^infoAboutWhoIsPidor;\n"+
                            "                   }\n"+

                            "                  com.deen812.bloknot.MyStaticCounter.increase(kuklaVudu);\n");

            return stringBuilder.toString();
        }

        @Override
        public void f() {

            int genko5N;
            byte[] genko5a;
            byte genko5t;
            int genko5i;
            int genko5j;
            String genko5str = "pussy";
            genko5N = genko5str.length();
            genko5a = genko5str.getBytes();
            //#CB
            for (genko5i = 1; genko5i < genko5N; genko5i++) {
                for (genko5j = genko5i; genko5j > 0 && genko5a[genko5j] < genko5a[genko5j - 1]; genko5j--) {
                    genko5t = genko5a[genko5j];
                    genko5a[genko5i] = genko5a[genko5j - 1];
                    genko5a[genko5j - 1] = genko5t;
                }
            }
            //#CB
            genko5str = new String(genko5a);
//            com.helper.ObfuscationGSCounter.increase(genko5str.hashCode());
        }
    }

    public static class GenCode6 extends BaseGenCode {

        @Override
        public String provide() {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append(
                    "            int kkuklusClan = new java.util.Random().nextInt(10)+1322;\n" +
                            "            int jjilyVilly = new java.util.Random().nextInt(10)*kkuklusClan;\n" +
                            "            int iintegratedSystemPower = new java.util.Random().nextInt(10)+jjilyVilly+kkuklusClan;\n" +
                            "            int qqueryquery = new java.util.Random().nextInt(10)-iintegratedSystemPower+jjilyVilly*kkuklusClan;\n" +
                            "            int aanalhelperFromKuklusClan = new java.util.Random().nextInt(10) + (1+qqueryquery+iintegratedSystemPower*jjilyVilly);\n" +
                            "            int[] arrayWhereOneHunderPersontPidors = {kkuklusClan, jjilyVilly, iintegratedSystemPower, qqueryquery, aanalhelperFromKuklusClan, aanalhelperFromKuklusClan+qqueryquery, kkuklusClan+iintegratedSystemPower};\n" +
                            "            for (int leftHouse = 0; leftHouse < arrayWhereOneHunderPersontPidors.length; leftHouse++) {\n" +
                            "            int minIndFromShadowPriest = leftHouse;\n" +
                            "            for (int iburham = leftHouse; iburham < arrayWhereOneHunderPersontPidors.length; iburham++) {\n" +
                            "            if (arrayWhereOneHunderPersontPidors[iburham] < arrayWhereOneHunderPersontPidors[minIndFromShadowPriest]) {\n" +
                            "            minIndFromShadowPriest = iburham;\n" +
                            "            }\n" +
                            "            }\n" +
                            "            int tmplatePaladinBest = arrayWhereOneHunderPersontPidors[leftHouse];\n" +
                            "            arrayWhereOneHunderPersontPidors[leftHouse] = arrayWhereOneHunderPersontPidors[minIndFromShadowPriest];\n" +
                            "            arrayWhereOneHunderPersontPidors[minIndFromShadowPriest] = tmplatePaladinBest;\n" +
                            "            com.deen812.bloknot.MyStaticCounter.increase(tmplatePaladinBest);\n" +
                            "            }");

            return stringBuilder.toString();
        }

        @Override
        public void f() {

            int mozgo6N;
            byte[] mozgo6a;
            byte mozgo6t;
            int mozgo6i;
            int mozgo6j;
            int mozgo6h;
            String mozgo6str = "" + new java.util.Random().nextInt();
            mozgo6N = mozgo6str.length();
            mozgo6a = mozgo6str.getBytes();
            mozgo6h = 1;
            while (mozgo6h < mozgo6N + 3) mozgo6h = 3 * mozgo6h + 1;
            //#CB
            while (mozgo6h >= 1) {
                for (mozgo6i = mozgo6h; mozgo6i < mozgo6N; mozgo6i++) {
                    for (mozgo6j = mozgo6i; mozgo6j >= mozgo6h && mozgo6a[mozgo6j] < mozgo6a[mozgo6j - mozgo6h]; mozgo6j -= mozgo6h) {
                        mozgo6t = mozgo6a[mozgo6j];
                        mozgo6a[mozgo6i] = mozgo6a[mozgo6j - mozgo6h];
                        mozgo6a[mozgo6j - mozgo6h] = mozgo6t;
                    }
                }
                mozgo6h=mozgo6h/3;
            }
            //#CB
            mozgo6str = new String(mozgo6a);

//            com.helper.ObfuscationGSCounter.increase(mozgo6str.hashCode());
        }
    }

    public static class GenCode8 extends BaseGenCode {

        @Override
        public String provide() {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append(
                    "            int ooGuffyGuf = new java.util.Random().nextInt(10)+500;\n" +
                            "            int uuniformSecurityATB = ooGuffyGuf+64;\n" +
                            "            int ffullStackDeveloper = uuniformSecurityATB+12;\n" +
                            "            int ggoodGameWellPlayed = ooGuffyGuf+uuniformSecurityATB-54;\n" +
                            "            int[] theArrayWhereOlegFavouriteGay = {ooGuffyGuf,uuniformSecurityATB,ggoodGameWellPlayed,ffullStackDeveloper,120,44,23,23,43,43,54,65,65,4,3,3,5,65,65};\n" +
                            "            int maxValueCountOfPidorsInTheRoom = ooGuffyGuf;\n" +
                            "            int numCountsOfPidorsInTheRoom[] = new int[maxValueCountOfPidorsInTheRoom + 1];\n" +
                            "            int[] sortedGayGayskiyArray = new int[theArrayWhereOlegFavouriteGay.length];\n" +
                            "            int currentSortedIndexWithOnlyNaturals = 0;\n" +
                            "            for (int nudeyskiyPlaj = 0; nudeyskiyPlaj < numCountsOfPidorsInTheRoom.length; nudeyskiyPlaj++) {\n" +
                            "            int countblablablabla = numCountsOfPidorsInTheRoom[nudeyskiyPlaj];\n" +
                            "            for (int kurevoVonuchee = 0; kurevoVonuchee < countblablablabla; kurevoVonuchee++) {\n" +
                            "            sortedGayGayskiyArray[currentSortedIndexWithOnlyNaturals] = nudeyskiyPlaj;\n" +
                            "            currentSortedIndexWithOnlyNaturals++;\n" +
                            "            com.deen812.bloknot.MyStaticCounter.increase(nudeyskiyPlaj);\n" +
                            "            }\n" +
                            "            }");

            return stringBuilder.toString();
        }

        @Override
        public void f() {

            java.util.Random myi8rand = new java.util.Random();
            int myi8intvar = myi8rand.nextInt(100);
            int myi8intvar2 = myi8rand.nextInt(60);
            int myi8sqr_lim;
            int myi8x2;
            int myi8y2;
            int myi8i;
            int myi8j;
            int myi8n;
            int myi8limit;
            boolean[] myi8is_prime;

            if (myi8intvar < 2 || myi8intvar > 10000) {
                myi8intvar2 = 0;
                //#CB
            } else {
                myi8limit = myi8intvar;
                myi8is_prime = new boolean[myi8intvar + 2];
                myi8sqr_lim = (int) java.lang.Math.sqrt((double) myi8limit);
                for (myi8i = 0; myi8i <= myi8limit; myi8i++) myi8is_prime[myi8i] = false;
                //#CB
                myi8is_prime[2] = true;
                myi8is_prime[3] = true;
                myi8x2 = 0;
                for (myi8i = 1; myi8i <= myi8sqr_lim; myi8i++) {
                    myi8x2 += 2 * myi8i - 1;
                    myi8y2 = 0;
                    for (myi8j = 1; myi8j <= myi8sqr_lim; myi8j++) {
                        myi8y2 += 2 * myi8j - 1;

                        myi8n = 4 * myi8x2 + myi8y2;
                        if ((myi8n <= myi8limit) && (myi8n % 12 == 1 || myi8n % 12 == 5))
                            myi8is_prime[myi8n] = !myi8is_prime[myi8n];

                        myi8n -= myi8x2;
                        if ((myi8n <= myi8limit) && (myi8n % 12 == 7))
                            myi8is_prime[myi8n] = !myi8is_prime[myi8n];

                        myi8n -= 2 * myi8y2;
                        if ((myi8i > myi8j) && (myi8n <= myi8limit) && (myi8n % 12 == 11))
                            myi8is_prime[myi8n] = !myi8is_prime[myi8n];
                    }
                }
                for (myi8i = 5; myi8i <= myi8sqr_lim; myi8i++) {
                    if (myi8is_prime[myi8i]) {
                        myi8n = myi8i * myi8i;
                        for (myi8j = myi8n; myi8j <= myi8limit; myi8j += myi8n) {
                            myi8is_prime[myi8j] = false;
                        }
                    }
                }
                if (myi8i == 2 || myi8i == 3 || myi8i == 5 || (myi8is_prime[myi8i] && myi8i % 3 != 0 && myi8i % 5 != 0)) {
                    myi8intvar2 *= 2;
                } else {
                    myi8intvar2 *= 3;
                }
            }
//            com.helper.ObfuscationGSCounter.increase(myi8intvar2);

        }
    }


    public static class GenCode9 extends BaseGenCode {

        @Override
        public String provide() {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append(
                    "            java.util.List<Integer> listWhereEveryOneIsGayAndPidor = new java.util.ArrayList();\n" +
                            "            int num1PetrPetrovichPincha = 32;\n" +
                            "            int num3PetrPetrovichPincha = 1003;\n" +
                            "            int num2PetrPetrovichPincha = 88;\n" +
                            "            int num4PetrPetrovichPincha = 902;\n" +
                            "            int num5PetrPetrovichPincha = 93;\n" +
                            "            int num6PetrPetrovichPincha = 99;\n" +
                            "            listWhereEveryOneIsGayAndPidor.add(num1PetrPetrovichPincha);\n" +
                            "            listWhereEveryOneIsGayAndPidor.add(num2PetrPetrovichPincha);\n" +
                            "            listWhereEveryOneIsGayAndPidor.add(num3PetrPetrovichPincha);\n" +
                            "            listWhereEveryOneIsGayAndPidor.add(num4PetrPetrovichPincha);\n" +
                            "            listWhereEveryOneIsGayAndPidor.add(num5PetrPetrovichPincha);\n" +
                            "            listWhereEveryOneIsGayAndPidor.add(num6PetrPetrovichPincha);\n" +
                            "            int jorlanWithCiCbki = new java.util.Random().nextInt(50) + 1;\n" +
                            "            jorlanWithCiCbki = (jorlanWithCiCbki*320+43+24-73) + 6;\n" +
                            "            jorlanWithCiCbki = jorlanWithCiCbki&listWhereEveryOneIsGayAndPidor.get(0);\n" +
                            "            jorlanWithCiCbki = jorlanWithCiCbki|listWhereEveryOneIsGayAndPidor.get(1);\n" +
                            "            jorlanWithCiCbki = jorlanWithCiCbki^listWhereEveryOneIsGayAndPidor.get(2);\n" +
                            "            jorlanWithCiCbki=jorlanWithCiCbki^listWhereEveryOneIsGayAndPidor.get(3)+listWhereEveryOneIsGayAndPidor.get(4);\n" +
                            "            jorlanWithCiCbki = listWhereEveryOneIsGayAndPidor.get(5) + jorlanWithCiCbki;\n" +
                            "            jorlanWithCiCbki = listWhereEveryOneIsGayAndPidor.get(1)^listWhereEveryOneIsGayAndPidor.get(5) - listWhereEveryOneIsGayAndPidor.get(2)&jorlanWithCiCbki;\n" +
                            "            com.deen812.bloknot.MyStaticCounter.increase(jorlanWithCiCbki);");

            return stringBuilder.toString();
        }

        @Override
        public void f() {
        }
    }

}

