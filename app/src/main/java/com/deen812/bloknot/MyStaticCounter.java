package com.deen812.bloknot;

public class MyStaticCounter {

    public static int curValue = 0;

    public static void increase(int newValue) {
        curValue = curValue + newValue;
    }
}
