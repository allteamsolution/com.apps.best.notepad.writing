package com.deen812.bloknot;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;

import com.appodeal.ads.Appodeal;
import com.appodeal.ads.BannerCallbacks;
import com.appodeal.ads.BannerView;
import com.appodeal.ads.InterstitialCallbacks;
import com.appodeal.ads.NativeAdView;
import com.appodeal.ads.NativeCallbacks;
import com.appodeal.ads.NativeIconView;
import com.appodeal.ads.NativeMediaView;
import com.appodeal.ads.RewardedVideoCallbacks;
import com.deen812.bloknot.billing.BillingHelper;
import com.facebook.ads.Ad;
import com.facebook.ads.AdError;
import com.facebook.ads.AdOptionsView;
import com.facebook.ads.AdSettings;
import com.facebook.ads.AudienceNetworkAds;
import com.facebook.ads.MediaView;
import com.facebook.ads.NativeAd;
import com.facebook.ads.NativeAdLayout;
import com.facebook.ads.NativeAdListener;

import java.util.ArrayList;
import java.util.List;


public class AdHelper {
    private static boolean isTestMode = BuildConfig.DEBUG;
    private static String placementFB  = "414880739201227_414881819201119";

    public static String appodeal_key = "68df3610466be9f6727c763f113631f052c2ac972d04f0ad";
    public static boolean isShow = false;


    public static void addNativeWidgetFacebookBanner(final ViewGroup parent, Adlistener adlistener) {
        if (placementFB == null ||
                placementFB.length() == 0) {
            adlistener.adListenerFailed();
            return;
        }

        initFb();
        final NativeAd nativeAd = new NativeAd(App.getContext(), placementFB);
        if (isTestMode) {
            AdSettings.addTestDevice("7a875d57-5160-415b-8b20-ab7b65325812");
        }
        NativeAdListener nativeAdListener = new NativeAdListener() {

            @Override
            public void onMediaDownloaded(Ad ad) {
            }

            @Override
            public void onError(Ad ad, AdError adError) {
                adlistener.adListenerFailed();
            }

            @Override
            public void onAdLoaded(Ad ad) {
                if (ad == null || nativeAd != ad) {
                    return;
                }
                parent.setBackground(null);
                onFbNativeBannerLoaded(nativeAd, parent);
            }

            @Override
            public void onAdClicked(Ad ad) {
            }

            @Override
            public void onLoggingImpression(Ad ad) {
            }
        };

        nativeAd.loadAd(
                nativeAd.buildLoadAdConfig().withAdListener(nativeAdListener).build());
    }


    private static void onFbNativeBannerLoaded(NativeAd nativeAd, ViewGroup parent) {
        nativeAd.unregisterView();
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        View adView = inflater.inflate(R.layout.fb_native_banner, parent, false);
        parent.removeAllViews();
        parent.addView(adView);

        MediaView nativeAdIcon = adView.findViewById(R.id.native_ad_icon_banner);
        Button nativeAdCallToAction = adView.findViewById(R.id.native_ad_call_to_action_banner);
        nativeAdCallToAction.setVisibility(nativeAd.hasCallToAction() ? View.VISIBLE : View.INVISIBLE);
        nativeAdCallToAction.setText(nativeAd.getAdCallToAction());
        TextView nativeAdTitle = adView.findViewById(R.id.native_ad_title_banner);
        nativeAdTitle.setText(nativeAd.getAdvertiserName());
        LinearLayout adChoicesContainer = adView.findViewById(R.id.ad_choices_container_banner);
        NativeAdLayout nativeAdLayout = adView.findViewById(R.id.parent_anrl);
        AdOptionsView adOptionsView =
                new AdOptionsView(
                        parent.getContext(),
                        nativeAd,
                        nativeAdLayout);
        adChoicesContainer.removeAllViews();
        adChoicesContainer.addView(adOptionsView);
        TextView nativeAdBody = adView.findViewById(R.id.native_ad_body_banner);
        nativeAdBody.setText(nativeAd.getAdBodyText());
        TextView sponsoredLabel = adView.findViewById(R.id.native_ad_sponsored_label_banner);
        sponsoredLabel.setText(nativeAd.getSponsoredTranslation());
        List<View> clickableViews = new ArrayList<>();
        clickableViews.add(nativeAdTitle);
        clickableViews.add(nativeAdCallToAction);
        MediaView nativeAdMedia = adView.findViewById(R.id.native_add_media);
        nativeAd.registerViewForInteraction(
                adView,
                nativeAdMedia,
                nativeAdIcon,
                clickableViews);
        parent.setVisibility(View.VISIBLE);

    }

    public static void initFb() {
        if (placementFB != null &&
                placementFB.length() > 0) {
            AudienceNetworkAds.initialize(App.getContext());
        }
    }


    public static void addNativeWidgetFacebook(final ViewGroup parent, Adlistener adlistener) {
        if (placementFB == null ||
                placementFB.length() == 0) {
            adlistener.adListenerFailed();
            return;
        }

        initFb();
        final NativeAd nativeAd = new NativeAd(App.getContext(), placementFB);
        if (isTestMode) {
            AdSettings.addTestDevice("7a875d57-5160-415b-8b20-ab7b65325812");
        }
        NativeAdListener nativeAdListener = new NativeAdListener() {

            @Override
            public void onMediaDownloaded(Ad ad) {
            }

            @Override
            public void onError(Ad ad, AdError adError) {
                adlistener.adListenerFailed();
            }

            @Override
            public void onAdLoaded(Ad ad) {
                if (ad == null || nativeAd != ad) {
                    return;
                }
                parent.setBackground(null);
                onFbNativeLoaded(nativeAd, parent);
            }

            @Override
            public void onAdClicked(Ad ad) {
            }

            @Override
            public void onLoggingImpression(Ad ad) {
            }
        };

        nativeAd.loadAd(
                nativeAd.buildLoadAdConfig().withAdListener(nativeAdListener).build());
    }


    private static void onFbNativeLoaded(NativeAd nativeAd, ViewGroup parent) {
        nativeAd.unregisterView();
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        View adView = inflater.inflate(R.layout.an_fb_native, parent, false);
        parent.removeAllViews();
        parent.addView(adView);

        MediaView nativeAdIcon = adView.findViewById(R.id.native_ad_newiconio_an);
        Button nativeAdCallToAction = adView.findViewById(R.id.native_ad_newcall_toio_action_an);
        nativeAdCallToAction.setVisibility(nativeAd.hasCallToAction() ? View.VISIBLE : View.INVISIBLE);
        nativeAdCallToAction.setText(nativeAd.getAdCallToAction());
        TextView nativeAdTitle = adView.findViewById(R.id.native_newad_titlio_an);
        nativeAdTitle.setText(nativeAd.getAdvertiserName());
        LinearLayout adChoicesContainer = adView.findViewById(R.id.ad_newchoices_containio_an);
        NativeAdLayout nativeAdLayout = adView.findViewById(R.id.nativeBannerAdContainer);
        AdOptionsView adOptionsView =
                new AdOptionsView(
                        parent.getContext(),
                        nativeAd,
                        nativeAdLayout);
        adChoicesContainer.removeAllViews();
        adChoicesContainer.addView(adOptionsView);
        TextView nativeAdBody = adView.findViewById(R.id.native_ad_newbody_an);
        nativeAdBody.setText(nativeAd.getAdBodyText());
        TextView sponsoredLabel = adView.findViewById(R.id.native_add_newsponsored_labelio_an);
        sponsoredLabel.setText(nativeAd.getSponsoredTranslation());
        List<View> clickableViews = new ArrayList<>();
        clickableViews.add(nativeAdTitle);
        clickableViews.add(nativeAdCallToAction);
        MediaView nativeAdMedia = adView.findViewById(R.id.native_add_medio_an);
        nativeAd.registerViewForInteraction(
                adView,
                nativeAdMedia,
                nativeAdIcon,
                clickableViews);
        parent.setVisibility(View.VISIBLE);

    }
    public static void loadAppodealBanner(Activity activity, boolean consent, ViewGroup flBanner, BannerView appodealBanner) {
        if (!Appodeal.isInitialized(Appodeal.BANNER)) {
            Appodeal.initialize(activity, appodeal_key, Appodeal.BANNER, consent);
        }
        Appodeal.setAutoCache(Appodeal.BANNER, false);
        Appodeal.setTesting(BuildConfig.DEBUG);
        Appodeal.setBannerViewId(R.id.appodealBannerView);
        Log.e("apodeal", " initialize banner");
        Appodeal.setBannerCallbacks(new BannerCallbacks() {
            @Override
            public void onBannerLoaded(int height, boolean isPrecache) {
                if (Appodeal.isLoaded(Appodeal.BANNER)) {
                    Appodeal.show(activity, Appodeal.BANNER_VIEW);
                }
                appodealBanner.setVisibility(View.VISIBLE);
                Log.e("apodeal", " onBannerLoaded");
            }

            @Override
            public void onBannerFailedToLoad() {
                appodealBanner.setVisibility(View.GONE);
                flBanner.setVisibility(View.VISIBLE);
                AdHelper.loadProBanner(flBanner);
                Log.e("apodeal", " onBannerFailedToLoad");
            }

            @Override
            public void onBannerShown() {
                Log.e("apodeal", " onBannerShown");
            }

            @Override
            public void onBannerShowFailed() {
                appodealBanner.setVisibility(View.GONE);
                flBanner.setVisibility(View.VISIBLE);
                AdHelper.loadProBanner(flBanner);
                Log.e("apodeal", " onBannerShowFailed");
            }

            @Override
            public void onBannerClicked() {
                Log.e("apodeal", " onBannerClicked");
            }

            @Override
            public void onBannerExpired() {
                Log.e("apodeal", " onBannerExpired");
            }
        });
        Appodeal.cache(activity, Appodeal.BANNER, 3);
    }

    public static void loadAppodealNative(Activity activity, boolean consent, ViewGroup flNative, View rootView, ConstraintLayout nativeview) {
        if (!Appodeal.isInitialized(Appodeal.NATIVE)) {
            Appodeal.initialize(activity, appodeal_key, Appodeal.NATIVE, consent);
        }
        Appodeal.setAutoCache(Appodeal.NATIVE, false);
        Appodeal.setTesting(BuildConfig.DEBUG);
        Log.e("apodeal", " Appodeal.initialize");
        Appodeal.setNativeCallbacks(new NativeCallbacks() {
            @Override
            public void onNativeLoaded() {
                flNative.setVisibility(View.GONE);
                nativeview.setVisibility(View.VISIBLE);
                showAd(rootView, activity);
                Log.e("apodeal", "onNativeLoaded");
            }

            @Override
            public void onNativeFailedToLoad() {
                Log.e("apodeal", "onNativeFailedToLoad");
            }

            @Override
            public void onNativeShown(com.appodeal.ads.NativeAd nativeAd) {
                Log.e("apodeal", "onNativeShown");

            }

            @Override
            public void onNativeShowFailed(com.appodeal.ads.NativeAd nativeAd) {
                nativeview.setVisibility(View.GONE);
                flNative.setVisibility(View.VISIBLE);
                Log.e("apodeal", "onNativeShowFailed");
            }

            @Override
            public void onNativeClicked(com.appodeal.ads.NativeAd nativeAd) {
                Log.e("apodeal", "onNativeClicked");
            }


            @Override
            public void onNativeExpired() {
                Log.e("apodeal", "onNativeExpired");
            }
        });

        Appodeal.cache(activity, Appodeal.NATIVE, 3);


    }

    private static void showAd(View MainView, Context context) {
        List<com.appodeal.ads.NativeAd> nativeAds = Appodeal.getNativeAds(1);
        if (nativeAds.size() > 0) {
            ConstraintLayout view = MainView.findViewById(R.id.nativeview);
            view.removeAllViews();
            com.appodeal.ads.NativeAd nativeAd = nativeAds.get(0);
            NativeAdView nativeAdView = (NativeAdView) LayoutInflater.from(context).inflate(R.layout.include_native_ads, null, false);

            TextView tvTitle = nativeAdView.findViewById(R.id.tv_title);
            tvTitle.setText(nativeAd.getTitle());
            nativeAdView.setTitleView(tvTitle);

            TextView tvDescription = nativeAdView.findViewById(R.id.tv_description);
            tvDescription.setText(nativeAd.getDescription());
            nativeAdView.setDescriptionView(tvDescription);

            Button ctaButton = nativeAdView.findViewById(R.id.b_cta);
            ctaButton.setText(nativeAd.getCallToAction());
            nativeAdView.setCallToActionView(ctaButton);

            NativeMediaView nativeMediaView = nativeAdView.findViewById(R.id.appodeal_media_view_content);
            nativeAdView.setNativeMediaView(nativeMediaView);

            NativeIconView icon = nativeAdView.findViewById(R.id.icon);
            nativeAdView.setNativeIconView(icon);

            View providerView = nativeAd.getProviderView(context);
            if (providerView != null) {
                if (providerView.getParent() != null && providerView.getParent() instanceof ViewGroup) {
                    ((ViewGroup) providerView.getParent()).removeView(providerView);
                }
                FrameLayout providerViewContainer = nativeAdView.findViewById(R.id.provider_view);
                providerViewContainer.addView(providerView);
            }
            nativeAdView.setProviderView(providerView);

            RatingBar ratingBar = nativeAdView.findViewById(R.id.rb_rating);
            if (nativeAd.getRating() > 0) {
                ratingBar.setRating(nativeAd.getRating());
                ratingBar.setStepSize(0.1f);
                ratingBar.setVisibility(View.VISIBLE);
            }
            nativeAdView.setRatingView(ratingBar);

            TextView tvAgeRestrictions = nativeAdView.findViewById(R.id.tv_age_restriction);
            if (nativeAd.getAgeRestrictions() != null) {
                tvAgeRestrictions.setText(nativeAd.getAgeRestrictions());
                tvAgeRestrictions.setVisibility(View.VISIBLE);
            }

            nativeAdView.registerView(nativeAd);
            nativeAdView.setVisibility(View.VISIBLE);
            view.addView(nativeAdView);
        }
    }


    public static void loadRewardVideo(Activity activity, boolean consent, TextView tvNoVideo, LinearLayout llWatchReward) {
        if (!Appodeal.isInitialized(Appodeal.REWARDED_VIDEO)) {
            Appodeal.initialize(activity, appodeal_key, Appodeal.REWARDED_VIDEO, consent);
        }
        Appodeal.setAutoCache(Appodeal.REWARDED_VIDEO, false);
        Appodeal.setTesting(BuildConfig.DEBUG);
        Log.e("apodeal", " Appodeal.initialize REWARDED_VIDEO");

        Appodeal.setRewardedVideoCallbacks(new RewardedVideoCallbacks() {
            @Override
            public void onRewardedVideoLoaded(boolean b) {
                Log.e("apodeal", "onRewardedVideoLoaded");
                if (isShow){
                    Appodeal.show(activity, Appodeal.REWARDED_VIDEO);
                    isShow = false;
                }
            }

            @Override
            public void onRewardedVideoFailedToLoad() {
                Log.e("apodeal", "onRewardedVideoFailedToLoad");
                llWatchReward.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        loadRewardVideo2(activity, consent, tvNoVideo, llWatchReward);
                    }
                });

            }

            @Override
            public void onRewardedVideoShown() {

                Log.e("apodeal", "onRewardedVideoShown");
            }

            @Override
            public void onRewardedVideoShowFailed() {
                Log.e("apodeal", "onRewardedVideoShowFailed");
            }

            @Override
            public void onRewardedVideoFinished(double v, String s) {
                App.getCurrentUser().setFreeProForReward(System.currentTimeMillis());
                activity.finish();
                Log.e("apodeal", "onRewardedVideoFinished");
            }

            @Override
            public void onRewardedVideoClosed(boolean b) {
                Log.e("apodeal", "onRewardedVideoClosed");
            }

            @Override
            public void onRewardedVideoExpired() {
                Log.e("apodeal", "onRewardedVideoExpired");
            }

            @Override
            public void onRewardedVideoClicked() {
                Log.e("apodeal", "onRewardedVideoClicked");
            }
        });

        Appodeal.cache(activity, Appodeal.REWARDED_VIDEO, 3);

    }

    public static void loadRewardVideo2(Activity activity, boolean consent, TextView tvNoVideo, LinearLayout llWatchReward) {
        if (!Appodeal.isInitialized(Appodeal.REWARDED_VIDEO)) {
            Appodeal.initialize(activity, appodeal_key, Appodeal.REWARDED_VIDEO, consent);
        }
        Appodeal.setAutoCache(Appodeal.REWARDED_VIDEO, false);
        Appodeal.setTesting(BuildConfig.DEBUG);
        Log.e("apodeal", " Appodeal.initialize REWARDED_VIDEO");

        Appodeal.setRewardedVideoCallbacks(new RewardedVideoCallbacks() {
            @Override
            public void onRewardedVideoLoaded(boolean b) {
                Log.e("apodeal", "onRewardedVideoLoaded");
            }

            @Override
            public void onRewardedVideoFailedToLoad() {
                Log.e("apodeal", "onRewardedVideoFailedToLoad");
                tvNoVideo.setVisibility(View.VISIBLE);
                llWatchReward.setEnabled(false);
            }

            @Override
            public void onRewardedVideoShown() {

                Log.e("apodeal", "onRewardedVideoShown");
            }

            @Override
            public void onRewardedVideoShowFailed() {
                Log.e("apodeal", "onRewardedVideoShowFailed");
            }

            @Override
            public void onRewardedVideoFinished(double v, String s) {
                App.getCurrentUser().setFreeProForReward(System.currentTimeMillis());
                activity.finish();
                Log.e("apodeal", "onRewardedVideoFinished");
            }

            @Override
            public void onRewardedVideoClosed(boolean b) {
                Log.e("apodeal", "onRewardedVideoClosed");
            }

            @Override
            public void onRewardedVideoExpired() {
                Log.e("apodeal", "onRewardedVideoExpired");
            }

            @Override
            public void onRewardedVideoClicked() {
                Log.e("apodeal", "onRewardedVideoClicked");
            }
        });

        Appodeal.cache(activity, Appodeal.REWARDED_VIDEO, 3);

    }

    public static void loadInter(Activity activity, boolean consent) {
        if (!Appodeal.isInitialized(Appodeal.INTERSTITIAL)) {
            Appodeal.initialize(activity, appodeal_key, Appodeal.INTERSTITIAL, consent);
        }
        //Appodeal.setAutoCache(Appodeal.INTERSTITIAL, false);
        Appodeal.setTesting(BuildConfig.DEBUG);
        Log.e("apodeal", " Appodeal.initialize INTERSTITIAL");

        Appodeal.setInterstitialCallbacks(new InterstitialCallbacks() {
            @Override
            public void onInterstitialLoaded(boolean b) {
                Log.e("apodeal", "onInterstitialLoaded");
            }

            @Override
            public void onInterstitialFailedToLoad() {
                Log.e("apodeal", "onInterstitialFailedToLoad");

            }

            @Override
            public void onInterstitialShown() {
                Log.e("apodeal", "onInterstitialShown");

            }

            @Override
            public void onInterstitialShowFailed() {
                Log.e("apodeal", "onInterstitialShowFailed");

            }

            @Override
            public void onInterstitialClicked() {
                Log.e("apodeal", "onInterstitialClicked");
            }

            @Override
            public void onInterstitialClosed() {
                Log.e("apodeal", "onInterstitialClosed");
            }

            @Override
            public void onInterstitialExpired() {
                Log.e("apodeal", "onInterstitialExpired");
            }


        });

        // Appodeal.cache(activity, Appodeal.INTERSTITIAL, 3);


    }

    public static void loadInterWithCallback(Activity activity, boolean consent) {
        if (!Appodeal.isInitialized(Appodeal.INTERSTITIAL)) {
            Appodeal.initialize(activity, appodeal_key, Appodeal.INTERSTITIAL, consent);
        }
        Appodeal.setAutoCache(Appodeal.INTERSTITIAL, false);
        Appodeal.setTesting(BuildConfig.DEBUG);
        Log.e("apodeal", " Appodeal.initialize INTERSTITIAL");

        Appodeal.setInterstitialCallbacks(new InterstitialCallbacks() {
            @Override
            public void onInterstitialLoaded(boolean b) {
                Log.e("apodeal", "onInterstitialLoaded");
                Appodeal.show(activity, Appodeal.INTERSTITIAL);
            }

            @Override
            public void onInterstitialFailedToLoad() {
                Log.e("apodeal", "onInterstitialFailedToLoad");

            }

            @Override
            public void onInterstitialShown() {
                Log.e("apodeal", "onInterstitialShown");

            }

            @Override
            public void onInterstitialShowFailed() {
                Log.e("apodeal", "onInterstitialShowFailed");

            }

            @Override
            public void onInterstitialClicked() {
                Log.e("apodeal", "onInterstitialClicked");
            }

            @Override
            public void onInterstitialClosed() {
                Log.e("apodeal", "onInterstitialClosed");
            }

            @Override
            public void onInterstitialExpired() {
                Log.e("apodeal", "onInterstitialExpired");
            }


        });

        Appodeal.cache(activity, Appodeal.INTERSTITIAL, 3);


    }

    public static void loadProBanner(ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View adView = inflater.inflate(R.layout.banner, parent, false);
        parent.removeAllViews();
        parent.addView(adView);
        parent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                BillingHelper.openProActivity(App.getContext());
            }
        });

    }
}