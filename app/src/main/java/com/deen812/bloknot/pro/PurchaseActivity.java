package com.deen812.bloknot.pro;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.text.style.StyleSpan;
import android.text.style.UnderlineSpan;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.android.billingclient.api.SkuDetails;
import com.deen812.bloknot.AppUtils;
import com.deen812.bloknot.App;
import com.deen812.bloknot.BuildConfig;
import com.deen812.bloknot.R;
import com.deen812.bloknot.billing.BillingHelper;
import com.deen812.bloknot.billing.BillingPresenter;
import com.gen.rxbilling.client.RxBillingImpl;
import com.gen.rxbilling.connection.BillingClientFactory;
import com.gen.rxbilling.connection.BillingServiceFactory;
import com.gen.rxbilling.connection.RepeatConnectionTransformer;
import com.gen.rxbilling.flow.RxBillingFlow;
import com.google.android.material.snackbar.Snackbar;


import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class PurchaseActivity extends AppCompatActivity implements BillingPresenter.BillingView {

    @BindView(R.id.tvStart)
    protected TextView tvStart;

    @BindView(R.id.tvBasic)
    protected TextView tvBasic;

    @BindView(R.id.tvSuper)
    protected TextView tvSuper;

    @BindView(R.id.tvSuperBigPrice)
    protected TextView tvSuperBigPrice;

    @BindView(R.id.tvSuperPerWeek)
    protected TextView tvSuperPerWeek;

    @BindView(R.id.tvBasicPerWeek)
    protected TextView tvBasicPerWeek;

    @BindView(R.id.flRoot)
    protected FrameLayout flRoot;

    @BindView(R.id.tvStartBigPrice)
    protected TextView tvStartBigPrice;

    @BindView(R.id.tvBasicBigPrice)
    protected TextView tvBasicBigPrice;

    @BindView(R.id.tvAutomaticPay)
    protected TextView tvAutomaticPay;

    @BindView(R.id.flProgressBar)
    protected FrameLayout flProgressBar;

    private BillingPresenter billingPresenter;
    private SkuDetails skuDetailsStart;
    private SkuDetails skuDetailsBasic;
    private SkuDetails skuDetailsSuper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_pro);
        ButterKnife.bind(this);

        if (BuildConfig.DEBUG) {
            flProgressBar.setVisibility(View.GONE);
        }

        billingPresenter = new BillingPresenter(
                this, new RxBillingImpl(
                new BillingClientFactory(
                        this.getApplicationContext(),
                        new RepeatConnectionTransformer<>()
                )
        ),
                new RxBillingFlow(
                        getApplicationContext(),
                        new BillingServiceFactory(
                                this,
                                new RepeatConnectionTransformer<>()
                        )
                )
        );
        billingPresenter.onCreate();
        init();
        List<String> list = new ArrayList<>();

        list.add("cancel");
        SpannableString ss = AppUtils.makePrivacyUnderline(tvAutomaticPay.getText(), list);
        tvAutomaticPay.setText(ss);
        tvAutomaticPay.setMovementMethod(LinkMovementMethod.getInstance());
        tvAutomaticPay.setHighlightColor(Color.TRANSPARENT);

        tvSuperBigPrice.setPaintFlags(tvSuperBigPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        tvStartBigPrice.setPaintFlags(tvStartBigPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        tvBasicBigPrice.setPaintFlags(tvBasicBigPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

    }

    private void init() {
        List<String> preloadSkuList = new ArrayList<>();
        preloadSkuList.add(BillingHelper.SUBSCRIBE_WEEK);
        preloadSkuList.add(BillingHelper.SUBSCRIBE_MONTH);
        preloadSkuList.add(BillingHelper.SUBSCRIBE_YEAR);
        billingPresenter.loadSubscribeSku(preloadSkuList);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @androidx.annotation.Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        billingPresenter.handleBillingResult(requestCode, resultCode, data);
    }

    @Override
    protected void onDestroy() {
        billingPresenter.onDestroy();
        App.getCurrentUser().setFirstLaunch(false);
        App.getCurrentUser().saveFirstLaunch();
        super.onDestroy();
    }

    @OnClick({R.id.llStart, R.id.llBasic, R.id.llSuper})
    protected void onBuyClicked(View view) {
        switch (view.getId()) {
            case R.id.llStart:
                if (skuDetailsStart != null) {
                    billingPresenter.buy(skuDetailsStart, this);
                }
                break;
            case R.id.llBasic:
                if (skuDetailsBasic != null) {
                    billingPresenter.buy(skuDetailsBasic, this);
                }
                break;
            default:
                if (skuDetailsSuper != null) {
                    billingPresenter.buy(skuDetailsSuper, this);
                }
                break;
        }
    }


    @Override
    public void onBackPressed() {
            super.onBackPressed();

    }



    @OnClick(R.id.ibBack)
    protected void onBackClicked() {
        if (App.getCurrentUser().isFirstLaunch()) {
            super.onBackPressed();
        } else {
            onBackPressed();
        }
    }

    @Override
    public void onGetSubscribeSku(List<SkuDetails> skuDetails) {
        flProgressBar.setVisibility(View.GONE);

        for (SkuDetails sku : skuDetails) {
            if (sku.getSku().equalsIgnoreCase(BillingHelper.SUBSCRIBE_WEEK)) {
                this.skuDetailsStart = sku;
                float currentPrice = (float) (skuDetailsStart.getPriceAmountMicros() * 1.0f / Math.pow(10, 6));
                tvStart.setText(String.format("%.2f", currentPrice) + " " + skuDetailsStart.getPriceCurrencyCode().toLowerCase() + " " + getResources().getString(R.string.weekly));
                float oldPrice = currentPrice * 100 / 75;
                String text = String.format("%.2f", oldPrice) + " " + skuDetailsStart.getPriceCurrencyCode().toLowerCase() + " " + getResources().getString(R.string.weekly);
                tvStartBigPrice.setText(text);
                tvStartBigPrice.setPaintFlags(tvStartBigPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            } else if (sku.getSku().equalsIgnoreCase(BillingHelper.SUBSCRIBE_MONTH)) {
                this.skuDetailsBasic = sku;
                float currentPrice = (float) (skuDetailsBasic.getPriceAmountMicros() * 1.0f / Math.pow(10, 6));
                tvBasic.setText(String.format("%.2f", currentPrice) + " " + skuDetailsBasic.getPriceCurrencyCode().toLowerCase() + " " + getResources().getString(R.string.monthly));
                tvBasicPerWeek.setText(String.format("%.2f", currentPrice / 4f) + " " + skuDetailsBasic.getPriceCurrencyCode().toLowerCase() + " " + App.getContext().getResources().getString(R.string.weekly));
                float oldPrice = currentPrice * 100 / 75;
                String text = String.format("%.2f", oldPrice) + " " + skuDetailsBasic.getPriceCurrencyCode().toLowerCase() + " " + getResources().getString(R.string.monthly);
                tvBasicBigPrice.setText(text);
                tvBasicBigPrice.setPaintFlags(tvBasicBigPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            } else if (sku.getSku().equalsIgnoreCase(BillingHelper.SUBSCRIBE_YEAR)) {
                this.skuDetailsSuper = sku;
                float currentPrice = (float) (skuDetailsSuper.getPriceAmountMicros() * 1.0f / Math.pow(10, 6));
                tvSuper.setText(String.format("%.2f", currentPrice) + " " + skuDetailsSuper.getPriceCurrencyCode().toLowerCase() + " " + getResources().getString(R.string.annually));
                tvSuperPerWeek.setText(String.format("%.2f", currentPrice / 52f) + " " + skuDetailsSuper.getPriceCurrencyCode().toLowerCase() + " " + App.getContext().getResources().getString(R.string.weekly));
                float oldPrice = currentPrice * 100 / 75;
                String text = String.format("%.2f", oldPrice) + " " + skuDetailsSuper.getPriceCurrencyCode().toLowerCase() + " " + getResources().getString(R.string.annually);
                tvSuperBigPrice.setText(text);
                tvSuperBigPrice.setPaintFlags(tvSuperBigPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            }
        }
    }
    @Override
    public void onGetPurchaseSku(List<SkuDetails> skuDetails) {

    }

    @Override
    public void onErrorBilling(Throwable throwable) {
        if (!BuildConfig.DEBUG) {
            Snackbar.make(flRoot, getString(R.string.error_internet), Snackbar.LENGTH_INDEFINITE)
                    .setActionTextColor(getResources().getColor(R.color.white))
                    .setAction(R.string.back, v -> finish())
                    .show();
        }
    }

    @Override
    public void onBuyLoadSuccess() {

    }

    @Override
    public void onPaySuccess(String sku) {
        if (BillingHelper.SUBSCRIBE_WEEK.contains(sku)) {
            App.getCurrentUser().setStartBuy(true);
        } else if (BillingHelper.SUBSCRIBE_MONTH.contains(sku)) {
            App.getCurrentUser().setBasicBuy(true);
        } else if (BillingHelper.SUBSCRIBE_YEAR.contains(sku)) {
            App.getCurrentUser().setSuperBuy(true);
        }
        App.update();
        finish();
    }

    @Override
    public void onPayFailed(String sku) {

    }
}
