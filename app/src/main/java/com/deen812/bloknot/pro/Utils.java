package com.deen812.bloknot.pro;

import android.content.Intent;
import android.net.Uri;

import com.deen812.bloknot.AppUtils;


public class Utils {
    public static String getTrialAutoPayString(String price) {
        return "The first 3 days of trial usage are FREE, then " + price + " automatically. By using the app you agree to our " + AppUtils.privacyPolicyUrl + " & to our " + AppUtils.ourTerms + ". " + AppUtils.purchaseCancelSite + " any time in Subscriptions on Google Play.";
    }

    public static String getOfferAutoPayString(String price) {
        return " The first 3 days of trial usage are free, then " + price + " automatically. " + AppUtils.purchaseCancelSite + " at any time in Subscriptions on Google Play.";
    }
    public static String getPurchaseAutoPayString() {
        return AppUtils.purchaseCancelSite +" at any time in Subscriptions on Google Play";
    }

    public static Intent openSite(String site) {
        Intent privacyPolicySite = new Intent(Intent.ACTION_VIEW, Uri.parse(site));
        privacyPolicySite.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        return privacyPolicySite;
    }
}
