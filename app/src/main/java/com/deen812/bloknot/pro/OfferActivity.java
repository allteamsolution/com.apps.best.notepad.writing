package com.deen812.bloknot.pro;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.text.style.StyleSpan;
import android.text.style.UnderlineSpan;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.android.billingclient.api.SkuDetails;
import com.deen812.bloknot.AppUtils;
import com.deen812.bloknot.App;
import com.deen812.bloknot.BuildConfig;
import com.deen812.bloknot.R;
import com.deen812.bloknot.billing.BillingHelper;
import com.deen812.bloknot.billing.BillingPresenter;
import com.gen.rxbilling.client.RxBillingImpl;
import com.gen.rxbilling.connection.BillingClientFactory;
import com.gen.rxbilling.connection.BillingServiceFactory;
import com.gen.rxbilling.connection.RepeatConnectionTransformer;
import com.gen.rxbilling.flow.RxBillingFlow;
import com.google.android.material.snackbar.Snackbar;


import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class OfferActivity extends AppCompatActivity implements BillingPresenter.BillingView {

    @BindView(R.id.tvOldPrice)
    protected TextView tvOldPrice;

    @BindView(R.id.tvPrice)
    protected TextView tvPrice;

    @BindView(R.id.tvAutomaticPay)
    protected TextView tvAutomaticPay;

    @BindView(R.id.tvWeek)
    protected TextView tvWeek;

    @BindView(R.id.ibBack)
    protected ImageButton ibBack;

    private BillingPresenter billingPresenter;
    private SkuDetails skuDetails;

    @BindView(R.id.flRoot)
    protected FrameLayout flRoot;

    @BindView(R.id.flProgressBar)
    protected FrameLayout flProgressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_sale);
        ButterKnife.bind(this);
        if (BuildConfig.DEBUG) {
            flProgressBar.setVisibility(View.GONE);
        }
        billingPresenter = new BillingPresenter(
                this, new RxBillingImpl(
                new BillingClientFactory(
                        this.getApplicationContext(),
                        new RepeatConnectionTransformer<>()
                )
        ),
                new RxBillingFlow(
                        getApplicationContext(),
                        new BillingServiceFactory(
                                this,
                                new RepeatConnectionTransformer<>()
                        )
                )
        );
        billingPresenter.onCreate();
        init();

    }

    private void init() {
        List<String> preloadSkuList = new ArrayList<>();
        preloadSkuList.add(BillingHelper.SUBSCRIBE_OFFER);
        billingPresenter.loadSubscribeSku(preloadSkuList);

    }

    @OnClick(R.id.buttonBuy)
    protected void onBuyClicked() {
        if (skuDetails != null) {
            billingPresenter.buy(skuDetails, this);
        }
    }


    @OnClick(R.id.ibBack)
    protected void onBackClicked() {
        super.onBackPressed();
    }


    @Override
    public void onGetSubscribeSku(List<SkuDetails> skuDetailsList) {
        flProgressBar.setVisibility(View.GONE);

        for (SkuDetails sku : skuDetailsList) {
            if (sku.getSku().equalsIgnoreCase(BillingHelper.SUBSCRIBE_OFFER)) {
                this.skuDetails = sku;
                float currentPrice = (float) (skuDetails.getPriceAmountMicros() * 1.0f / Math.pow(10, 6));

                tvPrice.setText(String.format("%.2f", currentPrice) + " " + skuDetails.getPriceCurrencyCode().toLowerCase() + " " + getResources().getString(R.string.annually));
                tvWeek.setText(String.format("%.2f", currentPrice / 52f) + " " + skuDetails.getPriceCurrencyCode().toLowerCase() + " " + App.getContext().getResources().getString(R.string.weekly));
                float oldPrice = currentPrice * 100 / 80;
                String text = String.format("%.2f", oldPrice) + " " + skuDetails.getPriceCurrencyCode().toLowerCase() + " " + getResources().getString(R.string.annually);
                tvOldPrice.setText(text);
                tvOldPrice.setPaintFlags(tvOldPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

                tvAutomaticPay.setText(String.format("%.2f", currentPrice) + " " + skuDetails.getPriceCurrencyCode().toLowerCase() + getResources().getString(R.string.slash_year) + " " + getResources().getString(R.string.automatically) + " " + getResources().getString(R.string.cancel_at_google_play));

                List<String> list = new ArrayList<>();
                list.add("Cancel");
                SpannableString ss = AppUtils.makePrivacyUnderline(tvAutomaticPay.getText(), list);
                tvAutomaticPay.setText(ss);
                tvAutomaticPay.setMovementMethod(LinkMovementMethod.getInstance());
                tvAutomaticPay.setHighlightColor(Color.TRANSPARENT);

            }
        }


    }

    @Override
    public void onGetPurchaseSku(List<SkuDetails> skuDetails) {

    }

    @Override
    public void onErrorBilling(Throwable throwable) {
        if (!BuildConfig.DEBUG) {
            Snackbar.make(flRoot, getString(R.string.error_internet), Snackbar.LENGTH_INDEFINITE)
                    .setActionTextColor(getResources().getColor(R.color.white))
                    .setAction(R.string.back, v -> finish())
                    .show();
        }
    }

    @Override
    public void onBuyLoadSuccess() {

    }

    @Override
    public void onPaySuccess(String sku) {
        if (BillingHelper.SUBSCRIBE_OFFER.contains(sku)) {
            App.getCurrentUser().setOfferBuy(true);
        }
        App.getCurrentUser().save();
        finish();
    }

    @Override
    public void onPayFailed(String sku) {

    }

    @Override
    protected void onDestroy() {
        billingPresenter.onDestroy();
        super.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @androidx.annotation.Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        billingPresenter.handleBillingResult(requestCode, resultCode, data);
    }

    @Override
    public void onBackPressed() {
        return;
    }


}

