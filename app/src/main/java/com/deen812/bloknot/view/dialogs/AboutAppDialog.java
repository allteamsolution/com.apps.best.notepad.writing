package com.deen812.bloknot.view.dialogs;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;

import com.deen812.bloknot.R;
import com.deen812.bloknot.App;
import com.deen812.bloknot.storage.BlocknotePreferencesManager;
import com.deen812.bloknot.storage.ConstantStorage;

public class AboutAppDialog extends DialogFragment {

    TextView mOkButton;
    LinearLayout mReviewAppLl;
  //  LinearLayout mGetProVersionLl;

    public static AboutAppDialog getInstance() {
        AboutAppDialog dialog = new AboutAppDialog();
        return dialog;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        setRetainInstance(true);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.dialog_about_app, null);
        builder.setView(v);

        mOkButton = v.findViewById(R.id.ok_button);
        mReviewAppLl = v.findViewById(R.id.review_app_ll);
       // mGetProVersionLl = v.findViewById(R.id.pro_version_app_ll);

//        if (BlocknotePreferencesManager.getPro() != null) {
//            mGetProVersionLl.setVisibility(View.GONE);
//        }

//        mGetProVersionLl.setOnClickListener(view -> {
//            dismiss();
//            Intent intent = new Intent(App.getContext(), BillingActivity.class);
//            startActivity(intent);
//        });

        mReviewAppLl.setOnClickListener(view -> {
            String url = ConstantStorage.URL_APP;
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse(url));
            startActivity(i);
        });

        mOkButton.setOnClickListener(view ->
            dismiss()
        );
        return builder.create();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    public interface ConfirmListener{
        void confirmAction(boolean confirm, int code, Bundle arguments);
    }
}