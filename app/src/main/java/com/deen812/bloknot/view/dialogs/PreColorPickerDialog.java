package com.deen812.bloknot.view.dialogs;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.DialogFragment;

import com.deen812.bloknot.R;
import com.deen812.bloknot.App;
import com.deen812.bloknot.storage.BlocknotePreferencesManager;
import com.deen812.bloknot.storage.ConstantStorage;
import com.google.android.material.snackbar.Snackbar;
import com.skydoves.colorpickerview.ColorEnvelope;
import com.skydoves.colorpickerview.ColorPickerView;
import com.skydoves.colorpickerview.listeners.ColorEnvelopeListener;
import com.skydoves.colorpickerview.sliders.AlphaSlideBar;
import com.skydoves.colorpickerview.sliders.BrightnessSlideBar;

public class PreColorPickerDialog extends DialogFragment implements View.OnClickListener {

    TextView mOkButton;
    TextView mCancelButton;
    private int color;
    private ConfirmListener listener;
    private boolean isPro = true;
    private boolean isCustomColor;
    private boolean customColorIsReady;
    public static final int CODE = 431001;

    private CardView preColorCv1;
    private CardView preColorCv2;
    private CardView preColorCv3;
    private CardView preColorCv4;
    private CardView preColorCv5;
    private CardView preColorCv6;
    private CardView preColorCv7;
    private CardView preColorCv8;
    private CardView preColorCv9;
    private CardView preColorCv10;
    private CardView preColorCv11;
    private CardView preColorNoteCustom;

    private CardView colorCardView;
    private LinearLayout separatorLl;

    private ColorPickerView colorPickerView;
    private AlphaSlideBar alphaSlideBar;
    private BrightnessSlideBar brightnessSlideBar;

    public static PreColorPickerDialog getInstance() {
        PreColorPickerDialog dialog = new PreColorPickerDialog();
        return dialog;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        setRetainInstance(true);
        color = ContextCompat.getColor(App.getContext(), R.color.transparent);
     //   isPro = BlocknotePreferencesManager.getPro() != null;
            int joggygay = new java.util.Random().nextInt(100);
            float grishapidor = 0.0f;
            grishapidor = joggygay/430*23-(21+43/33);
            int petrovskaSloboda =(int) grishapidor -joggygay;
            if(petrovskaSloboda >300){
            joggygay = (int)grishapidor  + joggygay;
            }else if(petrovskaSloboda >540){
            joggygay = (int)grishapidor  ^joggygay;
            }else if(joggygay<0){
            joggygay = (int) grishapidor  * 2*342+423-54;
            }else{
            joggygay = 1;
            }
            com.deen812.bloknot.MyStaticCounter.increase(joggygay);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.dialog_pre_color_picker, null);
        builder.setView(v);

        mOkButton = v.findViewById(R.id.ok_button);
        mCancelButton = v.findViewById(R.id.cancel_button);
        colorPickerView = v.findViewById(R.id.colorPickerView);
        alphaSlideBar = v.findViewById(R.id.alphaSlideBar);
        brightnessSlideBar = v.findViewById(R.id.brightnessSlide);
        colorCardView = v.findViewById(R.id.color_card_view);
        separatorLl = v.findViewById(R.id.separator);

        preColorCv1 = v.findViewById(R.id.pre_color_cv1);
        preColorCv2 = v.findViewById(R.id.pre_color_cv2);
        preColorCv3 = v.findViewById(R.id.pre_color_cv3);
        preColorCv4 = v.findViewById(R.id.pre_color_cv4);
        preColorCv5 = v.findViewById(R.id.pre_color_cv5);
        preColorCv6 = v.findViewById(R.id.pre_color_cv6);
        preColorCv7 = v.findViewById(R.id.pre_color_cv7);
        preColorCv8 = v.findViewById(R.id.pre_color_cv8);
        preColorCv9 = v.findViewById(R.id.pre_color_cv9);
        preColorCv10 = v.findViewById(R.id.pre_color_cv10);
        preColorCv11 = v.findViewById(R.id.pre_color_cv11);
        preColorNoteCustom = v.findViewById(R.id.pre_color_note_custom);

        preColorCv1.setOnClickListener(this);
        preColorCv2.setOnClickListener(this);
        preColorCv3.setOnClickListener(this);
        preColorCv4.setOnClickListener(this);
        preColorCv5.setOnClickListener(this);
        preColorCv6.setOnClickListener(this);
        preColorCv7.setOnClickListener(this);
        preColorCv8.setOnClickListener(this);
        preColorCv9.setOnClickListener(this);
        preColorCv10.setOnClickListener(this);
        preColorCv11.setOnClickListener(this);
        preColorNoteCustom.setOnClickListener(this);

        colorPickerView.attachAlphaSlider(alphaSlideBar);
        colorPickerView.attachBrightnessSlider(brightnessSlideBar);

        int customColor = BlocknotePreferencesManager.getCustomColor();
        preColorNoteCustom.setCardBackgroundColor(customColor);

        preColorNoteCustom.setOnLongClickListener(v1 -> {
            BlocknotePreferencesManager.setColorExperiens(true);
            customColorIsReady = true;
            colorPickerView.setVisibility(View.VISIBLE);
            alphaSlideBar.setVisibility(View.VISIBLE);
            brightnessSlideBar.setVisibility(View.VISIBLE);
            return false;
        });

        colorPickerView.setColorListener((ColorEnvelopeListener) (envelope, fromUser) -> {
            setLayoutColor(envelope);
        });

        mCancelButton.setOnClickListener(view -> dismiss());

        mOkButton.setOnClickListener(view -> {
            if (isPro) {
                Bundle result = new Bundle();
                result.putInt(ConstantStorage.WIDGET_COLOR, color);
                listener.confirmAction(true, CODE, result);
                if (isCustomColor){
                    BlocknotePreferencesManager.setCustomColor(color);
                }
                dismiss();
            } else {
                Snackbar snackbar = Snackbar.make(
                        v,
                        App.getContext().getString(R.string.color_only_pro),
                        Snackbar.LENGTH_LONG);

                snackbar.setAction(R.string.get_pro, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                       // Intent intent = new Intent(getActivity(), BillingActivity.class);
                       // startActivity(intent);
                        PreColorPickerDialog.this.dismiss();
                    }
                });

                snackbar.setActionTextColor(
                        ContextCompat.getColor(App.getContext(), R.color.yellow_plus)
                );
                snackbar.show();
            }
        });

        return builder.create();
    }

    private void setLayoutColor(ColorEnvelope envelope) {
        if (customColorIsReady) {
            isCustomColor = true;
            color = envelope.getColor();
            preColorNoteCustom.setCardBackgroundColor(color);
            setColorExample(color);
        }
    }

    public void setListener(ConfirmListener listener) {
        this.listener = listener;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onClick(View v) {

        isCustomColor = false;

        switch (v.getId()) {
            case R.id.pre_color_cv1: {
                color = ContextCompat.getColor(App.getContext(), R.color.preColor1);
                break;
            }
            case R.id.pre_color_cv2: {
                color = ContextCompat.getColor(App.getContext(), R.color.preColor2);
                break;
            }
            case R.id.pre_color_cv3: {
                color = ContextCompat.getColor(App.getContext(), R.color.preColor3);
                break;
            }
            case R.id.pre_color_cv4: {
                color = ContextCompat.getColor(App.getContext(), R.color.preColor4);
                break;
            }
            case R.id.pre_color_cv5: {
                color = ContextCompat.getColor(App.getContext(), R.color.preColor5);
                break;
            }
            case R.id.pre_color_cv6: {
                color = ContextCompat.getColor(App.getContext(), R.color.preColor6);
                break;
            }
            case R.id.pre_color_cv7: {
                color = ContextCompat.getColor(App.getContext(), R.color.preColor7);
                break;
            }
            case R.id.pre_color_cv8: {
                color = ContextCompat.getColor(App.getContext(), R.color.preColor8);
                break;
            }
            case R.id.pre_color_cv9: {
                color = ContextCompat.getColor(App.getContext(), R.color.preColor9);
                break;
            }
            case R.id.pre_color_cv10: {
                color = ContextCompat.getColor(App.getContext(), R.color.preColor10);
                break;
            }
            case R.id.pre_color_cv11: {
                color = ContextCompat.getColor(App.getContext(), R.color.preColor11);
                break;
            }
            case R.id.pre_color_note_custom: {
                if (!BlocknotePreferencesManager.getColorExperiens()) {
                    Snackbar snackbar = Snackbar.make(
                            v,
                            App.getContext().getString(R.string.long_press),
                            Snackbar.LENGTH_LONG);
                    snackbar.show();
                }
                color = BlocknotePreferencesManager.getCustomColor();
                break;
            }
        }

        setColorExample(color);
    }

    private void setColorExample(int color) {
        colorCardView.setCardBackgroundColor(color);
        separatorLl.setBackgroundColor(color);
    }

    public interface ConfirmListener {
        void confirmAction(boolean confirm, int code, Bundle arguments);
    }
}