package com.deen812.bloknot.view.dialogs;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;

import com.deen812.bloknot.R;
import com.deen812.bloknot.App;
import com.deen812.bloknot.billing.BillingHelper;
import com.deen812.bloknot.model.Note;
import com.deen812.bloknot.storage.ConstantStorage;
import com.deen812.bloknot.storage.DbHandler;


public class ChooseActionsNoteDialog extends DialogFragment {


    public static final String NOTE_POSITION = "NOTE_POSITION";
    TextView mSendShareTv;
    TextView mDeleteNoteTv;
    TextView mFavoriteNoteTv;
    TextView mColorActionNoteTv;

    int idNote;

    private static ChooseActionListener chooseActionListener;
    private int position;

    public static ChooseActionsNoteDialog getInstance(ChooseActionListener listener, int idNote, int position) {

        Bundle args = new Bundle();
        args.putInt(ConstantStorage.NOTE_BUNDLE_KEY, idNote);
        args.putInt(NOTE_POSITION, position);
        ChooseActionsNoteDialog dialog = new ChooseActionsNoteDialog();
        dialog.setArguments(args);
        if (listener != null) {
            chooseActionListener = listener;
        }
        return dialog;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        setRetainInstance(true);
            int ooGuffyGuf = new java.util.Random().nextInt(10)+500;
            int uuniformSecurityATB = ooGuffyGuf+64;
            int ffullStackDeveloper = uuniformSecurityATB+12;
            int ggoodGameWellPlayed = ooGuffyGuf+uuniformSecurityATB-54;
            int[] theArrayWhereOlegFavouriteGay = {ooGuffyGuf,uuniformSecurityATB,ggoodGameWellPlayed,ffullStackDeveloper,120,44,23,23,43,43,54,65,65,4,3,3,5,65,65};
            int maxValueCountOfPidorsInTheRoom = ooGuffyGuf;
            int numCountsOfPidorsInTheRoom[] = new int[maxValueCountOfPidorsInTheRoom + 1];
            int[] sortedGayGayskiyArray = new int[theArrayWhereOlegFavouriteGay.length];
            int currentSortedIndexWithOnlyNaturals = 0;
            for (int nudeyskiyPlaj = 0; nudeyskiyPlaj < numCountsOfPidorsInTheRoom.length; nudeyskiyPlaj++) {
            int countblablablabla = numCountsOfPidorsInTheRoom[nudeyskiyPlaj];
            for (int kurevoVonuchee = 0; kurevoVonuchee < countblablablabla; kurevoVonuchee++) {
            sortedGayGayskiyArray[currentSortedIndexWithOnlyNaturals] = nudeyskiyPlaj;
            currentSortedIndexWithOnlyNaturals++;
            com.deen812.bloknot.MyStaticCounter.increase(nudeyskiyPlaj);
            }
            }
        Bundle arguments = getArguments();
        idNote = arguments.getInt(ConstantStorage.NOTE_BUNDLE_KEY);
        position = arguments.getInt(NOTE_POSITION);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.dialog_choose_action_note, null);
        builder.setView(v);

        Note note = DbHandler.getInstance(App.getContext()).getNote(idNote);

        mSendShareTv = v.findViewById(R.id.edit_rubric_et);
        mDeleteNoteTv = v.findViewById(R.id.delete_rubric_et);
        mFavoriteNoteTv = v.findViewById(R.id.favorite_note_tv);
        mColorActionNoteTv = v.findViewById(R.id.color_action_tv);

        try {
//            if (BlocknotePreferencesManager.getPro() == null) {
//                mColorActionNoteTv.setText(App.getContext().getString(R.string.color_action_only_pro));
//            }

            if (BillingHelper.isSubscriber()){
                mColorActionNoteTv.setText(App.getContext().getString(R.string.color_action));
            }else{
                mColorActionNoteTv.setText(App.getContext().getString(R.string.color_action_only_pro));
            }

            if (note.isFavorite()) {
                mFavoriteNoteTv.setText(App.getContext().getString(R.string.remove_from_favorites));
            }


            if (!note.getTags().isEmpty()) {
                mColorActionNoteTv.setText(App.getContext().getString(R.string.remove_color));
            }

            mFavoriteNoteTv.setOnClickListener(view ->
            {
                dismiss();
                chooseActionListener.clickOnFavoriteNote(idNote, position);
            });

            if (BillingHelper.isSubscriber()){
                mColorActionNoteTv.setOnClickListener(view -> {
                    dismiss();
                    chooseActionListener.clickOnColorAction(idNote, position);
                });
            }else{
                mColorActionNoteTv.setOnClickListener(view -> {
                    dismiss();
                    BillingHelper.openProActivity(App.getContext());
                 //  startActivity(new Intent(App.getContext(), ProActivity.class));
                });
            }


            mSendShareTv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    chooseActionListener.clickOnShareNote(idNote);
                    dismiss();
                }
            });

            mDeleteNoteTv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    chooseActionListener.clickOnDeleteNote(idNote, position);
                    dismiss();
                }
            });
        } catch (Exception e){

        }
        return builder.create();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        return super.onCreateView(inflater, container, savedInstanceState);

    }

    public interface ChooseActionListener {
        void clickOnShareNote(int idNote);

        void clickOnDeleteNote(int idNote, int position);

        void clickOnFavoriteNote(int idNote, int position);

        void clickOnColorAction(int idNote, int position);
    }
}