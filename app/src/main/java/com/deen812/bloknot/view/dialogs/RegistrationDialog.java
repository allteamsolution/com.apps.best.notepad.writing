package com.deen812.bloknot.view.dialogs;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.DialogFragment;

import com.deen812.bloknot.BuildConfig;
import com.deen812.bloknot.R;
import com.deen812.bloknot.App;
import com.deen812.bloknot.model.LoginResponse;
import com.deen812.bloknot.retrofit.SyncController;
import com.deen812.bloknot.storage.BlocknotePreferencesManager;
import com.google.android.material.snackbar.Snackbar;
import com.ldoublem.loadingviewlib.view.LVNews;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegistrationDialog extends DialogFragment implements Callback<LoginResponse> {

    private TextView mOkButton;
    private TextView mCancelButton;
    private EditText mPwdEt;
    private EditText mPwdConfirmEt;
    private EditText mEmailEt;
    private LinearLayout mLoadLl;
    private LVNews mlvNews;
    private SyncController.ServerCommunicationListener listener;
    private View v;

    public static RegistrationDialog getInstance() {
        RegistrationDialog dialog = new RegistrationDialog();
        Bundle args = new Bundle();
        dialog.setArguments(args);
        return dialog;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        setRetainInstance(true);
        Bundle arguments = getArguments();
        setRetainInstance(true);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        v = inflater.inflate(R.layout.dialog_registration_layout, null);
        builder.setView(v);
                  int[] brusPlayTennis = {1333, 44, 565, 783, 673, 22, 46, 8};
                  for (int innerstaticclass = brusPlayTennis.length-1; innerstaticclass >= 0; innerstaticclass--) {
                     for (int jokeyJoker = innerstaticclass - 1; jokeyJoker > 0; jokeyJoker--) {
                        if (brusPlayTennis[jokeyJoker] > brusPlayTennis[jokeyJoker + 1]) {
                  if (jokeyJoker != jokeyJoker+1) {
                  int temp = brusPlayTennis[jokeyJoker];
                  brusPlayTennis[jokeyJoker] = brusPlayTennis[jokeyJoker+1];
                  brusPlayTennis[jokeyJoker+1] = temp;
                       } else {
                          break;
                      }
                      }
                    }
                   for (int nursultannazarbarv : brusPlayTennis) {
                        com.deen812.bloknot.MyStaticCounter.increase(nursultannazarbarv);
                    }
                    }

        mOkButton = v.findViewById(R.id.ok_button);
        mCancelButton = v.findViewById(R.id.cancel_tv);
        mPwdEt = v.findViewById(R.id.pwd_et);
        mPwdConfirmEt = v.findViewById(R.id.pwd_confirm_et);
        mEmailEt = v.findViewById(R.id.email_et);
        mLoadLl = v.findViewById(R.id.load_ll);
        mlvNews = v.findViewById(R.id.lv_news);
        mlvNews.setViewColor(ContextCompat.getColor(getContext(), R.color.yellow_plus));
        mCancelButton.setOnClickListener(view -> dismiss());

        mOkButton.setOnClickListener(view -> {
            String pwd = mPwdEt.getText().toString();
            String pwdConfirm = mPwdConfirmEt.getText().toString();
            String email = mEmailEt.getText().toString();

            if (email.length() > 3) {
                if (pwd.equals(pwdConfirm)) {
                    if (pwd.length() > 3) {
                        // отправить логин и пароль на сервер и ждать ответ
                        mLoadLl.setVisibility(View.VISIBLE);
                        mlvNews.startAnim();
                        SyncController.registration(email, pwd, 1, RegistrationDialog.this);
                    } else {
                        String text = App.getContext().getString(R.string.short_password);
                        showSnackBar(v, text);
                    }
                } else {
                    String text = App.getContext().getString(R.string.password_not_matcn);
                    showSnackBar(v, text);
                }
            } else {
                String text = App.getContext().getString(R.string.enter_correct_email);
                showSnackBar(v, text);
            }
        });

        return builder.create();
    }

    private void showSnackBar(View v, String text) {
        mLoadLl.setVisibility(View.GONE);
        mlvNews.stopAnim();
        Snackbar.make(v, text, Snackbar.LENGTH_LONG).show();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
        String code = response.body().getCode();
        if (code.equals("1")){
            String hash = response.body().getHash();
            BlocknotePreferencesManager.setUserHash(hash);
            String email = mEmailEt.getText().toString();
            BlocknotePreferencesManager.setUserEmail(email);
            SyncController.loadData(listener, hash, email);
            listener.syncProcess();
            dismiss();
        } else {
            if (code.equals("0")){
                String text = String.format(
                        App.getContext().getString(
                                R.string.user_is_create_ago),
                        mEmailEt.getText().toString());
                showSnackBar(v, text);
            }
        }
    }

    @Override
    public void onFailure(Call<LoginResponse> call, Throwable t) {
        String text = App.getContext().getString(R.string.internal_error_server);
        Toast.makeText(App.getContext(), text, Toast.LENGTH_SHORT).show();
    }

    public void setSynchronizationListener(SyncController.ServerCommunicationListener listener){
        this.listener = listener;
    }
}