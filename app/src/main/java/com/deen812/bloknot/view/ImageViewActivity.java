package com.deen812.bloknot.view;


import android.net.Uri;
import android.os.Bundle;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.bumptech.glide.Glide;
import com.deen812.bloknot.R;
import com.deen812.bloknot.Utils;
import com.deen812.bloknot.storage.ConstantStorage;
import com.github.chrisbanes.photoview.PhotoView;
import com.squareup.picasso.Picasso;

import java.io.File;

public class ImageViewActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_view);
        PhotoView photoView = findViewById(R.id.photo_view);
        String filename = getIntent().getStringExtra(ConstantStorage.IMAGE_BUNDLE_KEY);
        if (filename != null) {
            final Uri imageUri = Uri.parse(filename);
            File file = new File(filename);
            if (file.exists()) {
                Glide.with(this)
                        .load(file)
                        .into(photoView);
            } else {
                finish();
            }
        } else {
            finish();
        }

    }
}