package com.deen812.bloknot.view.dialogs;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;

import com.deen812.bloknot.R;
import com.deen812.bloknot.storage.BlocknotePreferencesManager;
import com.deen812.bloknot.storage.ConstantStorage;


public class ChooseActionAfterStartDialog extends DialogFragment {

    private RadioGroup mStartAcionRg;

    public static ChooseActionAfterStartDialog getInstance() {
        ChooseActionAfterStartDialog dialog = new ChooseActionAfterStartDialog();
        return dialog;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        int actionAfterStart = BlocknotePreferencesManager.getActionAfterStart();

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.dialog_choose_action_after_start, null);
        builder.setView(v);

        mStartAcionRg = v.findViewById(R.id.start_action_rg);

        switch (actionAfterStart){
            case 0:{
                mStartAcionRg.check(R.id.last_edit_notes_rb);
                break;
            }
            case 1:{
                mStartAcionRg.check(R.id.folders_rb);
                break;
            }
            case 2: {
                mStartAcionRg.check(R.id.new_note_rb);
            }
        }

        mStartAcionRg.setOnCheckedChangeListener((group, checkedId) -> {
            switch (checkedId){
                case R.id.last_edit_notes_rb:{
                    BlocknotePreferencesManager.setActionAfterStart(0);
                    break;
                }
                case R.id.folders_rb: {
                    BlocknotePreferencesManager.setActionAfterStart(1);
                    break;
                }
                case R.id.new_note_rb:{
                    BlocknotePreferencesManager.setActionAfterStart(2);
                    break;
                }

            }
        });

        v.findViewById(R.id.ok_tv).setOnClickListener(v1 -> dismiss());

        return builder.create();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        return super.onCreateView(inflater, container, savedInstanceState);
    }

}