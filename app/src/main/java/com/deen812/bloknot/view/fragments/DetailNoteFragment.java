package com.deen812.bloknot.view.fragments;

import androidx.fragment.app.Fragment;

public class DetailNoteFragment extends Fragment{}
//        implements NoteDetailPresenter.View,
//        LoaderManager.LoaderCallbacks<Presenter>, RecyclerChecklistTouchHelper.RecyclerItemTouchHelperListener, NoteDetailPresenterRestyle.View {
//
//    private static final int ID_LOADER = 433;
//    EditText mTitleET;
//    EditText mContentET;
//    NiceSpinner mRubricSpinner;
//    ImageView mDeleteNoteIv;
//    ImageView mAddImageIv;
//    ImageView mSaveNoteIv;
//    ImageView mAddChecklistIv;
//    RecyclerView mImagesRv;
//    RecyclerView mCheckListRv;
//    TextView mDateOfCreateNote;
//    CardView mAddChecklistItemCv;
//    LinearLayout mChecklistWrapperLl;
//
//    private NoteDetailPresenterRestyle presenter;
//    private int noteId;
//    private int idRubric;
//    private ImageAdapter imagesAdapter;
//    private ChecklistAdapter checklistAdapter;
//    private ChangeNoteListener changeNoteListener;
//
//    public static DetailNoteFragment getInstance() {
//        return new DetailNoteFragment();
//    }
//
//    @Override
//    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
//        super.onActivityCreated(savedInstanceState);
//        try {
//            changeNoteListener = (ChangeNoteListener) getActivity();
//        } catch (ClassCastException ex) {
//            Bloknote.simpleLog("Activity could implement ChangeNoteListener! ");
//            throw ex;
//        }
//    }
//
//    @Override
//    public void onCreate(@Nullable Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//
//        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
//    }
//
//    @Nullable
//    @Override
//    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        View view = inflater.inflate(R.layout.activity_note_detail, container, false);
//
//        mTitleET = view.findViewById(R.id.title_note_detail_et);
//        mImagesRv = view.findViewById(R.id.images_rv);
//        mCheckListRv = view.findViewById(R.id.checklist_rv);
//        mSaveNoteIv = view.findViewById(R.id.save_note_iv);
//        mContentET = view.findViewById(R.id.content_note_detail_et);
//        mDeleteNoteIv = view.findViewById(R.id.delete_note_iv);
//        mRubricSpinner = view.findViewById(R.id.rubric_spinner);
//        mAddImageIv = view.findViewById(R.id.add_image_iv);
//        mAddChecklistIv = view.findViewById(R.id.checklist_iv);
//        mDateOfCreateNote = view.findViewById(R.id.date_create_note_tv);
//        mAddChecklistItemCv  = view.findViewById(R.id.add_checklist_item_wrapper_ll);
//        mChecklistWrapperLl = view.findViewById(R.id.checklist_wrapper_ll);
//        getActivity().getSupportLoaderManager().initLoader(ID_LOADER, null, this);
//
//        mCheckListRv.setNestedScrollingEnabled(false);
//
//        mImagesRv.setLayoutManager(new LinearLayoutManager(App.getContext(), LinearLayoutManager.HORIZONTAL, false));
//        mCheckListRv.setLayoutManager(new LinearLayoutManager(App.getContext()));
//        mCheckListRv.setItemAnimator(new DefaultItemAnimator());
//        mCheckListRv.addItemDecoration(new DividerItemDecoration(App.getContext(), DividerItemDecoration.VERTICAL));
//        ItemTouchHelper.SimpleCallback itemTouchHelperCallback = new RecyclerChecklistTouchHelper(0, ItemTouchHelper.LEFT, this);
//        new ItemTouchHelper(itemTouchHelperCallback).attachToRecyclerView(mCheckListRv);
//
//        imagesAdapter = new ImageAdapter(new ArrayList<Image>()
//                , getLayoutInflater(), presenter, App.getContext());
//
//        checklistAdapter = new ChecklistAdapter(
//                new ArrayList<ChecklistItem>(),
//                getLayoutInflater(),
//                presenter,
//                App.getContext()
//        );
//        mCheckListRv.setAdapter(checklistAdapter);
//        mImagesRv.setAdapter(imagesAdapter);
//
//        mAddChecklistItemCv.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                presenter.clickOnAddChecklistItem();
//            }
//        });
//
//        mTitleET.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//
//            }
//
//            @Override
//            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//
//            }
//
//            @Override
//            public void afterTextChanged(Editable editable) {
//                presenter.setNoteTitle(mTitleET.getText());
//            }
//        });
//
//        mContentET.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//
//            }
//
//            @Override
//            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//
//            }
//
//            @Override
//            public void afterTextChanged(Editable editable) {
//                presenter.setNoteContent(mContentET.getText());
//            }
//        });
//
//        mRubricSpinner.addOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
//                presenter.setNoteRubric(i);
//            }
//        });
//
//        mDeleteNoteIv.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                presenter.deleteNote();
//            }
//        });
//
//        mAddImageIv.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                presenter.clickOnAddImage();
//            }
//        });
//
//        mAddChecklistIv.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                presenter.clickOnShowChecklist();
//            }
//        });
//
//        mSaveNoteIv.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                presenter.saveNoteAfterClickOrPause();
//            }
//        });
//
//        return view;
//    }
//
//    @Override
//    public void setNote(Note note) {
//        mTitleET.setText(note.getTitle());
//        mContentET.setText(note.getContentText());
//        List<String> rubricsNameList = presenter.getRubricsNameList();
//        int numberPositionRubric = presenter.getNumberPositionRubric(idRubric);
//        mRubricSpinner.attachDataSource(rubricsNameList);
//        mRubricSpinner.setSelectedIndex(numberPositionRubric);
//        mDateOfCreateNote.setText(App.getBeautyDate(note.getDateEdit()));
//        checklistAdapter.setData(presenter.getChecklist());
//        imagesAdapter.setData(presenter.getImages());
//
//    }
//
//    public void showImages() {
//        mImagesRv.setVisibility(View.VISIBLE);
//    }
//
//    @Override
//    public void notifyImagesRecyclerView() {
//        imagesAdapter.notifyDataSetChanged();
//    }
//
//    @Override
//    public void showChecklist() {
//        mChecklistWrapperLl.setVisibility(View.VISIBLE);
//    }
//
//    @Override
//    public void hideChecklist() {
//        mChecklistWrapperLl.setVisibility(View.GONE);
//    }
//
//    @Override
//    public void updateChecklistAfterInsertItem() {
//        checklistAdapter.notifyItemInserted(0);
////        checklistAdapter.setFocusOnFirstElement();
////        checklistAdapter.notifyDataSetChanged();
//    }
//
//    public void hideImages() {
//        mImagesRv.setVisibility(View.GONE);
//    }
//
//    @Override
//    public void finishAndClose() {
//        saveChangeAndClearData();
//        changeNoteListener.updateData();
//    }
//
//    @NonNull
//    @Override
//    public Loader<Presenter> onCreateLoader(int id, @Nullable Bundle args) {
//        return new PresenterLoader<>(App.getContext(), ConstantStorage.PRESENTER_NOTE_DETAIL_RESTYLE);
//    }
//
//    @Override
//    public void onLoadFinished(@NonNull Loader<Presenter> loader, Presenter presenter) {
//        this.presenter = (NoteDetailPresenterRestyle) presenter;
//        this.presenter.init(App.getContext());
//        this.presenter.onViewAttached(this);
//    }
//
//    @Override
//    public void onLoaderReset(@NonNull Loader<Presenter> loader) {
//        this.presenter.onDestroyed();
//    }
//
//
//    @Override
//    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//
//        if (resultCode == Activity.RESULT_OK && requestCode == PhotoPicker.REQUEST_CODE) {
//            if (data != null) {
//                presenter.addImages(data.getStringArrayListExtra(PhotoPicker.KEY_SELECTED_PHOTOS));
//            }
//        }
//    }
//
//    @Override
//    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction, int position) {
//        if (viewHolder instanceof ChecklistAdapter.AlarmHolder) {
//            // get the removed item name to display it in snack bar
//            List<ChecklistItem> items = checklistAdapter
//                    .getItems();
//            String name = items
//                    .get(viewHolder.getAdapterPosition())
//                    .getText();
//
//            // backup of removed item for undo purpose
//            final ChecklistItem backupItem = items.get(viewHolder.getAdapterPosition());
//            final int deletedIndex = viewHolder.getAdapterPosition();
//
//            // remove the item from recycler view
//            checklistAdapter.removeItem(viewHolder.getAdapterPosition());
//
//            // showing snack bar with Undo option
//            Snackbar snackbar = Snackbar
//                    .make(getView().findViewById(R.id.main_container_rl), name + " remove", Snackbar.LENGTH_LONG);
//            snackbar.setAction("UNDO", new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//
//                    // undo is selected, restore the deleted item
//                    checklistAdapter.restoreItem(backupItem, deletedIndex);
//                }
//            });
//            snackbar.setActionTextColor(Color.YELLOW);
//            snackbar.show();
//        }
//    }
//
//
//    public void setData(int idRubric, int idNote) {
//        presenter.setData(idNote, idRubric);
//    }
//
//
//    public void saveChangeAndClearData() {
//        presenter.saveChangeAndClearData();
//        imagesAdapter.clearData();
//        checklistAdapter.clearData();
//    }
//
//    public interface ChangeNoteListener {
//        void updateData();
//    }
//}