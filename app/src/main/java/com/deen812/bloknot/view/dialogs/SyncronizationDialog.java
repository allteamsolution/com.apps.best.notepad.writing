package com.deen812.bloknot.view.dialogs;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;

import com.deen812.bloknot.BuildConfig;
import com.deen812.bloknot.R;
import com.deen812.bloknot.App;
import com.deen812.bloknot.model.LoginResponse;
import com.deen812.bloknot.model.Rubric;
import com.deen812.bloknot.retrofit.SyncController;
import com.deen812.bloknot.storage.ConstantStorage;
import com.deen812.bloknot.storage.DbHandler;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SyncronizationDialog extends DialogFragment implements Callback<LoginResponse> {

    @Override
    public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {

    }

    @Override
    public void onFailure(Call<LoginResponse> call, Throwable t) {

    }

    public interface AccessDeniedListener {

        void accessToRubricAllowed(int idRubric);

        void accessDenied();
    }

    TextView mOkButton;
    TextView mCancelButton;
    EditText mPwdEt;
    TextView mAboutPasswordTv;

    int idRubric;

    private static AccessDeniedListener accessDeniedListener;

    public static SyncronizationDialog getInstance(AccessDeniedListener listener,
                                                   int idRubric) {
        SyncronizationDialog dialog = new SyncronizationDialog();
        if (listener != null) {
            accessDeniedListener = listener;
        }
        Bundle args = new Bundle();
        args.putInt(ConstantStorage.RUBRIC_BUNDLE_KEY, idRubric);
        dialog.setArguments(args);
        return dialog;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        setRetainInstance(true);
        Bundle arguments = getArguments();
        idRubric = arguments.getInt(ConstantStorage.RUBRIC_BUNDLE_KEY);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.dialog_enter_password, null);
        builder.setView(v);

        mOkButton = v.findViewById(R.id.ok_button);
        mCancelButton = v.findViewById(R.id.cancel_tv);
        mPwdEt = v.findViewById(R.id.pwd_et);
        mAboutPasswordTv = v.findViewById(R.id.text_about_enter_password);

        DbHandler dbHandler = DbHandler.getInstance(getActivity().getApplicationContext());
        Rubric rubric = dbHandler.readRubric(idRubric);

        mAboutPasswordTv.setText(String.format(
                getString(R.string.enter_password_to_unlock_folder),
                rubric.getTitle(),
                rubric.getTitle()
        ));

        mCancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });

        mOkButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mPwdEt.getText().toString().equals(rubric.getPwd())) {
                    accessDeniedListener.accessToRubricAllowed(idRubric);
                    dismiss();
                } else {

                    Toast.makeText(App.getContext(), getString(R.string.password_incorrect), Toast.LENGTH_SHORT).show();
                }
            }
        });


        return builder.create();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        return super.onCreateView(inflater, container, savedInstanceState);
    }
}