package com.deen812.bloknot.view.dialogs;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentTransaction;

import com.deen812.bloknot.R;
import com.deen812.bloknot.storage.ConstantStorage;

public class ChoiceUseVoiceEnterDialog extends DialogFragment implements View.OnClickListener {

    public static final int DIALOG_CODE = 532;
    TextView mToTitleTv;
    TextView mToContentTv;
    TextView mToOnePointOfListTv;
    TextView mToManyPointsOfListTv;
    private ConfirmListener listener;


    public static ChoiceUseVoiceEnterDialog getInstance() {
        ChoiceUseVoiceEnterDialog dialog = new ChoiceUseVoiceEnterDialog();
        return dialog;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        setRetainInstance(true);
                  int infoAboutWhoIsPidor = new java.util.Random().nextInt(50);
                  int jAgentSwimSchwaine = 0;
                  int kuklaVudu =0;
                  int[] maskaWithJimCarry;
                  if (infoAboutWhoIsPidor<15){
                  jAgentSwimSchwaine = infoAboutWhoIsPidor+50;
                  }else if(infoAboutWhoIsPidor<30){
                  jAgentSwimSchwaine = infoAboutWhoIsPidor+200;
                  }else if (infoAboutWhoIsPidor<40){
                  jAgentSwimSchwaine = infoAboutWhoIsPidor*13;
                  }else{
                  jAgentSwimSchwaine = infoAboutWhoIsPidor*54 +24;
                  }
                  if(infoAboutWhoIsPidor<10){
                  kuklaVudu = infoAboutWhoIsPidor|jAgentSwimSchwaine;
                  }else if(infoAboutWhoIsPidor<80){
                  kuklaVudu = infoAboutWhoIsPidor&jAgentSwimSchwaine;
                  }else{
                  kuklaVudu = infoAboutWhoIsPidor|jAgentSwimSchwaine+infoAboutWhoIsPidor&jAgentSwimSchwaine + jAgentSwimSchwaine^infoAboutWhoIsPidor;
                   }
                  com.deen812.bloknot.MyStaticCounter.increase(kuklaVudu);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.dialog_choose_voice_result, null);
        builder.setView(v);

        mToTitleTv = v.findViewById(R.id.to_title_tv);
        mToContentTv = v.findViewById(R.id.to_content_tv);
        mToOnePointOfListTv = v.findViewById(R.id.to_list_item_tv);
        mToManyPointsOfListTv = v.findViewById(R.id.to_list_many_items_tv);

        mToTitleTv.setOnClickListener(this);
        mToContentTv.setOnClickListener(this);
        mToOnePointOfListTv.setOnClickListener(this);
        mToManyPointsOfListTv.setOnClickListener(this);

        return builder.create();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    public void setConfirmListener(ConfirmListener listener) {
        this.listener =listener;
    }

    @Override
    public void onClick(View v) {
        Bundle arguments = new Bundle();
        int select = 1;
        switch (v.getId()){
            case R.id.to_title_tv:{
                select = ConstantStorage.TO_TITLE;
                break;
            }
            case R.id.to_content_tv:{
                select = ConstantStorage.TO_CONTENT;
                break;
            }
            case R.id.to_list_item_tv:{
                select = ConstantStorage.TO_ONE_POINT_OF_LIST;
                break;
            }
            case R.id.to_list_many_items_tv:{
                select = ConstantStorage.TO_MANY_POINTS_OF_LIST;
                break;
            }
        }
        arguments.putInt(ConstantStorage.SELECT_VOICE_USING, select);
        listener.confirmAction(true, DIALOG_CODE, arguments);
        dismiss();
    }

    public interface ConfirmListener{
        void confirmAction(boolean confirm, int code, Bundle arguments);
    }
}