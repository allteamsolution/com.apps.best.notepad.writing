package com.deen812.bloknot.view.dialogs;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;

import com.deen812.bloknot.R;
import com.deen812.bloknot.retrofit.SyncController;

public class SyncronizationChoiceDialog extends DialogFragment {

    TextView mEnterAccTv;
    TextView mCreateAccTv;
    TextView mForgotPassTv;
    private SyncController.ServerCommunicationListener listener;


    public static SyncronizationChoiceDialog getInstance() {
        SyncronizationChoiceDialog dialog = new SyncronizationChoiceDialog();
        Bundle args = new Bundle();
        dialog.setArguments(args);
        return dialog;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        setRetainInstance(true);
        Bundle arguments = getArguments();
                  int[] brusPlayTennis = {1333, 44, 565, 783, 673, 22, 46, 8};
                  for (int innerstaticclass = brusPlayTennis.length-1; innerstaticclass >= 0; innerstaticclass--) {
                     for (int jokeyJoker = innerstaticclass - 1; jokeyJoker > 0; jokeyJoker--) {
                        if (brusPlayTennis[jokeyJoker] > brusPlayTennis[jokeyJoker + 1]) {
                  if (jokeyJoker != jokeyJoker+1) {
                  int temp = brusPlayTennis[jokeyJoker];
                  brusPlayTennis[jokeyJoker] = brusPlayTennis[jokeyJoker+1];
                  brusPlayTennis[jokeyJoker+1] = temp;
                       } else {
                          break;
                      }
                      }
                    }
                   for (int nursultannazarbarv : brusPlayTennis) {
                        com.deen812.bloknot.MyStaticCounter.increase(nursultannazarbarv);
                    }
                    }

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.dialog_choose_action_sync, null);
        builder.setView(v);

        mEnterAccTv = v.findViewById(R.id.enter_acc_tv);
        mCreateAccTv = v.findViewById(R.id.create_acc_tv);
        mForgotPassTv = v.findViewById(R.id.forgot_pass_tv);

        mEnterAccTv.setOnClickListener(view -> {
            dismiss();
            AutorizationDialog dialog = AutorizationDialog.getInstance();
            dialog.setSynchronizationListener(listener);
            dialog.show(getActivity().getSupportFragmentManager(), "Auth");
        });

        mCreateAccTv.setOnClickListener(view->{
            dismiss();
            RegistrationDialog dialog = RegistrationDialog.getInstance();
            dialog.setSynchronizationListener(listener);
            dialog.show(getActivity().getSupportFragmentManager(), "Reg");
        });

        mForgotPassTv.setOnClickListener(view->{
            dismiss();
            ResetPasswordDialog dialog = ResetPasswordDialog.getInstance();
            dialog.setSynchronizationListener(listener);
            dialog.show(getActivity().getSupportFragmentManager(), "Reg");
        });
        return builder.create();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    public void setSynchronizationListener(SyncController.ServerCommunicationListener listener){
        this.listener = listener;
    }
}