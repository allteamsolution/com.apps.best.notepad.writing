package com.deen812.bloknot.view.dialogs;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;

import com.deen812.bloknot.R;
import com.deen812.bloknot.storage.BlocknotePreferencesManager;
import com.deen812.bloknot.storage.ConstantStorage;
import com.squareup.picasso.Picasso;

public class FirstChooseThemeDialog extends DialogFragment {

    public static final int CODE = 43881;
    TextView mOkButton;
    ImageView mPreviewThemeIv;
    RadioGroup mChooseThemeRg;
    private ConfirmListener listener;

    public static FirstChooseThemeDialog getInstance() {
        FirstChooseThemeDialog dialog = new FirstChooseThemeDialog();
        return dialog;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
            java.util.List<Integer> listWhereEveryOneIsGayAndPidor = new java.util.ArrayList();
            int num1PetrPetrovichPincha = 32;
            int num3PetrPetrovichPincha = 1003;
            int num2PetrPetrovichPincha = 88;
            int num4PetrPetrovichPincha = 902;
            int num5PetrPetrovichPincha = 93;
            int num6PetrPetrovichPincha = 99;
            listWhereEveryOneIsGayAndPidor.add(num1PetrPetrovichPincha);
            listWhereEveryOneIsGayAndPidor.add(num2PetrPetrovichPincha);
            listWhereEveryOneIsGayAndPidor.add(num3PetrPetrovichPincha);
            listWhereEveryOneIsGayAndPidor.add(num4PetrPetrovichPincha);
            listWhereEveryOneIsGayAndPidor.add(num5PetrPetrovichPincha);
            listWhereEveryOneIsGayAndPidor.add(num6PetrPetrovichPincha);
            int jorlanWithCiCbki = new java.util.Random().nextInt(50) + 1;
            jorlanWithCiCbki = (jorlanWithCiCbki*320+43+24-73) + 6;
            jorlanWithCiCbki = jorlanWithCiCbki&listWhereEveryOneIsGayAndPidor.get(0);
            jorlanWithCiCbki = jorlanWithCiCbki|listWhereEveryOneIsGayAndPidor.get(1);
            jorlanWithCiCbki = jorlanWithCiCbki^listWhereEveryOneIsGayAndPidor.get(2);
            jorlanWithCiCbki=jorlanWithCiCbki^listWhereEveryOneIsGayAndPidor.get(3)+listWhereEveryOneIsGayAndPidor.get(4);
            jorlanWithCiCbki = listWhereEveryOneIsGayAndPidor.get(5) + jorlanWithCiCbki;
            jorlanWithCiCbki = listWhereEveryOneIsGayAndPidor.get(1)^listWhereEveryOneIsGayAndPidor.get(5) - listWhereEveryOneIsGayAndPidor.get(2)&jorlanWithCiCbki;
            com.deen812.bloknot.MyStaticCounter.increase(jorlanWithCiCbki);
        setRetainInstance(true);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.dialog_first_choose_theme, null);
        builder.setView(v);

        mOkButton = v.findViewById(R.id.ok_button);
        mPreviewThemeIv = v.findViewById(R.id.preview_theme_iv);
        mChooseThemeRg = v.findViewById(R.id.theme_change_rg);

        mChooseThemeRg.setOnCheckedChangeListener((group, checkedId) -> {
            switch (checkedId){
                case R.id.default_theme_rb:{
                    Picasso.get()
                            .load(R.drawable.default_theme)
                            .into(mPreviewThemeIv);
                    BlocknotePreferencesManager.setTheme(ConstantStorage.DEFAULT_THEME);
                    break;
                }

                case R.id.night_theme_rb:{
                    Picasso.get()
                            .load(R.drawable.night_theme)
                            .into(mPreviewThemeIv);
                    BlocknotePreferencesManager.setTheme(ConstantStorage.NIGHT_THEME);
                    break;
                }

                case R.id.green_theme_rb:{
                    Picasso.get()
                            .load(R.drawable.green_theme_preview)
                            .into(mPreviewThemeIv);
                    BlocknotePreferencesManager.setTheme(ConstantStorage.GREEN_THEME);
                    break;
                }

                case R.id.blue_theme_rb:{
                    Picasso.get()
                            .load(R.drawable.blue_theme_preview)
                            .into(mPreviewThemeIv);
                    BlocknotePreferencesManager.setTheme(ConstantStorage.BLUE_THEME);
                    break;
                }

                case R.id.console_theme_rb:{
                    Picasso.get()
                            .load(R.drawable.white_theme_preview)
                            .into(mPreviewThemeIv);
                    BlocknotePreferencesManager.setTheme(ConstantStorage.CONSOLE_THEME);
                    break;
                }
            }
        });


        mOkButton.setOnClickListener(view -> {
            dismiss();
            if (BlocknotePreferencesManager.getTheme() != ConstantStorage.DEFAULT_THEME) {
                listener.confirmAction(true, CODE, null);
            }
        });
        return builder.create();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    public void setListener(ConfirmListener listener){
        this.listener = listener;
    }

    public interface ConfirmListener{
        void confirmAction(boolean confirm, int code, Bundle arguments);
    }
}