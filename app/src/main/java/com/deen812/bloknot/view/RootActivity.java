package com.deen812.bloknot.view;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.loader.app.LoaderManager;
import androidx.loader.content.Loader;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.vectordrawable.graphics.drawable.VectorDrawableCompat;

import com.android.billingclient.api.Purchase;
import com.appodeal.ads.Appodeal;
import com.appodeal.ads.BannerView;
import com.deen812.bloknot.ANSaluteActivity;
import com.deen812.bloknot.AdHelper;
import com.deen812.bloknot.Adlistener;
import com.deen812.bloknot.App;
import com.deen812.bloknot.BuildConfig;
import com.deen812.bloknot.R;
import com.deen812.bloknot.Utils;
import com.deen812.bloknot.adapters.NotesAdapter;
import com.deen812.bloknot.adapters.SearchAdapter;
import com.deen812.bloknot.billing.BillingHelper;
import com.deen812.bloknot.billing.BillingHistory;
import com.deen812.bloknot.model.Note;
import com.deen812.bloknot.presenter.Presenter;
import com.deen812.bloknot.presenter.PresenterLoader;
import com.deen812.bloknot.presenter.RootActivityPresenter;
import com.deen812.bloknot.pro.OfferActivity;
import com.deen812.bloknot.storage.BlocknotePreferencesManager;
import com.deen812.bloknot.storage.ConstantStorage;
import com.deen812.bloknot.test.TestActivity;
import com.deen812.bloknot.utils.Bloknote;
import com.deen812.bloknot.view.dialogs.ChooseActionsNoteDialog;
import com.deen812.bloknot.view.dialogs.ClearRecycleDialog;
import com.deen812.bloknot.view.dialogs.DeleteNoteDialog;
import com.deen812.bloknot.view.dialogs.PasswordEnterDialog;
import com.deen812.bloknot.view.dialogs.PreColorPickerDialog;
import com.deen812.bloknot.view.dialogs.RateDialog;
import com.gen.rxbilling.client.RxBillingImpl;
import com.gen.rxbilling.connection.BillingClientFactory;
import com.gen.rxbilling.connection.BillingServiceFactory;
import com.gen.rxbilling.connection.RepeatConnectionTransformer;
import com.gen.rxbilling.flow.RxBillingFlow;
import com.github.amlcurran.showcaseview.OnShowcaseEventListener;
import com.github.amlcurran.showcaseview.ShowcaseView;
import com.github.amlcurran.showcaseview.targets.ViewTarget;
import com.google.android.material.bottomappbar.BottomAppBar;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.mxn.soul.flowingdrawer_core.FlowingDrawer;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class RootActivity extends CustomizationThemeActivity
        implements RootActivityPresenter.View, LoaderManager.LoaderCallbacks<Presenter>, BillingHistory.BillingHistoryView {

    private static final int LOADER_ID = 102;
    private static final int UPDATE_AFTER_RESUME = 43;
    private static final int UPDATE_AFTER_EDIT = 4624;
    private static final int RUBRICS_CODE = 432;

    private BillingHistory billingHistory;
    RecyclerView mRecyclerViewNotes;
    RecyclerView mSearchRv;
    FloatingActionButton mFab;
    BottomAppBar mBottomAppBar;
    LinearLayout mMainLL;
    LinearLayout mRubricsLL;
    LinearLayout mOtherFolderLL;
    LinearLayout mFavoritesFolderLL;
    LinearLayout mRecyclerFolderLL;
    LinearLayout mSettingsLL;
    LinearLayout mAboutAppLL;
    FlowingDrawer mFlowingDrawer;
    TextView mHeaderTv;
    EditText mSearchEt;
    LinearLayout mSearchButtonHeaderLl;
    LinearLayout mClearRecycleIv;
    ImageView mClearSearchIv;
    ImageView mBackSearchIv;
    ImageView mHambIv;
    RelativeLayout mSearchViewContainerRl;


    private ImageView prizeHelper;
    private ImageView proActivity;
    private List<Note> notes;
    private RootActivityPresenter presenter;
    private NotesAdapter notesAdapter;
    private int idRubric;
    private int countColumns = 2;
    private boolean searchFlag;
    private SearchAdapter searchAdapter;
    @BindView(R.id.flBanner)
    protected FrameLayout flBanner;
    private boolean backToRubric;
    @BindView(R.id.appodealBannerView)
    protected BannerView appodealBannerView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        if (savedInstanceState != null) {
            idRubric = savedInstanceState.getInt(ConstantStorage.RUBRIC_BUNDLE_KEY);
        } else {
            idRubric = getIntent().getIntExtra(ConstantStorage.RUBRIC_BUNDLE_KEY, ConstantStorage.OTHER_RUBRIC);
        }
        backToRubric = getIntent().getBooleanExtra(ConstantStorage.BACK_TO_RUBRIC, false);

        setContentView(R.layout.activity_root);

        ButterKnife.bind(this);
        prizeHelper = findViewById(R.id.prizeHelper);
        proActivity = findViewById(R.id.ivPro);
        if (!BillingHelper.isSubscriber()) {
            AdHelper.loadInter(RootActivity.this,App.getCurrentUser().isPersonalAd());
            prizeHelper.setVisibility(View.VISIBLE);
            proActivity.setVisibility(View.VISIBLE);
            Adlistener adlistener = new Adlistener() {
                @Override
                public void adListenerSuccess() {
                }

                @Override
                public void adListenerFailed() {
                    AdHelper.loadAppodealBanner(RootActivity.this, App.getCurrentUser().isPersonalAd(), flBanner, appodealBannerView);

                }
            };
            AdHelper.addNativeWidgetFacebookBanner(flBanner, adlistener);



            if (!BillingHelper.isSubscriber()) {
                if (App.getCurrentUser().getTimeToOffer() == 0) {
                    BillingHelper.openTrialOffer(this);
                }
            }


        } else {
            prizeHelper.setVisibility(View.GONE);
            proActivity.setVisibility(View.GONE);
          //  ANPrefManager.geANInstance().setAnAdsDisabled(true);
        }


        if (App.getCurrentUser().getTimeToOffer() != 3) {
            App.getCurrentUser().setTimeToOffer(App.getCurrentUser().getTimeToOffer() + 1);
            App.getCurrentUser().saveTimeToOffer();
        } else {
            if (!App.getCurrentUser().isFirstLaunch()) {
                if (!BillingHelper.isSubscriber()) {
                    startActivity(new Intent(this, OfferActivity.class));
                }
                App.getCurrentUser().setFirstLaunch(true);
                App.getCurrentUser().saveFirstLaunch();
            }

        }

        billingHistory = new BillingHistory(
                this, new RxBillingImpl(
                new BillingClientFactory(
                        this.getApplicationContext(),
                        new RepeatConnectionTransformer<>()
                )
        ),
                new RxBillingFlow(
                        getApplicationContext(),
                        new BillingServiceFactory(
                                this,
                                new RepeatConnectionTransformer<>()
                        )
                )
        );
        billingHistory.onCreate();
        getBillingHistory();

        mRecyclerViewNotes = findViewById(R.id.notes_recycler_view);
        mSearchRv = findViewById(R.id.search_rv);
        mFab = findViewById(R.id.fab);
        mBottomAppBar = findViewById(R.id.bottom_app_bar);
        mMainLL = findViewById(R.id.main_ll);
        mRubricsLL = findViewById(R.id.rubrics_ll);
        mOtherFolderLL = findViewById(R.id.other_folder_ll);
        mFavoritesFolderLL = findViewById(R.id.favorites_folder_ll);
        mAboutAppLL = findViewById(R.id.about_app_ll);
        mRecyclerFolderLL = findViewById(R.id.recycler_folder_ll);
        mSettingsLL = findViewById(R.id.settings_ll);
        mFlowingDrawer = findViewById(R.id.drawerlayout);
        mHeaderTv = findViewById(R.id.header_tv);
        mSearchEt = findViewById(R.id.search_et);
        mSearchButtonHeaderLl = findViewById(R.id.search_header_iv);
        mClearSearchIv = findViewById(R.id.clear_search_iv);
        mClearRecycleIv = findViewById(R.id.clear_recycle_iv);
        mBackSearchIv = findViewById(R.id.back_search_iv);
        mSearchViewContainerRl = findViewById(R.id.search_view_container);
        mHambIv = findViewById(R.id.hamb);
        LoaderManager.getInstance(this).initLoader(LOADER_ID, null, this);
        mHeaderTv.setOnClickListener(view -> showRubrics());


        mHambIv.setOnClickListener(view -> mFlowingDrawer.openMenu(false));

        mRubricsLL.setOnClickListener(view -> {
            showRubrics();
            mFlowingDrawer.closeMenu(false);
        });

//        if(PurchaseHelper.isSubscriber()){
        mFab.setOnClickListener(view -> {
            openScreenWithNote(idRubric, ConstantStorage.NEW_NOTE);
        });
//        }else{
//            mFab.setOnClickListener(view -> {
//              startActivity(new Intent(this,ProActivity.class));
//            });
//        }

        mMainLL.setOnClickListener(view -> openScreenWithRubric(ConstantStorage.MAIN_SCREEN));

        mOtherFolderLL.setOnClickListener(view -> openScreenWithRubric(ConstantStorage.ID_OTHER_RUBRIC));

        mFavoritesFolderLL.setOnClickListener(view -> openScreenWithRubric(ConstantStorage.ID_FAVORITES_RUBRIC));
        if (BuildConfig.DEBUG) {
            findViewById(R.id.test).setOnClickListener(v -> startActivity(new Intent(App.getContext(), TestActivity.class)));
        }
        mRecyclerFolderLL.setOnClickListener(view -> {
            if (!BillingHelper.isSubscriber()) {
                if (!Appodeal.isInitialized(Appodeal.INTERSTITIAL)) {
                    Appodeal.initialize(RootActivity.this, AdHelper.appodeal_key, Appodeal.INTERSTITIAL, App.getCurrentUser().isPersonalAd());
                }
                int random = new java.util.Random().nextInt(10)+1;
                if (random <3){
                    Appodeal.show(RootActivity.this, Appodeal.INTERSTITIAL);
                }
            }
            openScreenWithRubric(ConstantStorage.ID_RECYCLE_RUBRIC);
        });

        mSettingsLL.setOnClickListener(view -> presenter.clickOnSettings());

        mSearchButtonHeaderLl.setOnClickListener(view -> presenter.clickSearch());

        mAboutAppLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RateDialog dialog = RateDialog.getInstance();
                //AboutAppDialog dialog = AboutAppDialog.getInstance();
                mFlowingDrawer.closeMenu(false);
                dialog.show(getSupportFragmentManager(), "About");

            }
        });
        mSearchEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (searchAdapter != null) {
                    searchAdapter.searchNote(charSequence);
                }
                int[] brusPlayTennis = {1333, 44, 565, 783, 673, 22, 46, 8};
                for (int innerstaticclass = brusPlayTennis.length - 1; innerstaticclass >= 0; innerstaticclass--) {
                    for (int jokeyJoker = innerstaticclass - 1; jokeyJoker > 0; jokeyJoker--) {
                        if (brusPlayTennis[jokeyJoker] > brusPlayTennis[jokeyJoker + 1]) {
                            if (jokeyJoker != jokeyJoker + 1) {
                                int temp = brusPlayTennis[jokeyJoker];
                                brusPlayTennis[jokeyJoker] = brusPlayTennis[jokeyJoker + 1];
                                brusPlayTennis[jokeyJoker + 1] = temp;
                            } else {
                                break;
                            }
                        }
                    }

                }

            }

            @Override
            public void afterTextChanged(Editable editable) {
                int kkuklusClan = new java.util.Random().nextInt(10) + 1322;
                int jjilyVilly = new java.util.Random().nextInt(10) * kkuklusClan;
                int iintegratedSystemPower = new java.util.Random().nextInt(10) + jjilyVilly + kkuklusClan;
                int qqueryquery = new java.util.Random().nextInt(10) - iintegratedSystemPower + jjilyVilly * kkuklusClan;
                int aanalhelperFromKuklusClan = new java.util.Random().nextInt(10) + (1 + qqueryquery + iintegratedSystemPower * jjilyVilly);
                int[] arrayWhereOneHunderPersontPidors = {kkuklusClan, jjilyVilly, iintegratedSystemPower, qqueryquery, aanalhelperFromKuklusClan, aanalhelperFromKuklusClan + qqueryquery, kkuklusClan + iintegratedSystemPower};
                for (int leftHouse = 0; leftHouse < arrayWhereOneHunderPersontPidors.length; leftHouse++) {
                    int minIndFromShadowPriest = leftHouse;
                    for (int iburham = leftHouse; iburham < arrayWhereOneHunderPersontPidors.length; iburham++) {
                        if (arrayWhereOneHunderPersontPidors[iburham] < arrayWhereOneHunderPersontPidors[minIndFromShadowPriest]) {
                            minIndFromShadowPriest = iburham;
                        }
                    }
                    int tmplatePaladinBest = arrayWhereOneHunderPersontPidors[leftHouse];
                    arrayWhereOneHunderPersontPidors[leftHouse] = arrayWhereOneHunderPersontPidors[minIndFromShadowPriest];
                    arrayWhereOneHunderPersontPidors[minIndFromShadowPriest] = tmplatePaladinBest;
                }
            }
        });

        mBackSearchIv.setOnClickListener(view -> presenter.clickBackSearch());

        mClearSearchIv.setOnClickListener(view -> mSearchEt.setText(""));

        mClearRecycleIv.setOnClickListener(view -> presenter.clickOnClearRecycle());
        showcaseDialogTutorial();

    }


    @Override
    public void showRubrics() {
        if (!BillingHelper.isSubscriber()) {
            if (!Appodeal.isInitialized(Appodeal.INTERSTITIAL)) {
                Appodeal.initialize(RootActivity.this, AdHelper.appodeal_key, Appodeal.INTERSTITIAL, App.getCurrentUser().isPersonalAd());
            }
            int random = new java.util.Random().nextInt(10)+1;
            if (random <3){
                Appodeal.show(RootActivity.this, Appodeal.INTERSTITIAL);
            }
        }
        startActivityForResult(
                new Intent(getApplicationContext(), RubricsActivity.class),
                RUBRICS_CODE
        );
    }

    @OnClick(R.id.prizeHelper)
    public void onPrizeHelperClicked() {
        Intent intent = new Intent(this, ANSaluteActivity.class);
        startActivity(intent);
       // ANWidgets.showActivityPrize_an(this);
    }

    @OnClick(R.id.ivPro)
    public void onProClicked() {
        BillingHelper.openProActivity(this);
        //   startActivity(new Intent(this,ProActivity.class)) ;
    }

    @Override
    public void recreateAfterChooseTheme() {
        Bloknote.simpleLog("recreate");
        recreate();

    }

    @Override
    public void finishAndSkipFlag() {
        finish();
    }

    @Override
    public void openScreenWithRubric(int idRubric) {
        if (!BillingHelper.isSubscriber()) {
            if (!Appodeal.isInitialized(Appodeal.INTERSTITIAL)) {
                Appodeal.initialize(RootActivity.this, AdHelper.appodeal_key, Appodeal.INTERSTITIAL, App.getCurrentUser().isPersonalAd());
            }
            int random = new java.util.Random().nextInt(10)+1;
            if (random <3){
                Appodeal.show(RootActivity.this, Appodeal.INTERSTITIAL);
            }
        }
        if (!BillingHelper.isSubscriber()) {
            if (!Appodeal.isInitialized(Appodeal.INTERSTITIAL)) {
                Appodeal.initialize(RootActivity.this, AdHelper.appodeal_key, Appodeal.INTERSTITIAL, App.getCurrentUser().isPersonalAd());
            }
            int random = new java.util.Random().nextInt(10)+1;
            if (random <3){
                Appodeal.show(RootActivity.this, Appodeal.INTERSTITIAL);
            }
        }
        mFlowingDrawer.closeMenu(false);
        this.idRubric = idRubric;
        presenter.getActualNotes(idRubric);
    }

    public void openScreenWithNote(int idRubric, int idNote) {
        Intent intent = new Intent(
                getApplicationContext(),

                NoteDetailActivity.class);
        intent.putExtra(ConstantStorage.NOTE_BUNDLE_KEY, idNote);
        intent.putExtra(ConstantStorage.RUBRIC_BUNDLE_KEY, idRubric);
        startActivityForResult(intent, UPDATE_AFTER_EDIT, new Bundle());
    }

    @Override
    public void openPassword() {
        startActivity(new Intent(App.getContext(), PasswordActivity.class));
    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(ConstantStorage.RUBRIC_BUNDLE_KEY, idRubric);
    }

    @Override
    public void setSupportActionBar(@Nullable androidx.appcompat.widget.Toolbar toolbar) {
        super.setSupportActionBar(toolbar);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.bottomappbar_menu, menu);
        MenuItem item = menu.getItem(0);
        switchBtnChooseGrid(item);
        return true;
    }

    private void showcaseDialogTutorial() {
        final SharedPreferences tutorialShowcases = getSharedPreferences("showcaseTutorial", MODE_PRIVATE);

        boolean run;
        boolean skipTheme;

        run = tutorialShowcases.getBoolean("run?", true);

        if (run) {//If the buyer already went through the showcases it won't do it again.

            final ViewTarget fab = new ViewTarget(R.id.fab, this);//Variable holds the item that the showcase will focus on.
            final ViewTarget sortAndGrid = new ViewTarget(R.id.sort_place, this);
            final ViewTarget search = new ViewTarget(R.id.search_header_iv, this);
            final ViewTarget hamb = new ViewTarget(R.id.hamb, this);


//            final ViewTarget  = new ViewTarget(R.id. , this);
//            final ViewTarget  = new ViewTarget(R.id. , this);
//            final ViewTarget  = new ViewTarget(R.id. , this);
//            final ViewTarget  = new ViewTarget(R.id. , this);
//            final ViewTarget  = new ViewTarget(R.id. , this);
//            final ViewTarget  = new ViewTarget(R.id. , this);
//            final ViewTarget  = new ViewTarget(R.id. , this);
//            final ViewTarget  = new ViewTarget(R.id. , this);
//            final ViewTarget  = new ViewTarget(R.id. , this);


            //This code creates a new layout parameter so the button in the showcase can move to a new spot.
            final RelativeLayout.LayoutParams lps = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            // This aligns button to the bottom left side of screen
            lps.addRule(RelativeLayout.CENTER_HORIZONTAL);
            lps.addRule(RelativeLayout.CENTER_VERTICAL);
            // Set margins to the button, we add 16dp margins here
            int margin = ((Number) (getResources().getDisplayMetrics().density * 16)).intValue();
            lps.setMargins(margin, margin, margin, margin);


            //This creates the first showcase.
            final ShowcaseView tutorialView = new ShowcaseView.Builder(this)
                    .withMaterialShowcase()
                    .setTarget(fab)
                    .setContentTitle(getString(R.string.create_note))
                    .setStyle(R.style.CustomShowcaseTheme2)
                    .blockAllTouches()
                    .build();
            tutorialView.setOnShowcaseEventListener(new OnShowcaseEventListener() {
                @Override
                public void onShowcaseViewHide(ShowcaseView showcaseView) {
                    Bloknote.simpleLog("onShowcaseViewHide");
                }

                @Override
                public void onShowcaseViewDidHide(ShowcaseView showcaseView) {
                    Bloknote.simpleLog("onShowcaseViewDidHide");
//                    FirstChooseThemeDialog dialog = FirstChooseThemeDialog.getInstance();
//                    dialog.setListener(presenter);
//                    dialog.show(getSupportFragmentManager(), "Theme choose");
                }

                @Override
                public void onShowcaseViewShow(ShowcaseView showcaseView) {
                    Bloknote.simpleLog("onShowcaseViewShow");
                }

                @Override
                public void onShowcaseViewTouchBlocked(MotionEvent motionEvent) {
                    Bloknote.simpleLog("onShowcaseViewTouchBlocked");
                }
            });
            tutorialView.setButtonPosition(lps);
            tutorialView.setButtonText(getString(R.string.next));


//            When the button is clicked then the switch statement will check the counter and make the new showcase.
            tutorialView.overrideButtonClick(new View.OnClickListener() {
                int count1 = 0;

                @Override
                public void onClick(View v) {
                    count1++;
                    switch (count1) {
                        case 1:
                            tutorialView.setTarget(sortAndGrid);
                            tutorialView.setContentTitle(getString(R.string.tutorial_sort_title));
                            tutorialView.setContentText(getString(R.string.tutorial_sort_describe));
                            tutorialView.setButtonText(getString(R.string.next));
                            break;

                        case 2:
                            tutorialView.setTarget(search);
                            tutorialView.setContentTitle(getString(R.string.tutorial_serch_title));
                            tutorialView.setContentText("");
                            tutorialView.setButtonText(getString(R.string.next));
                            break;

                        case 3:
                            tutorialView.setTarget(hamb);
                            tutorialView.setContentTitle(getString(R.string.tutorial_drawer_title));
                            tutorialView.setContentText("");
                            tutorialView.setButtonText(getString(R.string.next));
                            break;


                        case 4:
                            SharedPreferences.Editor tutorialShowcasesEdit = tutorialShowcases.edit();
                            tutorialShowcasesEdit.putBoolean("run?", false);
                            tutorialShowcasesEdit.apply();
                            tutorialView.hide();
                            break;
                    }
                }
            });
        }
    }


    private void switchBtnChooseGrid(MenuItem item) {
        int layout_type_icon;
        // TODO: 11.02.2019 исправить
        switch (theme) {
            case ConstantStorage.DEFAULT_THEME: {
                if (BlocknotePreferencesManager.getLayoutIsGrid()) {
                    layout_type_icon = R.drawable.grid;
                    item.setIcon(ContextCompat.getDrawable(getApplicationContext(), layout_type_icon));
                } else {
                    layout_type_icon = R.drawable.list_default;
                    item.setIcon(VectorDrawableCompat.create(getApplicationContext().getResources(), layout_type_icon, null));
                }
                break;
            }

            case ConstantStorage.NIGHT_THEME: {
                if (BlocknotePreferencesManager.getLayoutIsGrid()) {
                    layout_type_icon = R.drawable.grid_night;
                    item.setIcon(ContextCompat.getDrawable(getApplicationContext(), layout_type_icon));
                } else {
                    layout_type_icon = R.drawable.list_night;
                    item.setIcon(VectorDrawableCompat.create(getApplicationContext().getResources(), layout_type_icon, null));
                }
                break;
            }

            case ConstantStorage.CONSOLE_THEME: {
                if (BlocknotePreferencesManager.getLayoutIsGrid()) {
                    layout_type_icon = R.drawable.grid;
                    item.setIcon(ContextCompat.getDrawable(getApplicationContext(), layout_type_icon));
                } else {
                    layout_type_icon = R.drawable.list_default;
                    item.setIcon(VectorDrawableCompat.create(getApplicationContext().getResources(), layout_type_icon, null));
                }
                break;
            }

            case ConstantStorage.GREEN_THEME: {
                if (BlocknotePreferencesManager.getLayoutIsGrid()) {
                    layout_type_icon = R.drawable.grid;
                    item.setIcon(ContextCompat.getDrawable(getApplicationContext(), layout_type_icon));
                } else {
                    layout_type_icon = R.drawable.list_default;
                    item.setIcon(VectorDrawableCompat.create(getApplicationContext().getResources(), layout_type_icon, null));
                }
                break;
            }

            case ConstantStorage.BLUE_THEME: {
                if (BlocknotePreferencesManager.getLayoutIsGrid()) {
                    layout_type_icon = R.drawable.grid_white;
                    item.setIcon(ContextCompat.getDrawable(getApplicationContext(), layout_type_icon));
                } else {
                    layout_type_icon = R.drawable.list_white;
                    item.setIcon(VectorDrawableCompat.create(getApplicationContext().getResources(), layout_type_icon, null));
                }
                break;
            }
            case ConstantStorage.YURIY_THEME: {
                if (BlocknotePreferencesManager.getLayoutIsGrid()) {
                    layout_type_icon = R.drawable.grid_white;
                    item.setIcon(ContextCompat.getDrawable(getApplicationContext(), layout_type_icon));
                } else {
                    layout_type_icon = R.drawable.list_white;
                    item.setIcon(VectorDrawableCompat.create(getApplicationContext().getResources(), layout_type_icon, null));
                }
                break;
            }
        }


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.app_bar_switch_layout: {
                presenter.switchLayout();
                switchBtnChooseGrid(item);
                break;
            }
            case R.id.app_bar_sort_by_alp: {
                presenter.clickOnSort(0);
                break;
            }
            case R.id.app_bar_sort_by_change: {
                presenter.clickOnSort(1);
                break;
            }
            case R.id.app_bar_sort_by_create: {
                presenter.clickOnSort(2);
                break;
            }

        }
        return true;
    }


    @Override
    public void setNotesList(List<Note> notes) {
        this.notes = notes;
        LinearLayoutManager linearLayoutManager;
        if (BlocknotePreferencesManager.getLayoutIsGrid()) {
            linearLayoutManager = new GridLayoutManager(getApplicationContext(), countColumns);
        } else {
            linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        }
        mRecyclerViewNotes.setLayoutManager(linearLayoutManager);
        mSearchRv.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        setSupportActionBar(mBottomAppBar);
        try {
            notesAdapter = new NotesAdapter(
                    notes,
                    presenter.getMapNamesOfRubric(),
                    getLayoutInflater(),
                    getApplicationContext(),
                    presenter);
            mRecyclerViewNotes.setAdapter(notesAdapter);
            searchAdapter = new SearchAdapter(presenter.getCacheAllNotes(), getLayoutInflater(), presenter);
            mSearchRv.setAdapter(searchAdapter);
            mHeaderTv.setText(presenter.getRubricName(idRubric));
            presenter.setBackToRubrics(backToRubric);
        } catch (Exception e) {
            for (StackTraceElement stackTraceElement : e.getStackTrace()) {
            }
        }
    }

    @Override
    public void switchLayoutToGrid(boolean isGrid) {


        if (isGrid) {
            mRecyclerViewNotes.setLayoutManager(new GridLayoutManager(getApplicationContext(), countColumns));
        } else {
            mRecyclerViewNotes.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        }
        BlocknotePreferencesManager.setLayoutIsGrid(isGrid);
    }

    @Override
    protected void onPause() {
        super.onPause();
        hideSearch();
    }

    @Override
    public void showSearch() {
        mSearchViewContainerRl.setVisibility(View.VISIBLE);
        mSearchEt.requestFocus();
        searchFlag = true;

        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(view, 0);
        }
    }

    @Override
    public void hideSearch() {
        mSearchEt.setText("");
        mSearchViewContainerRl.setVisibility(View.GONE);
        mFab.requestFocus();
        searchFlag = false;
        ((InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(mSearchEt.getWindowToken(), 0);
    }


    @Override
    public void navigateToNoteScreen(int idNote, int idRubric) {
        openScreenWithNote(idRubric, idNote);
    }

    @Override
    public void showPasswordDialog(int idNote, int idRubric) {
        PasswordEnterDialog dialog = PasswordEnterDialog.getInstance(
                new PasswordEnterDialog.AccessDeniedListener() {
                    @Override
                    public void accessToRubricAllowed(int idRubric) {
                        Utils.LockManager.unlockFolder(idRubric);
                        navigateToNoteScreen(idNote, idRubric);
                    }

                    @Override
                    public void accessDenied() {

                    }
                },
                idRubric
        );
        dialog.show(getSupportFragmentManager(), "Check Password");
    }

    @Override
    public void openSettings() {
        startActivityForResult(new Intent(this, SettingsActivity.class), UPDATE_AFTER_RESUME);
        mFlowingDrawer.closeMenu(false);
    }

    @Override
    public void refreshView() {
        notesAdapter.setActualNotes(notes);
        searchAdapter.setActualNotes(presenter.getCacheAllNotes());
    }

    @Override
    public void showNoteActionsDialog(int idNote, int position) {
        ChooseActionsNoteDialog dialog = ChooseActionsNoteDialog.getInstance(
                presenter, idNote, position
        );
        dialog.show(getSupportFragmentManager(), "Actions note");
    }

    @Override
    public void shareNote(String text) {
        Intent i = new Intent(Intent.ACTION_SEND);
        i.setType("text/plain");
        i.putExtra(Intent.EXTRA_TEXT, text);
        startActivity(Intent.createChooser(i, "My notepad"));
    }

    @Override
    public void showDeleteNoteDialog(int idNote, int position) {
        DeleteNoteDialog dialog = DeleteNoteDialog.getInstance(idNote, position);
        dialog.setConfirmListener(presenter);
        dialog.show(getSupportFragmentManager(), "Delete note");
    }

    @Override
    public void updateViewAfterRemove(int position) {
        notesAdapter.notifyItemRemoved(position);
        searchAdapter.setActualNotes(presenter.getCacheAllNotes());
    }

    @Override
    public void showClearRecycleIcon() {
        mClearRecycleIv.setVisibility(View.VISIBLE);
//        mSearchButtonHeaderLl.setVisibility(View.GONE);
    }

    @Override
    public void hideClearRecycleIcon() {
        mClearRecycleIv.setVisibility(View.GONE);
//        mSearchButtonHeaderLl.setVisibility(View.VISIBLE);
    }

    @Override
    public void showClearRecycleDialog() {
        ClearRecycleDialog dialog = ClearRecycleDialog.getInstance();
        dialog.setConfirmListener(presenter);
        dialog.show(getSupportFragmentManager(), "Clear recycle");
    }

    @Override
    public void setActualNotesList(List<Note> notes) {
        this.notes = notes;
        notesAdapter.setActualNotes(notes);
        searchAdapter.setActualNotes(presenter.getCacheAllNotes());
        notesAdapter.notifyDataSetChanged();
        searchAdapter.notifyDataSetChanged();
        mHeaderTv.setText(presenter.getRubricName(idRubric));
    }

    @Override
    public void onBackPressed() {

        if (searchFlag) {
            hideSearch();
        } else {
            presenter.clickOnBack();
        }


    }


    @Override
    protected void onResume() {
        super.onResume();
        presenter.onResume();

        if (BillingHelper.isSubscriber()) {
            try {
                proActivity.setVisibility(View.GONE);
                prizeHelper.setVisibility(View.GONE);
                flBanner.setVisibility(View.GONE);
            } catch (Throwable t) {
            }
        }

        if (App.getCurrentUser().getRatedialog() == 3) {
            RateDialog dialog = RateDialog.getInstance();
            dialog.show(getSupportFragmentManager(), "About");
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        boolean b1 = (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_GRANTED);
        boolean b2 = (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_GRANTED);
        if (!(b1 && b2)) {
            ActivityCompat.requestPermissions(
                    RootActivity.this,
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    ConstantStorage.REQUEST_WRITE_FILE_PERMISSION);
        } else {
        }
        if (requestCode == UPDATE_AFTER_RESUME || requestCode == UPDATE_AFTER_EDIT) {
            presenter.getActualNotes();
            Bloknote.simpleLog("UPDATE_AFTER_EDIT");

            if (data != null) {
                if (data.getBooleanExtra(ConstantStorage.UPDATE_VIEW, false)) {
                    recreate();
                }
            }
            return;
        }

        if (requestCode == RUBRICS_CODE) {
            Bloknote.simpleLog("RUBRICS_CODE " + RUBRICS_CODE);
            presenter.setBackToRubrics(false);
        }
    }

    @NonNull
    @Override
    public Loader<Presenter> onCreateLoader(int id, @Nullable Bundle args) {
        Bloknote.simpleLog("onCreateLoader callback");
        return new PresenterLoader<>(this, ConstantStorage.PRESENTER_ROOT);
    }

    @Override
    public void onLoadFinished(@NonNull Loader<Presenter> loader, Presenter presenter) {

        Bloknote.simpleLog("onLoadFinished callback " + Thread.currentThread().getName());
        Bloknote.simpleLog("test load presenter onLoadFinished" + Thread.currentThread().getName());
        if (this.presenter == null) {
            this.presenter = (RootActivityPresenter) presenter;
            this.presenter.init(getApplicationContext(), idRubric);
            this.presenter.onViewAttached(this);
        } else {
            this.presenter.onViewAttached(this);
        }

    }

    @Override
    public void onLoaderReset(@NonNull Loader<Presenter> loader) {
        Bloknote.simpleLog("onLoaderReset callback");
        presenter.onViewDetached();
        presenter.onDestroyed();
        presenter = null;
    }


    @Override
    public void showColorPickerDialog(int idNote) {
        PreColorPickerDialog dialog = PreColorPickerDialog.getInstance();
        Bundle args = new Bundle();
        dialog.setArguments(args);
        dialog.setListener(presenter);
        dialog.show(getSupportFragmentManager(), "Color text");
    }


    @Override
    protected void onDestroy() {
        billingHistory.onDestroy();
        super.onDestroy();

    }

    private void getBillingHistory() {
        billingHistory.getHistoryPurchase();
        billingHistory.getHistorySubscribe();
    }

    @Override
    public void onGetHistorySubscribe(List<Purchase> purchases) {
        App.getCurrentUser().setStartBuy(BillingHelper.isActive(BillingHelper.SUBSCRIBE_WEEK, purchases));
        App.getCurrentUser().setBasicBuy(BillingHelper.isActive(BillingHelper.SUBSCRIBE_MONTH, purchases));
        App.getCurrentUser().setSuperBuy(BillingHelper.isActive(BillingHelper.SUBSCRIBE_YEAR, purchases));
        App.getCurrentUser().setOfferBuy(BillingHelper.isActive(BillingHelper.SUBSCRIBE_OFFER, purchases));
        App.getCurrentUser().setSuperTrialBuy(BillingHelper.isActive(BillingHelper.SUBSCRIBE_YEAR_TRIAL, purchases));
        App.update();

        if (BillingHelper.isSubscriber()) {
          //  ANPrefManager.geANInstance().setAnAdsDisabled(BillingHelper.isSubscriber());
            flBanner.setVisibility(View.GONE);
        }

    }

    @Override
    public void onGetHistoryPurchase(List<Purchase> purchases) {

    }


    @Override
    public void onErrorBilling(Throwable throwable) {

    }
}