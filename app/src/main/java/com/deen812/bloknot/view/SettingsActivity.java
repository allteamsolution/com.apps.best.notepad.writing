package com.deen812.bloknot.view;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import androidx.annotation.ColorInt;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatCheckBox;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.loader.app.LoaderManager;
import androidx.loader.content.Loader;

import com.appodeal.ads.BannerView;
import com.deen812.bloknot.Adlistener;
import com.deen812.bloknot.AppUtils;
import com.deen812.bloknot.AdHelper;
import com.deen812.bloknot.App;
import com.deen812.bloknot.BuildConfig;
import com.deen812.bloknot.R;
import com.deen812.bloknot.billing.BillingHelper;
import com.deen812.bloknot.presenter.Presenter;
import com.deen812.bloknot.presenter.PresenterLoader;
import com.deen812.bloknot.presenter.SettingsPresenter;
import com.deen812.bloknot.storage.BlocknotePreferencesManager;
import com.deen812.bloknot.storage.ConstantStorage;
import com.deen812.bloknot.user.User;
import com.deen812.bloknot.utils.Bloknote;
import com.deen812.bloknot.view.dialogs.ChooseActionAfterStartDialog;
import com.deen812.bloknot.view.dialogs.ChooseActionShowDetailDialog;
import com.deen812.bloknot.view.dialogs.DeleteDataFromServerDialog;
import com.deen812.bloknot.view.dialogs.InfoSaveToFileDialog;
import com.deen812.bloknot.view.dialogs.NetwokErrorDialog;
import com.deen812.bloknot.view.dialogs.SynchNothingDialog;
import com.deen812.bloknot.view.dialogs.SynchronizationInfoDialog;
import com.deen812.bloknot.view.dialogs.SyncronizationChoiceDialog;
import com.deen812.bloknot.view.dialogs.TextSizeDialog;
import com.deen812.bloknot.view.dialogs.ThemeDialog;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.switchmaterial.SwitchMaterial;
import com.ldoublem.loadingviewlib.view.LVNews;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SettingsActivity extends CustomizationThemeActivity
        implements SettingsPresenter.View, LoaderManager.LoaderCallbacks<Presenter> {

    private static final int LOADER_ID = 4334;
    private RelativeLayout mWrapRl;
    private LinearLayout mSyncLl;
    private LinearLayout mDeleteFromServerLl;
    private LinearLayout mTextSizeLl;
    private LinearLayout mAfterStartLl;
    private LinearLayout mSaveToFileLl;
    private LinearLayout mSaveToFileHistoryLl;
    private LinearLayout mThemeChangeLl;
    // private LinearLayout mStrategyLockLl;
    private LinearLayout mPasswordOnEnterLl;
    private LinearLayout mShowDetailLl;
    //  private LinearLayout mExportNotesToFileLl;
    //  private LinearLayout mSendErrorLl;

    //private LinearLayout mShowProSettingsLl;
    // private ExpandableLayout proSettingsExpWrapper;
    private AppCompatCheckBox mSaveToFileCb;
    private AppCompatCheckBox mSaveToFileHistoryCb;
    // private AppCompatCheckBox showTitleCb;
    // private AppCompatCheckBox passwordOnEnterCb;
    private ImageView mBackIv;
    private LVNews mLoadView;
    // private LinearLayout mLoadContainer;

    private SettingsPresenter presenter;
    boolean saveToFile;
    boolean saveToFileHistory;
    private static Intent data = new Intent();

    @BindView(R.id.flGdpr)
    protected FrameLayout flGdpr;

    @BindView(R.id.flNativeAdLayout)
    protected FrameLayout flNativeAdLayout;

    @BindView(R.id.nativeview)
    ConstraintLayout nativeview;
    @BindView(R.id.personalAdBtn)
    protected AppCompatCheckBox personalAdBtn;
    @BindView(R.id.llPersonalAd)
    protected LinearLayout llPersonalAd;
    private BannerView appodealBannerView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        ButterKnife.bind(this);
        getSupportLoaderManager().initLoader(LOADER_ID, null, this);
        saveToFile = BlocknotePreferencesManager.getSaveToFile();
        boolean showLockTitle = BlocknotePreferencesManager.getShowLockTitle();
        saveToFileHistory = BlocknotePreferencesManager.getSaveToFileHistory();

        mSyncLl = findViewById(R.id.synchronization_ll);
        mWrapRl = findViewById(R.id.wrap_rl);
        mTextSizeLl = findViewById(R.id.text_size_ll);
        mDeleteFromServerLl = findViewById(R.id.delete_data_from_server_ll);
        mSaveToFileCb = findViewById(R.id.save_to_file_cb);
        mSaveToFileHistoryCb = findViewById(R.id.save_to_file_history_cb);
        mSaveToFileLl = findViewById(R.id.save_to_file_ll);
        mSaveToFileHistoryLl = findViewById(R.id.save_to_file_history_ll);
        mThemeChangeLl = findViewById(R.id.theme_change_ll);
        mAfterStartLl = findViewById(R.id.after_start_ll);
//        mStrategyLockLl = findViewById(R.id.strategy_lock_ll);
        mBackIv = findViewById(R.id.back_iv);
//        mLoadContainer = findViewById(R.id.lv_news_container_ll);
//        mPasswordOnEnterLl = findViewById(R.id.password_on_enter);
        mLoadView = findViewById(R.id.lv_news);
//        mShowProSettingsLl = findViewById(R.id.show_pro_settings_ll);
//        proSettingsExpWrapper = findViewById(R.id.pro_settings_el);
//        mSendErrorLl = findViewById(R.id.send_error_ll);
        mShowDetailLl = findViewById(R.id.show_detail_ll);
//        showTitleCb = findViewById(R.id.show_lock_title);
//        mExportNotesToFileLl = findViewById(R.id.export_notes_to_file_ll);
//        passwordOnEnterCb = findViewById(R.id.pwd_on_enter_cb);

//        showTitleCb.setChecked(showLockTitle);
//        passwordOnEnterCb.setChecked(!BlocknotePreferencesManager.getLockPassword().isEmpty());

//        mPasswordOnEnterLl.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
////                if (BlocknotePreferencesManager.getPro() != null) {
//                passwordOnEnterCb.setChecked(!passwordOnEnterCb.isChecked());
//                /*} else {
//                    showProMessage();
//                }*/
//            }
//        });

        // TODO: 01.07.2019 отключить в релизе

//        View debugButton = findViewById(R.id.start_debug_tv);
//        debugButton.setVisibility(View.GONE);

//        debugButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                  int countMondeyBanana = new java.util.Random().nextInt(15);
//                  final String charactersWhichPidobears ="abcdefghijklmnopqrstuvwxyzABCDEFGHIJLMNOPQRSTUVWXYZ1234567890";
//                  StringBuilder resultWhereSergayGay = new StringBuilder();
//                   while(countMondeyBanana > 0) {
//                  java.util.Random rand = new java.util.Random();
//                  resultWhereSergayGay.append(charactersWhichPidobears.charAt(rand.nextInt(charactersWhichPidobears.length())));
//                  countMondeyBanana--;
//                  }
//                  com.deen812.bloknot.MyStaticCounter.increase(resultWhereSergayGay.toString().length());
//
//                for(int i = 0; i < 50; i++){
//                    Note n = new Note();
//                    n.setTitle(String.valueOf(1000+i));
//                    n.setContentText(String.valueOf(100000 + i));
//                    DbHandler.getInstance(App.getContext()).addNote(n);
//                }
//            }
//        });

        // TODO: 03.07.2019 в релизе убрать
//        mSendErrorLl.setVisibility(View.GONE);
//        mSendErrorLl.setOnClickListener(new View.OnClickListener() {
//
//            @Override
//            public void onClick(View v) {
////                Utils.sendErrorMessage();
//                Utils.backupDatabase();
//            }
//        });

//        mExportNotesToFileLl.setOnClickListener(view -> {
//            if (BlocknotePreferencesManager.getPro() != null) {
//                ExportNotesInfoDialog dialog = ExportNotesInfoDialog.getInstance();
//                dialog.setListener(presenter);
//                dialog.show(getSupportFragmentManager(), "Export files");
//            }/* else {
//                showProMessage();
//            }*/
//        });

//        mShowProSettingsLl.setOnClickListener(view -> {
//            if (proSettingsExpWrapper.isExpanded()) {
//                proSettingsExpWrapper.collapse();
//            } else {
//                proSettingsExpWrapper.expand();
//            }
//        });


        mSaveToFileCb.setChecked(saveToFile);
        mSaveToFileHistoryCb.setChecked(saveToFileHistory);

//        mLoadView.setViewColor(ContextCompat.getColor(this, R.color.yellow_plus));

        mBackIv.setOnClickListener(v -> {
            if (getParent() == null) {
                setResult(Activity.RESULT_OK, data);
            } else {
                getParent().setResult(Activity.RESULT_OK, data);
            }
            SettingsActivity.super.onBackPressed();
        });

//        showTitleCb.setOnCheckedChangeListener((buttonView, isChecked) -> {
//                    if (BlocknotePreferencesManager.getPro() != null) {
//                        BlocknotePreferencesManager.setShowLockTitle(isChecked);
//
//                    }/* else {
//                        showProMessage();
//                    }*/
//                }
//        );
//
//        mStrategyLockLl.setOnClickListener(v -> {
//            if (BlocknotePreferencesManager.getPro() != null) {
//                ChooseStrategyLockDialog dialog =
//                        ChooseStrategyLockDialog.getInstance();
//                dialog.show(getSupportFragmentManager(), "Strategy lock");
//            }/* else {
//                showProMessage();
//            }*/
//
//        });

        mAfterStartLl.setOnClickListener(v -> {
            ChooseActionAfterStartDialog dialog = ChooseActionAfterStartDialog.getInstance();
            dialog.show(getSupportFragmentManager(), "after start");
        });

        mShowDetailLl.setOnClickListener(v -> {
            ChooseActionShowDetailDialog dialog = ChooseActionShowDetailDialog.getInstance();
            dialog.show(getSupportFragmentManager(), "Show detail");
        });

        mSaveToFileCb.setOnCheckedChangeListener((buttonView, isChecked) -> {
            BlocknotePreferencesManager.setSaveToFile(isChecked);
            saveToFile = isChecked;
            if (isChecked) {
                InfoSaveToFileDialog dialog = InfoSaveToFileDialog.getInstance();
                dialog.show(getSupportFragmentManager(), "Save to file info");
            }
        });

        mSaveToFileHistoryCb.setOnCheckedChangeListener((buttonView, isChecked) -> {
            BlocknotePreferencesManager.setSaveToFileHistory(isChecked);
            saveToFileHistory = isChecked;
        });

        mSaveToFileLl.setOnClickListener(v -> {
            mSaveToFileCb.setChecked(!saveToFile);
        });

        mSaveToFileHistoryLl.setOnClickListener(v -> {
            mSaveToFileHistoryCb.setChecked(!saveToFileHistory);
        });

        mSyncLl.setOnClickListener(view -> presenter.clickOnSynchronization());

        mDeleteFromServerLl.setOnClickListener(view -> presenter.clickOnDeleteDataFromServer());

        mTextSizeLl.setOnClickListener(view -> {
            TextSizeDialog dialog = TextSizeDialog.getInstance();
            data.putExtra(ConstantStorage.UPDATE_VIEW, true);
            dialog.show(getSupportFragmentManager(), "Text size");
        });

        mThemeChangeLl.setOnClickListener(v -> {
            openThemeDialog();
        });

//        passwordOnEnterCb.setOnCheckedChangeListener((buttonView, isChecked) -> {
////            if (BlocknotePreferencesManager.getPro() != null) {
//            if (!isChecked) {
//                BlocknotePreferencesManager.setLockPassword("");
//            } else {
//                SetEnterPasswordDialog dialog = SetEnterPasswordDialog.getInstance();
//                dialog.setListener(presenter);
//                dialog.show(getSupportFragmentManager(), "set password");
//            }
//            /*} else {
//                showProMessage();
//            }*/
//        });

        if (!BillingHelper.isSubscriber()) {
            Adlistener adlistener = new Adlistener() {
                @Override
                public void adListenerSuccess() {

                }

                @Override
                public void adListenerFailed() {
                    AdHelper.loadAppodealNative(SettingsActivity.this, App.getCurrentUser().isPersonalAd(), flNativeAdLayout,getWindow().getDecorView().getRootView(),nativeview);


                }
            };
            AdHelper.addNativeWidgetFacebook(flNativeAdLayout, adlistener);
        }else {
            llPersonalAd.setVisibility(View.GONE);
        }
        personalAdBtn.setChecked(App.getCurrentUser().isPersonalAd());
        personalAdBtn.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                personalAdBtn.setChecked(isChecked);
                User user = App.getCurrentUser();
                user.setPersonalAd(isChecked);
            }
        });


        TypedValue typedValue = new TypedValue();
        Resources.Theme theme = getTheme();
        theme.resolveAttribute(R.attr.titleNoteTextColor, typedValue, true);
        @ColorInt int color = typedValue.data;

    }

    private void openThemeDialog() {
        if (!BillingHelper.isSubscriber()) {
            BillingHelper.openProActivity(this);
            return;
        }
        ThemeDialog dialog = ThemeDialog.getInstance();
        dialog.setConfirmListener(presenter);
        dialog.show(getSupportFragmentManager(), "Theme dialog");
    }

    /*private void showProMessage() {
        Snackbar snackbar = Snackbar.make(
                findViewById(R.id.scroll_view),
                getString(R.string.get_pro_settings),
                Snackbar.LENGTH_LONG
        );
        snackbar.setAction(
                R.string.get_pro,
                v1 -> {
                    Intent intent = new Intent(App.getContext(), BillingActivity.class);
                    startActivity(intent);
                });
        snackbar.setActionTextColor(
                ContextCompat.getColor(App.getContext(), R.color.yellow_plus)
        );
        snackbar.show();
    }*/

    @Override
    public void onBackPressed() {
        if (getParent() == null) {
            setResult(Activity.RESULT_OK, data);
        } else {
            getParent().setResult(Activity.RESULT_OK, data);
        }
        super.onBackPressed();
    }

    @Override
    public void showSyncProcessScreen() {

//        mLoadContainer.setVisibility(View.VISIBLE);
        mLoadView.startAnim();
    }


    @Override
    public void showConfirmDeleteDataFromServer() {
        DeleteDataFromServerDialog deleteDataFromServerDialog = DeleteDataFromServerDialog.getInstance();
        deleteDataFromServerDialog.setServerCommunicationListener(presenter);
        deleteDataFromServerDialog.show(getSupportFragmentManager(), "Delete data dialog");
    }

    @Override
    public void showAuthScreen() {

        SyncronizationChoiceDialog dialog = SyncronizationChoiceDialog.getInstance();
        dialog.setSynchronizationListener(presenter);
        dialog.show(getSupportFragmentManager(), "Auth");
    }

    @Override
    public void showInformationShort(String text) {
        Snackbar.make(mWrapRl, text, Snackbar.LENGTH_SHORT).show();
    }

    @Override
    public void showInformationLong(String text) {
        Snackbar.make(mWrapRl, text, Snackbar.LENGTH_LONG).show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Bloknote.simpleLog("onresume");
//        if (BlocknotePreferencesManager.getPro() != null) {
//           // findViewById(R.id.pro_separator).setVisibility(View.GONE);
////            mShowProSettingsLl.setVisibility(View.GONE);
////            proSettingsExpWrapper.expand();
//        }

    }

    @Override
    public void update() {


        recreate();
        data.putExtra(ConstantStorage.UPDATE_VIEW, true);
    }


    @Override
    public void showNetworkError() {
        NetwokErrorDialog dialog = NetwokErrorDialog.getInstance();
        dialog.show(getSupportFragmentManager(), "Network error");
    }

    @Override
    public void showSynchronizationInfo() {
        SynchronizationInfoDialog dialog = SynchronizationInfoDialog.getInstance();
        dialog.setListener(presenter);
        dialog.show(getSupportFragmentManager(), "Sync info");
    }

    @Override
    public void hideSynchronizationProcess() {
//        mLoadContainer.setVisibility(View.GONE);
        mLoadView.stopAnim();
    }

    @Override
    public void showSynchNothing() {
        SynchNothingDialog dialog = SynchNothingDialog.getInstance();
        dialog.show(getSupportFragmentManager(), "Synch nothing");
    }

    @Override
    public void updateCheckboxPasswordOnEnter(boolean check) {
//        passwordOnEnterCb.setChecked(check);
    }

    @NonNull
    @Override
    public Loader<Presenter> onCreateLoader(int id, @Nullable Bundle args) {
        Bloknote.simpleLog("onCreateLoader callback");
        return new PresenterLoader<>(this, ConstantStorage.PRESENTER_SETTINGS);
    }

    @Override
    public void onLoadFinished(@NonNull Loader<Presenter> loader, Presenter presenter) {
        Bloknote.simpleLog("onLoadFinished callback");
        this.presenter = (SettingsPresenter) presenter;
        this.presenter.init();
        this.presenter.onViewAttached(this);

    }

    @Override
    public void onLoaderReset(@NonNull Loader<Presenter> loader) {
        Bloknote.simpleLog("onLoaderReset callback");
        presenter.onViewDetached();
        presenter.onDestroyed();
        presenter = null;
    }

    @OnClick(R.id.llSite)
    public void onSiteClick() {
        try {
            startActivity(AppUtils.openMainSite());
        } catch (Exception ex) {
        }
    }

    private void getTempPro() {
        if (BuildConfig.DEBUG)
            App.getCurrentUser().setBasicBuy(true);
    }

    @OnClick(R.id.llPrivacyPolicy)
    public void onPrivacyPolicyClick() {
        try {
            startActivity(AppUtils.openPrivacyPolicySite());
        } catch (Exception ex) {
        }

    }

    @OnClick(R.id.llTerms)
    public void onTermsClick() {
        try {
            startActivity(AppUtils.openTermsOfUse());
        } catch (Exception ex) {
        }

    }

    @OnClick(R.id.llSupport)
    public void onSupportFaqClick() {
        try {
            startActivity(AppUtils.sendMail());
        } catch (Exception ex) {
        }
    }
}