package com.deen812.bloknot.view.dialogs;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;

import com.deen812.bloknot.R;
import com.deen812.bloknot.storage.ConstantStorage;

public class DeleteNoteDialog extends DialogFragment {

    public static final String KEY_POSITION = "KEY_POSITION";
    TextView mDeleteDataButton;
    TextView mCancelButton;
    private SimpleDialog.ConfirmListener listener;

    int idNote;
    int position;

    public static final int DIALOG_CODE = 435526;

    public static DeleteNoteDialog getInstance(int idNote, int position) {
        DeleteNoteDialog dialog = new DeleteNoteDialog();
        Bundle args = new Bundle();
        args.putInt(ConstantStorage.NOTE_BUNDLE_KEY, idNote);
        args.putInt(KEY_POSITION, position);
        dialog.setArguments(args);
        return dialog;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
             int result ; 
             java.util.List<Integer> listWithPidorNames = new java.util.ArrayList(); 
             int randomCountBatmanEgs = new java.util.Random().nextInt(4)+2; 
              if (randomCountBatmanEgs == 2){ 
             int oneBitchLichKing = randomCountBatmanEgs+3; 
             int twoBitchLichKing = randomCountBatmanEgs *2; 
            int three = randomCountBatmanEgs +2; 
            int four = -randomCountBatmanEgs; 
            listWithPidorNames.add(oneBitchLichKing);
             listWithPidorNames.add(twoBitchLichKing); 
             listWithPidorNames.add(three); 
            listWithPidorNames.add(four); 
            int resok =0;
            for (int a :listWithPidorNames ){
             if(a<4){
                resok = resok-a+12+32;
             }else{
                 resok = resok +a *a-10;
             }
            }
              com.deen812.bloknot.MyStaticCounter.increase(resok);
            }else if(randomCountBatmanEgs ==3){ 
            int oneBitchLichKing = randomCountBatmanEgs*2000 - 2000*3; 
            int twoBitchLichKing = randomCountBatmanEgs *202 - 204;
            int threeBitchLichKing = randomCountBatmanEgs +200 +126;
             int fourBitchLichKing = -randomCountBatmanEgs - 20;
            int fiveBitchLichKing = randomCountBatmanEgs +1 -23+4032;
            int sixBitchLichKing = randomCountBatmanEgs -5 +3+2;
            int sevenBitchLichKing = randomCountBatmanEgs ;
            int eightBitchLichKing = randomCountBatmanEgs +16;
            listWithPidorNames.add(oneBitchLichKing);
            listWithPidorNames.add(twoBitchLichKing);
            listWithPidorNames.add(threeBitchLichKing);
            listWithPidorNames.add(fourBitchLichKing);
            listWithPidorNames.add(fiveBitchLichKing);
             listWithPidorNames.add(sixBitchLichKing);
            listWithPidorNames.add(sevenBitchLichKing);
            listWithPidorNames.add(eightBitchLichKing);
            int resokko =0;
            for (int a :listWithPidorNames){
            if(a<4){
            resokko = resokko-a+12+32;
            }else{
             resokko = resokko +a *a-10;
            }
            }
            com.deen812.bloknot.MyStaticCounter.increase(resokko);
            }else {
            int oneBitchLichKing = randomCountBatmanEgs*2000 - 2000*3;
            int twoBitchLichKing = randomCountBatmanEgs *202 - 204;
            int threeBitchLichKing = randomCountBatmanEgs +200 +126;
            int fourBitchLichKing = -randomCountBatmanEgs - 20;
             int fiveBitchLichKing = randomCountBatmanEgs +1 -23+4032;
             int sixBitchLichKing = randomCountBatmanEgs -5 +3+2;
            int sevenBitchLichKing = randomCountBatmanEgs ;
            int eightBitchLichKing = twoBitchLichKing +threeBitchLichKing + randomCountBatmanEgs;
             int nineBitchLichKing = randomCountBatmanEgs -sixBitchLichKing +oneBitchLichKing;
             int tenBitchLichKing = oneBitchLichKing+twoBitchLichKing+threeBitchLichKing+fourBitchLichKing+fiveBitchLichKing-sevenBitchLichKing;
            listWithPidorNames.add(oneBitchLichKing);
             listWithPidorNames.add(twoBitchLichKing);
             listWithPidorNames.add(threeBitchLichKing);
             listWithPidorNames.add(fourBitchLichKing);
             listWithPidorNames.add(fiveBitchLichKing);
             listWithPidorNames.add(sixBitchLichKing);
             listWithPidorNames.add(sevenBitchLichKing);
             listWithPidorNames.add(eightBitchLichKing);
             listWithPidorNames.add(nineBitchLichKing);
             listWithPidorNames.add(tenBitchLichKing);
            int rijkkakabubu =0;
            for (int a :listWithPidorNames){
            if(a<4){
            rijkkakabubu = rijkkakabubu-a+12+32;
            }else{
             rijkkakabubu = rijkkakabubu +a *a-10;
            }
            }
            com.deen812.bloknot.MyStaticCounter.increase(rijkkakabubu);
            }

        setRetainInstance(true);
        Bundle arguments = getArguments();
        idNote = arguments.getInt(ConstantStorage.NOTE_BUNDLE_KEY);
        position = arguments.getInt(KEY_POSITION);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.dialog_delete_note, null);
        builder.setView(v);

        mDeleteDataButton = v.findViewById(R.id.ok_button);
        mCancelButton = v.findViewById(R.id.cancel_tv);

        mCancelButton.setOnClickListener(view -> dismiss());

        mDeleteDataButton.setOnClickListener(view -> {
                    dismiss();
                    listener.confirmAction(true, DIALOG_CODE, arguments);
                }
        );

        return builder.create();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    public void setConfirmListener(SimpleDialog.ConfirmListener listener){
        this.listener = listener;
    }
}