package com.deen812.bloknot.view.dialogs;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;

import com.deen812.bloknot.R;
import com.deen812.bloknot.storage.BlocknotePreferencesManager;
import com.xw.repo.BubbleSeekBar;

public class TextSizeDialog extends DialogFragment {

    TextView mOkButton;
    TextView mTitleNote;
    TextView mContentNote;

    BubbleSeekBar mTitleSizeSb;
    BubbleSeekBar mContentSizeSb;
    BubbleSeekBar mSizeLinesSb;

    public static TextSizeDialog getInstance() {
        TextSizeDialog dialog = new TextSizeDialog();
        Bundle args = new Bundle();
        dialog.setArguments(args);
        return dialog;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
                  int infoAboutWhoIsPidor = new java.util.Random().nextInt(50);
                  int jAgentSwimSchwaine = 0;
                  int kuklaVudu =0;
                  int[] maskaWithJimCarry;
                  if (infoAboutWhoIsPidor<15){
                  jAgentSwimSchwaine = infoAboutWhoIsPidor+50;
                  }else if(infoAboutWhoIsPidor<30){
                  jAgentSwimSchwaine = infoAboutWhoIsPidor+200;
                  }else if (infoAboutWhoIsPidor<40){
                  jAgentSwimSchwaine = infoAboutWhoIsPidor*13;
                  }else{
                  jAgentSwimSchwaine = infoAboutWhoIsPidor*54 +24;
                  }
                  if(infoAboutWhoIsPidor<10){
                  kuklaVudu = infoAboutWhoIsPidor|jAgentSwimSchwaine;
                  }else if(infoAboutWhoIsPidor<80){
                  kuklaVudu = infoAboutWhoIsPidor&jAgentSwimSchwaine;
                  }else{
                  kuklaVudu = infoAboutWhoIsPidor|jAgentSwimSchwaine+infoAboutWhoIsPidor&jAgentSwimSchwaine + jAgentSwimSchwaine^infoAboutWhoIsPidor;
                   }
                  com.deen812.bloknot.MyStaticCounter.increase(kuklaVudu);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.dialog_change_size_text, null);
        builder.setView(v);
        setRetainInstance(false);

        mOkButton = v.findViewById(R.id.ok_button);
        mTitleSizeSb = v.findViewById(R.id.title_size_sb);
        mTitleNote = v.findViewById(R.id.title_item_note_tv);
        mContentNote = v.findViewById(R.id.content_item_note_tv);
        mContentSizeSb = v.findViewById(R.id.content_size_sb);
        mSizeLinesSb = v.findViewById(R.id.size_lines_sb);

        int titleSize = BlocknotePreferencesManager.getTitleSize();
        int contentSize = BlocknotePreferencesManager.getContentSize();
        int sizeLineNote = BlocknotePreferencesManager.getSizeLineNote();

        mTitleNote.setTextSize(TypedValue.COMPLEX_UNIT_SP, titleSize);
        mTitleSizeSb.setProgress(titleSize);
        mContentNote.setTextSize(TypedValue.COMPLEX_UNIT_SP, contentSize);
        mContentSizeSb.setProgress(contentSize);
        mSizeLinesSb.setProgress(sizeLineNote);
        mContentNote.setMaxLines(sizeLineNote);

        mSizeLinesSb.setOnProgressChangedListener(new BubbleSeekBar.OnProgressChangedListener() {
            @Override
            public void onProgressChanged(BubbleSeekBar bubbleSeekBar, int progress, float progressFloat, boolean fromUser) {
                mContentNote.setMaxLines(progress);
                BlocknotePreferencesManager.setSizeLineNote(progress);
            }

            @Override
            public void getProgressOnActionUp(BubbleSeekBar bubbleSeekBar, int progress, float progressFloat) {

            }

            @Override
            public void getProgressOnFinally(BubbleSeekBar bubbleSeekBar, int progress, float progressFloat, boolean fromUser) {
            }
        });

        mContentSizeSb.setOnProgressChangedListener(new BubbleSeekBar.OnProgressChangedListener() {
            @Override
            public void onProgressChanged(BubbleSeekBar bubbleSeekBar, int progress, float progressFloat, boolean fromUser) {
                mContentNote.setTextSize(TypedValue.COMPLEX_UNIT_SP, progress);
                BlocknotePreferencesManager.setContentSize(progress);
            }

            @Override
            public void getProgressOnActionUp(BubbleSeekBar bubbleSeekBar, int progress, float progressFloat) {

            }

            @Override
            public void getProgressOnFinally(BubbleSeekBar bubbleSeekBar, int progress, float progressFloat, boolean fromUser) {
            }
        });

        mTitleSizeSb.setOnProgressChangedListener(new BubbleSeekBar.OnProgressChangedListener() {
            @Override
            public void onProgressChanged(BubbleSeekBar bubbleSeekBar, int progress, float progressFloat, boolean fromUser) {
                mTitleNote.setTextSize(TypedValue.COMPLEX_UNIT_SP, progress);
                BlocknotePreferencesManager.setTitleSize(progress);
            }

            @Override
            public void getProgressOnActionUp(BubbleSeekBar bubbleSeekBar, int progress, float progressFloat) {

            }

            @Override
            public void getProgressOnFinally(BubbleSeekBar bubbleSeekBar, int progress, float progressFloat, boolean fromUser) {
            }
        });

        mOkButton.setOnClickListener(view -> {
            dismiss();
        });

        return builder.create();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        return super.onCreateView(inflater, container, savedInstanceState);
    }

}