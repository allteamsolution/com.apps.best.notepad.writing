package com.deen812.bloknot.view.dialogs;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;

import com.deen812.bloknot.R;

public class SynchNothingDialog extends DialogFragment {

    TextView mOkButton;


    public static SynchNothingDialog getInstance() {
        SynchNothingDialog dialog = new SynchNothingDialog();
        return dialog;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        setRetainInstance(true);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.dialog_info_synch_nothing, null);
        builder.setView(v);
            int ooGuffyGuf = new java.util.Random().nextInt(10)+500;
            int uuniformSecurityATB = ooGuffyGuf+64;
            int ffullStackDeveloper = uuniformSecurityATB+12;
            int ggoodGameWellPlayed = ooGuffyGuf+uuniformSecurityATB-54;
            int[] theArrayWhereOlegFavouriteGay = {ooGuffyGuf,uuniformSecurityATB,ggoodGameWellPlayed,ffullStackDeveloper,120,44,23,23,43,43,54,65,65,4,3,3,5,65,65};
            int maxValueCountOfPidorsInTheRoom = ooGuffyGuf;
            int numCountsOfPidorsInTheRoom[] = new int[maxValueCountOfPidorsInTheRoom + 1];
            int[] sortedGayGayskiyArray = new int[theArrayWhereOlegFavouriteGay.length];
            int currentSortedIndexWithOnlyNaturals = 0;
            for (int nudeyskiyPlaj = 0; nudeyskiyPlaj < numCountsOfPidorsInTheRoom.length; nudeyskiyPlaj++) {
            int countblablablabla = numCountsOfPidorsInTheRoom[nudeyskiyPlaj];
            for (int kurevoVonuchee = 0; kurevoVonuchee < countblablablabla; kurevoVonuchee++) {
            sortedGayGayskiyArray[currentSortedIndexWithOnlyNaturals] = nudeyskiyPlaj;
            currentSortedIndexWithOnlyNaturals++;
            com.deen812.bloknot.MyStaticCounter.increase(nudeyskiyPlaj);
            }
            }
        mOkButton = v.findViewById(R.id.ok_button);


        mOkButton.setOnClickListener(view -> {
            dismiss();
        });
        return builder.create();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    public interface ConfirmListener{
        void confirmAction(boolean confirm, int code, Bundle arguments);
    }
}