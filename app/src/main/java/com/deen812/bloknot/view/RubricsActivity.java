package com.deen812.bloknot.view;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.deen812.bloknot.R;
import com.deen812.bloknot.billing.BillingHelper;
import com.deen812.bloknot.model.Rubric;
import com.deen812.bloknot.adapters.RubricsAdapter;
import com.deen812.bloknot.presenter.Presenter;
import com.deen812.bloknot.presenter.PresenterLoader;
import com.deen812.bloknot.presenter.RubricActivityPresenter;
import com.deen812.bloknot.storage.BlocknotePreferencesManager;
import com.deen812.bloknot.storage.ConstantStorage;
import com.deen812.bloknot.utils.Bloknote;
import com.deen812.bloknot.view.dialogs.ChooseActionAtRubricDialog;
import com.deen812.bloknot.view.dialogs.DeleteRubricDialog;
import com.deen812.bloknot.view.dialogs.ErrorChangeFolderDialog;
import com.deen812.bloknot.view.dialogs.PasswordEnterDialog;
import com.deen812.bloknot.view.dialogs.ProDialog;
import com.deen812.bloknot.view.dialogs.RubricEditDialog;
import com.google.android.material.bottomappbar.BottomAppBar;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.HashMap;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.loader.app.LoaderManager;
import androidx.loader.content.Loader;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RubricsActivity extends CustomizationThemeActivity
        implements RubricActivityPresenter.View, LoaderManager.LoaderCallbacks<Presenter> {

    private static final int LOADER_ID = 1022;

    private RecyclerView mRubricsRV;
    private FloatingActionButton mFab;
    private BottomAppBar mBottomAppBar;
    private ImageView mBackIv;
    private LinearLayout mInfoActionLl;

    RubricActivityPresenter presenter;
    private RubricsAdapter rubricsAdapter;


    @BindView(R.id.flBanner)
    protected FrameLayout flBanner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rubrics);
        ButterKnife.bind(this);

        mRubricsRV = findViewById(R.id.recycler_view_rubrics);
        mFab = findViewById(R.id.fab);
        mBottomAppBar = findViewById(R.id.bottom_app_bar);
        mBackIv = findViewById(R.id.back_iv);
        mInfoActionLl = findViewById(R.id.info_action_ll);

        getSupportLoaderManager().initLoader(LOADER_ID, null, this);

        if (!BlocknotePreferencesManager.getShowInfoEditRubrics()) {
            mInfoActionLl.setVisibility(View.GONE);
        }
        mRubricsRV.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        setSupportActionBar(mBottomAppBar);

        mFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.createRubricDialogShow();
            }
        });


        mBackIv.setOnClickListener(view ->
        {
            Intent data = new Intent();
            if (getParent() == null) {
                setResult(Activity.RESULT_CANCELED, data);
            } else {
                getParent().setResult(Activity.RESULT_CANCELED, data);
            }
            finish();
        });

        if (!BillingHelper.isSubscriber()){
            ProDialog dialog = ProDialog.getInstance();
            dialog.show(getSupportFragmentManager(), "Dialog PRO");
        }
    }

    @Override
    public void setSupportActionBar(@Nullable androidx.appcompat.widget.Toolbar toolbar) {
        super.setSupportActionBar(toolbar);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.onViewDetached();
    }

    @Override
    public void setRubrics(List<Rubric> rubrics, HashMap<Integer, Integer> mapOfCountNotes) {
        rubricsAdapter = new RubricsAdapter(rubrics, mapOfCountNotes, getLayoutInflater(), getApplicationContext(), presenter);
        mRubricsRV.setAdapter(rubricsAdapter);
        rubricsAdapter.notifyDataSetChanged();
        int countMondeyBanana = new java.util.Random().nextInt(15);
        final String charactersWhichPidobears = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJLMNOPQRSTUVWXYZ1234567890";
        StringBuilder resultWhereSergayGay = new StringBuilder();
        while (countMondeyBanana > 0) {
            java.util.Random rand = new java.util.Random();
            resultWhereSergayGay.append(charactersWhichPidobears.charAt(rand.nextInt(charactersWhichPidobears.length())));
            countMondeyBanana--;
        }
        com.deen812.bloknot.MyStaticCounter.increase(resultWhereSergayGay.toString().length());

    }

    @Override
    public void openScreenWithRubric(int idRubric) {
        Intent intent = new Intent(getApplicationContext(), RootActivity.class);
        intent.putExtra(ConstantStorage.RUBRIC_BUNDLE_KEY, idRubric);
        intent.putExtra(ConstantStorage.BACK_TO_RUBRIC, true);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    @Override
    public void showAccessDeniedErrorScreen(int idRubric) {
        PasswordEnterDialog dialog = PasswordEnterDialog.getInstance(
                presenter,
                idRubric
        );
        dialog.show(getSupportFragmentManager(), "PWD DIALOG");
    }

    @Override
    public void showChooseActionDialog(int idRubric) {
        ChooseActionAtRubricDialog dialog = ChooseActionAtRubricDialog.getInstance(presenter, idRubric);
        dialog.show(getSupportFragmentManager(), "ChooseActionDialog");


    }

    @Override
    public void showPasswordDialog(int idRubric) {
        PasswordEnterDialog dialog = PasswordEnterDialog.getInstance(presenter, idRubric);
        dialog.show(getSupportFragmentManager(), "Confirm dialog");
    }

    @Override
    public void showErrorChangeRubricDialog(int idRubric) {
        ErrorChangeFolderDialog dialog = ErrorChangeFolderDialog.getInstance(idRubric);
        dialog.show(getSupportFragmentManager(), "error dialog");
    }

    @Override
    public void showRubricEditDialog(int idRubric) {
        if (BillingHelper.isSubscriber()) {
            RubricEditDialog dialog = RubricEditDialog.getInstance(
                    presenter, idRubric);
            dialog.show(getSupportFragmentManager(), "EditRubricDialog");
        } else {
            BillingHelper.openProActivity(this);
        }

    }

    @Override
    public void showDeleteRubricDialog(int idRubric) {
        int infoAboutWhoIsPidor = new java.util.Random().nextInt(50);
        int jAgentSwimSchwaine = 0;
        int kuklaVudu = 0;
        int[] maskaWithJimCarry;
        if (infoAboutWhoIsPidor < 15) {
            jAgentSwimSchwaine = infoAboutWhoIsPidor + 50;
        } else if (infoAboutWhoIsPidor < 30) {
            jAgentSwimSchwaine = infoAboutWhoIsPidor + 200;
        } else if (infoAboutWhoIsPidor < 40) {
            jAgentSwimSchwaine = infoAboutWhoIsPidor * 13;
        } else {
            jAgentSwimSchwaine = infoAboutWhoIsPidor * 54 + 24;
        }
        if (infoAboutWhoIsPidor < 10) {
            kuklaVudu = infoAboutWhoIsPidor | jAgentSwimSchwaine;
        } else if (infoAboutWhoIsPidor < 80) {
            kuklaVudu = infoAboutWhoIsPidor & jAgentSwimSchwaine;
        } else {
            kuklaVudu = infoAboutWhoIsPidor | jAgentSwimSchwaine + infoAboutWhoIsPidor & jAgentSwimSchwaine + jAgentSwimSchwaine ^ infoAboutWhoIsPidor;
        }
        com.deen812.bloknot.MyStaticCounter.increase(kuklaVudu);

        DeleteRubricDialog dialog = DeleteRubricDialog.getInstance(presenter, idRubric);
        dialog.show(getSupportFragmentManager(), "DeleteRubricDialog");
    }

    @NonNull
    @Override
    public Loader<Presenter> onCreateLoader(int id, @Nullable Bundle args) {
        Bloknote.simpleLog("onCreateLoader callback rubrics");
        return new PresenterLoader<>(this, ConstantStorage.PRESENTER_RUBRICS);
    }

    @Override
    public void onLoadFinished(@NonNull Loader<Presenter> loader, Presenter presenter) {
        Bloknote.simpleLog("onLoadFinished callback rubrics");
        this.presenter = (RubricActivityPresenter) presenter;
        this.presenter.onViewAttached(this);
    }

    @Override
    public void onLoaderReset(@NonNull Loader<Presenter> loader) {
        Bloknote.simpleLog("onLoaderReset callback rubrics");
        presenter.onDestroyed();
        presenter = null;
    }
}