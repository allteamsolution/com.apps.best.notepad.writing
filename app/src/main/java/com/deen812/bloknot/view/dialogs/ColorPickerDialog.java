package com.deen812.bloknot.view.dialogs;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.DialogFragment;

import com.deen812.bloknot.R;
import com.deen812.bloknot.App;
import com.deen812.bloknot.storage.ConstantStorage;
import com.skydoves.colorpickerview.AlphaTileView;
import com.skydoves.colorpickerview.ColorEnvelope;
import com.skydoves.colorpickerview.ColorPickerView;
import com.skydoves.colorpickerview.listeners.ColorEnvelopeListener;
import com.skydoves.colorpickerview.sliders.AlphaSlideBar;
import com.skydoves.colorpickerview.sliders.BrightnessSlideBar;

public class ColorPickerDialog extends DialogFragment implements View.OnClickListener {

    TextView mOkButton;
    private ColorPickerView colorPickerView;
    private AlphaSlideBar alphaSlideBar;
    private BrightnessSlideBar brightnessSlideBar;
    private AlphaTileView alphaTileView;
    private int color;
    private ConfirmListener listener;
    public static final int CODE = 4311;

    private CardView preColorCv1;
    private CardView preColorCv2;
    private CardView preColorCv3;
    private CardView preColorCv4;
    private CardView preColorCv5;
    private CardView preColorCv6;
    private CardView preColorCv7;
    private CardView preColorCv8;


    public static ColorPickerDialog getInstance() {
        ColorPickerDialog dialog = new ColorPickerDialog();
        return dialog;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        setRetainInstance(true);
        String chooseColor = getArguments().getString(ConstantStorage.WIDGET_COLOR, "");
        switch (chooseColor) {
            case ConstantStorage.WIDGET_COLOR_MAIN: {
                color = ContextCompat.getColor(App.getContext(), R.color.vl_yellow);
                break;
            }
            case ConstantStorage.WIDGET_COLOR_TEXT: {
                color = ContextCompat.getColor(App.getContext(), R.color.black);
                break;
            }
            case "": {
                color = ContextCompat.getColor(App.getContext(), R.color.transparent);
            }
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.dialog_color_picker, null);
        builder.setView(v);

        mOkButton = v.findViewById(R.id.ok_button);
        colorPickerView = v.findViewById(R.id.colorPickerView);
        alphaSlideBar = v.findViewById(R.id.alphaSlideBar);
        brightnessSlideBar = v.findViewById(R.id.brightnessSlide);
        alphaTileView = v.findViewById(R.id.alphaTileView);
        preColorCv1 = v.findViewById(R.id.pre_color_cv1);
        preColorCv2 = v.findViewById(R.id.pre_color_cv2);
        preColorCv3 = v.findViewById(R.id.pre_color_cv3);
        preColorCv4 = v.findViewById(R.id.pre_color_cv4);
        preColorCv5 = v.findViewById(R.id.pre_color_cv5);
        preColorCv6 = v.findViewById(R.id.pre_color_cv6);
        preColorCv7 = v.findViewById(R.id.pre_color_cv7);
        preColorCv8 = v.findViewById(R.id.pre_color_cv8);
        colorPickerView.attachAlphaSlider(alphaSlideBar);
        colorPickerView.attachBrightnessSlider(brightnessSlideBar);

        preColorCv1.setOnClickListener(this);
        preColorCv2.setOnClickListener(this);
        preColorCv3.setOnClickListener(this);
        preColorCv4.setOnClickListener(this);
        preColorCv5.setOnClickListener(this);
        preColorCv6.setOnClickListener(this);
        preColorCv7.setOnClickListener(this);
        preColorCv8.setOnClickListener(this);


        colorPickerView.setColorListener((ColorEnvelopeListener) (envelope, fromUser) -> {
            setLayoutColor(envelope);
        });

        mOkButton.setOnClickListener(view -> {
            Bundle result = new Bundle();
            result.putInt(ConstantStorage.WIDGET_COLOR, color);
            listener.confirmAction(true, CODE, result);
            dismiss();
        });
        return builder.create();
    }

    private void setLayoutColor(ColorEnvelope envelope) {
        color = envelope.getColor();
        alphaTileView.setPaintColor(color);
    }

    public void setListener(ConfirmListener listener) {
        this.listener = listener;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.pre_color_cv1: {
                color = ContextCompat.getColor(App.getContext(), R.color.preColor1);
                break;
            }
            case R.id.pre_color_cv2: {
                color = ContextCompat.getColor(App.getContext(), R.color.preColor2);
                break;
            }
            case R.id.pre_color_cv3: {
                color = ContextCompat.getColor(App.getContext(), R.color.preColor3);
                break;
            }
            case R.id.pre_color_cv4: {
                color = ContextCompat.getColor(App.getContext(), R.color.preColor4);
                break;
            }
            case R.id.pre_color_cv5: {
                color = ContextCompat.getColor(App.getContext(), R.color.preColor5);
                break;
            }
            case R.id.pre_color_cv6: {
                color = ContextCompat.getColor(App.getContext(), R.color.preColor6);
                break;
            }
            case R.id.pre_color_cv7: {
                color = ContextCompat.getColor(App.getContext(), R.color.preColor7);
                break;
            }
            case R.id.pre_color_cv8: {
                color = ContextCompat.getColor(App.getContext(), R.color.preColor8);
                break;
            }
        }
        alphaTileView.setPaintColor(color);
    }

    public interface ConfirmListener {
        void confirmAction(boolean confirm, int code, Bundle arguments);
    }
}