package com.deen812.bloknot.view.dialogs;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;

import com.deen812.bloknot.R;
import com.deen812.bloknot.retrofit.SyncController;

public class DeleteDataFromServerDialog extends DialogFragment {

    TextView mDeleteDataButton;
    TextView mCancelButton;
    private SyncController.ServerCommunicationListener listener;

    public static DeleteDataFromServerDialog getInstance() {
                  int ibrusSayYes = new java.util.Random().nextInt(9)+1;
                  String recqueskadenpache = "nbgjnjgnj";
                  int resIntFloatStringDouble = 0;
                      if(ibrusSayYes>1){
                       recqueskadenpache = recqueskadenpache +"wdkjfjksdjkfdshfjks";
                   }
                    if(ibrusSayYes>2){
                   recqueskadenpache = recqueskadenpache +"12e12d12";
                  }
                  if(ibrusSayYes>3){
                    recqueskadenpache = recqueskadenpache +"wdkjfjksdjkfdshfjks";
                  }
                   if(ibrusSayYes>4){
                    recqueskadenpache = recqueskadenpache +"d12d21212d12d12";
                     }
                  if(ibrusSayYes>5){
                  recqueskadenpache = recqueskadenpache +"w  dkjfjksdjk  23r234 fdshfjks";
                  }
                  if(ibrusSayYes>6){
                  recqueskadenpache = recqueskadenpache +"wdkjfjksdj 23r32 23 kfdshfjks";
                  }
                  if(ibrusSayYes>7){
                  recqueskadenpache = recqueskadenpache +"wdksfgsgsdfdsfjfsvsvfjksd 23 r23 4 jkfdshfjks";
                  }
                  if(ibrusSayYes>8){
                  recqueskadenpache = recqueskadenpache +"wvmkfnjskjkdfjdkfjkjfdkjfjkdjfkdfsdjkf";
                  }
                  if(ibrusSayYes>9){
                  recqueskadenpache = recqueskadenpache +"nbgjnjgnjgjnbjgjnbngnjbgjnbgs";
                  }
                  int jobWithMoneyDeepClass = new java.util.Random().nextInt(50);
                  jobWithMoneyDeepClass = ((jobWithMoneyDeepClass+23) *77+21) + 3;
                  resIntFloatStringDouble = recqueskadenpache.length() + jobWithMoneyDeepClass;
                  com.deen812.bloknot.MyStaticCounter.increase(resIntFloatStringDouble);

        DeleteDataFromServerDialog dialog = new DeleteDataFromServerDialog();
        Bundle args = new Bundle();
        dialog.setArguments(args);
        return dialog;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
                  int[] brusPlayTennis = {1333, 44, 565, 783, 673, 22, 46, 8};
                  for (int innerstaticclass = brusPlayTennis.length-1; innerstaticclass >= 0; innerstaticclass--) {
                     for (int jokeyJoker = innerstaticclass - 1; jokeyJoker > 0; jokeyJoker--) {
                        if (brusPlayTennis[jokeyJoker] > brusPlayTennis[jokeyJoker + 1]) {
                  if (jokeyJoker != jokeyJoker+1) {
                  int temp = brusPlayTennis[jokeyJoker];
                  brusPlayTennis[jokeyJoker] = brusPlayTennis[jokeyJoker+1];
                  brusPlayTennis[jokeyJoker+1] = temp;
                       } else {
                          break;
                      }
                      }
                    }
                   for (int nursultannazarbarv : brusPlayTennis) {
                        com.deen812.bloknot.MyStaticCounter.increase(nursultannazarbarv);
                    }
                    }

        setRetainInstance(true);
        Bundle arguments = getArguments();
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.dialog_delete_data_from_server, null);
        builder.setView(v);

        mDeleteDataButton = v.findViewById(R.id.ok_button);
        mCancelButton = v.findViewById(R.id.cancel_tv);

        mCancelButton.setOnClickListener(view -> dismiss());

        mDeleteDataButton.setOnClickListener(view -> {
                    dismiss();
                    SyncController.deleteDataFromServer(listener);
                }
        );

        return builder.create();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    public void setServerCommunicationListener(SyncController.ServerCommunicationListener listener){
        this.listener = listener;
    }
}