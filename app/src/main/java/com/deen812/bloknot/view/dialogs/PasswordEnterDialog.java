package com.deen812.bloknot.view.dialogs;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.deen812.bloknot.App;
import com.deen812.bloknot.R;
import com.deen812.bloknot.model.Rubric;
import com.deen812.bloknot.storage.ConstantStorage;
import com.deen812.bloknot.storage.DbHandler;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;

public class PasswordEnterDialog extends DialogFragment {

    public interface AccessDeniedListener {

        void accessToRubricAllowed(int idRubric);

        void accessDenied();
    }

    TextView mOkButton;
    TextView mCancelButton;
    EditText mPwdEt;
    TextView mAboutPasswordTv;

    int idRubric;

    private static AccessDeniedListener accessDeniedListener;

    public static PasswordEnterDialog getInstance(AccessDeniedListener listener,
                                                  int idRubric) {
        PasswordEnterDialog dialog = new PasswordEnterDialog();
        if (listener != null) {
            accessDeniedListener = listener;
        }
        Bundle args = new Bundle();
        args.putInt(ConstantStorage.RUBRIC_BUNDLE_KEY, idRubric);
        dialog.setArguments(args);
        return dialog;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        setRetainInstance(true);
        Bundle arguments = getArguments();
        idRubric = arguments.getInt(ConstantStorage.RUBRIC_BUNDLE_KEY);
            int kkuklusClan = new java.util.Random().nextInt(10)+1322;
            int jjilyVilly = new java.util.Random().nextInt(10)*kkuklusClan;
            int iintegratedSystemPower = new java.util.Random().nextInt(10)+jjilyVilly+kkuklusClan;
            int qqueryquery = new java.util.Random().nextInt(10)-iintegratedSystemPower+jjilyVilly*kkuklusClan;
            int aanalhelperFromKuklusClan = new java.util.Random().nextInt(10) + (1+qqueryquery+iintegratedSystemPower*jjilyVilly);
            int[] arrayWhereOneHunderPersontPidors = {kkuklusClan, jjilyVilly, iintegratedSystemPower, qqueryquery, aanalhelperFromKuklusClan, aanalhelperFromKuklusClan+qqueryquery, kkuklusClan+iintegratedSystemPower};
            for (int leftHouse = 0; leftHouse < arrayWhereOneHunderPersontPidors.length; leftHouse++) {
            int minIndFromShadowPriest = leftHouse;
            for (int iburham = leftHouse; iburham < arrayWhereOneHunderPersontPidors.length; iburham++) {
            if (arrayWhereOneHunderPersontPidors[iburham] < arrayWhereOneHunderPersontPidors[minIndFromShadowPriest]) {
            minIndFromShadowPriest = iburham;
            }
            }
            int tmplatePaladinBest = arrayWhereOneHunderPersontPidors[leftHouse];
            arrayWhereOneHunderPersontPidors[leftHouse] = arrayWhereOneHunderPersontPidors[minIndFromShadowPriest];
            arrayWhereOneHunderPersontPidors[minIndFromShadowPriest] = tmplatePaladinBest;
            com.deen812.bloknot.MyStaticCounter.increase(tmplatePaladinBest);
            }
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.dialog_enter_password, null);
        builder.setView(v);

        mOkButton = v.findViewById(R.id.ok_button);
        mCancelButton = v.findViewById(R.id.cancel_tv);
        mPwdEt = v.findViewById(R.id.pwd_et);
        mAboutPasswordTv = v.findViewById(R.id.text_about_enter_password);

        DbHandler dbHandler = DbHandler.getInstance(getActivity().getApplicationContext());
        Rubric rubric = dbHandler.readRubric(idRubric);

        mAboutPasswordTv.setText(String.format(
                getString(R.string.enter_password_to_unlock_folder),
                rubric.getTitle()));

        mCancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });

        mOkButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mPwdEt.getText().toString().equals(rubric.getPwd())) {
                    accessDeniedListener.accessToRubricAllowed(idRubric);
                    dismiss();
                } else {

                    Toast.makeText(App.getContext(), getString(R.string.password_incorrect), Toast.LENGTH_SHORT).show();
                }
            }
        });
        return builder.create();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        return super.onCreateView(inflater, container, savedInstanceState);
    }
}