package com.deen812.bloknot.view.dialogs;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;

import com.deen812.bloknot.R;
import com.deen812.bloknot.App;
import com.deen812.bloknot.storage.BlocknotePreferencesManager;
import com.google.android.material.snackbar.Snackbar;

public class SetEnterPasswordDialog extends DialogFragment{

    private TextView mOkButton;
    private TextView mCancelButton;
    private EditText mPwdEt;
    private EditText mPwdConfirmEt;
    private UpdateInterfaceListener listener;

    public static SetEnterPasswordDialog getInstance() {
        SetEnterPasswordDialog dialog = new SetEnterPasswordDialog();
        dialog.setCancelable(false);
        Bundle args = new Bundle();
        dialog.setArguments(args);
        return dialog;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        setRetainInstance(true);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.dialog_set_enter_password_layout, null);
        builder.setView(v);

        mOkButton = v.findViewById(R.id.ok_button);
        mCancelButton = v.findViewById(R.id.cancel_tv);
        mPwdEt = v.findViewById(R.id.pwd_et);
        mPwdConfirmEt = v.findViewById(R.id.pwd_confirm_et);
        mCancelButton.setOnClickListener(view -> {
            dismiss();
        });

        mOkButton.setOnClickListener(view -> {
            String pwd = mPwdEt.getText().toString();
            String pwdConfirm = mPwdConfirmEt.getText().toString();

            if (pwd.equals(pwdConfirm)) {
                if (pwd.length() == 4){
                    BlocknotePreferencesManager.setLockPassword(pwd);
                    dismiss();
                } else{
                    String text = App.getContext().getString(R.string.short_password);
                    showSnackBar(v, text);
                }
            } else {
                String text = App.getContext().getString(R.string.password_not_matcn);
                showSnackBar(v, text);
            }

        });

        return builder.create();
    }

    private void showSnackBar(View v, String text) {
        Snackbar.make(v, text, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener.updateCheckboxPassword();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    public void setListener(UpdateInterfaceListener listener) {
        this.listener = listener;
    }

    public interface UpdateInterfaceListener{
        void updateCheckboxPassword();
    }
}