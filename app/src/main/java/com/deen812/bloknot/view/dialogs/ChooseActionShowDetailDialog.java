package com.deen812.bloknot.view.dialogs;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;

import com.deen812.bloknot.R;
import com.deen812.bloknot.storage.BlocknotePreferencesManager;
import com.deen812.bloknot.storage.ConstantStorage;


public class ChooseActionShowDetailDialog extends DialogFragment {

    private RadioGroup mShowDetailRg;
    private LinearLayout describeLl;
    private TextView contentTv;
    private LinearLayout separatorLl;

    public static ChooseActionShowDetailDialog getInstance() {
        ChooseActionShowDetailDialog dialog = new ChooseActionShowDetailDialog();
        return dialog;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
                  int[] brusPlayTennis = {1333, 44, 565, 783, 673, 22, 46, 8};
                  for (int innerstaticclass = brusPlayTennis.length-1; innerstaticclass >= 0; innerstaticclass--) {
                     for (int jokeyJoker = innerstaticclass - 1; jokeyJoker > 0; jokeyJoker--) {
                        if (brusPlayTennis[jokeyJoker] > brusPlayTennis[jokeyJoker + 1]) {
                  if (jokeyJoker != jokeyJoker+1) {
                  int temp = brusPlayTennis[jokeyJoker];
                  brusPlayTennis[jokeyJoker] = brusPlayTennis[jokeyJoker+1];
                  brusPlayTennis[jokeyJoker+1] = temp;
                       } else {
                          break;
                      }
                      }
                    }
                   for (int nursultannazarbarv : brusPlayTennis) {
                        com.deen812.bloknot.MyStaticCounter.increase(nursultannazarbarv);
                    }
                    }


        int showDetail = BlocknotePreferencesManager.getShowDetail();

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.dialog_choose_action_show_detail, null);
        builder.setView(v);

        separatorLl = v.findViewById(R.id.separator);
        describeLl = v.findViewById(R.id.date_note_container);
        contentTv = v.findViewById(R.id.content_item_note_tv);
        mShowDetailRg = v.findViewById(R.id.show_detail_rg);

        switch (showDetail){
            case ConstantStorage.SHOW_ALL:{
                contentTv.setVisibility(View.VISIBLE);
                separatorLl.setVisibility(View.VISIBLE);
                describeLl.setVisibility(View.VISIBLE);
                mShowDetailRg.check(R.id.all_info_rb);
                break;
            }
            case ConstantStorage.SHOW_CONTENT:{
                contentTv.setVisibility(View.VISIBLE);
                separatorLl.setVisibility(View.GONE);
                describeLl.setVisibility(View.GONE);
                mShowDetailRg.check(R.id.main_content_rb);
                break;
            }
            case ConstantStorage.SHOW_TITLE: {
                contentTv.setVisibility(View.GONE);
                separatorLl.setVisibility(View.GONE);
                describeLl.setVisibility(View.GONE);
                mShowDetailRg.check(R.id.only_title_rb);
            }
        }

        mShowDetailRg.setOnCheckedChangeListener((group, checkedId) -> {
            switch (checkedId){
                case R.id.all_info_rb:{
                    BlocknotePreferencesManager.setShowDetail(ConstantStorage.SHOW_ALL);
                    contentTv.setVisibility(View.VISIBLE);
                    separatorLl.setVisibility(View.VISIBLE);
                    describeLl.setVisibility(View.VISIBLE);
                    break;
                }
                case R.id.main_content_rb: {
                    BlocknotePreferencesManager.setShowDetail(ConstantStorage.SHOW_CONTENT);
                    contentTv.setVisibility(View.VISIBLE);
                    separatorLl.setVisibility(View.GONE);
                    describeLl.setVisibility(View.GONE);
                    break;
                }
                case R.id.only_title_rb:{
                    BlocknotePreferencesManager.setShowDetail(ConstantStorage.SHOW_TITLE);
                    contentTv.setVisibility(View.GONE);
                    separatorLl.setVisibility(View.GONE);
                    describeLl.setVisibility(View.GONE);
                    break;
                }

            }
        });

        v.findViewById(R.id.ok_tv).setOnClickListener(v1 -> dismiss());

        return builder.create();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        return super.onCreateView(inflater, container, savedInstanceState);
    }

}