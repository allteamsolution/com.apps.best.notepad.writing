package com.deen812.bloknot.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.deen812.bloknot.R;
import com.deen812.bloknot.App;
import com.deen812.bloknot.billing.Encryption;
import com.deen812.bloknot.storage.BlocknotePreferencesManager;
import com.deen812.bloknot.storage.ConstantStorage;
import com.deen812.bloknot.utils.Bloknote;
import com.squareup.picasso.Picasso;



import java.util.Iterator;

public class BillingActivity extends CustomizationThemeActivity
        implements View.OnClickListener {
    @Override
    public void onClick(View v) {

    }

//    RadioGroup radioGroup;
//    TextView buyTextView;
//    ImageView backIv;
//    String coast = "empty";
//    int imageResource;
//
//    private class PurchaseListener extends EmptyRequestListener<Purchase> {
//        @Override
//        public void onSuccess(Purchase purchase) {
////            Bloknote.simpleLog("BillingActivity onSuccess");
////            String data = purchase.data;
////            String payload = purchase.payload;
////            String signature = purchase.signature;
////            String sku = purchase.sku;
////            Bloknote.simpleLog(
////                    "data: " + data + " "
////                            + "payload: " + payload + " "
////                            + "signature: " + signature + " "
////                            + "sku: " + sku
////                    + "toJson: " + purchase.toJson()
////            );
//            BlocknotePreferencesManager.setPro(purchase.sku);
//            // here you can process the loaded purchase
//        }
//
//        @Override
//        public void onError(int response, Exception e) {
//            Bloknote.simpleLog("BillingActivity onError");
//            Bloknote.simpleLog(e.getMessage());
//            // handle errors here
//        }
//    }
//
//    private class InventoryCallback implements Inventory.Callback {
//        @Override
//        public void onLoaded(Inventory.Products products) {
//            Bloknote.simpleLog("BillingActivity onLoaded");
//            if (BlocknotePreferencesManager.getPro() == null) {
//                if (products != null) {
//                    Bloknote.simpleLog(products.toString());
//                    for (Inventory.Product p : products) {
//                        for (Purchase purchase : p.getPurchases()) {
//                            BlocknotePreferencesManager.setPro(purchase.sku);
//                            finish();
//                        }
//                    }
//                }
//            }
//            // your code here
//        }
//    }
//
//   // private final ActivityCheckout mCheckout = Checkout.forActivity(this, App.get().getBilling());
//   // private Inventory mInventory;
//
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_billing);
//
//        radioGroup = findViewById(R.id.billing_rg);
//        buyTextView = findViewById(R.id.buy_tv);
//        backIv = findViewById(R.id.back_iv);
//
//        backIv.setOnClickListener(view -> finish());
//        buyTextView.setOnClickListener(this);
//        radioGroup.setOnCheckedChangeListener((group, checkedId) -> {
//            switch (checkedId) {
//                case R.id.one_dollar_rg: {
//                    coast = ConstantStorage.ONE_DOLLAR;
//                    imageResource = R.drawable.buy_1_5;
//                    break;
//                }
//                case R.id.three_dollar_rg: {
//                    coast = ConstantStorage.THREE_DOLLARS;
//                    imageResource = R.drawable.buy_3;
//                    break;
//                }
//                case R.id.five_dollar_rg: {
//                    coast = ConstantStorage.FIVE_DOLLARS;
//                    imageResource = R.drawable.buy_5;
//                    break;
//                }
//                case R.id.ten_dollar_rg: {
//                    coast = ConstantStorage.TEN_DOLLARS;
//                    imageResource = R.drawable.buy_10;
//                    break;
//                }
//                case R.id.fifteen_dollar_rg: {
//                    coast = ConstantStorage.FIFTEEN_DOLLARS;
//                    imageResource = R.drawable.buy_15;
//                    break;
//                }
//                case R.id.thirty_dollar_rg: {
//                    coast = ConstantStorage.THIRTY_DOLLARS;
//                    imageResource = R.drawable.buy_30;
//                    break;
//                }
//                case R.id.fifty_dollar_rg: {
//                    coast = ConstantStorage.FIFTY_DOLLARS;
//                    imageResource = R.drawable.buy_50;
//                    break;
//                }
//                case R.id.hundred_dollar_rg: {
//                    coast = ConstantStorage.HUNDRED_DOLLARS;
//                    imageResource = R.drawable.buy_100;
//                    break;
//                }
//            }
//
//
//            buyTextView.setVisibility(View.VISIBLE);
//        });
//
////        mCheckout.start();
////
////        mCheckout.createPurchaseFlow(new PurchaseListener());
//
//        //mInventory = mCheckout.makeInventory();
//        InventoryCallback callback = new InventoryCallback() {
//            @Override
//            public void onLoaded(Inventory.Products products) {
//                super.onLoaded(products);
//                Iterator<Inventory.Product> iterator = products.iterator();
//                while (iterator.hasNext()) {
//                    Sku unlock_99 = iterator.next().getSku(coast);
//                    if (unlock_99 != null) {
//                        Bloknote.simpleLog(unlock_99.toString());
//                    }
//                }
//            }
//        };
////        mInventory.load(Inventory.Request.create()
////                .loadAllPurchases()
////                .loadSkus(ProductTypes.IN_APP, coast), callback);
//
//    }
//
//    @Override
//    protected void onDestroy() {
//       // mCheckout.stop();
//        super.onDestroy();
//    }
//
//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//        //mCheckout.onActivityResult(requestCode, resultCode, data);
//    }
//
//    @Override
//    public void onClick(View v) {
//
////        mCheckout.whenReady(new Checkout.EmptyListener() {
////            @Override
////            public void onReady(BillingRequests requests) {
////                Bloknote.simpleLog(requests.toString());
////                requests.purchase(ProductTypes.IN_APP, coast, null, mCheckout.getPurchaseFlow());
////            }
////        });
//    }
}