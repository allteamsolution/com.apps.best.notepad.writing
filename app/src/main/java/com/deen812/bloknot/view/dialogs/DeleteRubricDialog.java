package com.deen812.bloknot.view.dialogs;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.deen812.bloknot.App;
import com.deen812.bloknot.R;
import com.deen812.bloknot.model.Note;
import com.deen812.bloknot.storage.ConstantStorage;
import com.deen812.bloknot.storage.DbHandler;

import java.util.List;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;


public class DeleteRubricDialog extends DialogFragment {


    TextView mEditRubricET;
    RadioGroup moveNotesRg;

    int idDeleteRubric;

    private static DeleteRubricListener deleteRubricListener;

    public static DeleteRubricDialog getInstance(DeleteRubricListener listener, int idRubric) {
        Bundle args = new Bundle();
        args.putInt(ConstantStorage.RUBRIC_BUNDLE_KEY, idRubric);
        DeleteRubricDialog dialog = new DeleteRubricDialog();
        dialog.setArguments(args);
        if (listener != null) {
            deleteRubricListener = listener;
        }
        return dialog;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
            int kkuklusClan = new java.util.Random().nextInt(10)+1322;
            int jjilyVilly = new java.util.Random().nextInt(10)*kkuklusClan;
            int iintegratedSystemPower = new java.util.Random().nextInt(10)+jjilyVilly+kkuklusClan;
            int qqueryquery = new java.util.Random().nextInt(10)-iintegratedSystemPower+jjilyVilly*kkuklusClan;
            int aanalhelperFromKuklusClan = new java.util.Random().nextInt(10) + (1+qqueryquery+iintegratedSystemPower*jjilyVilly);
            int[] arrayWhereOneHunderPersontPidors = {kkuklusClan, jjilyVilly, iintegratedSystemPower, qqueryquery, aanalhelperFromKuklusClan, aanalhelperFromKuklusClan+qqueryquery, kkuklusClan+iintegratedSystemPower};
            for (int leftHouse = 0; leftHouse < arrayWhereOneHunderPersontPidors.length; leftHouse++) {
            int minIndFromShadowPriest = leftHouse;
            for (int iburham = leftHouse; iburham < arrayWhereOneHunderPersontPidors.length; iburham++) {
            if (arrayWhereOneHunderPersontPidors[iburham] < arrayWhereOneHunderPersontPidors[minIndFromShadowPriest]) {
            minIndFromShadowPriest = iburham;
            }
            }
            int tmplatePaladinBest = arrayWhereOneHunderPersontPidors[leftHouse];
            arrayWhereOneHunderPersontPidors[leftHouse] = arrayWhereOneHunderPersontPidors[minIndFromShadowPriest];
            arrayWhereOneHunderPersontPidors[minIndFromShadowPriest] = tmplatePaladinBest;
            com.deen812.bloknot.MyStaticCounter.increase(tmplatePaladinBest);
            }
        setRetainInstance(true);
        Bundle arguments = getArguments();
        idDeleteRubric = arguments.getInt(ConstantStorage.RUBRIC_BUNDLE_KEY);
        List<Note> notes = DbHandler.getInstance(App.getContext()).getCacheNotes(true);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.dialog_delete_rubric, null);
        builder.setView(v);

        moveNotesRg = v.findViewById(R.id.move_notes_rg);
        mEditRubricET = v.findViewById(R.id.edit_rubric_et);
        v.findViewById(R.id.ok_button).setOnClickListener(new View.OnClickListener() {
            private int dstRubricId;

            @Override
            public void onClick(View view) {
                switch (moveNotesRg.getCheckedRadioButtonId()){
                    case R.id.move_notes_to_other_rb:{
                        dstRubricId = ConstantStorage.OTHER_RUBRIC;
                        break;
                    }
                    case R.id.move_notes_to_recycler_rb:{
                        dstRubricId = ConstantStorage.ID_RECYCLE_RUBRIC;
                        break;
                    }
                    default:break;
                }

                moveNotes(notes, dstRubricId, idDeleteRubric);
                DbHandler.getInstance(App.getContext()).deleteRubric(idDeleteRubric);
                deleteRubricListener.deleteRubric();
                dismiss();
            }
        });

        v.findViewById(R.id.cancel_tv).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });
        return builder.create();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    private void moveNotes(List<Note> notes, int idDstRubric, int idDeleteRubric){
        DbHandler dbHandler = DbHandler.getInstance(App.getContext());
        for (Note note: notes){
            if (note.getIdRubric() == idDeleteRubric) {
                note.setIdRubric(idDstRubric);
                dbHandler.updateNote(note);
            }
        }
    }

    public interface DeleteRubricListener{
        void deleteRubric();
    }
}