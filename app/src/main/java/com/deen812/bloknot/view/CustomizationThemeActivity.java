package com.deen812.bloknot.view;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;

import com.deen812.bloknot.App;
import com.deen812.bloknot.R;
import com.deen812.bloknot.Utils;
import com.deen812.bloknot.storage.BlocknotePreferencesManager;
import com.deen812.bloknot.storage.ConstantStorage;

public class CustomizationThemeActivity extends AppCompatActivity {

    public int theme;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        theme = BlocknotePreferencesManager.getTheme();
        switch (theme) {
            case ConstantStorage.DEFAULT_THEME: {
                setTheme(R.style.DefaultTheme);
                break;
            }
            case ConstantStorage.NIGHT_THEME: {
                setTheme(R.style.ThemeNight);
                break;
            }
            case ConstantStorage.GREEN_THEME: {
                setTheme(R.style.ThemeGreen);
                break;
            }
            case ConstantStorage.BLUE_THEME: {
                setTheme(R.style.ThemeBlue);
                break;
            }
            case ConstantStorage.CONSOLE_THEME: {
                setTheme(R.style.ThemeConsole);
                break;
            }
            case ConstantStorage.YURIY_THEME: {
                setTheme(R.style.ThemeYuriy);
                break;
            }
        }
        Utils.Theme.setAttributes(this);
        super.onCreate(savedInstanceState);
    }

    public void sendErrorMessage(Intent emailIntent) {
        startActivity(Intent.createChooser(emailIntent, "Send email"));
    }
}