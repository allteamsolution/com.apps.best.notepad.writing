package com.deen812.bloknot.view;

import com.deen812.bloknot.model.Alarm;

import java.util.LinkedList;
import java.util.List;

public interface ViewCalendar {
    void showCalendar();
    void showEvents(LinkedList<Alarm> events);
    void openAddEventWindow(int idNote);

    void setTextSelectedDay(String s);
}