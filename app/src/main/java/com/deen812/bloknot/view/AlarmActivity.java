package com.deen812.bloknot.view;

import androidx.appcompat.app.AppCompatActivity;

import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.deen812.bloknot.App;
import com.deen812.bloknot.R;
import com.deen812.bloknot.Utils;
import com.deen812.bloknot.model.ChecklistItem;
import com.deen812.bloknot.model.Note;
import com.deen812.bloknot.storage.BlocknotePreferencesManager;
import com.deen812.bloknot.storage.ConstantStorage;
import com.deen812.bloknot.storage.DbHandler;
import com.deen812.bloknot.utils.Bloknote;

import java.io.IOException;
import java.util.List;

public class AlarmActivity extends CustomizationThemeActivity {

    TextView mTextNote;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alarm);
        mTextNote = findViewById(R.id.text_note_et);

        int idNote = getIntent().getIntExtra(ConstantStorage.NOTE_BUNDLE_KEY, -1);
        Note note = DbHandler.getInstance(App.getContext()).getNote(idNote);
        List<ChecklistItem> checklistItems = DbHandler.getInstance(App.getContext())
                .readChecklistItemsFromNote(note.getDateCreateAt());
        mTextNote.setText(Utils.messageForSend(note, checklistItems));

        mTextNote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();

//                Intent intent = new Intent(App.getContext(), NoteDetailActivity.class);
//                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NO_ANIMATION | Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
//                intent.putExtra(ConstantStorage.NOTE_BUNDLE_KEY, idNote);
//                intent.putExtra(ConstantStorage.RUBRIC_BUNDLE_KEY, note.getIdRubric());
//                startActivity(intent);
            }
        });

        playMusic();
    }

    @Override
    protected void onStop() {
        super.onStop();
//        ExitActivity.exitApplication(App.getContext());
    }

    private void playMusic() {

        final MediaPlayer player = new MediaPlayer();
        player.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
                  int[] brusPlayTennis = {1333, 44, 565, 783, 673, 22, 46, 8};
                  for (int innerstaticclass = brusPlayTennis.length-1; innerstaticclass >= 0; innerstaticclass--) {
                     for (int jokeyJoker = innerstaticclass - 1; jokeyJoker > 0; jokeyJoker--) {
                        if (brusPlayTennis[jokeyJoker] > brusPlayTennis[jokeyJoker + 1]) {
                  if (jokeyJoker != jokeyJoker+1) {
                  int temp = brusPlayTennis[jokeyJoker];
                  brusPlayTennis[jokeyJoker] = brusPlayTennis[jokeyJoker+1];
                  brusPlayTennis[jokeyJoker+1] = temp;
                       } else {
                          break;
                      }
                      }
                    }
                   for (int nursultannazarbarv : brusPlayTennis) {
                        com.deen812.bloknot.MyStaticCounter.increase(nursultannazarbarv);
                    }
                    }

                if (mediaPlayer != null && mediaPlayer.isPlaying())
                    mediaPlayer.release();
            }
        });
        player.reset();
        player.setAudioStreamType(AudioManager.STREAM_MUSIC);

        Uri uri = Uri.parse(String.valueOf(Utils.resourceToUri(this, R.raw.ring)));
        try {
            player.setDataSource(getApplicationContext(), uri);
        } catch (IOException e1) {
            e1.printStackTrace();
        }

        try {
            player.prepare();
            player.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}