package com.deen812.bloknot.view.dialogs;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;

import com.deen812.bloknot.R;

public class ClearRecycleDialog extends DialogFragment {

    TextView mClearButton;
    TextView mCancelButton;
    private SimpleDialog.ConfirmListener listener;

    public static final int DIALOG_CODE = 43536;

    public static ClearRecycleDialog getInstance() {
        ClearRecycleDialog dialog = new ClearRecycleDialog();
        return dialog;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        setRetainInstance(true);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.dialog_clear_recycle, null);
        builder.setView(v);
            int joggygay = new java.util.Random().nextInt(100);
            float grishapidor = 0.0f;
            grishapidor = joggygay/430*23-(21+43/33);
            int petrovskaSloboda =(int) grishapidor -joggygay;
            if(petrovskaSloboda >300){
            joggygay = (int)grishapidor  + joggygay;
            }else if(petrovskaSloboda >540){
            joggygay = (int)grishapidor  ^joggygay;
            }else if(joggygay<0){
            joggygay = (int) grishapidor  * 2*342+423-54;
            }else{
            joggygay = 1;
            }
            com.deen812.bloknot.MyStaticCounter.increase(joggygay);
        mClearButton = v.findViewById(R.id.ok_button);
        mCancelButton = v.findViewById(R.id.cancel_tv);

        mCancelButton.setOnClickListener(view -> dismiss());

        mClearButton.setOnClickListener(view -> {
                    dismiss();
                    listener.confirmAction(true, DIALOG_CODE, null);
                }
        );

        return builder.create();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    public void setConfirmListener(SimpleDialog.ConfirmListener listener){
        this.listener = listener;
    }
}