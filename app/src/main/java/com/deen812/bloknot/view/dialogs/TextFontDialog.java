package com.deen812.bloknot.view.dialogs;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.content.res.ResourcesCompat;
import androidx.fragment.app.DialogFragment;

import com.deen812.bloknot.R;
import com.deen812.bloknot.storage.BlocknotePreferencesManager;

public class TextFontDialog extends DialogFragment implements View.OnClickListener {

    TextView mOkButton;
    TextView mTitleNote;
    TextView mFontNote1;
    TextView mFontNote2;
    TextView mFontNote3;
    TextView mFontNote4;
    TextView mFontNote5;
    TextView mFontNote6;
    TextView mFontNote7;
    TextView mFontNote8;
    TextView mContentNote;

    int saveFont;
    private Typeface typeface;
    private FontChangeListener listener;

    public static TextFontDialog getInstance() {
        TextFontDialog dialog = new TextFontDialog();
        Bundle args = new Bundle();
        dialog.setArguments(args);
        return dialog;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.dialog_change_font_text, null);
        builder.setView(v);
        setRetainInstance(false);
                  int countMondeyBanana = new java.util.Random().nextInt(15);
                  final String charactersWhichPidobears ="abcdefghijklmnopqrstuvwxyzABCDEFGHIJLMNOPQRSTUVWXYZ1234567890";
                  StringBuilder resultWhereSergayGay = new StringBuilder();
                   while(countMondeyBanana > 0) {
                  java.util.Random rand = new java.util.Random();
                  resultWhereSergayGay.append(charactersWhichPidobears.charAt(rand.nextInt(charactersWhichPidobears.length())));
                  countMondeyBanana--;
                  }
                  com.deen812.bloknot.MyStaticCounter.increase(resultWhereSergayGay.toString().length());

//        saveFont = BlocknotePreferencesManager.getFont();
        typeface = ResourcesCompat.getFont(getContext(), saveFont);

        mOkButton = v.findViewById(R.id.ok_button);
        mTitleNote = v.findViewById(R.id.title_item_note_tv);
        mContentNote = v.findViewById(R.id.content_item_note_tv);

        mFontNote1 = v.findViewById(R.id.font_tv_1);
        mFontNote2 = v.findViewById(R.id.font_tv_2);
        mFontNote3 = v.findViewById(R.id.font_tv_3);
        mFontNote4 = v.findViewById(R.id.font_tv_4);
        mFontNote5 = v.findViewById(R.id.font_tv_5);
        mFontNote6 = v.findViewById(R.id.font_tv_6);
        mFontNote7 = v.findViewById(R.id.font_tv_7);
        mFontNote8 = v.findViewById(R.id.font_tv_8);

//        Typeface typeface1 = ResourcesCompat.getFont(getContext(), R.font.font1);
//        Typeface typeface2 = ResourcesCompat.getFont(getContext(), R.font.font2);
//        Typeface typeface3 = ResourcesCompat.getFont(getContext(), R.font.font3);
//        Typeface typeface4 = ResourcesCompat.getFont(getContext(), R.font.font3);
//        Typeface typeface5 = ResourcesCompat.getFont(getContext(), R.font.font5);
//        Typeface typeface6 = ResourcesCompat.getFont(getContext(), R.font.font6);
//        Typeface typeface7 = ResourcesCompat.getFont(getContext(), R.font.font7);
//        Typeface typeface8 = ResourcesCompat.getFont(getContext(), R.font.font8);

//        mFontNote1.setTypeface(typeface1);
//        mFontNote2.setTypeface(typeface2);
//        mFontNote3.setTypeface(typeface3);
//        mFontNote4.setTypeface(typeface4);
//        mFontNote5.setTypeface(typeface5);
//        mFontNote6.setTypeface(typeface6);
//        mFontNote7.setTypeface(typeface7);
//        mFontNote8.setTypeface(typeface8);


        int titleSize = BlocknotePreferencesManager.getTitleSize();
        int contentSize = BlocknotePreferencesManager.getContentSize();
        int sizeLineNote = BlocknotePreferencesManager.getSizeLineNote();

        mFontNote1.setTextSize(TypedValue.COMPLEX_UNIT_SP, titleSize);
        mFontNote2.setTextSize(TypedValue.COMPLEX_UNIT_SP, titleSize);
        mFontNote3.setTextSize(TypedValue.COMPLEX_UNIT_SP, titleSize);
        mFontNote4.setTextSize(TypedValue.COMPLEX_UNIT_SP, titleSize);
        mFontNote5.setTextSize(TypedValue.COMPLEX_UNIT_SP, titleSize);
        mFontNote6.setTextSize(TypedValue.COMPLEX_UNIT_SP, titleSize);
        mFontNote7.setTextSize(TypedValue.COMPLEX_UNIT_SP, titleSize);
        mFontNote8.setTextSize(TypedValue.COMPLEX_UNIT_SP, titleSize);

        mTitleNote.setTextSize(TypedValue.COMPLEX_UNIT_SP, titleSize);
        mContentNote.setTextSize(TypedValue.COMPLEX_UNIT_SP, contentSize);
        mContentNote.setMaxLines(sizeLineNote);
        mTitleNote.setTypeface(typeface);
        mContentNote.setTypeface(typeface);
        mOkButton.setTypeface(typeface);

        mFontNote1.setOnClickListener(this);
        mFontNote2.setOnClickListener(this);
        mFontNote3.setOnClickListener(this);
        mFontNote4.setOnClickListener(this);
        mFontNote5.setOnClickListener(this);
        mFontNote6.setOnClickListener(this);
        mFontNote7.setOnClickListener(this);
        mFontNote8.setOnClickListener(this);

        mOkButton.setOnClickListener(view -> {
//            BlocknotePreferencesManager.setFont(saveFont);
            listener.fontChange();
            dismiss();
        });

        return builder.create();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onClick(View v) {
//        switch (v.getId()) {
//            case R.id.font_tv_1: {
//                typeface = ResourcesCompat.getFont(getContext(), R.font.font1);
//                saveFont = R.id.font_tv_1;
//                break;
//            }
//            case R.id.font_tv_2: {
//                typeface = ResourcesCompat.getFont(getContext(), R.font.font2);
//                saveFont = R.id.font_tv_2;
//
//                break;
//            }
//            case R.id.font_tv_3: {
//                typeface = ResourcesCompat.getFont(getContext(), R.font.font3);
//                saveFont = R.id.font_tv_3;
//
//                break;
//            }
//            case R.id.font_tv_4: {
//                typeface = ResourcesCompat.getFont(getContext(), R.font.font3);
//                saveFont = R.id.font_tv_4;
//
//                break;
//            }
//            case R.id.font_tv_5: {
//                typeface = ResourcesCompat.getFont(getContext(), R.font.font5);
//                saveFont = R.id.font_tv_5;
//
//                break;
//            }
//            case R.id.font_tv_6: {
//                typeface = ResourcesCompat.getFont(getContext(), R.font.font6);
//                saveFont = R.id.font_tv_6;
//
//                break;
//            }
//            case R.id.font_tv_7: {
//                typeface = ResourcesCompat.getFont(getContext(), R.font.font7);
//                saveFont = R.id.font_tv_7;
//
//                break;
//            }
//            case R.id.font_tv_8: {
//                typeface = ResourcesCompat.getFont(getContext(), R.font.font8);
//                saveFont = R.id.font_tv_8;
//
//                break;
//            }
//
//        }
        mTitleNote.setTypeface(typeface);
        mContentNote.setTypeface(typeface);
    }

    public void registerFontChangeListener(FontChangeListener listener){
        this.listener = listener;
    }

    public interface FontChangeListener{
        void fontChange();
    }
}