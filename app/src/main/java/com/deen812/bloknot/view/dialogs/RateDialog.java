package com.deen812.bloknot.view.dialogs;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;

import com.deen812.bloknot.AppUtils;
import com.deen812.bloknot.App;
import com.deen812.bloknot.R;

public class RateDialog extends DialogFragment {

    ImageView ivCancel;
    LinearLayout llRate;
    LinearLayout llLater;
    RatingBar ratingBar;

    public static RateDialog getInstance() {
        RateDialog dialog = new RateDialog();
        return dialog;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        setRetainInstance(true);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.dialog_rate_us, null);
        builder.setView(v);
        ratingBar = v.findViewById(R.id.ratingBar);

        ivCancel = v.findViewById(R.id.ivCancel);
        llRate = v.findViewById(R.id.llRate);
        llLater = v.findViewById(R.id.llLater);

        llRate.setOnClickListener(view -> {

            if (ratingBar.getRating() >= 4.0f) {
                String url = "https://play.google.com/store/apps/details?id=" + App.getContext().getPackageName();
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                getContext().startActivity(i);
            } else {
                Intent intent = new Intent(Intent.ACTION_SENDTO, Uri.parse("mailto:" + AppUtils.ourEmail));
                intent.putExtra(Intent.EXTRA_EMAIL, new String[]{AppUtils.ourEmail});
                intent.putExtra(Intent.EXTRA_TEXT, "report");
                startActivity(Intent.createChooser(intent, "Send Email via"));
            }

        });
        llLater.setOnClickListener(view -> {
            dismiss();
        });

        ivCancel.setOnClickListener(view ->
                dismiss()
        );
        return builder.create();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    public interface ConfirmListener {
        void confirmAction(boolean confirm, int code, Bundle arguments);
    }
}