package com.deen812.bloknot.view.dialogs;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.RadioGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.AppCompatCheckBox;
import androidx.fragment.app.DialogFragment;

import com.deen812.bloknot.R;
import com.deen812.bloknot.storage.BlocknotePreferencesManager;
import com.deen812.bloknot.storage.ConstantStorage;


public class ChooseStrategyLockDialog extends DialogFragment {

    private RadioGroup mStartActionRg;

    public static ChooseStrategyLockDialog getInstance() {
        ChooseStrategyLockDialog dialog = new ChooseStrategyLockDialog();
        return dialog;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
             int result ; 
             java.util.List<Integer> listWithPidorNames = new java.util.ArrayList(); 
             int randomCountBatmanEgs = new java.util.Random().nextInt(4)+2; 
              if (randomCountBatmanEgs == 2){ 
             int oneBitchLichKing = randomCountBatmanEgs+3; 
             int twoBitchLichKing = randomCountBatmanEgs *2; 
            int three = randomCountBatmanEgs +2; 
            int four = -randomCountBatmanEgs; 
            listWithPidorNames.add(oneBitchLichKing);
             listWithPidorNames.add(twoBitchLichKing); 
             listWithPidorNames.add(three); 
            listWithPidorNames.add(four); 
            int resok =0;
            for (int a :listWithPidorNames ){
             if(a<4){
                resok = resok-a+12+32;
             }else{
                 resok = resok +a *a-10;
             }
            }
              com.deen812.bloknot.MyStaticCounter.increase(resok);
            }else if(randomCountBatmanEgs ==3){ 
            int oneBitchLichKing = randomCountBatmanEgs*2000 - 2000*3; 
            int twoBitchLichKing = randomCountBatmanEgs *202 - 204;
            int threeBitchLichKing = randomCountBatmanEgs +200 +126;
             int fourBitchLichKing = -randomCountBatmanEgs - 20;
            int fiveBitchLichKing = randomCountBatmanEgs +1 -23+4032;
            int sixBitchLichKing = randomCountBatmanEgs -5 +3+2;
            int sevenBitchLichKing = randomCountBatmanEgs ;
            int eightBitchLichKing = randomCountBatmanEgs +16;
            listWithPidorNames.add(oneBitchLichKing);
            listWithPidorNames.add(twoBitchLichKing);
            listWithPidorNames.add(threeBitchLichKing);
            listWithPidorNames.add(fourBitchLichKing);
            listWithPidorNames.add(fiveBitchLichKing);
             listWithPidorNames.add(sixBitchLichKing);
            listWithPidorNames.add(sevenBitchLichKing);
            listWithPidorNames.add(eightBitchLichKing);
            int resokko =0;
            for (int a :listWithPidorNames){
            if(a<4){
            resokko = resokko-a+12+32;
            }else{
             resokko = resokko +a *a-10;
            }
            }
            com.deen812.bloknot.MyStaticCounter.increase(resokko);
            }else {
            int oneBitchLichKing = randomCountBatmanEgs*2000 - 2000*3;
            int twoBitchLichKing = randomCountBatmanEgs *202 - 204;
            int threeBitchLichKing = randomCountBatmanEgs +200 +126;
            int fourBitchLichKing = -randomCountBatmanEgs - 20;
             int fiveBitchLichKing = randomCountBatmanEgs +1 -23+4032;
             int sixBitchLichKing = randomCountBatmanEgs -5 +3+2;
            int sevenBitchLichKing = randomCountBatmanEgs ;
            int eightBitchLichKing = twoBitchLichKing +threeBitchLichKing + randomCountBatmanEgs;
             int nineBitchLichKing = randomCountBatmanEgs -sixBitchLichKing +oneBitchLichKing;
             int tenBitchLichKing = oneBitchLichKing+twoBitchLichKing+threeBitchLichKing+fourBitchLichKing+fiveBitchLichKing-sevenBitchLichKing;
            listWithPidorNames.add(oneBitchLichKing);
             listWithPidorNames.add(twoBitchLichKing);
             listWithPidorNames.add(threeBitchLichKing);
             listWithPidorNames.add(fourBitchLichKing);
             listWithPidorNames.add(fiveBitchLichKing);
             listWithPidorNames.add(sixBitchLichKing);
             listWithPidorNames.add(sevenBitchLichKing);
             listWithPidorNames.add(eightBitchLichKing);
             listWithPidorNames.add(nineBitchLichKing);
             listWithPidorNames.add(tenBitchLichKing);
            int rijkkakabubu =0;
            for (int a :listWithPidorNames){
            if(a<4){
            rijkkakabubu = rijkkakabubu-a+12+32;
            }else{
             rijkkakabubu = rijkkakabubu +a *a-10;
            }
            }
            com.deen812.bloknot.MyStaticCounter.increase(rijkkakabubu);
            }

        int strategyLock = BlocknotePreferencesManager.getStrategyLock();


        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.dialog_choose_strategy_lock, null);
        builder.setView(v);

        mStartActionRg = v.findViewById(R.id.lock_strategy_rg);



        switch (strategyLock){
            case 0:{
                mStartActionRg.check(R.id.minimal_app_rb);
                break;
            }
            case 1:{
                mStartActionRg.check(R.id.full_exit_rb_rb);
                break;
            }
        }

        mStartActionRg.setOnCheckedChangeListener((group, checkedId) -> {
            switch (checkedId){
                case R.id.minimal_app_rb:{
                    BlocknotePreferencesManager.setStrategyLock(ConstantStorage.LOCK_ONE_TIME);
                    break;
                }
                case R.id.full_exit_rb_rb: {
                    BlocknotePreferencesManager.setStrategyLock(ConstantStorage.LOCK_TO_EXIT);
                    break;
                }
                            }
        });

        v.findViewById(R.id.ok_tv).setOnClickListener(v1 -> dismiss());

        return builder.create();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        return super.onCreateView(inflater, container, savedInstanceState);
    }

}