package com.deen812.bloknot.view.dialogs;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;

import com.deen812.bloknot.R;
import com.deen812.bloknot.billing.BillingHelper;
import com.deen812.bloknot.pro.PurchaseActivity;

public class ProDialog extends DialogFragment {

    ImageView ivCancel;
    LinearLayout llGetPro;

    public static ProDialog getInstance() {
        ProDialog dialog = new ProDialog();
        return dialog;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        setRetainInstance(true);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.dialog_pro, null);
        builder.setView(v);

        ivCancel = v.findViewById(R.id.ivCancel);
        llGetPro = v.findViewById(R.id.llGetPro);
        llGetPro.setOnClickListener(view -> {
            startActivity(new Intent(getContext(), PurchaseActivity.class));
            BillingHelper.openProActivity(getContext());
            //startActivity(new Intent(getContext(), ProActivity.class));
        });

        ivCancel.setOnClickListener(view ->
                dismiss()
        );
        return builder.create();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    public interface ConfirmListener {
        void confirmAction(boolean confirm, int code, Bundle arguments);
    }
}