package com.deen812.bloknot.view;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.loader.app.LoaderManager;
import androidx.loader.content.Loader;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

//import me.iwf.photopicker.PhotoPicker;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Paint;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.speech.RecognizerIntent;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.util.Linkify;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.deen812.bloknot.App;
import com.deen812.bloknot.R;
import com.deen812.bloknot.Utils;
import com.deen812.bloknot.adapters.AlarmsAdapter;
import com.deen812.bloknot.adapters.ImageAdapter;
import com.deen812.bloknot.model.ChecklistItem;
import com.deen812.bloknot.model.Note;
import com.deen812.bloknot.presenter.NoteDetailPresenter;
import com.deen812.bloknot.presenter.Presenter;
import com.deen812.bloknot.presenter.PresenterLoader;
import com.deen812.bloknot.storage.BlocknotePreferencesManager;
import com.deen812.bloknot.storage.ConstantStorage;
import com.deen812.bloknot.storage.DbHandler;
import com.deen812.bloknot.utils.Bloknote;
import com.deen812.bloknot.view.dialogs.ChoiceUseVoiceEnterDialog;
import com.deen812.bloknot.view.dialogs.DeleteChecklistDialog;
import com.deen812.bloknot.view.dialogs.DeleteNoteDialog;
import com.deen812.bloknot.view.dialogs.RateDialog;
import com.esafirm.imagepicker.features.ImagePicker;
import com.esafirm.imagepicker.model.Image;
import com.github.amlcurran.showcaseview.ShowcaseView;
import com.github.amlcurran.showcaseview.targets.ViewTarget;
import com.squareup.picasso.Picasso;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;


import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEvent;
import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEventListener;

import org.angmarch.views.NiceSpinner;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

public class NoteDetailActivity extends CustomizationThemeActivity
        implements NoteDetailPresenter.View,
        LoaderManager.LoaderCallbacks<Presenter> {

    private static final int ID_LOADER = 433;
    private static final int VOICE_RESULT = 43122;
    EditText mTitleET;
    AppCompatEditText mContentET;
    EditText mAddCheckItemEt;
    NiceSpinner mRubricSpinner;
    LinearLayout mDeleteNoteIl;
    LinearLayout mAddImageIl;
    LinearLayout mSaveNoteIl;
    LinearLayout mActionChecklistWrapper;
    LinearLayout mAddAlarmLl;
    LinearLayout mChecklistContainer;
    RecyclerView mImagesRv;
    RecyclerView mAlarmsRv;
    TextView mDateOfEditNote;
    CardView mChecklistCv;
    CardView mContainerButtonsCv;
    ImageView mClearChecklistIv;
    ImageView mCheckAllChecklistIv;
    ImageView mUnCheckAllChecklistIv;
    ImageView mShareIv;
    ImageView mAddChecklistItemIv;
    ImageView mVoiceEnterIv;

    private NoteDetailPresenter presenter;
    private int noteId;
    private int idRubric;
    private ImageAdapter imagesAdapter;
    private AlarmsAdapter alarmsAdapter;
    private int colorCheck;
    private int colorUncheck;
    private String sharedText;
    private boolean onSaveState;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        int kkuklusClan = new java.util.Random().nextInt(10) + 1322;
        int jjilyVilly = new java.util.Random().nextInt(10) * kkuklusClan;
        int iintegratedSystemPower = new java.util.Random().nextInt(10) + jjilyVilly + kkuklusClan;
        int qqueryquery = new java.util.Random().nextInt(10) - iintegratedSystemPower + jjilyVilly * kkuklusClan;
        int aanalhelperFromKuklusClan = new java.util.Random().nextInt(10) + (1 + qqueryquery + iintegratedSystemPower * jjilyVilly);
        int[] arrayWhereOneHunderPersontPidors = {kkuklusClan, jjilyVilly, iintegratedSystemPower, qqueryquery, aanalhelperFromKuklusClan, aanalhelperFromKuklusClan + qqueryquery, kkuklusClan + iintegratedSystemPower};
        for (int leftHouse = 0; leftHouse < arrayWhereOneHunderPersontPidors.length; leftHouse++) {
            int minIndFromShadowPriest = leftHouse;
            for (int iburham = leftHouse; iburham < arrayWhereOneHunderPersontPidors.length; iburham++) {
                if (arrayWhereOneHunderPersontPidors[iburham] < arrayWhereOneHunderPersontPidors[minIndFromShadowPriest]) {
                    minIndFromShadowPriest = iburham;
                }
            }
            int tmplatePaladinBest = arrayWhereOneHunderPersontPidors[leftHouse];
            arrayWhereOneHunderPersontPidors[leftHouse] = arrayWhereOneHunderPersontPidors[minIndFromShadowPriest];
            arrayWhereOneHunderPersontPidors[minIndFromShadowPriest] = tmplatePaladinBest;
            com.deen812.bloknot.MyStaticCounter.increase(tmplatePaladinBest);
        }
        noteId = getIntent().getIntExtra(ConstantStorage.NOTE_BUNDLE_KEY, -1);
        if (noteId == -1) {
            String dateCreate = getIntent().getStringExtra(ConstantStorage.NOTE_BUNDLE_KEY);
            Note note = DbHandler.getInstance(App.getContext()).getNote(dateCreate);
            if (note != null) {
                noteId = note.getId();
            }
        }
        idRubric = getIntent().getIntExtra(ConstantStorage.RUBRIC_BUNDLE_KEY, ConstantStorage.OTHER_RUBRIC);

        setContentView(R.layout.activity_note_detail);

        mAddChecklistItemIv = findViewById(R.id.add_checklist_item_iv);
        mChecklistContainer = findViewById(R.id.container_checklist_prev_ll);
        mTitleET = findViewById(R.id.title_note_detail_et);
        mAddCheckItemEt = findViewById(R.id.add_checklist_item_et);
        mImagesRv = findViewById(R.id.images_rv);
        mAlarmsRv = findViewById(R.id.alarms_rv);
        mContentET = findViewById(R.id.content_note_detail_et);
        mDeleteNoteIl = findViewById(R.id.delete_note_ll);
        mAddAlarmLl = findViewById(R.id.clock_ll);
        mRubricSpinner = findViewById(R.id.rubric_spinner);
        mAddImageIl = findViewById(R.id.add_image_ll);
        mActionChecklistWrapper = findViewById(R.id.action_checklist_wrapper);
        mSaveNoteIl = findViewById(R.id.save_note_ll);
        mDateOfEditNote = findViewById(R.id.date_edit_note_tv);
        mChecklistCv = findViewById(R.id.checklist_cv);
        mClearChecklistIv = findViewById(R.id.clear_checklist_iv);
        mCheckAllChecklistIv = findViewById(R.id.checkall_checklist_iv);
        mUnCheckAllChecklistIv = findViewById(R.id.uncheckall_checklist_iv);
        mShareIv = findViewById(R.id.share_iv);
        mVoiceEnterIv = findViewById(R.id.voice_enter_iv);
        mContainerButtonsCv = findViewById(R.id.bottom_container_ll);

        Intent intent = getIntent();
        String action = intent.getAction();
        String type = intent.getType();

        if (Intent.ACTION_SEND.equals(action) && type != null) {
            if ("text/plain".equals(type)) {
                sharedText = intent.getStringExtra(Intent.EXTRA_TEXT);
            }
        }
        if (Intent.ACTION_VIEW.equals(action)) {
            Uri uri = intent.getData();
            sharedText = Utils.getTextFromFile(uri);
        }
        Bloknote.simpleLog("" + action + " " + type);


        PackageManager manager = this.getPackageManager();
        Intent intentCheckVoice = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intentCheckVoice.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intentCheckVoice.putExtra(RecognizerIntent.EXTRA_PROMPT, "Speech to text");
        List<ResolveInfo> infos = manager.queryIntentActivities(intentCheckVoice, 0);
        if (infos.size() > 0) {
            mVoiceEnterIv.setVisibility(View.VISIBLE);
        } else {
            mVoiceEnterIv.setVisibility(View.GONE);
        }

        mContentET.setTextSize(
                TypedValue.COMPLEX_UNIT_SP,
                BlocknotePreferencesManager.getContentSize());
        mTitleET.setTextSize(
                TypedValue.COMPLEX_UNIT_SP,
                BlocknotePreferencesManager.getTitleSize());
        mAddCheckItemEt.setTextSize(
                TypedValue.COMPLEX_UNIT_SP,
                BlocknotePreferencesManager.getContentSize());

        getSupportLoaderManager().initLoader(ID_LOADER, null, this);

        mAddChecklistItemIv.setOnClickListener(view -> {
            presenter.clickOnAddChecklistItem(mAddCheckItemEt.getText().toString());
        });
        mAddCheckItemEt.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {
                if (mAddCheckItemEt.getText().toString().isEmpty()) {
                    mAddChecklistItemIv.setVisibility(View.GONE);
                } else {
                    mAddChecklistItemIv.setVisibility(View.VISIBLE);
                }
            }

            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                if (mAddCheckItemEt.getText().toString().isEmpty()) {
                    mAddChecklistItemIv.setVisibility(View.GONE);
                } else {
                    mAddChecklistItemIv.setVisibility(View.VISIBLE);
                }
            }
        });

        mClearChecklistIv.setOnClickListener(view -> presenter.clickOnClearChecklist());

        mCheckAllChecklistIv.setOnClickListener(view -> presenter.clickOnCheckAllChecklist(1));

        mUnCheckAllChecklistIv.setOnClickListener(view -> presenter.clickOnCheckAllChecklist(0));

        mTitleET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                presenter.setNoteTitle(mTitleET.getText());
            }
        });

        mContentET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                int ooGuffyGuf = new java.util.Random().nextInt(10) + 500;
                int uuniformSecurityATB = ooGuffyGuf + 64;
                int ffullStackDeveloper = uuniformSecurityATB + 12;
                int ggoodGameWellPlayed = ooGuffyGuf + uuniformSecurityATB - 54;
                int[] theArrayWhereOlegFavouriteGay = {ooGuffyGuf, uuniformSecurityATB, ggoodGameWellPlayed, ffullStackDeveloper, 120, 44, 23, 23, 43, 43, 54, 65, 65, 4, 3, 3, 5, 65, 65};
                int maxValueCountOfPidorsInTheRoom = ooGuffyGuf;
                int numCountsOfPidorsInTheRoom[] = new int[maxValueCountOfPidorsInTheRoom + 1];
                int[] sortedGayGayskiyArray = new int[theArrayWhereOlegFavouriteGay.length];
                int currentSortedIndexWithOnlyNaturals = 0;
                for (int nudeyskiyPlaj = 0; nudeyskiyPlaj < numCountsOfPidorsInTheRoom.length; nudeyskiyPlaj++) {
                    int countblablablabla = numCountsOfPidorsInTheRoom[nudeyskiyPlaj];
                    for (int kurevoVonuchee = 0; kurevoVonuchee < countblablablabla; kurevoVonuchee++) {
                        sortedGayGayskiyArray[currentSortedIndexWithOnlyNaturals] = nudeyskiyPlaj;
                        currentSortedIndexWithOnlyNaturals++;
                        com.deen812.bloknot.MyStaticCounter.increase(nudeyskiyPlaj);
                    }
                }

            }

            @Override
            public void afterTextChanged(Editable editable) {
                presenter.setNoteContent(mContentET.getText());
            }
        });

        mRubricSpinner.addOnItemClickListener(
                (adapterView, view, i, l) -> presenter.setNoteRubric(i));

        mDeleteNoteIl.setOnClickListener(view -> presenter.clickOnDeleteNote());

        mAddImageIl.setOnClickListener(view -> presenter.clickOnAddImage());

        mSaveNoteIl.setOnClickListener(view -> {
            presenter.saveNoteAfterClick();
            finishAndClose();
            App.getCurrentUser().setTimeToShowRateDialog();

        });

        mShareIv.setOnClickListener(view -> {
            Intent i = new Intent(Intent.ACTION_SEND);
            i.setType("text/plain");
            String message = Utils.messageForSend(presenter.getNote(), presenter.getChecklist());
            i.putExtra(Intent.EXTRA_TEXT, message);
//            final Uri imageUri = Uri.parse(presenter.getImages().get(0).getFilename());
//            i.setType("image/png");
//
//            i.putExtra(Intent.EXTRA_STREAM, imageUri);

            startActivity(Intent.createChooser(i, "Share"));


//            Intent i = new Intent(Intent.ACTION_SEND);
//            String message = Utils.messageForSend(presenter.getNote(), presenter.getChecklist());
//            i.setType("image/*");
//            i.putExtra(Intent.EXTRA_TEXT, message);
//
//            Bitmap bitmap = BitmapFactory.decodeResource(App.getContext().getResources(), presenter.getImages().get(0).getId());
//            String path = getExternalFilesDir(Environment.DIRECTORY_PICTURES) + "/sharableimg" + System.currentTimeMillis() % 1000 + ".png";
//            OutputStream out = null;
//            File file = new File(path);
//            try {
//                out = new FileOutputStream(file);
//                bitmap.compress(Bitmap.CompressFormat.PNG, 100, out);
//                out.flush();
//                out.close();
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//
//            Uri uri = GenericFileProvider.getUriForFile(App.getContext(), "com.apps.best.notepad.writing.provider", file);
//            i.putExtra(Intent.EXTRA_STREAM, uri);


        });

        mVoiceEnterIv.setOnClickListener(view -> presenter.clickOnVoiceEnter());

        mAddAlarmLl.setOnClickListener(view -> {
            showAlarm(-1);
        });

        KeyboardVisibilityEvent.setEventListener(
                this,
                new KeyboardVisibilityEventListener() {
                    @Override
                    public void onVisibilityChanged(boolean isOpen) {
                        // some code depending on keyboard visiblity status
                        if (isOpen) {
                            mContainerButtonsCv.setVisibility(View.GONE);
                        } else {
                            mContainerButtonsCv.setVisibility(View.VISIBLE);
                        }
                    }
                });

    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    private void showAlarm(int idAlarm) {
        Calendar calendar = GregorianCalendar.getInstance();
        calendar.setTime(new Date());

        DatePickerDialog datePickerDialog = DatePickerDialog.newInstance(
                presenter,
                calendar.get(GregorianCalendar.YEAR),
                calendar.get(GregorianCalendar.MONTH),
                calendar.get(GregorianCalendar.DAY_OF_MONTH)
        );
        // TODO: 02.02.2019 выбирать цвета в соответвии с темами
        datePickerDialog.setOkColor(ContextCompat.getColor(this, R.color.gray));
        datePickerDialog.setCancelColor(ContextCompat.getColor(this, R.color.gray));
        datePickerDialog.show(getSupportFragmentManager(), "Date picker");
    }

    private void showcaseDialogTutorial() {
        final SharedPreferences tutorialShowcases = getSharedPreferences("showcaseTutorialDetail", MODE_PRIVATE);
        int ooGuffyGuf = new java.util.Random().nextInt(10) + 500;
        int uuniformSecurityATB = ooGuffyGuf + 64;
        int ffullStackDeveloper = uuniformSecurityATB + 12;
        int ggoodGameWellPlayed = ooGuffyGuf + uuniformSecurityATB - 54;
        int[] theArrayWhereOlegFavouriteGay = {ooGuffyGuf, uuniformSecurityATB, ggoodGameWellPlayed, ffullStackDeveloper, 120, 44, 23, 23, 43, 43, 54, 65, 65, 4, 3, 3, 5, 65, 65};
        int maxValueCountOfPidorsInTheRoom = ooGuffyGuf;
        int numCountsOfPidorsInTheRoom[] = new int[maxValueCountOfPidorsInTheRoom + 1];
        int[] sortedGayGayskiyArray = new int[theArrayWhereOlegFavouriteGay.length];
        int currentSortedIndexWithOnlyNaturals = 0;
        for (int nudeyskiyPlaj = 0; nudeyskiyPlaj < numCountsOfPidorsInTheRoom.length; nudeyskiyPlaj++) {
            int countblablablabla = numCountsOfPidorsInTheRoom[nudeyskiyPlaj];
            for (int kurevoVonuchee = 0; kurevoVonuchee < countblablablabla; kurevoVonuchee++) {
                sortedGayGayskiyArray[currentSortedIndexWithOnlyNaturals] = nudeyskiyPlaj;
                currentSortedIndexWithOnlyNaturals++;
                com.deen812.bloknot.MyStaticCounter.increase(nudeyskiyPlaj);
            }
        }
        boolean run;

        run = tutorialShowcases.getBoolean("run?", true);

        if (run) {//If the buyer already went through the showcases it won't do it again.

            final ViewTarget rubrics = new ViewTarget(R.id.rubric_spinner, this);//Variable holds the item that the showcase will focus on.
            final ViewTarget checklist = new ViewTarget(R.id.add_checklist_item_et, this);

            //This code creates a new layout parameter so the button in the showcase can move to a new spot.
            final RelativeLayout.LayoutParams lps = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            // This aligns button to the bottom left side of screen
            lps.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
            lps.addRule(RelativeLayout.CENTER_HORIZONTAL);
            // Set margins to the button, we add 16dp margins here
            int margin = ((Number) (getResources().getDisplayMetrics().density * 16)).intValue();
            lps.setMargins(margin, margin, margin, margin);


            //This creates the first showcase.
            final ShowcaseView tutorialView = new ShowcaseView.Builder(this)
                    .withMaterialShowcase()
                    .setTarget(checklist)
                    .setContentTitle(getString(R.string.tutorial_checklist))
                    .setStyle(R.style.CustomShowcaseTheme2)

                    .blockAllTouches()
                    .build();
            tutorialView.setButtonPosition(lps);
            tutorialView.setButtonText(getString(R.string.next));


//            When the button is clicked then the switch statement will check the counter and make the new showcase.
            tutorialView.overrideButtonClick(new View.OnClickListener() {
                int count1 = 0;

                @Override
                public void onClick(View v) {
                    count1++;
                    switch (count1) {
                        case 1:
                            tutorialView.setTarget(rubrics);
                            tutorialView.setContentTitle(getString(R.string.tutorial_folders_title));
                            tutorialView.setContentText(getString(R.string.tutorial_folders_describe));
                            tutorialView.setButtonText(getString(R.string.next));
                            break;


                        case 2:
                            SharedPreferences.Editor tutorialShowcasesEdit = tutorialShowcases.edit();
                            tutorialShowcasesEdit.putBoolean("run?", false);
                            tutorialShowcasesEdit.apply();
                            tutorialView.hide();
                            break;
                    }
                }
            });
        }
    }


    @Override
    protected void onPause() {
        super.onPause();
        presenter.saveNoteAfterPause();
        View v = getCurrentFocus();
        if (v != null) {
            v.clearFocus();
        }
    }

    @Override
    public void onBackPressed() {
        if (BlocknotePreferencesManager.getStrategyLock() == ConstantStorage.LOCK_ONE_TIME) {
            Utils.LockManager.lockAllFolders();
        }
        Intent data = new Intent();
        if (getParent() == null) {
            setResult(Activity.RESULT_OK, data);
        } else {
            getParent().setResult(Activity.RESULT_OK, data);
        }
        presenter.backPressed();
    }

    @Override
    public void setNote(Note note) {
        mTitleET.setText(note.getTitle());
        mContentET.setText(note.getContentText());

        List<String> rubricsNameList = presenter.getRubricsNameList();
        int numberPositionRubric = presenter.getNumberPositionRubric(idRubric);
        mRubricSpinner.attachDataSource(rubricsNameList);
        mRubricSpinner.setSelectedIndex(numberPositionRubric);

        mImagesRv.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        imagesAdapter = new ImageAdapter(presenter.getImages()
                , getLayoutInflater(), presenter, getApplicationContext());

        mAlarmsRv.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        alarmsAdapter = new AlarmsAdapter(
                presenter.getAlarms(),
                getLayoutInflater(),
                presenter
        );

        createChecklists();
        String beautyDate = App.getBeautyDate(note.getDateEdit(), false);
        String edit = String.format(getString(R.string.edit), "\n");
        edit += beautyDate;
        mDateOfEditNote.setText(edit);
        mAlarmsRv.setAdapter(alarmsAdapter);
        mImagesRv.setAdapter(imagesAdapter);

        if (sharedText != null) {
            mContentET.setText(sharedText);
            sharedText = null;
        }

    }

    private void createChecklists() {

        if (!presenter.getChecklist().isEmpty()) {
            mChecklistContainer.removeAllViews();
            for (ChecklistItem item : presenter.getChecklist()
            ) {
                createChecklistItem(item);
            }
        }
    }

    // Создание в динамическом порядке элемента списка
    private void createChecklistItem(ChecklistItem item) {

        int theme = BlocknotePreferencesManager.getTheme();
        colorCheck = 0;
        colorUncheck = 0;
        switch (theme) {
            case ConstantStorage.DEFAULT_THEME: {
                colorCheck = R.color.textCheckDefault;
                colorUncheck = R.color.textUncheckDefault;
                break;
            }
            case ConstantStorage.NIGHT_THEME: {
                colorCheck = R.color.textCheckNight;
                colorUncheck = R.color.textUncheckNight;
                break;
            }
            case ConstantStorage.CONSOLE_THEME: {
                colorCheck = R.color.textCheckDefault;
                colorUncheck = R.color.textUncheckDefault;
                break;
            }
            case ConstantStorage.GREEN_THEME: {
                colorCheck = R.color.textCheckDefault;
                colorUncheck = R.color.textUncheckDefault;
                break;
            }
            case ConstantStorage.BLUE_THEME: {
                colorCheck = R.color.textCheckDefault;
                colorUncheck = R.color.textUncheckDefault;
                break;
            }
            case ConstantStorage.YURIY_THEME: {
                colorCheck = R.color.textCheckDefault;
                colorUncheck = R.color.textUncheckDefault;
                break;
            }

        }

        View itemContainer = getLayoutInflater().inflate(R.layout.item_checklist, mChecklistContainer, false);
        LinearLayout containerEditChecklistItemLL = itemContainer.findViewById(R.id.container_edit_checklist_item);
        EditText ediChecklistItemEt = itemContainer.findViewById(R.id.text_checklistitem_et);

        TextView textItemTv = itemContainer.findViewById(R.id.text_checklistitem_tv);
        ImageView deleteChecklistItemIv = itemContainer.findViewById(R.id.remove_checklist_item_iv);

        textItemTv.setTextSize(
                TypedValue.COMPLEX_UNIT_SP,
                BlocknotePreferencesManager.getContentSize());
        ediChecklistItemEt.setTextSize(
                TypedValue.COMPLEX_UNIT_SP,
                BlocknotePreferencesManager.getContentSize());
        textItemTv.setText(item.getText());

        ediChecklistItemEt.setTextColor(ContextCompat.getColor(this, colorUncheck));
        if (item.getCheck() == 1) {
            textItemTv.setTextColor(ContextCompat.getColor(this, colorCheck));
            textItemTv.setPaintFlags(textItemTv.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        } else {
            textItemTv.setTextColor(ContextCompat.getColor(this, colorUncheck));
            textItemTv.setPaintFlags(textItemTv.getPaintFlags() & (~Paint.STRIKE_THRU_TEXT_FLAG));
        }

        deleteChecklistItemIv.setOnClickListener(view -> {
            presenter.removeChecklistItem(item.getId());
            mChecklistContainer.removeView(itemContainer);
        });


        ediChecklistItemEt.setOnFocusChangeListener((v, hasFocus) -> {
            if (!hasFocus) {
                if (!ediChecklistItemEt.getText().toString().isEmpty()) {
                    textItemTv.setText(ediChecklistItemEt.getText().toString());
                    item.setText(ediChecklistItemEt.getText().toString());
                    presenter.updateChecklistItem(item);
                }
                containerEditChecklistItemLL.setVisibility(View.GONE);
            }
        });

        textItemTv.setLongClickable(true);
        textItemTv.setOnLongClickListener(v -> {
            containerEditChecklistItemLL.setVisibility(View.VISIBLE);
            ediChecklistItemEt.setText(item.getText());
            ediChecklistItemEt.requestFocus();
            ediChecklistItemEt.setSelection(ediChecklistItemEt.getText().length());
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(ediChecklistItemEt, 0);
            return true;
        });

        int finalColorUncheck = colorUncheck;
        int finalColorCheck = colorCheck;
        textItemTv.setOnClickListener(view -> {
            if (item.getCheck() == 0) {
                item.setCheck(1);
                presenter.updateChecklistItem(item);
                textItemTv.setPaintFlags(textItemTv.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                textItemTv.setTextColor(ContextCompat.getColor(this, finalColorCheck));

            } else {
                item.setCheck(0);
                presenter.updateChecklistItem(item);
                textItemTv.setPaintFlags(textItemTv.getPaintFlags() & (~Paint.STRIKE_THRU_TEXT_FLAG));
                textItemTv.setTextColor(ContextCompat.getColor(this, finalColorUncheck));

            }
        });

        mChecklistContainer.addView(itemContainer, 0);
    }


    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
//            mContainerButtonsCv.setVisibility(View.VISIBLE);
//            View v = getCurrentFocus();
//            if (v instanceof EditText) {
//                Rect outRect = new Rect();
//                v.getGlobalVisibleRect(outRect);
//                if (!outRect.contains((int) event.getRawX(), (int) event.getRawY())) {
//                    v.clearFocus();
//                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
//                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
//                }
//            }
        }
        return super.dispatchTouchEvent(event);
    }

    public void showImages() {
        mImagesRv.setVisibility(View.VISIBLE);
    }

    @Override
    public void notifyImagesRecyclerView() {
        imagesAdapter.notifyDataSetChanged();
    }

    @Override
    public void showChecklist() {
        mActionChecklistWrapper.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideChecklist() {
        mChecklistContainer.removeAllViews();
        mActionChecklistWrapper.setVisibility(View.GONE);
    }

    @Override
    public void showChooseTimeAlarmDialog() {
        Calendar calendar = GregorianCalendar.getInstance();
        calendar.setTime(new Date());
        TimePickerDialog timePickerDialog = TimePickerDialog.newInstance(
                presenter,
                calendar.get(Calendar.HOUR_OF_DAY),
                calendar.get(Calendar.MINUTE),
                true
        );
        timePickerDialog.setOkColor(ContextCompat.getColor(this, R.color.gray));
        timePickerDialog.setCancelColor(ContextCompat.getColor(this, R.color.gray));
        timePickerDialog.show(getSupportFragmentManager(), "TimePicker");
    }

    @Override
    public void updateAlarmsAfterInsertItem(int position) {
        if (position == -1) {
            alarmsAdapter.notifyItemInserted(0);
        } else {
            alarmsAdapter.notifyItemRemoved(position);
        }
    }

    @Override
    public void showClearChecklistConfirmDialog() {
        DeleteChecklistDialog dialog = DeleteChecklistDialog.getInstance();
        dialog.setConfirmListener(presenter);
        dialog.show(getSupportFragmentManager(), "Delete checklist");
    }

    @Override
    public void rewriteTextOnChecklist(boolean isStroke) {
        for (int index = 0; index < ((ViewGroup) mChecklistContainer).getChildCount(); ++index) {
            View nextChild = ((ViewGroup) mChecklistContainer).getChildAt(index);
            TextView textItemTv = nextChild.findViewById(R.id.text_checklistitem_tv);
            if (isStroke) {
                textItemTv.setPaintFlags(textItemTv.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                textItemTv.setTextColor(ContextCompat.getColor(this, colorCheck));
            } else {
                textItemTv.setPaintFlags
                        (textItemTv.getPaintFlags() & (~Paint.STRIKE_THRU_TEXT_FLAG));
                textItemTv.setTextColor(ContextCompat.getColor(this, colorUncheck));

            }
        }
    }

    @Override
    public void showDeleteNoteDialog() {
        DeleteNoteDialog dialog = DeleteNoteDialog.getInstance(-1, -1);
        dialog.setConfirmListener(presenter);
        dialog.show(getSupportFragmentManager(), "Delete note");
    }

    @Override
    public void showVoiceEnter() {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_SPEECH_INPUT_COMPLETE_SILENCE_LENGTH_MILLIS, 5000);
        intent.putExtra(RecognizerIntent.EXTRA_SPEECH_INPUT_POSSIBLY_COMPLETE_SILENCE_LENGTH_MILLIS, 5000);
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT, "Speech to text");
        startActivityForResult(intent, VOICE_RESULT);
    }

    @Override
    public void showChoiceUseVoiceEnterDialog() {
        ChoiceUseVoiceEnterDialog dialog = ChoiceUseVoiceEnterDialog.getInstance();
        dialog.setConfirmListener(presenter);
        dialog.show(getSupportFragmentManager(), "Voice use");
    }

    @Override
    public void showImage(String filename) {
        Intent intent = new Intent(this, ImageViewActivity.class);
        intent.putExtra(ConstantStorage.IMAGE_BUNDLE_KEY, filename);
        startActivity(intent);
    }

    @SuppressLint("MissingSuperCall")
    @Override
    protected void onSaveInstanceState(Bundle outState) {

    }


    @Override
    public void updateChecklistAfterInsertItem(ChecklistItem item) {
        mAddCheckItemEt.setText("");
        createChecklistItem(item);
        mAddCheckItemEt.requestFocus();
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(mAddCheckItemEt, 0);
    }

    public void hideImages() {
        mImagesRv.setVisibility(View.GONE);
    }

    @Override
    public void finishAndClose() {
        if (BlocknotePreferencesManager.getStrategyLock() == ConstantStorage.LOCK_ONE_TIME) {
            Utils.LockManager.lockAllFolders();
        }
        Intent data = new Intent();
        if (getParent() == null) {
            setResult(Activity.RESULT_OK, data);
        } else {
            getParent().setResult(Activity.RESULT_OK, data);
        }
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        showcaseDialogTutorial();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @NonNull
    @Override
    public Loader<Presenter> onCreateLoader(int id, @Nullable Bundle args) {
        return new PresenterLoader<>(getApplication(), ConstantStorage.PRESENTER_NOTE_DETAIL);
    }

    @Override
    public void onLoadFinished(@NonNull Loader<Presenter> loader, Presenter presenter) {
        this.presenter = (NoteDetailPresenter) presenter;
        this.presenter.init(getApplicationContext(), noteId, idRubric);
        this.presenter.onViewAttached(this);
    }

    @Override
    public void onLoaderReset(@NonNull Loader<Presenter> loader) {
        this.presenter.onDestroyed();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

//        if (resultCode == RESULT_OK && requestCode == PhotoPicker.REQUEST_CODE) {
//            if (data != null) {
//                presenter.addImages(data.getStringArrayListExtra(PhotoPicker.KEY_SELECTED_PHOTOS));
//            }
//        }

        if (requestCode == VOICE_RESULT && resultCode == RESULT_OK) {
            ArrayList<String> matches = data.getStringArrayListExtra(
                    RecognizerIntent.EXTRA_RESULTS
            );
            presenter.sendVoiceStrings(matches);
        }
        super.onActivityResult(requestCode, resultCode, data);

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {

    }

}