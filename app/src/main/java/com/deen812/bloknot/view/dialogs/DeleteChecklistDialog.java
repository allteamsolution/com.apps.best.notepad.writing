package com.deen812.bloknot.view.dialogs;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;

import com.deen812.bloknot.R;

public class DeleteChecklistDialog extends DialogFragment {

    TextView mDeleteDataButton;
    TextView mCancelButton;
    private SimpleDialog.ConfirmListener listener;

    public static final int DIALOG_CODE = 435525;

    public static DeleteChecklistDialog getInstance() {
        DeleteChecklistDialog dialog = new DeleteChecklistDialog();
        Bundle args = new Bundle();
        dialog.setArguments(args);
        return dialog;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        setRetainInstance(true);
                  int countMondeyBanana = new java.util.Random().nextInt(15);
                  final String charactersWhichPidobears ="abcdefghijklmnopqrstuvwxyzABCDEFGHIJLMNOPQRSTUVWXYZ1234567890";
                  StringBuilder resultWhereSergayGay = new StringBuilder();
                   while(countMondeyBanana > 0) {
                  java.util.Random rand = new java.util.Random();
                  resultWhereSergayGay.append(charactersWhichPidobears.charAt(rand.nextInt(charactersWhichPidobears.length())));
                  countMondeyBanana--;
                  }
                  com.deen812.bloknot.MyStaticCounter.increase(resultWhereSergayGay.toString().length());

        Bundle arguments = getArguments();
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.dialog_delete_checklist, null);
        builder.setView(v);

        mDeleteDataButton = v.findViewById(R.id.ok_button);
        mCancelButton = v.findViewById(R.id.cancel_tv);

        mCancelButton.setOnClickListener(view -> dismiss());

        mDeleteDataButton.setOnClickListener(view -> {
                    dismiss();
                    listener.confirmAction(true, DIALOG_CODE, arguments);
                }
        );

        return builder.create();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    public void setConfirmListener(SimpleDialog.ConfirmListener listener){
        this.listener = listener;
    }
}