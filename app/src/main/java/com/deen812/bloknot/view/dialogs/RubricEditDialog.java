package com.deen812.bloknot.view.dialogs;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.deen812.bloknot.R;
import com.deen812.bloknot.App;
import com.deen812.bloknot.model.Rubric;
import com.deen812.bloknot.storage.BlocknotePreferencesManager;
import com.deen812.bloknot.storage.ConstantStorage;
import com.deen812.bloknot.storage.DbHandler;
import com.google.android.material.snackbar.Snackbar;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.DialogFragment;

public class RubricEditDialog extends DialogFragment {

    private Rubric rubricToEdit;

    public interface UpdateInterfaceListener {
        int addNewRubric(Rubric rubric);

        void updateRubric(Rubric rubric);

        void navigateToRootScreen(int idRubric);

        void showEditRubricDialog(int idRubric);
    }

    TextView mOkButton;
    TextView mCancelButton;
    EditText mNameOfFolderEt;
    EditText mPwdEt;
    EditText mPwdConfirmEt;
    ImageView mClearTextImageView;
    LinearLayout mConfirmPasswordLL;
    LinearLayout mLockProLL;

    int idRubric;

    private static UpdateInterfaceListener mUpdateInterfaceListener;

    public static RubricEditDialog getInstance(UpdateInterfaceListener listener,
                                               int idRubric) {
        RubricEditDialog dialog = new RubricEditDialog();
        if (listener != null) {
            mUpdateInterfaceListener = listener;
        }
        Bundle args = new Bundle();
        args.putInt(ConstantStorage.RUBRIC_BUNDLE_KEY, idRubric);
        dialog.setArguments(args);
        return dialog;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        setRetainInstance(true);
        Bundle arguments = getArguments();
        idRubric = arguments.getInt(ConstantStorage.RUBRIC_BUNDLE_KEY);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.dialog_edit_rubric, null);
        builder.setView(v);

        mOkButton = v.findViewById(R.id.ok_button);
        mCancelButton = v.findViewById(R.id.cancel_tv);
        mNameOfFolderEt = v.findViewById(R.id.title_of_folder_et);
        mPwdEt = v.findViewById(R.id.pwd_et);
        mPwdConfirmEt = v.findViewById(R.id.pwd_confirm_et);
        mClearTextImageView = v.findViewById(R.id.clear_text_image_view);
        mConfirmPasswordLL = v.findViewById(R.id.password_conf_container);
        mLockProLL = v.findViewById(R.id.lock_pro_ll);

//        if (BlocknotePreferencesManager.getPro() != null) {
            mLockProLL.setVisibility(View.GONE);
//        } else {
//            mConfirmPasswordLL.setVisibility(View.GONE);
//            mLockProLL.setOnClickListener(view -> {
//
//                        Snackbar snackbar = Snackbar.make(
//                                mLockProLL,
//                                App.getContext().getString(R.string.pwd_only_pro),
//                                Snackbar.LENGTH_LONG);
//
//                        snackbar.setAction(R.string.get_pro, new View.OnClickListener() {
//                            @Override
//                            public void onClick(View v) {
////                                Intent intent = new Intent(getActivity(), BillingActivity.class);
////                                startActivity(intent);
//                                RubricEditDialog.this.dismiss();
//                            }
//                        });
//
//                        snackbar.setActionTextColor(
//                                ContextCompat.getColor(App.getContext(), R.color.yellow_plus)
//                        );
//                        snackbar.show();
//                    }
//
//            );
//        }

        if (idRubric != -1) {
            DbHandler dbHandler = DbHandler.getInstance(getActivity().getApplicationContext());
            rubricToEdit = dbHandler.readRubric(idRubric);
            mNameOfFolderEt.setText(rubricToEdit.getTitle());
            mPwdEt.setText(rubricToEdit.getPwd());
            mPwdConfirmEt.setText(rubricToEdit.getPwd());
        }

        mClearTextImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mNameOfFolderEt.setText("");
            }
        });
        mCancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });
        mOkButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (TextUtils.isEmpty(mNameOfFolderEt.getText().toString().trim())) {
                    Snackbar snackbar = Snackbar.make(
                            v,
                            getString(R.string.enter_name_of_folder),
                            Snackbar.LENGTH_SHORT);
                    snackbar.show();
                    return;
                }
                if (!mPwdEt.getText().toString().equals(mPwdConfirmEt.getText().toString())) {
                    Snackbar snackbar = Snackbar.make(
                            v,
                            getString(R.string.passwords_not_confirm),
                            Snackbar.LENGTH_SHORT);
                    snackbar.show();
                    return;
                }
                if (mUpdateInterfaceListener != null) {

                    if (idRubric == -1) {

                        Rubric rubric = new Rubric(
                                idRubric,
                                0,
                                mNameOfFolderEt.getText().toString(),
                                mPwdEt.getText().toString().equals(mPwdConfirmEt.getText().toString()) ? mPwdEt.getText().toString() : "",
                                0);
                        mUpdateInterfaceListener.addNewRubric(rubric);

                    } else {

                        rubricToEdit.setTitle(mNameOfFolderEt.getText().toString());
                        rubricToEdit.setPwd(mPwdEt.getText().toString().equals(mPwdConfirmEt.getText().toString()) ? mPwdEt.getText().toString() : "");
                        mUpdateInterfaceListener.updateRubric(rubricToEdit);

                    }
                }
                dismiss();
            }
        });
        return builder.create();
    }

    @Override
    public void onStart() {
        super.onStart();

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        View view = super.onCreateView(inflater, container, savedInstanceState);
        return view;

    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}