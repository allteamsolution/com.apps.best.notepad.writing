package com.deen812.bloknot.view.dialogs;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;

import com.deen812.bloknot.R;
import com.deen812.bloknot.storage.BlocknotePreferencesManager;
import com.deen812.bloknot.storage.ConstantStorage;
import com.deen812.bloknot.utils.Bloknote;

public class ThemeDialog extends DialogFragment {

    public static final int CODE = 43145;
    private ConfirmListener listener;

    private RadioGroup mThemeChangeRg;

    public static ThemeDialog getInstance() {
        ThemeDialog dialog = new ThemeDialog();
        return dialog;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        setRetainInstance(true);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.dialog_theme, null);
        builder.setView(v);

        mThemeChangeRg = v.findViewById(R.id.theme_change_rg);

        switch (BlocknotePreferencesManager.getTheme()) {
            case ConstantStorage
                    .DEFAULT_THEME: {
                mThemeChangeRg.check(R.id.default_theme_rb);
                break;
            }
            case ConstantStorage
                    .NIGHT_THEME: {
                mThemeChangeRg.check(R.id.night_theme_rb);
                break;
            }
            case ConstantStorage
                    .CONSOLE_THEME: {
                mThemeChangeRg.check(R.id.console_theme_rb);
                break;
            }
            case ConstantStorage
                    .GREEN_THEME: {
                mThemeChangeRg.check(R.id.green_theme_rb);
                break;
            }
            case ConstantStorage
                    .BLUE_THEME: {
                mThemeChangeRg.check(R.id.blue_theme_rb);
                break;
            }
            case ConstantStorage
                    .YURIY_THEME: {
                mThemeChangeRg.check(R.id.blue_theme_rb_yuriy);
                break;
            }
        }
        mThemeChangeRg.setOnCheckedChangeListener((group, checkedId) -> {
            switch (checkedId) {
                case R.id.default_theme_rb: {
                    BlocknotePreferencesManager.setTheme(ConstantStorage.DEFAULT_THEME);
                    break;
                }
                case R.id.night_theme_rb: {
                    BlocknotePreferencesManager.setTheme(ConstantStorage.NIGHT_THEME);
                    break;
                }
                case R.id.console_theme_rb: {
                    BlocknotePreferencesManager.setTheme(ConstantStorage.CONSOLE_THEME);
                    break;
                }
                case R.id.green_theme_rb: {
                    BlocknotePreferencesManager.setTheme(ConstantStorage.GREEN_THEME);
                    break;
                }
                case R.id.blue_theme_rb: {
                    BlocknotePreferencesManager.setTheme(ConstantStorage.BLUE_THEME);
                    break;
                }
                case R.id.blue_theme_rb_yuriy: {
                    BlocknotePreferencesManager.setTheme(ConstantStorage.YURIY_THEME);
                    break;
                }
            }
            listener.confirmAction(true, ThemeDialog.CODE, null);
            dismiss();
        });

        return builder.create();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    public void setConfirmListener(ConfirmListener listener) {
        this.listener = listener;
    }

    public interface ConfirmListener {
        void confirmAction(boolean confirm, int code, Bundle arguments);
    }
}