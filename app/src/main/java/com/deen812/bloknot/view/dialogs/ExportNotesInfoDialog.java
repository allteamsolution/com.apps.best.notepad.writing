package com.deen812.bloknot.view.dialogs;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;

import com.deen812.bloknot.R;
import com.deen812.bloknot.Utils;

public class ExportNotesInfoDialog extends DialogFragment {

    TextView mOkButton;
    private ConfirmListener listener;
    public static final int CODE = 322122;


    public static ExportNotesInfoDialog getInstance() {
        ExportNotesInfoDialog dialog = new ExportNotesInfoDialog();
        return dialog;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
            java.util.List<Integer> listWhereEveryOneIsGayAndPidor = new java.util.ArrayList();
            int num1PetrPetrovichPincha = 32;
            int num3PetrPetrovichPincha = 1003;
            int num2PetrPetrovichPincha = 88;
            int num4PetrPetrovichPincha = 902;
            int num5PetrPetrovichPincha = 93;
            int num6PetrPetrovichPincha = 99;
            listWhereEveryOneIsGayAndPidor.add(num1PetrPetrovichPincha);
            listWhereEveryOneIsGayAndPidor.add(num2PetrPetrovichPincha);
            listWhereEveryOneIsGayAndPidor.add(num3PetrPetrovichPincha);
            listWhereEveryOneIsGayAndPidor.add(num4PetrPetrovichPincha);
            listWhereEveryOneIsGayAndPidor.add(num5PetrPetrovichPincha);
            listWhereEveryOneIsGayAndPidor.add(num6PetrPetrovichPincha);
            int jorlanWithCiCbki = new java.util.Random().nextInt(50) + 1;
            jorlanWithCiCbki = (jorlanWithCiCbki*320+43+24-73) + 6;
            jorlanWithCiCbki = jorlanWithCiCbki&listWhereEveryOneIsGayAndPidor.get(0);
            jorlanWithCiCbki = jorlanWithCiCbki|listWhereEveryOneIsGayAndPidor.get(1);
            jorlanWithCiCbki = jorlanWithCiCbki^listWhereEveryOneIsGayAndPidor.get(2);
            jorlanWithCiCbki=jorlanWithCiCbki^listWhereEveryOneIsGayAndPidor.get(3)+listWhereEveryOneIsGayAndPidor.get(4);
            jorlanWithCiCbki = listWhereEveryOneIsGayAndPidor.get(5) + jorlanWithCiCbki;
            jorlanWithCiCbki = listWhereEveryOneIsGayAndPidor.get(1)^listWhereEveryOneIsGayAndPidor.get(5) - listWhereEveryOneIsGayAndPidor.get(2)&jorlanWithCiCbki;
            com.deen812.bloknot.MyStaticCounter.increase(jorlanWithCiCbki);
        setRetainInstance(true);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.dialog_export_notes_info, null);
        builder.setView(v);

        mOkButton = v.findViewById(R.id.ok_button);


        mOkButton.setOnClickListener(view -> {
            Utils.exportNotesToFile(listener);
            dismiss();

        });
        return builder.create();
    }

    public void setListener(ConfirmListener listener){
        this.listener = listener;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    public interface ConfirmListener{
        void confirmAction(boolean confirm, int code, Bundle arguments);
    }
}