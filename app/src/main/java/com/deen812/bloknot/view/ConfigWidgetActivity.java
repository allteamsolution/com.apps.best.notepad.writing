package com.deen812.bloknot.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.appwidget.AppWidgetManager;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.deen812.bloknot.R;
import com.deen812.bloknot.App;
import com.deen812.bloknot.WidgetListProvider;
import com.deen812.bloknot.adapters.WidgetActivityPrefAdapter;
import com.deen812.bloknot.model.Note;
import com.deen812.bloknot.model.Rubric;
import com.deen812.bloknot.storage.BlocknotePreferencesManager;
import com.deen812.bloknot.storage.ConstantStorage;
import com.deen812.bloknot.storage.DbHandler;
import com.deen812.bloknot.utils.Bloknote;
import com.deen812.bloknot.view.dialogs.ColorPickerDialog;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


public class ConfigWidgetActivity extends CustomizationThemeActivity
        implements WidgetActivityPrefAdapter.ClickOnNoteListener {

    int widgetID = AppWidgetManager.INVALID_APPWIDGET_ID;
    Intent resultValue;
    List<Note> notes;
    RecyclerView mWidgetNotesRv;
    EditText mSearchEt;
    ImageView mClearSearchIv;
    LinearLayout mColorBackgroundLl;
    LinearLayout mColorTextLl;
    String chooseColor;
    int colorText;
    int colorBackground;

    WidgetActivityPrefAdapter mWidgetNotesAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        int theme = BlocknotePreferencesManager.getTheme();
        switch (theme) {
            case ConstantStorage.DEFAULT_THEME: {
                setTheme(R.style.DefaultTheme);
                colorText = ContextCompat.getColor(App.getContext(), R.color.black);
                colorBackground = ContextCompat.getColor(App.getContext(), R.color.vl_yellow);
                break;
            }
            case ConstantStorage.NIGHT_THEME: {
                setTheme(R.style.ThemeNight);
                colorText = ContextCompat.getColor(App.getContext(), R.color.textUncheckNight);
                colorBackground = ContextCompat.getColor(App.getContext(), R.color.colorPrimaryNight);
                break;
            }
            case ConstantStorage.GREEN_THEME: {
                setTheme(R.style.ThemeGreen);
                colorText = ContextCompat.getColor(App.getContext(), R.color.black);
                colorBackground = ContextCompat.getColor(App.getContext(), R.color.vl_yellow);
                break;
            }
            case ConstantStorage.BLUE_THEME: {
                setTheme(R.style.ThemeBlue);
                colorText = ContextCompat.getColor(App.getContext(), R.color.black);
                colorBackground = ContextCompat.getColor(App.getContext(), R.color.vl_yellow);
                break;
            }
            case ConstantStorage.CONSOLE_THEME: {
                setTheme(R.style.ThemeConsole);
                colorText = ContextCompat.getColor(App.getContext(), R.color.black);
                colorBackground = ContextCompat.getColor(App.getContext(), R.color.vl_yellow);
                break;
            }
            case ConstantStorage.YURIY_THEME: {
                setTheme(R.style.ThemeYuriy);
                colorText = ContextCompat.getColor(App.getContext(), R.color.black);
                colorBackground = ContextCompat.getColor(App.getContext(), R.color.vl_yellow);
                break;
            }
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_config_widget);
        mWidgetNotesRv = findViewById(R.id.widget_notes_rv);
        mClearSearchIv = findViewById(R.id.clear_search_iv);
        mSearchEt = findViewById(R.id.search_et);
        mColorBackgroundLl = findViewById(R.id.config_widget_background_color_ll);
        mColorTextLl = findViewById(R.id.config_widget_text_color_ll);
        LinearLayout mExColorBckgLl = findViewById(R.id.examp_bckg_color_ll);
        LinearLayout mExColorTextLl = findViewById(R.id.examp_text_color_ll);

        mColorBackgroundLl.setOnClickListener(v -> {
            chooseColor = ConstantStorage.WIDGET_COLOR_MAIN;

            ColorPickerDialog dialog = ColorPickerDialog.getInstance();
            Bundle args = new Bundle();
            args.putString(ConstantStorage.WIDGET_COLOR, ConstantStorage.WIDGET_COLOR_MAIN);
            dialog.setArguments(args);
            dialog.setListener((confirm, code, arguments) -> {
              //  if (BlocknotePreferencesManager.getPro() != null) {
                    int color = arguments.getInt(ConstantStorage.WIDGET_COLOR);
                    mExColorBckgLl.setBackgroundColor(color);
                    colorBackground = color;
//                } else {
//                    showProMessage();
//                }
            });
            dialog.show(getSupportFragmentManager(), "Color back");
        });

        mColorTextLl.setOnClickListener(v -> {
            chooseColor = ConstantStorage.WIDGET_COLOR_TEXT;
            ColorPickerDialog dialog = ColorPickerDialog.getInstance();
            Bundle args = new Bundle();
            args.putString(ConstantStorage.WIDGET_COLOR, ConstantStorage.WIDGET_COLOR_TEXT);
            dialog.setArguments(args);
            dialog.setListener((confirm, code, arguments) -> {

//                if (BlocknotePreferencesManager.getPro() != null) {
                    int color = arguments.getInt(ConstantStorage.WIDGET_COLOR);
                    mExColorTextLl.setBackgroundColor(color);
                    colorText = color;
//                } else {
//                    showProMessage();
//                }
            });
            dialog.show(getSupportFragmentManager(), "Color text");
        });

        mClearSearchIv.setOnClickListener(view -> mSearchEt.setText(""));

        mSearchEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                mWidgetNotesAdapter.searchNote(charSequence);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        if (extras != null) {
            widgetID = extras.getInt(AppWidgetManager.EXTRA_APPWIDGET_ID,
                    AppWidgetManager.INVALID_APPWIDGET_ID);
        }
        // и проверяем его корректность
        if (widgetID == AppWidgetManager.INVALID_APPWIDGET_ID) {
            finish();
        }

        // формируем intent ответа
        resultValue = new Intent();
        resultValue.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, widgetID);

        // отрицательный ответ
        setResult(RESULT_CANCELED, resultValue);

        notes = new ArrayList<>();
        notes.addAll(DbHandler.getInstance(App.getContext()).readAllNotes());
        List<Rubric> rubrics = DbHandler.getInstance(App.getContext()).readRubrics();
        Iterator<Note> iterator = notes.iterator();
        while (iterator.hasNext()) {
            Note n = iterator.next();
            for (Rubric r : rubrics) {
                if (r.getId() == n.getIdRubric() && !r.getPwd().isEmpty()) {
                    iterator.remove();
                }
            }
        }
        mWidgetNotesAdapter = new WidgetActivityPrefAdapter(notes, getLayoutInflater(), this);
        mWidgetNotesRv.setLayoutManager(new LinearLayoutManager(this));
        mWidgetNotesRv.setAdapter(mWidgetNotesAdapter);
    }

    private void showProMessage() {
        Snackbar snackbar = Snackbar.make(
                findViewById(R.id.widget_cong_header_ll),
                getString(R.string.choose_color_pro),
                Snackbar.LENGTH_LONG
        );
        snackbar.setAction(
                R.string.get_pro,
                v1 -> {
//                    Intent intent = new Intent(App.getContext(), BillingActivity.class);
//                    startActivity(intent);
                });
        snackbar.setActionTextColor(
                ContextCompat.getColor(App.getContext(), R.color.yellow_plus)
        );
        snackbar.show();
    }

    @Override
    public void clickOnNote(String dateOfCreateNoteId, String content) {
        SharedPreferences sp = getSharedPreferences(ConstantStorage.WIDGET_PREF, MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.putString(ConstantStorage.WIDGET_NOTE_ID + widgetID, dateOfCreateNoteId);
//        editor.putString(ConstantStorage.WIDGET_NOTE_CONTENT + widgetID, dateOfCreateNoteId);
        editor.putInt(ConstantStorage.WIDGET_COLOR_MAIN + widgetID, colorBackground);
        editor.putInt(ConstantStorage.WIDGET_COLOR_TEXT + widgetID, colorText);
        editor.commit();

        // положительный ответ
        setResult(RESULT_OK, resultValue);

        Bloknote.simpleLog("ConfigWidgetActivity clickOnNote " + dateOfCreateNoteId);
        finish();
        AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(this);
        WidgetListProvider.updateWidget(this, appWidgetManager, widgetID);
    }


}