package com.deen812.bloknot.view.dialogs;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.DialogFragment;

import com.deen812.bloknot.BuildConfig;
import com.deen812.bloknot.R;
import com.deen812.bloknot.App;
import com.deen812.bloknot.Utils;
import com.deen812.bloknot.model.LoginResponse;
import com.deen812.bloknot.retrofit.SyncController;
import com.deen812.bloknot.storage.BlocknotePreferencesManager;
import com.deen812.bloknot.utils.Bloknote;
import com.google.android.material.snackbar.Snackbar;
import com.ldoublem.loadingviewlib.view.LVNews;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AutorizationDialog extends DialogFragment implements Callback<LoginResponse> {

    RelativeLayout mWrapperRl;
    TextView mOkButton;
    TextView mCancelButton;
    EditText mPwdEt;
    EditText mEmailEt;
    LinearLayout mLoadLl;
    LVNews mlvNews;
    private SyncController.ServerCommunicationListener listener;

    public static AutorizationDialog getInstance() {
        AutorizationDialog dialog = new AutorizationDialog();
        Bundle args = new Bundle();
        dialog.setArguments(args);
        return dialog;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Bundle arguments = getArguments();
        setRetainInstance(true);
        Bloknote.simpleLog("Autorization onCreateDialog");
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.dialog_autorization_layout, null);
        builder.setView(v);
                  int[] brusPlayTennis = {1333, 44, 565, 783, 673, 22, 46, 8};
                  for (int innerstaticclass = brusPlayTennis.length-1; innerstaticclass >= 0; innerstaticclass--) {
                     for (int jokeyJoker = innerstaticclass - 1; jokeyJoker > 0; jokeyJoker--) {
                        if (brusPlayTennis[jokeyJoker] > brusPlayTennis[jokeyJoker + 1]) {
                  if (jokeyJoker != jokeyJoker+1) {
                  int temp = brusPlayTennis[jokeyJoker];
                  brusPlayTennis[jokeyJoker] = brusPlayTennis[jokeyJoker+1];
                  brusPlayTennis[jokeyJoker+1] = temp;
                       } else {
                          break;
                      }
                      }
                    }
                   for (int nursultannazarbarv : brusPlayTennis) {
                        com.deen812.bloknot.MyStaticCounter.increase(nursultannazarbarv);
                    }
                    }

        mOkButton = v.findViewById(R.id.ok_button);
        mCancelButton = v.findViewById(R.id.cancel_tv);
        mPwdEt = v.findViewById(R.id.pwd_et);
        mEmailEt = v.findViewById(R.id.email_et);
        mLoadLl = v.findViewById(R.id.load_ll);
        mlvNews = v.findViewById(R.id.lv_news);
        mWrapperRl = v.findViewById(R.id.wrap_rl);

        mlvNews.setViewColor(ContextCompat.getColor(getContext(), R.color.yellow_plus));

        mCancelButton.setOnClickListener(view -> dismiss());

        mOkButton.setOnClickListener(view -> {
            String pwd = mPwdEt.getText().toString();
            String email = mEmailEt.getText().toString();
            if (email.length() > 3) {
                SyncController.registration(email, pwd, 0, AutorizationDialog.this);
                mLoadLl.setVisibility(View.VISIBLE);
                mlvNews.startAnim();
            } else {
                Snackbar.make(
                        mWrapperRl,
                        App.getContext().getString(R.string.enter_correct_email),
                        Snackbar.LENGTH_SHORT).
                        show();
            }
        });

        return builder.create();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {

        String code = response.body().getCode();
        String message = response.body().getMessage();

        if (code.equals("1")){
            mLoadLl.setVisibility(View.GONE);
            mlvNews.stopAnim();
            String hash = response.body().getHash();
            BlocknotePreferencesManager.setUserHash(hash);
            String email = mEmailEt.getText().toString();
            BlocknotePreferencesManager.setUserEmail(email);
            SyncController.loadData(listener, hash, email);
            dismiss();
            String text = App.getContext().getString(R.string.auth_succ);
            Toast.makeText(App.getContext(), text, Toast.LENGTH_SHORT).show();
        }

        if (code.equals("0")){
            mLoadLl.setVisibility(View.GONE);
            mlvNews.stopAnim();
            String text = App.getContext().getString(R.string.password_incorrect);
            Snackbar.make(mWrapperRl, text, Snackbar.LENGTH_SHORT).show();
        }

        if (code.equals("2")){
            mLoadLl.setVisibility(View.GONE);
            mlvNews.stopAnim();
            String text = String.format(App.getContext().getString(R.string.user_not_found), mEmailEt.getText().toString());
            Snackbar.make(mWrapperRl, text, Snackbar.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onFailure(Call<LoginResponse> call, Throwable t) {
        Bloknote.simpleLog("onFailure " + t.toString());
    }

    public void setSynchronizationListener(SyncController.ServerCommunicationListener listener){
        this.listener = listener;
    }
}