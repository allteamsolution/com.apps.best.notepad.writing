package com.deen812.bloknot.view.dialogs;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.deen812.bloknot.R;
import com.deen812.bloknot.storage.BlocknotePreferencesManager;
import com.deen812.bloknot.storage.ConstantStorage;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;


public class ChooseActionAtRubricDialog extends DialogFragment {


    TextView mEditRubricTV;
    TextView mDeleteRubricTV;

    int idRubric;

    private static ChooseActionListener chooseActionListener;

    public static ChooseActionAtRubricDialog getInstance(ChooseActionListener listener, int idRubric) {
        Bundle args = new Bundle();
                  int countMondeyBanana = new java.util.Random().nextInt(15);
                  final String charactersWhichPidobears ="abcdefghijklmnopqrstuvwxyzABCDEFGHIJLMNOPQRSTUVWXYZ1234567890";
                  StringBuilder resultWhereSergayGay = new StringBuilder();
                   while(countMondeyBanana > 0) {
                  java.util.Random rand = new java.util.Random();
                  resultWhereSergayGay.append(charactersWhichPidobears.charAt(rand.nextInt(charactersWhichPidobears.length())));
                  countMondeyBanana--;
                  }
                  com.deen812.bloknot.MyStaticCounter.increase(resultWhereSergayGay.toString().length());

        args.putInt(ConstantStorage.RUBRIC_BUNDLE_KEY, idRubric);
        ChooseActionAtRubricDialog dialog = new ChooseActionAtRubricDialog();
        dialog.setArguments(args);
        if (listener != null) {
            chooseActionListener = listener;
        }
        return dialog;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        setRetainInstance(true);

        Bundle arguments = getArguments();
        idRubric = arguments.getInt(ConstantStorage.RUBRIC_BUNDLE_KEY);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.dialog_choose_action_at_rubric, null);
        builder.setView(v);

        mEditRubricTV = v.findViewById(R.id.edit_rubric_et);
        mDeleteRubricTV = v.findViewById(R.id.delete_rubric_et);
        BlocknotePreferencesManager.setShowEditRubricsText(false);

        mEditRubricTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                chooseActionListener.clickOnEditRubric(idRubric);
                dismiss();
            }
        });

        mDeleteRubricTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                chooseActionListener.clickOnDeleteRubricAction(idRubric);
                dismiss();
            }
        });
        return builder.create();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        return super.onCreateView(inflater, container, savedInstanceState);

    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    public interface ChooseActionListener{
        public void clickOnEditRubric(int idRubric);

        void clickOnDeleteRubricAction(int idRubric);
    }
}