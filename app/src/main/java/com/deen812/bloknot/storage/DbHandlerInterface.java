package com.deen812.bloknot.storage;

import com.deen812.bloknot.model.Alarm;
import com.deen812.bloknot.model.ChecklistItem;
import com.deen812.bloknot.model.Image;
import com.deen812.bloknot.model.Note;
import com.deen812.bloknot.model.Rubric;

import java.util.List;
import java.util.Map;

public interface DbHandlerInterface {

    long addNote(Note note);
    Note getNote(int Id);
    List<Note> readAllNotes();
    int getNotesCount();
    void updateNote(Note note);

    Note getNote(String dateOfCreateId);

    List<Note> getAllNotesFromRubric(int idRubric);
    boolean deleteNote(int id);
    void deleteAllNotes();

    void addRubric(Rubric rubric);
    Rubric readRubric(int id);
    void updateRubric(Rubric rubric);
    void deleteRubric(int id);
    List<Rubric> readRubrics();

    int addImage(Image image);
    Image readImage(int id);
    List<Image> readImagesFromNote(String idNoteDateCreate);
    void deleteImagesFromNote(String idNoteDateCreate);
    void updateImage(Image image);
    void deleteImage(int idImage);
    List<Image> readAllImages();

    int addChecklistItem(ChecklistItem item);
    ChecklistItem readChecklistItem(int id);
    List<ChecklistItem> readChecklistItemsFromNote(String idNote);
    void updateChecklistItem(ChecklistItem item);
    void deleteChecklistItem(int idChecklistItem);
    void deleteAllChecklistItemsFromNote(String idNote);
    List<ChecklistItem> readAllChecklistItems();

    void deleteAlarm(int id);

    void deleteAllAlarmsFromNote(int noteId);

    void close();

    Map<Integer, String> getMapNamesOfRubrics();
    List<Note> getCacheNotes(boolean returnAllNotes);

    //region =========== ALARMS ===========
    int addAlarm(Alarm alarm);
    List<Alarm> getAlarmsFromNote(int noteId);
    List<Alarm> readAllAlarms();
}