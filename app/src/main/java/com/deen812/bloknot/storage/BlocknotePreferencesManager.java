package com.deen812.bloknot.storage;

import android.content.Context;
import android.content.SharedPreferences;

import com.deen812.bloknot.R;

public class BlocknotePreferencesManager {

    private static final String APP_FIRST_START = "bloknot.APP_FIRST_START";
    private static final String PREFERENCES = "PREFERENCES";
    private static final String APP_LAYOUT_IS_GRID = "APP_LAYOUT_IS_GRID";
    private static final String APP_USER_EMAIL = "user_email";
    private static final String APP_USER_HASH = "user_hash";
    private static final String APP_SORT_NOTES = "APP_SORT_NOTES";
    private static final String APP_SAVE_TO_FILE = "APP_SAVE_TO_FILE";
    private static final String APP_SIZE_LINE_NOTE = "APP_SIZE_LINE_NOTE";
    private static final String APP_SIZE_CONTENT = "APP_SIZE_CONTENT";
    private static final String APP_SIZE_TITLE = "APP_SIZE_TITLE";
    private static final String APP_FONT = "APP_FONT";
    private static final String APP_ACTION_AFTER_START = "APP_ACTION_AFTER_START";
    private static final String APP_STRATEGY_LOCK = "APP_STRATEGY_LOCK";
    private static final String APP_THEME = "APP_THEME";
    private static final String START_COUNT = "START_COUNT";
    private static final String PRO_UNLOCK = "PRO_UNLOCK";
    private static final String SHOW_DETAIL = "SHOW_DETAIL";
    private static final String CUSTOM_COLOR = "CUSTOM_COLOR";
    private static final String COLOR_EXPERIENS = "COLOR_EXPERIENS";
    private static final String FILE_HISTORY = "FILE_HISTORY";
    private static final String SHOW_LOCK_TITLE = "SHOW_LOCK_TITLE";
    private static final String LOCK_APP = "LOCK_APP";
    private static final String EDIT_RUBRICS_SHOW = "EDIT_RUBRICS_SHOW";
    private static final String LOCK_PASSWORD = "LOCK_PASSWORD";
    private static BlocknotePreferencesManager instance;
    private static SharedPreferences mySharedPreferences;
    private static final String SHOW_DETAIL_NOTE = "SHOW_DETAIL_NOTE";

    private BlocknotePreferencesManager(Context context) {
        mySharedPreferences = context.getSharedPreferences(PREFERENCES, Context.MODE_PRIVATE);
    }

    public static BlocknotePreferencesManager init(Context context){
        if(instance == null){
            instance = new BlocknotePreferencesManager(context);
        }
        return instance;
    }

    public static boolean getFirstStart() {
        return mySharedPreferences.getBoolean(APP_FIRST_START, true);
    }

    public static void setFirstStart(boolean value){
        mySharedPreferences.edit().putBoolean(APP_FIRST_START, value).apply();
    }

    public static boolean getLayoutIsGrid() {
        return mySharedPreferences.getBoolean(APP_LAYOUT_IS_GRID, false);
    }

    public static void setLayoutIsGrid(boolean value) {
        mySharedPreferences.edit().putBoolean(APP_LAYOUT_IS_GRID, value).apply();
    }

    public static String getUserEmail() {
        return mySharedPreferences.getString(APP_USER_EMAIL, "");
    }

    public static void setUserEmail(String value) {
        mySharedPreferences.edit().putString(APP_USER_EMAIL, value).apply();
    }

    public static String getUserHash() {
        return mySharedPreferences.getString(APP_USER_HASH, "");
    }

    public static void setUserHash(String value) {
        mySharedPreferences.edit().putString(APP_USER_HASH, value).apply();
    }

    public static int getSortNotes() {
        return mySharedPreferences.getInt(APP_SORT_NOTES, 0);
    }

    public static void setSortNotes(int value) {
        mySharedPreferences.edit().putInt(APP_SORT_NOTES, value).apply();
    }

    public static boolean getSaveToFile() {
        return mySharedPreferences.getBoolean(APP_SAVE_TO_FILE, true);
    }

    public static void setSaveToFile(boolean value) {
        mySharedPreferences.edit().putBoolean(APP_SAVE_TO_FILE, value).apply();
    }

    public static int getSizeLineNote() {
        return mySharedPreferences.getInt(APP_SIZE_LINE_NOTE, 3);
    }

    public static void setSizeLineNote(int value) {
        mySharedPreferences.edit().putInt(APP_SIZE_LINE_NOTE, value).apply();
    }

    public static int getContentSize() {
        return mySharedPreferences.getInt(APP_SIZE_CONTENT, 18);

    }

    public static void setContentSize(int value) {
        mySharedPreferences.edit().putInt(APP_SIZE_CONTENT, value).apply();
    }

    public static int getTitleSize() {
        return mySharedPreferences.getInt(APP_SIZE_TITLE, 22);
    }

    public static void setTitleSize(int value) {
        mySharedPreferences.edit().putInt(APP_SIZE_TITLE, value).apply();
    }

    public static void setActionAfterStart(int value) {
        mySharedPreferences.edit().putInt(APP_ACTION_AFTER_START, value).apply();

    }
    public static int getActionAfterStart() {
        return mySharedPreferences.getInt(APP_ACTION_AFTER_START, 0);
    }

    public static void setStrategyLock(int value) {
        mySharedPreferences.edit().putInt(APP_STRATEGY_LOCK, value).apply();
    }

    public static int getStrategyLock() {
        return mySharedPreferences.getInt(APP_STRATEGY_LOCK, ConstantStorage.LOCK_ONE_TIME);
    }

    public static void setTheme(int value) {
        mySharedPreferences.edit().putInt(APP_THEME, value).apply();
    }

    public static int getTheme() {
        return mySharedPreferences.getInt(APP_THEME, ConstantStorage.DEFAULT_THEME);
    }

    public static int getStartCount() {
        return mySharedPreferences.getInt(START_COUNT, 0);
    }

    public static void setStartCount(int value) {
        mySharedPreferences.edit().putInt(START_COUNT, value).apply();
    }

//    public static String getPro() {
//        return mySharedPreferences.getString(PRO_UNLOCK, null);
//    }

    public static void setPro(String value) {
        mySharedPreferences.edit().putString(PRO_UNLOCK, value).apply();
    }

    public static void setDetailShow(boolean value) {
        mySharedPreferences.edit().putBoolean(SHOW_DETAIL, value).apply();

    }

    public static boolean getDetailShow() {
        return mySharedPreferences.getBoolean(SHOW_DETAIL, true);
    }

    public static void setCustomColor(int value) {
        mySharedPreferences.edit().putInt(CUSTOM_COLOR, value).apply();

    }

    public static int getCustomColor() {
        return mySharedPreferences.getInt(CUSTOM_COLOR, 0);
    }

    public static void setColorExperiens(boolean value) {
        mySharedPreferences.edit().putBoolean(COLOR_EXPERIENS, value).apply();
    }

    public static boolean getColorExperiens() {
        return mySharedPreferences.getBoolean(COLOR_EXPERIENS, false);
    }

    public static int getShowDetail() {
        return mySharedPreferences.getInt(SHOW_DETAIL_NOTE, ConstantStorage.SHOW_ALL);
    }

    public static void setShowDetail(int value) {
        mySharedPreferences.edit().putInt(SHOW_DETAIL_NOTE, value).apply();
    }

    public static void setSaveToFileHistory(boolean value) {
        mySharedPreferences.edit().putBoolean(FILE_HISTORY, value).apply();
    }

    public static boolean getSaveToFileHistory() {
        return mySharedPreferences.getBoolean(FILE_HISTORY, true);
    }

    public static void setShowLockTitle(boolean value) {
        mySharedPreferences.edit().putBoolean(SHOW_LOCK_TITLE, value).apply();
    }

    public static boolean getShowLockTitle() {
        return mySharedPreferences.getBoolean(SHOW_LOCK_TITLE, false);
    }

    public static void setShowEditRubricsText(boolean value) {
        mySharedPreferences.edit().putBoolean(EDIT_RUBRICS_SHOW, value).apply();

    }

    public static boolean getShowInfoEditRubrics() {
        return mySharedPreferences.getBoolean(EDIT_RUBRICS_SHOW, true);
    }

    public static void setLockPassword(String value) {
        mySharedPreferences.edit().putString(LOCK_PASSWORD, value).apply();

    }

    public static String getLockPassword() {
        return mySharedPreferences.getString(LOCK_PASSWORD, "");
    }
}