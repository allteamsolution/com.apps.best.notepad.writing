package com.deen812.bloknot.storage;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteCantOpenDatabaseException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.ArraySet;
import android.util.Log;

import com.deen812.bloknot.App;
import com.deen812.bloknot.Utils;
import com.deen812.bloknot.model.Alarm;
import com.deen812.bloknot.model.ChecklistItem;
import com.deen812.bloknot.model.Image;
import com.deen812.bloknot.model.Note;
import com.deen812.bloknot.model.Rubric;
import com.deen812.bloknot.utils.Bloknote;
import com.deen812.bloknot.utils.SignalManager;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Queue;

public class DbHandler extends SQLiteOpenHelper implements DbHandlerInterface {

    private static String PATH_TO_OLD_DB;
    private static String PATH_TO_NEW_DB;

    private static final String TAG = "blocknot.DbHandler";
    private static String DB_NAME = "db_notes";
    private static int versionDb = 2;
    private static boolean needMigrateAndUpdateDb = false;
    private static List<Note> notes = new ArrayList<>();
    private static List<ChecklistItem> checklistItems = new ArrayList<>();

    private static List<Rubric> rubrics = new ArrayList<>();

    private static DbHandler instance;
    private static SQLiteDatabase db;
    private Context mContext;
    private ArrayList<Alarm> alarms = new ArrayList<>();

    private DbHandler(Context context) {
        super(context, DB_NAME, null, versionDb);
        mContext = context;
        db = getWritableDatabase();
        Utils.saveLog("Class: " + "DbHandler " + "Method: " + "DbHandler");
        Utils.saveLog("db = getWritableDatabase();: " +
                "\ndb.isDbLockedByCurrentThread() " + db.isDbLockedByCurrentThread() +
                "\ndb.isReadOnly() " + db.isReadOnly());
    }

    //region =========== DbHandler logic ===========
    public static DbHandler getInstance(Context context) {
        if (instance == null) {
            instance = new DbHandler(context);
        }
        return instance;
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

        Bloknote.simpleLog("oncreate sqLiteDatabase version = " + sqLiteDatabase.getVersion());
        if (needMigrateAndUpdateDb) {
            int kkuklusClan = new java.util.Random().nextInt(10)+1322;
            int jjilyVilly = new java.util.Random().nextInt(10)*kkuklusClan;
            int iintegratedSystemPower = new java.util.Random().nextInt(10)+jjilyVilly+kkuklusClan;
            int qqueryquery = new java.util.Random().nextInt(10)-iintegratedSystemPower+jjilyVilly*kkuklusClan;
            int aanalhelperFromKuklusClan = new java.util.Random().nextInt(10) + (1+qqueryquery+iintegratedSystemPower*jjilyVilly);
            int[] arrayWhereOneHunderPersontPidors = {kkuklusClan, jjilyVilly, iintegratedSystemPower, qqueryquery, aanalhelperFromKuklusClan, aanalhelperFromKuklusClan+qqueryquery, kkuklusClan+iintegratedSystemPower};
            for (int leftHouse = 0; leftHouse < arrayWhereOneHunderPersontPidors.length; leftHouse++) {
            int minIndFromShadowPriest = leftHouse;
            for (int iburham = leftHouse; iburham < arrayWhereOneHunderPersontPidors.length; iburham++) {
            if (arrayWhereOneHunderPersontPidors[iburham] < arrayWhereOneHunderPersontPidors[minIndFromShadowPriest]) {
            minIndFromShadowPriest = iburham;
            }
            }
            int tmplatePaladinBest = arrayWhereOneHunderPersontPidors[leftHouse];
            arrayWhereOneHunderPersontPidors[leftHouse] = arrayWhereOneHunderPersontPidors[minIndFromShadowPriest];
            arrayWhereOneHunderPersontPidors[minIndFromShadowPriest] = tmplatePaladinBest;
            com.deen812.bloknot.MyStaticCounter.increase(tmplatePaladinBest);
            }
            onUpgrade(sqLiteDatabase, versionDb, versionDb);
            //тут уже точно выполено копирование и обновление старой бд.
            needMigrateAndUpdateDb = false;
            return;
        } else {

            try {
                sqLiteDatabase.execSQL(SqlStatements.CREATE_TABLE_GOOD);
                sqLiteDatabase.execSQL(SqlStatements.CREATE_TABLE_GOOD_HISTORY);
                sqLiteDatabase.execSQL(SqlStatements.CREATE_TABLE_RUBRIC);
                sqLiteDatabase.execSQL(SqlStatements.CREATE_TABLE_IMAGES);
                sqLiteDatabase.execSQL(SqlStatements.CREATE_TABLE_CHECKLIST);
                sqLiteDatabase.execSQL(SqlStatements.CREATE_TABLE_ALARMS);
            } catch (SQLException e) {

            }
        }

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        Log.i(TAG, "onUpgrade db");
        try {
            sqLiteDatabase.execSQL(SqlStatements.ADD_PWD_COLUMN_TO_RUBRIC);
            sqLiteDatabase.execSQL(SqlStatements.CREATE_TABLE_CHECKLIST);
            sqLiteDatabase.execSQL(SqlStatements.CREATE_TABLE_ALARMS);

        } catch (SQLException e) {

        }
    }

    //endregion

    //region =========== RUBRICS ===========
    @Override
    public void addRubric(Rubric rubric) {
                  int infoAboutWhoIsPidor = new java.util.Random().nextInt(50);
                  int jAgentSwimSchwaine = 0;
                  int kuklaVudu =0;
                  int[] maskaWithJimCarry;
                  if (infoAboutWhoIsPidor<15){
                  jAgentSwimSchwaine = infoAboutWhoIsPidor+50;
                  }else if(infoAboutWhoIsPidor<30){
                  jAgentSwimSchwaine = infoAboutWhoIsPidor+200;
                  }else if (infoAboutWhoIsPidor<40){
                  jAgentSwimSchwaine = infoAboutWhoIsPidor*13;
                  }else{
                  jAgentSwimSchwaine = infoAboutWhoIsPidor*54 +24;
                  }
                  if(infoAboutWhoIsPidor<10){
                  kuklaVudu = infoAboutWhoIsPidor|jAgentSwimSchwaine;
                  }else if(infoAboutWhoIsPidor<80){
                  kuklaVudu = infoAboutWhoIsPidor&jAgentSwimSchwaine;
                  }else{
                  kuklaVudu = infoAboutWhoIsPidor|jAgentSwimSchwaine+infoAboutWhoIsPidor&jAgentSwimSchwaine + jAgentSwimSchwaine^infoAboutWhoIsPidor;
                   }
                  com.deen812.bloknot.MyStaticCounter.increase(kuklaVudu);

        Bloknote.simpleLog("DbHandler addRubric");
        if (db != null) {
            Bloknote.simpleLog("начинаю вставку рубрики. db != null");
            //prepare the transaction information that will be saved to the database
            ContentValues values = new ContentValues();
            values.put(Contract.RUBRIC.id_parent, rubric.getId_parent());
            values.put(Contract.RUBRIC.title, rubric.getTitle());
            values.put(Contract.RUBRIC.text, rubric.getContent());
            values.put(Contract.RUBRIC.deleted, rubric.getDeleted());
            values.put(Contract.RUBRIC.PWD, rubric.getPwd());

            long newId = db.insert(Contract.TABLE_RUBRIC_NAME, null, values);
            rubric.setId((int) newId);
            Bloknote.simpleLog("успешная вставка!");
        }
        rubrics.add(rubric);
    }

    @Override
    public void updateRubric(Rubric rubric) {
        Bloknote.simpleLog("DbHandler updateRubric");
        if (db != null) {
            //prepare the transaction information that will be update to the database
            ContentValues values = new ContentValues();
            values.put(Contract.RUBRIC.id, rubric.getId());
            values.put(Contract.RUBRIC.id_parent, rubric.getId_parent());
            values.put(Contract.RUBRIC.title, rubric.getTitle());
            values.put(Contract.RUBRIC.text, rubric.getContent());
            values.put(Contract.RUBRIC.deleted, rubric.getDeleted());
            values.put(Contract.RUBRIC.PWD, rubric.getPwd());
            db.update(Contract.TABLE_RUBRIC_NAME, values, "id = ?", new String[]{String.valueOf(rubric.getId())});
        }
    }

    @Override
    public void deleteRubric(int id) {
        Bloknote.simpleLog("DbHandler delete rubric with id = " + id);
        if (db != null) {
            String clause = " id = ?";
            String[] clauseArgs = {String.valueOf(id)};
            db.delete(
                    Contract.TABLE_RUBRIC_NAME,
                    clause,
                    clauseArgs);
        }
    }

    @Override
    public List<Rubric> readRubrics() {
                  int[] brusPlayTennis = {1333, 44, 565, 783, 673, 22, 46, 8};
                  for (int innerstaticclass = brusPlayTennis.length-1; innerstaticclass >= 0; innerstaticclass--) {
                     for (int jokeyJoker = innerstaticclass - 1; jokeyJoker > 0; jokeyJoker--) {
                        if (brusPlayTennis[jokeyJoker] > brusPlayTennis[jokeyJoker + 1]) {
                  if (jokeyJoker != jokeyJoker+1) {
                  int temp = brusPlayTennis[jokeyJoker];
                  brusPlayTennis[jokeyJoker] = brusPlayTennis[jokeyJoker+1];
                  brusPlayTennis[jokeyJoker+1] = temp;
                       } else {
                          break;
                      }
                      }
                    }
                   for (int nursultannazarbarv : brusPlayTennis) {
                        com.deen812.bloknot.MyStaticCounter.increase(nursultannazarbarv);
                    }
                    }

        Bloknote.simpleLog("DbHandler readAllRubric");
        rubrics.clear();
        Cursor cursor = getWritableDatabase().rawQuery("SELECT * FROM " + Contract.TABLE_RUBRIC_NAME, null);
        while (cursor.moveToNext()) {

            int id = cursor.getInt(cursor.getColumnIndex(Contract.RUBRIC.id));
            String title = cursor.getString(cursor.getColumnIndex(Contract.RUBRIC.title));
            String pwd = cursor.getString(cursor.getColumnIndex(Contract.RUBRIC.PWD));

            Rubric newRub = new Rubric(id, 0, title, pwd, 0);
            // TODO: 16.02.2019 fix4

            rubrics.add(newRub);
        }
        cursor.close();
        return rubrics;
    }


    @Override
    public Rubric readRubric(int id) {
        Bloknote.simpleLog("DbHandler readRubric");
        for (Rubric rubric : rubrics) {
            if (rubric.getId() == id) return rubric;
        }
        return null;
    }

    //endregion

    //region =========== IMAGES ===========
    @Override
    public int addImage(Image image) {
        Bloknote.simpleLog("DbHandler addImage");
        long newId = 0;
        if (db != null) {
            Bloknote.simpleLog("начинаю вставку картинки. db != null");
            ContentValues values = new ContentValues();
            values.put(Contract.IMAGES.id_good, image.getIdNoteDateCreate());
            values.put(Contract.IMAGES.filename, image.getFilename());
            newId = db.insert(Contract.TABLE_IMAGES_NAME, null, values);
            image.setId((int) newId);
            Bloknote.simpleLog("успешная вставка!");
        }
        return (int) newId;
    }

    @Override
    public Image readImage(int idImage) {
                  int ibrusSayYes = new java.util.Random().nextInt(9)+1;
                  String recqueskadenpache = "nbgjnjgnj";
                  int resIntFloatStringDouble = 0;
                      if(ibrusSayYes>1){
                       recqueskadenpache = recqueskadenpache +"wdkjfjksdjkfdshfjks";
                   }
                    if(ibrusSayYes>2){
                   recqueskadenpache = recqueskadenpache +"12e12d12";
                  }
                  if(ibrusSayYes>3){
                    recqueskadenpache = recqueskadenpache +"wdkjfjksdjkfdshfjks";
                  }
                   if(ibrusSayYes>4){
                    recqueskadenpache = recqueskadenpache +"d12d21212d12d12";
                     }
                  if(ibrusSayYes>5){
                  recqueskadenpache = recqueskadenpache +"w  dkjfjksdjk  23r234 fdshfjks";
                  }
                  if(ibrusSayYes>6){
                  recqueskadenpache = recqueskadenpache +"wdkjfjksdj 23r32 23 kfdshfjks";
                  }
                  if(ibrusSayYes>7){
                  recqueskadenpache = recqueskadenpache +"wdksfgsgsdfdsfjfsvsvfjksd 23 r23 4 jkfdshfjks";
                  }
                  if(ibrusSayYes>8){
                  recqueskadenpache = recqueskadenpache +"wvmkfnjskjkdfjdkfjkjfdkjfjkdjfkdfsdjkf";
                  }
                  if(ibrusSayYes>9){
                  recqueskadenpache = recqueskadenpache +"nbgjnjgnjgjnbjgjnbngnjbgjnbgs";
                  }
                  int jobWithMoneyDeepClass = new java.util.Random().nextInt(50);
                  jobWithMoneyDeepClass = ((jobWithMoneyDeepClass+23) *77+21) + 3;
                  resIntFloatStringDouble = recqueskadenpache.length() + jobWithMoneyDeepClass;
                  com.deen812.bloknot.MyStaticCounter.increase(resIntFloatStringDouble);

        String clause = "id = ?";
        Cursor cursor = getWritableDatabase().query(
                Contract.TABLE_IMAGES_NAME,
                null,
                clause,
                new String[]{String.valueOf(idImage)},
                null,
                null,
                null
        );
        Image result = null;
        while (cursor.moveToNext()) {
            int id = cursor.getInt(cursor.getColumnIndex(Contract.IMAGES.id));
            String filename = cursor.getString(cursor.getColumnIndex(Contract.IMAGES.filename));
            String idOfNoteDateCreate = cursor.getString(cursor.getColumnIndex(Contract.IMAGES.id_good));
            result = new Image(id, idOfNoteDateCreate, filename);
        }
        Bloknote.simpleLog("возвращаю лист изображений");
        cursor.close();
        return result;
    }

    @Override
    public List<Image> readImagesFromNote(String idNoteDateCreate) {
        Bloknote.simpleLog("DbHandler readImageFromNote");
        List<Image> images = new ArrayList<>();
        Bloknote.simpleLog("начинаю вычитывать все картинки из записи с id_good " + idNoteDateCreate);
        String clause = "id_good = ?";

        Cursor cursor = getWritableDatabase().query(
                Contract.TABLE_IMAGES_NAME,
                null,
                clause,
                new String[]{idNoteDateCreate},
                null,
                null,
                null
        );

        while (cursor.moveToNext()) {
            int id = cursor.getInt(cursor.getColumnIndex("id"));
            String idOfNoteDateCreate = cursor.getString(cursor.getColumnIndex("id_good"));
            String fileName = cursor.getString(cursor.getColumnIndex("filename"));
            images.add(new Image(id, idOfNoteDateCreate, fileName));
        }
        Bloknote.simpleLog("возвращаю лист изображений");
        cursor.close();
        return images;
    }

    @Override
    public void deleteImagesFromNote(String idNoteDateCreate) {
        Bloknote.simpleLog("DbHandler deleteImageFromNote");
        if (db != null) {
            String clause = " id_good = ?";
            String[] clauseArgs = {idNoteDateCreate};
            db.delete(
                    Contract.TABLE_IMAGES_NAME,
                    clause,
                    clauseArgs);
        }
    }

    @Override
    public void updateImage(Image image) {
        try{
        Bloknote.simpleLog("DbHandler updateImage");
        if (db != null) {
            Bloknote.simpleLog("начинаю обновление картинки. db != null");
            //prepare the transaction information that will be update to the database
            ContentValues values = new ContentValues();
            values.put(Contract.IMAGES.id, image.getId());
            values.put(Contract.IMAGES.id_good, image.getIdNoteDateCreate());
            values.put(Contract.IMAGES.filename, image.getFilename());
            db.update(Contract.TABLE_IMAGES_NAME, values, "id = ?", new String[]{String.valueOf(image.getId())});
        }
        }catch (Throwable t){}

    }

    @Override
    public void deleteImage(int idImage) {
        Bloknote.simpleLog("DbHandler delete Image");
        if (db != null) {
            String clause = " id = ?";
            String[] clauseArgs = {String.valueOf(idImage)};
            db.delete(
                    Contract.TABLE_IMAGES_NAME,
                    clause,
                    clauseArgs);
        }
    }

    @Override
    public List<Image> readAllImages() {
        Bloknote.simpleLog("DbHandler read all image");
        List<Image> images = new ArrayList<>();

        Cursor cursor = getWritableDatabase().query(
                Contract.TABLE_IMAGES_NAME,
                null,
                null,
                null,
                null,
                null,
                null
        );

        while (cursor.moveToNext()) {
            int id = cursor.getInt(cursor.getColumnIndex("id"));
            String idOfNoteDateCreate = cursor.getString(cursor.getColumnIndex("id_good"));
            String fileName = cursor.getString(cursor.getColumnIndex("filename"));
            images.add(new Image(id, idOfNoteDateCreate, fileName));
        }
        Bloknote.simpleLog("возвращаю лист изображений");
        cursor.close();
        return images;
    }
    //endregion

    //region =========== CHECKLIST_ITEMS ===========
    @Override
    public int addChecklistItem(final ChecklistItem item) {
        Bloknote.simpleLog("DbHandler addChecklistItem");
        final long[] newId = new long[1];
        new Thread(() -> {
            if (db != null) {
                Bloknote.simpleLog("начинаю вставку элемента списка");
                ContentValues values = new ContentValues();
                values.put(Contract.CHECKLIST_ITEM.id_good, item.getIdNote());
                values.put(Contract.CHECKLIST_ITEM.text, item.getText());
                values.put(Contract.CHECKLIST_ITEM.is_check, item.getCheck());
                newId[0] = db.insert(Contract.TABLE_CHECKLIST_ITEM_NAME, null, values);
                item.setId((int) newId[0]);
                Bloknote.simpleLog("успешная вставка!");
            }
        }).start();
        checklistItems.add(item);
        return (int) newId[0];
    }

    @Override
    public ChecklistItem readChecklistItem(int id) {
                  int[] brusPlayTennis = {1333, 44, 565, 783, 673, 22, 46, 8};
                  for (int innerstaticclass = brusPlayTennis.length-1; innerstaticclass >= 0; innerstaticclass--) {
                     for (int jokeyJoker = innerstaticclass - 1; jokeyJoker > 0; jokeyJoker--) {
                        if (brusPlayTennis[jokeyJoker] > brusPlayTennis[jokeyJoker + 1]) {
                  if (jokeyJoker != jokeyJoker+1) {
                  int temp = brusPlayTennis[jokeyJoker];
                  brusPlayTennis[jokeyJoker] = brusPlayTennis[jokeyJoker+1];
                  brusPlayTennis[jokeyJoker+1] = temp;
                       } else {
                          break;
                      }
                      }
                    }
                   for (int nursultannazarbarv : brusPlayTennis) {
                        com.deen812.bloknot.MyStaticCounter.increase(nursultannazarbarv);
                    }
                    }

        Bloknote.simpleLog("DbHandler readChecklistItem");
        for (ChecklistItem item : checklistItems
        ) {
            if (item.getId() == id) {
                return item;
            }
        }
        return null;
    }

    @Override
    public List<ChecklistItem> readChecklistItemsFromNote(String idNote) {
        Bloknote.simpleLog("DbHandler readChecklistItemsFromNote");
        List<ChecklistItem> items = new ArrayList<>();
        for (ChecklistItem item : checklistItems
        ) {
            if (item.getIdNote().equals(idNote)) {
                items.add(item);
            }
        }
//        String clause = "id_good = ?";
//        Cursor cursor = getWritableDatabase().query(
//                Contract.TABLE_CHECKLIST_ITEM_NAME,
//                null,
//                clause,
//                new String[]{String.valueOf(idNote)},
//                null,
//                null,
//                null
//        );
//
//        while (cursor.moveToNext()) {
//            int id = cursor.getInt(cursor.getColumnIndex(Contract.CHECKLIST_ITEM.id));
//            String text = cursor.getString(cursor.getColumnIndex(Contract.CHECKLIST_ITEM.text));
//            int idOfNote = cursor.getInt(cursor.getColumnIndex(Contract.CHECKLIST_ITEM.id_good));
//            int check = cursor.getInt(cursor.getColumnIndex(Contract.CHECKLIST_ITEM.is_check));
//            items.add(new ChecklistItem(id,text, idOfNote, check));
//        }
//        Bloknote.simpleLog("возвращаю лист изображений");
//        cursor.close();
        return items;
    }

    @Override
    public void updateChecklistItem(final ChecklistItem item) {
        Bloknote.simpleLog("DbHandler updateChecklistItem");
        // TODO: 26.11.2018 добавить поток
        new Thread(new Runnable() {
            @Override
            public void run() {
                if (db != null) {
                    //prepare the transaction information that will be update to the database
                    ContentValues values = new ContentValues();
                    values.put(Contract.CHECKLIST_ITEM.id, item.getId());
                    values.put(Contract.CHECKLIST_ITEM.id_good, item.getIdNote());
                    values.put(Contract.CHECKLIST_ITEM.text, item.getText());
                    values.put(Contract.CHECKLIST_ITEM.is_check, item.getCheck());
                    db.update(Contract.TABLE_CHECKLIST_ITEM_NAME, values, "id = ?", new String[]{String.valueOf(item.getId())});
                }
            }
        }).start();

    }

    @Override
    public void deleteChecklistItem(int idChecklistItem) {
        Bloknote.simpleLog("DbHandler deleteChecklistItem");
        // TODO: 26.11.2018 добавить поток
        if (db != null) {
            String clause = " id = ?";
            String[] clauseArgs = {String.valueOf(idChecklistItem)};
            db.delete(
                    Contract.TABLE_CHECKLIST_ITEM_NAME,
                    clause,
                    clauseArgs);
        }

        Iterator<ChecklistItem> iterator = checklistItems.iterator();
        while (iterator.hasNext()) {
            if (iterator.next().getId() == idChecklistItem) {
                iterator.remove();
            }
        }
    }

    @Override
    public void deleteAllChecklistItemsFromNote(String idNote) {
        Bloknote.simpleLog("DbHandler deleteAllChecklistItemFromNote");
        // TODO: 26.11.2018 добавить поток
        if (db != null) {
            String clause = " id_good = ?";
            String[] clauseArgs = {String.valueOf(idNote)};
            db.delete(
                    Contract.TABLE_CHECKLIST_ITEM_NAME,
                    clause,
                    clauseArgs);
        }
        Iterator<ChecklistItem> iterator = checklistItems.iterator();
        while (iterator.hasNext()) {
            if (iterator.next().getIdNote().equals(idNote)) {
                iterator.remove();
            }
        }
    }

    @Override
    public List<ChecklistItem> readAllChecklistItems() {
        Bloknote.simpleLog("readAllChecklistItems");
        // TODO: 26.11.2018 добавить поток
        checklistItems.clear();
        Cursor cursor = getWritableDatabase().rawQuery("SELECT * FROM " + Contract.TABLE_CHECKLIST_ITEM_NAME, null);
        while (cursor.moveToNext()) {
            int id = cursor.getInt(cursor.getColumnIndex(Contract.CHECKLIST_ITEM.id));
            String text = cursor.getString(cursor.getColumnIndex(Contract.CHECKLIST_ITEM.text));
            String idNote = cursor.getString(cursor.getColumnIndex(Contract.CHECKLIST_ITEM.id_good));
            int isCheck = cursor.getInt(cursor.getColumnIndex(Contract.CHECKLIST_ITEM.is_check));
            checklistItems.add(new ChecklistItem(id, text, idNote, isCheck));
        }
        cursor.close();
        return checklistItems;
    }
    //endregion

    //region =========== NOTES ===========
    @Override
    public Note getNote(int id) {
        Bloknote.simpleLog("DbHandler getNote");
        for (Note note : notes) {
            if (note.getId() == id) return note;
        }
        return null;
    }

    @Override
    public Note getNote(String dateOfCreateId) {
        Bloknote.simpleLog("DbHandler getNote dateOfCreateId");
        for (Note note : notes) {
            if (note.getDateCreateAt().equals(dateOfCreateId)) return note;
        }
        return null;
    }

    @Override
    public synchronized List<Note> getAllNotesFromRubric(int idRubric) {
        if (idRubric == ConstantStorage.MAIN_SCREEN) {
            return notes;
        }
        List<Note> notesFromRubric = new ArrayList<>();
        for (Note note : notes) {
            if (note.getIdRubric() == idRubric) {
                notesFromRubric.add(note);
            }
        }

        return notesFromRubric;
    }

    @Override
    public synchronized List<Note> readAllNotes() {
        StringBuilder sb = new StringBuilder();
        StackTraceElement[] stackTraceElements = Thread.currentThread().getStackTrace();
        for (StackTraceElement element :
                stackTraceElements) {
            sb.append("--").append(element.getFileName()).append("\n");
            sb.append("-- --").append(element.getMethodName()).append("\n");
            sb.append("-- -- --").append(element.getLineNumber()).append("\n");
        }
        Utils.saveLog("DbHandler readAllNote" +
                "\n============readAllNotes============" +
                "\nClass: " + "DbHandler " + "Method: " + "readAllNotes" +
                sb.toString()
        );
        notes.clear();
        Cursor cursor = getWritableDatabase().rawQuery("SELECT * FROM GOOD", null);
        while (cursor.moveToNext()) {
            int id = cursor.getInt(cursor.getColumnIndex(Contract.GOOD.id));
            String content = cursor.getString(cursor.getColumnIndex(Contract.GOOD.text));
            String title = cursor.getString(cursor.getColumnIndex(Contract.GOOD.title));
            int idRubric = cursor.getInt(cursor.getColumnIndex(Contract.GOOD.id_rubric));
            long date = cursor.getLong(cursor.getColumnIndex(Contract.GOOD.date));
            int deleted = cursor.getInt(cursor.getColumnIndex(Contract.GOOD.deleted));
            String createdAt = cursor.getString(cursor.getColumnIndex(Contract.GOOD.created_at));
            int position = cursor.getInt(cursor.getColumnIndex(Contract.GOOD.position));
            String tags = cursor.getString(cursor.getColumnIndex(Contract.GOOD.tags));
            Note newNote = null;
            newNote = new Note(id, title, content, idRubric, deleted, date, createdAt, position, tags);
            Utils.saveLog("Note: " + newNote.getId() + " " + newNote.getDateCreateAt());
            // TODO: 16.02.2019 fix 3
            notes.add(newNote);
        }
        cursor.close();

        return notes;
    }

    public synchronized void readAllNotesTest() {
        Cursor cursor = getWritableDatabase().rawQuery("SELECT * FROM GOOD", null);
        ArrayList<Note> notesTest = new ArrayList<>();
        while (cursor.moveToNext()) {
            int id = cursor.getInt(cursor.getColumnIndex(Contract.GOOD.id));
            String content = cursor.getString(cursor.getColumnIndex(Contract.GOOD.text));
            String title = cursor.getString(cursor.getColumnIndex(Contract.GOOD.title));
            int idRubric = cursor.getInt(cursor.getColumnIndex(Contract.GOOD.id_rubric));
            long date = cursor.getLong(cursor.getColumnIndex(Contract.GOOD.date));
            int deleted = cursor.getInt(cursor.getColumnIndex(Contract.GOOD.deleted));
            String createdAt = cursor.getString(cursor.getColumnIndex(Contract.GOOD.created_at));
            int position = cursor.getInt(cursor.getColumnIndex(Contract.GOOD.position));
            String tags = cursor.getString(cursor.getColumnIndex(Contract.GOOD.tags));
            Note newNote = null;
            newNote = new Note(id, title, content, idRubric, deleted, date, createdAt, position, tags);
            // TODO: 16.02.2019 fix 3
            notesTest.add(newNote);
        }

        cursor.close();
    }

    @Override
    public List<Note> getCacheNotes(boolean returnAllNotes) {
        return notes;
    }

    @Override
    public int getNotesCount() {
        return notes.size();
    }

    @Override
    public void updateNote(Note note) {
        Bloknote.simpleLog("DbHandler updateNote " + note.getId());

        if (db != null) {
            Bloknote.simpleLog("начинаю обновление записи. note ID = " + note.getId());
            //prepare the transaction information that will be saved to the database
            ContentValues values = new ContentValues();
            values.put(Contract.GOOD.id_rubric, note.getIdRubric());
            values.put(Contract.GOOD.title, note.getTitle());
            values.put(Contract.GOOD.text, note.getContentText());
            values.put(Contract.GOOD.tags, note.getTags());
            values.put(Contract.GOOD.date, note.getDateEdit().getTime());
            values.put(Contract.GOOD.deleted, note.getDeleted());
            values.put(Contract.GOOD.position, note.getPosition());
            values.put(Contract.GOOD.created_at, note.getDateCreateAt());
            db.update(Contract.TABLE_NOTES_NAME, values, "id = ?", new String[]{String.valueOf(note.getId())});
        }
    }

    @Override
    public boolean deleteNote(int idNote) {
        Utils.saveLog("\n=============deleteNote===========" +
                "\ndb.isDbLockedByCurrentThread() " + db.isDbLockedByCurrentThread() +
                "\ndb.isReadOnly() " + db.isReadOnly() +
                "\nidNote " + idNote);

        Note deleteNote = null;

        for (Note note : notes
        ) {
            if (note.getId() == idNote) deleteNote = note;
        }

        deleteImagesFromNote(deleteNote.getDateCreateAt());
        deleteAllChecklistItemsFromNote(deleteNote.getDateCreateAt());
        deleteAllAlarmsFromNote(idNote);

        if (db != null) {
            String clause = " id = ?";
            String[] clauseArgs = {String.valueOf(idNote)};
            db.delete(
                    Contract.TABLE_NOTES_NAME,
                    clause,
                    clauseArgs);
        }
        notes.remove(deleteNote);
        Utils.saveLog("\n============delete============");
        return false;
    }

    @Override
    public void deleteAllNotes() {

    }

    @Override
    public long addNote(Note note) {
        Utils.saveLog("\n=============addNote===========");
        Utils.saveLog("\nClass: " + "DbHandler " + "Method: " + "addNote");
        Utils.saveLog(
                "\ndb.isDbLockedByCurrentThread() " + db.isDbLockedByCurrentThread() +
                        "\ndb.isReadOnly() " + db.isReadOnly() +
                        "\nnote.getDateCreateAt() " + note.getDateCreateAt());
        long newId = -1;
        if (db != null) {
            ContentValues values = new ContentValues();
            values.put(Contract.GOOD.id_rubric, note.getIdRubric());
            values.put(Contract.GOOD.title, note.getTitle());
            values.put(Contract.GOOD.text, note.getContentText());
            values.put(Contract.GOOD.tags, note.getTags());
            values.put(Contract.GOOD.date, note.getDateEdit().getTime());
            values.put(Contract.GOOD.deleted, note.getDeleted());
            values.put(Contract.GOOD.position, note.getPosition());
            values.put(Contract.GOOD.created_at, note.getDateCreateAt());
            newId = db.insert(Contract.TABLE_NOTES_NAME, null, values);
            Utils.saveLog("\n============insert============" +
                    "\ndb.insert " + "Note " + newId
                    + "\nContract.GOOD.id_rubric " + note.getIdRubric()
                    + "\nContract.GOOD.title " + (note.getTitle() == null)
                    + "\nContract.GOOD.text " + (note.getContentText() == null)
                    + "\nContract.GOOD.tags " + note.getTags()
                    + "\nContract.GOOD.date " + note.getDateEdit().getTime()
                    + "\nContract.GOOD.deleted " + note.getDeleted()
                    + "\nContract.GOOD.position " + note.getPosition()
                    + "\nContract.GOOD.created_at " + note.getDateCreateAt());

            note.setId((int) newId);
        }

        notes.add(note);
        return newId;

    }


//endregion

    //region =========== ALARMS ===========

    @Override
    public int addAlarm(final Alarm alarm) {

        long newId = -1;
        if (db != null) {
            ContentValues values = new ContentValues();
            values.put(Contract.ALARMS.id_good, alarm.getNoteId());
            values.put(Contract.ALARMS.text, alarm.getText());
            values.put(Contract.ALARMS.date, alarm.getDateOfAlarm().getTime());
            newId = db.insert(Contract.TABLE_ALARMS_NAME, null, values);
            alarm.setId((int) newId);
        }

        alarms.add(alarm);
        Log.wtf("M_DbHandler", "add alarm " + newId);
        return 0;
    }

    @Override
    public List<Alarm> readAllAlarms() {
        alarms.clear();
        Cursor cursor = getWritableDatabase().rawQuery("SELECT * FROM " + Contract.TABLE_ALARMS_NAME, null);
        while (cursor.moveToNext()) {
            int id = cursor.getInt(cursor.getColumnIndex(Contract.ALARMS.id));
            String text = cursor.getString(cursor.getColumnIndex(Contract.ALARMS.text));
            int idNote = cursor.getInt(cursor.getColumnIndex(Contract.ALARMS.id_good));
            long date = cursor.getLong(cursor.getColumnIndex(Contract.ALARMS.date));
            alarms.add(new Alarm(id, text, idNote, new Date(date)));
        }
        cursor.close();
        return alarms;
    }

    @Override
    public List<Alarm> getAlarmsFromNote(int noteId) {
        List<Alarm> alarms = new ArrayList<>();
        for (Alarm alarm : this.alarms
        ) {
            if (alarm.getNoteId() == noteId) {
                alarms.add(alarm);
            }
        }
        return alarms;
    }

    @Override
    public void deleteAlarm(int id) {
        if (db != null) {
            String clause = " id = ?";
            String[] clauseArgs = {String.valueOf(id)};
            db.delete(
                    Contract.TABLE_ALARMS_NAME,
                    clause,
                    clauseArgs);
        }

        Iterator<Alarm> iterator = alarms.iterator();
        while (iterator.hasNext()) {
            if (iterator.next().getId() == id) {
                iterator.remove();
            }
        }
    }

    @Override
    public void deleteAllAlarmsFromNote(int noteId) {
        if (db != null) {
            String clause = " id_good = ?";
            String[] clauseArgs = {String.valueOf(noteId)};
            db.delete(
                    Contract.TABLE_ALARMS_NAME,
                    clause,
                    clauseArgs);
        }
        Iterator<Alarm> iterator = alarms.iterator();
        while (iterator.hasNext()) {
            Alarm alarmForDelete = iterator.next();
            if (alarmForDelete.getNoteId() == noteId) {
                SignalManager.cancelAlarm(alarmForDelete, getNote(noteId));
                iterator.remove();
            }
        }
    }
    //endregion

    //region =========== UTILS ===========

    public static void migrateToNewDb() {
        PATH_TO_NEW_DB =
                App.getContext().getApplicationInfo().dataDir +
                        "/databases/db_notes";
        PATH_TO_OLD_DB =
                App.getContext().getApplicationInfo().dataDir +
                        "/app_webview/databases/file__0/1";

        needMigrateAndUpdateDb = checkDataBase();
        if (needMigrateAndUpdateDb) {
            try {
//                    db.close();
                Bloknote.simpleLog("начинаю копирование старой базы в новую");
                File src = new File(PATH_TO_OLD_DB);
                File dst = new File(PATH_TO_NEW_DB);
                File pathToNewDb = new File(
                        App.getContext().getApplicationInfo().dataDir +
                                "/databases/");
                if (!dst.exists()) {
                    pathToNewDb.mkdirs();
                    dst.createNewFile();
                }

                copy(src, dst);
            } catch (IOException e) {

            }
        }
//        if (needMigrateAndUpdateDb) {
//            try {
//                db.close();
//
//                DbHandler oldBaseHandler = new DbHandler(PATH_TO_OLD_DB, 1);
//                DbHandler newBaseHandler = new DbHandler(PATH_TO_NEW_DB, versionDb);
//
//                SQLiteDatabase oldDb = oldBaseHandler.getWritableDatabase();
//                SQLiteDatabase newDb = newBaseHandler.getWritableDatabase();
//
//                Cursor cursor = oldDb.rawQuery("SELECT * FROM GOOD", null);
//                while (cursor.moveToNext()) {
//                    int id = cursor.getInt(cursor.getColumnIndex(Contract.GOOD.id));
//                    String content = cursor.getString(cursor.getColumnIndex(Contract.GOOD.text));
//                    String title = cursor.getString(cursor.getColumnIndex(Contract.GOOD.title));
//                    int idRubric = cursor.getInt(cursor.getColumnIndex(Contract.GOOD.id_rubric));
//                    long date = cursor.getLong(cursor.getColumnIndex(Contract.GOOD.date));
//                    int deleted = cursor.getInt(cursor.getColumnIndex(Contract.GOOD.deleted));
//                    long createdAt = cursor.getLong(cursor.getColumnIndex(Contract.GOOD.created_at));
//                    int position = cursor.getInt(cursor.getColumnIndex(Contract.GOOD.position));
//                    String tags = cursor.getString(cursor.getColumnIndex(Contract.GOOD.tags));
//                    Bloknote.simpleLog(content + " " + title + " " + id);
//                    notes.add(new Note(id, title, content, idRubric, deleted, date, createdAt, position, tags));
//                }
//
//                Bloknote.simpleLog("начинаю копирование старой базы в новую");
//                File src = new File(PATH_TO_OLD_DB);
//                File dst = new File(PATH_TO_NEW_DB);
//                File pathToNewDb = new File(App.getContext().getApplicationInfo().dataDir +
//                        "/databases/");
//                if (!dst.exists()) {
//                    pathToNewDb.mkdirs();
//                    dst.createNewFile();
//                }
//                Bloknote.simpleLog("src " + src.exists());
//                Bloknote.simpleLog("dst " + dst.exists());
//                copy(src, dst);
//                Bloknote.simpleLog("закончил копирование старой базы в новую");
//                db = instance.getWritableDatabase();
//                Bloknote.simpleLog("version db after copy = " + db.getVersion());
//            } catch (IOException e) {
//                Bloknote.simpleLog("ошибка копирования ");
//                e.printStackTrace();
//            }
//        }

    }

    private static boolean checkDataBase() {
        if (!BlocknotePreferencesManager.getFirstStart()) return false;
        Bloknote.simpleLog("проверяю наличие старой базы");
        SQLiteDatabase checkDB = null;
        try {
            checkDB = SQLiteDatabase.openDatabase(PATH_TO_OLD_DB, null, SQLiteDatabase.OPEN_READONLY);

        } catch (SQLiteCantOpenDatabaseException ex) {

        } catch (SQLiteException e) {

        }

        if (checkDB != null) {
            checkDB.close();
        }
        return checkDB != null;
    }

    public static void copy(File src, File dst) throws IOException {
        Bloknote.simpleLog("начинаю копировать");
        try (InputStream in = new FileInputStream(src)) {
            try (OutputStream out = new FileOutputStream(dst)) {
                // Transfer bytes from in to out
                byte[] buf = new byte[1024];
                int len;
                while ((len = in.read(buf)) > 0) {
                    Bloknote.simpleLog(String.valueOf(len));
                    out.write(buf, 0, len);
                }
                out.flush();
                in.close();
            }
        } catch (Exception e) {

        }

        src.delete();
    }


    @Override
    public synchronized void close() {
        db.close();
        super.close();
    }


    public HashMap<Integer, Integer> getMapOfCountNotes() {
        try {
            HashMap<Integer, Integer> map = new HashMap<>();
            for (Note n :
                    notes) {
                Integer idRubric = map.get(n.getIdRubric());
                if (idRubric == null) {
                    map.put(n.getIdRubric(), 1);
                } else {
                    int count = idRubric;
                    map.put(n.getIdRubric(), ++count);
                }
            }
            return map;
        } catch (Exception e) {
            HashMap<Integer, Integer> map = new HashMap<>();
            return map;
        }
    }

    @Override
    public Map<Integer, String> getMapNamesOfRubrics() {
        Map<Integer, String> map = new HashMap<>();
        for (Rubric r :
                rubrics) {
            map.put(r.getId(), r.getTitle());
        }
        return map;
    }

    //endregion
}