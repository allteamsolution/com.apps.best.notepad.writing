package com.deen812.bloknot.storage;

public class SqlStatements {

    public static final String CREATE_TABLE_RUBRIC =
            "CREATE TABLE IF NOT EXISTS RUBRIC (id  INTEGER PRIMARY KEY,id_parent INT, title TEXT,text TEXT,deleted INT, PWD TEXT) ";
    public static final String CREATE_TABLE_GOOD = "CREATE TABLE IF NOT EXISTS GOOD (id  INTEGER PRIMARY KEY,id_rubric INT,title TEXT,text TEXT,tags TEXT,date DATE,deleted INT,position INT,created_at DATETIME DEFAULT CURRENT_TIMESTAMP,favorite INTEGER)";
    public static final String CREATE_TABLE_IMAGES = "CREATE TABLE IF NOT EXISTS IMAGES (id  INTEGER PRIMARY KEY,id_good TEXT,filename TEXT)";
    public static final String CREATE_TABLE_GOOD_HISTORY = "CREATE TABLE IF NOT EXISTS GOOD_HISTORY (id  INTEGER PRIMARY KEY,id_good INT,title TEXT,text TEXT,tags TEXT,date DATE)";
    public static final String ADD_PWD_COLUMN_TO_RUBRIC = "ALTER TABLE RUBRIC ADD COLUMN PWD TEXT;";

    public static final String UPDATE_TABLE_GOOD = "ALTER TABLE GOOD ADD COLUMN favorite INTEGER;";


    public static final String CREATE_TABLE_CHECKLIST = "CREATE TABLE IF NOT EXISTS CHECKLIST (id  INTEGER PRIMARY KEY,id_good TEXT,text TEXT,is_check INT)";

    public static final String CREATE_TABLE_ALARMS = "CREATE TABLE IF NOT EXISTS ALARMS (id  INTEGER PRIMARY KEY,id_good INT,text TEXT,date LONG)";


}