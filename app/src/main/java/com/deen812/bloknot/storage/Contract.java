package com.deen812.bloknot.storage;

public class Contract {
    public static final String TABLE_NOTES_NAME = "GOOD";
    public static final String TABLE_IMAGES_NAME = "IMAGES";
    public static final String TABLE_RUBRIC_NAME = "RUBRIC";
    public static final String TABLE_CHECKLIST_ITEM_NAME = "CHECKLIST";
    public static final String TABLE_ALARMS_NAME = "ALARMS";

    public static class GOOD{
        public static final String id = "id";
        public static final String id_rubric = "id_rubric";
        public static final String title = "title";
        public static final String text = "text";
        public static final String tags = "tags";
        public static final String date = "date";
        public static final String deleted = "deleted";
        public static final String position = "position";
        public static final String created_at = "created_at";
    }

    public static class IMAGES{
        public static final String id = "id";
        public static final String id_good = "id_good";
        public static final String filename = "filename";
    }

    public static class RUBRIC{
        public static final String id = "id";
        public static final String id_parent = "id_parent";
        public static final String title = "title";
        public static final String text = "text";
        public static final String deleted = "deleted";
        public static final String PWD = "PWD";
    }

    public static class CHECKLIST_ITEM{
        public static final String id = "id";
        public static final String id_good = "id_good";
        public static final String text = "text";
        public static final String is_check = "is_check";
    }

    public static class ALARMS {
        public static final String id = "id";
        public static final String text = "text";
        public static final String id_good = "id_good";
        public static final String date = "date";
    }
}