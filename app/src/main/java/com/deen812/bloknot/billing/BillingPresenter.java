package com.deen812.bloknot.billing;

import android.content.Intent;

import androidx.fragment.app.FragmentActivity;

import com.android.billingclient.api.BillingClient;
import com.android.billingclient.api.SkuDetails;
import com.android.billingclient.api.SkuDetailsParams;
import com.gen.rxbilling.client.RxBilling;
import com.gen.rxbilling.flow.BuyItemRequest;
import com.gen.rxbilling.flow.RxBillingFlow;
import com.gen.rxbilling.flow.delegate.ActivityFlowDelegate;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;


public class BillingPresenter {


    public interface BillingView {

        //called when list of subscribe SKU has been loaded
        void onGetSubscribeSku(java.util.List<SkuDetails> skuDetails);

        //called when list of purchase SKU has been loaded
        void onGetPurchaseSku(java.util.List<SkuDetails> skuDetails);

        //called on any ERROR
        void onErrorBilling(Throwable throwable);

        //called on show GP Billing Window
        void onBuyLoadSuccess();

        //called on buy success
        void onPaySuccess(String sku);

        //called on buy failed
        void onPayFailed(String sku);
    }

    public static final int PAY_RESULT_REQUEST = 433;

    private final BillingView billingView;
    private CompositeDisposable compositeDisposable;
    private final RxBilling rxBilling;
    private final RxBillingFlow rxBillingFlow;

    public BillingPresenter(BillingView billingView, RxBilling rxBilling, RxBillingFlow rxBillingFlow) {
        this.billingView = billingView;
        this.rxBilling = rxBilling;
        this.rxBillingFlow = rxBillingFlow;
    }

    public void onCreate() {
        compositeDisposable = new CompositeDisposable();
    }

    public void loadSubscribeSku(java.util.List<String> preloadSkuList) {
        compositeDisposable.add(rxBilling.getSkuDetails(SkuDetailsParams.newBuilder()
                .setSkusList(preloadSkuList)
                .setType(BillingClient.SkuType.SUBS)
                .build())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(billingView::onGetSubscribeSku, billingView::onErrorBilling)
        );
    }

    public void loadPurchaseSku(java.util.List<String> preloadSkuList) {
        compositeDisposable.add(rxBilling.getSkuDetails(SkuDetailsParams.newBuilder()
                .setSkusList(preloadSkuList)
                .setType(BillingClient.SkuType.INAPP)
                .build())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(billingView::onGetPurchaseSku, billingView::onErrorBilling)
        );
    }

    public void buy(SkuDetails skuDetails, FragmentActivity activity) {

        compositeDisposable.add(rxBillingFlow.buyItem(new BuyItemRequest(skuDetails.getType(), skuDetails.getSku(), PAY_RESULT_REQUEST, String.valueOf(System.currentTimeMillis())),
                new ActivityFlowDelegate(activity))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(billingView::onBuyLoadSuccess, billingView::onErrorBilling)
        );
    }

    public void onDestroy() {
        compositeDisposable.dispose();
    }

    public void handleBillingResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PAY_RESULT_REQUEST) {
            if (resultCode != 0) {
                String dataExtra = data.getStringExtra("INAPP_PURCHASE_DATA");
                try {
                    SkuDetails skuDetails = new SkuDetails(dataExtra);
                    billingView.onPaySuccess(skuDetails.getSku());
                } catch (Exception e) {
                    e.printStackTrace();
                    billingView.onPayFailed("data");
                }
                billingView.onPaySuccess("data");
            } else {
                billingView.onPayFailed("data");
            }

        }
    }
}
