package com.deen812.bloknot.billing;

import android.content.Context;
import android.content.Intent;

import com.deen812.bloknot.App;
import com.deen812.bloknot.pro.OfferActivity;
import com.deen812.bloknot.pro.PurchaseActivity;
import com.deen812.bloknot.pro.TrialActivity;


public class BillingHelper {

    public static final String API_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAl3WrMgnVRLmyLlkVVsB5ZWZTDoHFq3eLernwgaUUxH7gghhScBi1OCm/Vf8BNfsp0zxGXO3IWwCyrFPbxjrpeIBE+LJyjTcubFKb8uzxBgx14qG+s9+X0/Ywn+CtIERY6gMibUtlmeKq6hYsHv0vekBcK76WNEKuFWq0Oq9XbHpDhG6DNPFQGECtqUb8zfTJI+5E/B/Zr6SRH8UcMAM8Yon+7GUjPY7XORUTN2bHxLT619off5M/TIpbkpyS47zoeo1na62Ol5995aif4rMZQsmm7fEPhJzekEVIuxmjPGnOAP8eRU8kAP1lxNhudy0C/RfkB9/DEtmehlmO0BXElQIDAQAB";

    public static final String SUBSCRIBE_WEEK = "subscribe_week";
    public static final String SUBSCRIBE_MONTH = "subscribe_month";
    public static final String SUBSCRIBE_YEAR = "subscribe_year";
    public static final String SUBSCRIBE_OFFER = "subscribe_offer";
    public static final String SUBSCRIBE_YEAR_TRIAL = "subscribe_year_trial";

    public static final String SUBSCRIBE_MONTH_1 = "subscribe_month_1";
    public static final String SUBSCRIBE_YEAR_1 = "subscribe_year_1";





    public static boolean isActive(String skuId, java.util.List<com.android.billingclient.api.Purchase> purchases) {
        for (com.android.billingclient.api.Purchase purchase : purchases) {
            if (skuId.equalsIgnoreCase(purchase.getSku())) {
                return true;
            }
        }
        return false;
    }

    public static boolean isSubscriber() {
        return (App.getCurrentUser().isBasicBuy() || App.getCurrentUser().isStartBuy() || App.getCurrentUser().isSuperBuy() || App.getCurrentUser().isOfferBuy() || App.getCurrentUser().isSuperTrialBuy());
    }

    public static void openOfferActivity(Context context) {
        Intent intent = new Intent(context, OfferActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }
    public static void openTrialOffer(Context context) {
        Intent intent;
        intent = new Intent(context, TrialActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);

    }

    public static void openProActivity(Context context) {
        Intent intent = new Intent(context, PurchaseActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);

    }

}
