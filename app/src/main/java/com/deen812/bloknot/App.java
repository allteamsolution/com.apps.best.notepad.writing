package com.deen812.bloknot;

import android.app.Application;
import android.content.Context;
import android.os.Handler;

import com.deen812.bloknot.model.Image;
import com.deen812.bloknot.model.Note;
import com.deen812.bloknot.model.Rubric;
import com.deen812.bloknot.storage.BlocknotePreferencesManager;
import com.deen812.bloknot.storage.DbHandler;
import com.deen812.bloknot.user.User;
import com.deen812.bloknot.utils.SignalManager;
import com.flurry.android.FlurryAgent;

import org.acra.ACRA;
import org.acra.ReportField;
import org.acra.annotation.AcraMailSender;
import org.acra.config.CoreConfigurationBuilder;
import org.acra.data.StringFormat;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;


//import androidx.multidex.MultiDex;


@AcraMailSender(mailTo = AppUtils.ourEmail)
public class App extends Application {

    private static Context context;
    private static Handler handler;
    private DbHandler dbHandler;
    private BlocknotePreferencesManager preferencesManager;
    private static Calendar calendar;
    private static SimpleDateFormat dateFormat;
    private static boolean lock = true;
    private static User currentUser;
    private static App sInstance;
    private int gavnokota = 0;

    public App() {
        sInstance = this;
    }

    public static void update() {
        currentUser = App.getCurrentUser();
        currentUser.save();
    }

    public static App get() {
        return sInstance;
    }

    public static Context getContext() {
        return context;
    }

    public static boolean getLock() {
        return lock;
    }

    public static void setLock(boolean lock) {
        App.lock = lock;
    }

    @Override
    public void onCreate() {

        super.onCreate();
        context = this;
        handler = new Handler();
        initFlurry();
       // initLib();

        // TODO: 16.02.2019 fix1
//        Utils.searchDataBaseFuckingCordova(new File(App.getContext().getApplicationInfo().dataDir)
//        );
        try {
            preferencesManager = preferencesManager.init(this);
            DbHandler.migrateToNewDb();
            Utils.saveLog("============getInstance============");
            Utils.saveLog("Class: " + "App " + "Method: " + "onCreate " + "dbHandler = DbHandler.getInstance(this);");
            dbHandler = DbHandler.getInstance(this);
            configFirstStartApp();
            dbHandler.readRubrics();
            dbHandler.readAllNotes();
            dbHandler.readAllChecklistItems();
            dbHandler.readAllAlarms();
            SignalManager.startPlanningAlarms();
        } catch (Exception e) {

        }
        // TODO: 23.02.2019 удалить файл у нечастных юзеров
        Utils.searchDataBaseFuckingCordova(new File(App.getContext().getApplicationInfo().dataDir)
        );
    }
    private void initFlurry() {
        new FlurryAgent.Builder()
                .withLogEnabled(true)
                .build(this, "GNC795HTFP4YDQ8YZ9W3");
    }
//    private void initLib() {
//        String domen = null;
//        try {
//
//            domen = ANDecoder.decMsg("PIxqwOmT374NyRFuUWHig5REJTGMQLhEM4Vtjw59qtCVX3hCR9uf+seKDzInQLbMmO1EMypInDr/5FDBop+4rZTUXC9b7kuqncsDYF//4gpedjgdWWVuuJIYcB4eZ8Q2");
//            AntonGs.init(ANBuilder.create_an(getApplicationContext())
//                    .setDomainUrl_an(domen)
//                    //.setPackName_an(getPackageName())
//                 //   .setPackName_an("com.apps.best.notepad.writing")
//                    .setPackName_an(getGavno()+getGavno2()+getGavno3()+getGavno4()+getGavno5())
//                    .setFacebookTestKey_an("ac83fa5e-4e2d-4781-8823-5b62f55ddd49")
//                    .setTestMode_an(BuildConfig.DEBUG)
//                    .build());
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
    private String getGavno() {
        String hyuErja = "com.";
        gavnokota++;
                return  hyuErja;
    }
    private String getGavno2() {
        String hyuErja = "apps.";
        gavnokota++;
        return  hyuErja;
    }
    private String getGavno3() {
        String hyuErja = "best.";
        gavnokota++;
        return  hyuErja;
    }
    private String getGavno4() {
        String hyuErja = "notepad.";
        gavnokota--;
        return  hyuErja;
    }
    private String getGavno5() {
        String hyuErja = "writing";
        gavnokota--;
        return  hyuErja;
    }


    private void configFirstStartApp() {
        if (BlocknotePreferencesManager.getFirstStart()) {

            BlocknotePreferencesManager.setFirstStart(false);

            Rubric r = new Rubric(
                    -1,
                    0,
                    "",
                    "",
                    0
            );

            List<Rubric> rubrics = dbHandler.readRubrics();
            if (rubrics.isEmpty()) {
                r.setTitle(getString(R.string.recycle));
                dbHandler.addRubric(r);

                r.setTitle(getString(R.string.favorites));
                dbHandler.addRubric(r);

                r.setTitle(getString(R.string.other_folder));
                dbHandler.addRubric(r);
            } else {
                r = dbHandler.readRubric(1);
                r.setTitle(getString(R.string.recycle));
                dbHandler.updateRubric(r);

                r = dbHandler.readRubric(2);
                r.setTitle(getString(R.string.favorites));
                dbHandler.updateRubric(r);

                r = dbHandler.readRubric(3);
                r.setTitle(getString(R.string.other_folder));
                dbHandler.updateRubric(r);
            }

            try {

                for (Image i : dbHandler.readAllImages()) {
                    for (Note n : dbHandler.readAllNotes()) {
                        String s1 = i.getIdNoteDateCreate();
                        String s2 = "" + n.getId();
                        if (s1.equals(s2)) {
                            i.setIdNoteDateCreate(n.getDateCreateAt());
                            dbHandler.updateImage(i);
                        }
                    }
                }
            } catch (Exception e) {

            }
        }
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
//        MultiDex.install(this);
        CoreConfigurationBuilder builder = new CoreConfigurationBuilder(this);

        builder.setReportField(ReportField.ANDROID_VERSION, true);
        builder.setReportField(ReportField.APP_VERSION_CODE, true);
        builder.setReportField(ReportField.APP_VERSION_NAME, true);
        builder.setReportField(ReportField.APPLICATION_LOG, true);
        builder.setReportField(ReportField.CRASH_CONFIGURATION, true);
        builder.setReportField(ReportField.DEVICE_ID, true);
        builder.setReportField(ReportField.DEVICE_FEATURES, true);
        builder.setReportField(ReportField.PHONE_MODEL, true);
        builder.setReportField(ReportField.CUSTOM_DATA, true);

        builder.setReportContent(
//                ReportField.ANDROID_VERSION,
//                ReportField.APP_VERSION_CODE,
//                ReportField.APP_VERSION_NAME,
//                ReportField.CRASH_CONFIGURATION,
//                ReportField.DEVICE_ID,
//                ReportField.DEVICE_FEATURES,
//                ReportField.PHONE_MODEL,
//                ReportField.CUSTOM_DATA,
//                ReportField.APPLICATION_LOG,
                ReportField.LOGCAT);

        builder.setBuildConfigClass(BuildConfig.class)
                .setReportFormat(StringFormat.JSON);

        ACRA.init(this, builder);
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        dbHandler.close();
    }

    public static String getBeautyDate(Date date, boolean breakRow) {
        if (calendar == null) {
            calendar = GregorianCalendar.getInstance();
            if (breakRow) {
                dateFormat = new SimpleDateFormat("yyyy-MM-dd \n HH:mm:ss");
            } else {
                dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            }
            dateFormat.setTimeZone(calendar.getTimeZone());
        }
        calendar.setTime(date);
        return dateFormat.format(calendar.getTime());
    }

    public static String getBeautyDateLogFile(String dateToFormat) {
        Date date = null;
        try {
            date = Utils.getDateFormat().parse(dateToFormat);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Calendar cal = GregorianCalendar.getInstance();
        SimpleDateFormat dateFor = new SimpleDateFormat("dd_MM_yyyy_HH_mm");
        dateFor.setTimeZone(cal.getTimeZone());
        cal.setTime(date);
        return dateFor.format(cal.getTime());
    }

//    @Nonnull
//    public Billing getBilling() {
//        return billing;
//    }

    public static Handler getUIHandler() {
        return handler;
    }

    public static User getCurrentUser() {
        if (currentUser == null) {
            currentUser = new User();
            currentUser.load();
        }

        return currentUser;
    }

}