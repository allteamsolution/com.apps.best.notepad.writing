package com.deen812.bloknot;

import android.content.Context;
import android.graphics.LinearGradient;
import android.graphics.Shader;
import android.util.AttributeSet;
import android.widget.TextView;

import androidx.core.content.ContextCompat;

public class GradientTextViewBlue extends TextView {

    public GradientTextViewBlue(Context context) {
        super(context);
    }

    public GradientTextViewBlue(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public GradientTextViewBlue(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);

        //Setting the gradient if layout is changed
        if (changed) {
            getPaint().setShader(new LinearGradient(0, 0, getWidth(), getHeight(),
                    ContextCompat.getColor(getContext(), R.color.colorStartBlue),
                    ContextCompat.getColor(getContext(), R.color.colorEndBlue),
                    Shader.TileMode.CLAMP));
        }
    }
}
