package com.deen812.bloknot;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.deen812.bloknot.view.SettingsActivity;


public class ANSaluteActivity extends Activity {

    private LinearLayout anflAds;
    private ConstraintLayout nativeview;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.an_salute);

        anflAds = findViewById(R.id.boosterflAds);
        nativeview = findViewById(R.id.nativeview);
        Adlistener adlistener = new Adlistener() {
            @Override
            public void adListenerSuccess() {

            }

            @Override
            public void adListenerFailed() {
                AdHelper.loadAppodealNative(ANSaluteActivity.this, App.getCurrentUser().isPersonalAd(), anflAds,getWindow().getDecorView().getRootView(),nativeview);


            }
        };
        AdHelper.addNativeWidgetFacebook(anflAds, adlistener);
        View ivCloseAn = findViewById(R.id.ivClosebooster);
        ivCloseAn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        View lat_parent = findViewById(R.id.booster_parent);
        lat_parent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

}