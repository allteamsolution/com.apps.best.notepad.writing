package com.deen812.bloknot.user;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.deen812.bloknot.App;


public class User {
    private static final String ALREADY_REF_SEND = "ALREADY_REF_SEND";
    private static final String START_BUY = "START_BUY";
    private static final String BASIC_BUY = "BASIC_BUY";
    private static final String SUPER_BUY = "SUPER_BUY";
    private static final String MONTH_FREE_BUY = "MONTH_FREE_BUY";
    private static final String YEAR_FREE_BUY = "YEAR_FREE_BUY";
    private static final String OFFER_BUY = "OFFER_BUY";
    private static final String IS_OFFER_SHOW = "IS_OFFER_SHOW";
    private static final String AUTO_SCAN = "auto_scan";
    private static final String AUTO_CLEAN = "auto_clean";
    private static final String TIME_TO_CLEAN = "time_to_clean";
    private static final String IS_FIRST_TIME_SETTINGS_DEFAULT = "is_first_time_settings_default";
    private static final String IS_TUTORIAL_FINISHED = "is_tutorial_finished";
    private static final String SUPER_TRIAL_BUY = "SUPER_TRIAL_BUY";
    final private static String FIRST_LAUNCH = "FIRST_LAUNCH";
    final private static String RATE_US_CLICKED = "RATE_US_CLICKED";
    final private static String APP_INSTELLED_FIRST_TIME = "APP_INSTELLED_FIRST_TIME";
    private static final String COUNT_OF_OFFER_SHOWS = "COUNT_OF_OFFER_SHOWS";
    private static final String CURRENT_THEME = "current_theme";
    private static final String TIME_TO_OFFER = "time_to_offer";
    private static final String RATE_DIALOG = "rate_dialog";

 //   private static final String SCREEN_MODE = "screen_mode";
 private static final String PERSONAL_AD = "PERSONAL_AD";
    private static final String FREE_PRO_FOR_REWARD = "FREE_PRO_FOR_REWARD";

    private long freeProForReward;
    private boolean personalAd;

    private boolean alreadyRefSend;
    private boolean startBuy;
    private boolean basicBuy;
    private boolean superBuy;
    private boolean monthFreeBuy;

    private  int countOfOfferShows;
    private boolean yearFreeBuy;
    private boolean offerBuy;
    private boolean superTrialBuy;
    private boolean offerShow;
    private boolean autoScan;
    private boolean autoClean;
    private boolean isDefaultSettingsFirstTime;
    private boolean isTutorialFinished;
    private long timeToClean;
    private boolean firstLaunch;
    private int rateUsClicked;
    private long timeInstalled;
    private int currentTheme;
    private int timeToOffer;
    private int ratedialog;
 //   private String screenMode;

    public void load() {
//NanoQxAddShit
        final SharedPreferences storage = PreferenceManager.getDefaultSharedPreferences(App.getContext());
        alreadyRefSend = storage.getBoolean(ALREADY_REF_SEND, false);
        startBuy = storage.getBoolean(START_BUY, false);
        basicBuy = storage.getBoolean(BASIC_BUY, false);
        superBuy = storage.getBoolean(SUPER_BUY, false);
        monthFreeBuy = storage.getBoolean(MONTH_FREE_BUY, false);
        yearFreeBuy = storage.getBoolean(YEAR_FREE_BUY, false);
        offerBuy = storage.getBoolean(OFFER_BUY, false);
        offerShow = storage.getBoolean(IS_OFFER_SHOW, false);
        superTrialBuy = storage.getBoolean(SUPER_TRIAL_BUY, false);
        autoClean = storage.getBoolean(AUTO_CLEAN, false);
        autoScan = storage.getBoolean(AUTO_SCAN, false);
        isDefaultSettingsFirstTime = storage.getBoolean(IS_FIRST_TIME_SETTINGS_DEFAULT, false);
        timeToClean = storage.getLong(TIME_TO_CLEAN, 0L);
        isTutorialFinished = storage.getBoolean(IS_TUTORIAL_FINISHED, false);
        firstLaunch = storage.getBoolean(FIRST_LAUNCH, false);
        rateUsClicked = storage.getInt(RATE_US_CLICKED, 0);
        timeInstalled = storage.getLong(APP_INSTELLED_FIRST_TIME, 0L);
        countOfOfferShows = storage.getInt(COUNT_OF_OFFER_SHOWS,0);
        currentTheme = storage.getInt(CURRENT_THEME,1);
        timeToOffer = storage.getInt(TIME_TO_OFFER,0);
        ratedialog = storage.getInt(RATE_DIALOG,0);
    //    screenMode = storage.getString(SCREEN_MODE,"0");
        personalAd = storage.getBoolean(PERSONAL_AD, true);
        freeProForReward = storage.getLong(FREE_PRO_FOR_REWARD, 0L);
    }

    public void save() {
        final SharedPreferences.Editor storage = PreferenceManager.getDefaultSharedPreferences(App.getContext()).edit();
        storage.putBoolean(ALREADY_REF_SEND, alreadyRefSend);
        storage.putBoolean(START_BUY, startBuy);
        storage.putBoolean(BASIC_BUY, basicBuy);
        storage.putBoolean(SUPER_BUY, superBuy);
        storage.putBoolean(MONTH_FREE_BUY, monthFreeBuy);
        storage.putBoolean(YEAR_FREE_BUY, yearFreeBuy);
        storage.putBoolean(OFFER_BUY, offerBuy);
        storage.putBoolean(SUPER_TRIAL_BUY, superTrialBuy);
        storage.putBoolean(IS_OFFER_SHOW, offerShow);
        storage.putBoolean(FIRST_LAUNCH, firstLaunch);
        storage.apply();
    }
    public void setFreeProForReward(long freePro) {
        freeProForReward = freePro;
        final SharedPreferences.Editor storage = PreferenceManager.getDefaultSharedPreferences(App.getContext()).edit();
        storage.putLong(FREE_PRO_FOR_REWARD, freeProForReward);
        storage.apply();
    }

    public long getFreeProForReward() {
        return freeProForReward;
    }

    public void setPersonalAd(boolean ad) {
        personalAd = ad;
        final SharedPreferences.Editor storage = PreferenceManager.getDefaultSharedPreferences(App.getContext()).edit();
        storage.putBoolean(PERSONAL_AD, personalAd);
        storage.apply();
    }

    public boolean isPersonalAd() {
        return personalAd;
    }
    public void saveFirstLaunch() {
        final SharedPreferences.Editor storage = PreferenceManager.getDefaultSharedPreferences(App.getContext()).edit();
        storage.putBoolean(FIRST_LAUNCH, firstLaunch);
        storage.apply();
    }

    public void saveRateUsClicked() {
        final SharedPreferences.Editor storage = PreferenceManager.getDefaultSharedPreferences(App.getContext()).edit();
        storage.putInt(RATE_US_CLICKED, rateUsClicked);
        storage.apply();
    }

    public void saveSettings() {
//NanoQxAddShit
        final SharedPreferences.Editor storage = PreferenceManager.getDefaultSharedPreferences(App.getContext()).edit();
        storage.putBoolean(AUTO_CLEAN, autoClean);
        storage.putBoolean(AUTO_SCAN, autoScan);
        storage.apply();
    }
    public void saveTimeToOffer() {
//NanoQxAddShit
        final SharedPreferences.Editor storage = PreferenceManager.getDefaultSharedPreferences(App.getContext()).edit();
        storage.putInt(TIME_TO_OFFER, timeToOffer);
        storage.apply();
    }

    public void saveDefaultSettings() {
//NanoQxAddShit
        final SharedPreferences.Editor storage = PreferenceManager.getDefaultSharedPreferences(App.getContext()).edit();
        storage.putBoolean(IS_FIRST_TIME_SETTINGS_DEFAULT, isDefaultSettingsFirstTime);
        storage.apply();
    }
    public void saveCurrentTheme() {
        final SharedPreferences.Editor storage = PreferenceManager.getDefaultSharedPreferences(App.getContext()).edit();
        storage.putInt(CURRENT_THEME, currentTheme);
        storage.apply();
    }
    public void saveTutorialFinished() {
        final SharedPreferences.Editor storage = PreferenceManager.getDefaultSharedPreferences(App.getContext()).edit();
        storage.putBoolean(IS_TUTORIAL_FINISHED, isTutorialFinished);
        storage.apply();
    }

//    public void saveSccreenMode() {
//        final SharedPreferences.Editor storage = PreferenceManager.getDefaultSharedPreferences(App.getContext()).edit();
//        storage.putString(SCREEN_MODE, screenMode);
//        storage.apply();
//    }


    public int getTimeToOffer() {
        return timeToOffer;
    }

    public void setTimeToOffer(int timeToOffer) {
        this.timeToOffer = timeToOffer;
    }

    public void saveTimeToClean() {
        final SharedPreferences.Editor storage = PreferenceManager.getDefaultSharedPreferences(App.getContext()).edit();
        storage.putLong(TIME_TO_CLEAN, timeToClean);
        storage.apply();
    }


    public void saveOfferShows() {
        final SharedPreferences.Editor storageEditor = PreferenceManager.getDefaultSharedPreferences(App.getContext()).edit();
        storageEditor.putInt(COUNT_OF_OFFER_SHOWS, countOfOfferShows);
        storageEditor.apply();
    }

    public int getCountOfOfferShows() {
        return countOfOfferShows;
    }

    public void setCountOfOfferShows(int countOfOfferShows) {
        this.countOfOfferShows = countOfOfferShows;
    }



    public boolean isTutorialFinished() {
        return isTutorialFinished;
    }

    public void setTutorialFinished(boolean tutorialFinished) {
        isTutorialFinished = tutorialFinished;
    }

    public boolean isAlreadyRefSend() {
        return alreadyRefSend;
    }

    public void setAlreadyRefSend(boolean alreadyRefSend) {
        this.alreadyRefSend = alreadyRefSend;
    }

    public boolean isStartBuy() {
        return startBuy;
    }

    public void setStartBuy(boolean startBuy) {
        this.startBuy = startBuy;
    }

    public boolean isBasicBuy() {
        return basicBuy;
    }

    public void setBasicBuy(boolean basicBuy) {
        this.basicBuy = basicBuy;
    }

    public boolean isSuperBuy() {
        return superBuy;
    }

    public void setSuperBuy(boolean superBuy) {
        this.superBuy = superBuy;
    }

    public boolean isMonthFreeBuy() {
        return monthFreeBuy;
    }

    public void setMonthFreeBuy(boolean monthFreeBuy) {
        this.monthFreeBuy = monthFreeBuy;
    }

    public boolean isYearFreeBuy() {
        return yearFreeBuy;
    }

    public void setYearFreeBuy(boolean yearFreeBuy) {
        this.yearFreeBuy = yearFreeBuy;
    }

    public boolean isOfferBuy() {
        return offerBuy;
    }

    public void setOfferBuy(boolean offerBuy) {
        this.offerBuy = offerBuy;
    }

    public boolean isOfferShow() {
        return offerShow;
    }

    public void setOfferShow(boolean offerShow) {
        this.offerShow = offerShow;
    }

    public boolean getAutoScan() {
        return autoScan;
    }

    public void setAutoScan(boolean autoScan) {
        this.autoScan = autoScan;
    }

    public boolean getAutoClean() {
        return autoClean;
    }

    public void setAutoClean(boolean autoClean) {
        this.autoClean = autoClean;
    }

    public boolean isDefaultSettingsFirstTime() {
        return isDefaultSettingsFirstTime;
    }

    public void setDefaultSettingsFirstTime(boolean defaultSettingsFirstTime) {
        isDefaultSettingsFirstTime = defaultSettingsFirstTime;
    }

    public long getTimeToClean() {
        return timeToClean;
    }

    public void setTimeToClean(long timeToClean) {
        this.timeToClean = timeToClean;
    }

    public boolean isSuperTrialBuy() {
        return superTrialBuy;
    }

    public void setSuperTrialBuy(boolean superTrialBuy) {
        this.superTrialBuy = superTrialBuy;
    }

    public boolean isFirstLaunch() {
        return firstLaunch;
    }

    public void setFirstLaunch(boolean firstLaunch) {
        this.firstLaunch = firstLaunch;
    }

    public int getRateUsClicked() {
        return rateUsClicked;
    }

    public void setRateUsClicked(int rateUsClicked) {
        this.rateUsClicked = rateUsClicked;
    }

    public long getTimeInstalled() {
        return timeInstalled;
    }

    public void setTimeInstalled(long timeInstalled) {
        this.timeInstalled = timeInstalled;
    }

    public int getCurrentTheme() {
        return currentTheme;
    }

    public void setCurrentTheme(int currentTheme) {
        this.currentTheme = currentTheme;
    }

    public void setTimeToShowRateDialog() {
        final SharedPreferences.Editor storage = PreferenceManager.getDefaultSharedPreferences(App.getContext()).edit();
        ratedialog = ratedialog+1;
        storage.putInt(RATE_DIALOG,ratedialog);
        storage.apply();
    }

    public int getRatedialog() {
        return ratedialog;
    }

    public void setRatedialog(int ratedialog) {
        this.ratedialog = ratedialog;
    }

    //    public String getScreenMode() {
//        return screenMode;
//    }
//
//    public void setScreenMode(String screenMode) {
//        this.screenMode = screenMode;
//    }
}