package com.deen812.bloknot.model;

import android.os.Parcel;

import com.deen812.bloknot.Utils;
import com.deen812.bloknot.utils.Bloknote;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Objects;

public class Note {

    private int id;
    private String title = "";
    private String content = "";
    private int idRubric = 3;
    private String dateCreatedAt = "";


    private int color = -1;
    private String tags = "";
    private Date dateOfLastRedaction = new Date();
    private int deleted;
    private int position;

    Calendar calendar = GregorianCalendar.getInstance();

    public Note() {
        dateCreatedAt = Utils.getDateFormat().format(new Date());
    }

    public Note(int id, String title, String content, int idRubric, int deleted, long dateOfLastRedaction, String createdAt, int position, String tags) {

        calendar.setTime(new Date(dateOfLastRedaction));
        calendar.set(Calendar.MILLISECOND, 0);
        this.dateOfLastRedaction = calendar.getTime();

        this.id = id;
        this.title = title == null ? "" : title;
        this.content = content == null ? "" : content;
        this.idRubric = idRubric;
        this.deleted = deleted;
        this.position = position;
        this.tags = tags == null ? "" : tags;
        this.dateCreatedAt = createdAt;
    }

    public String getTitle() {
        return title;
    }

    public String getContentText() {
        return content;
    }

    public int getId() {
        return id;
    }

    public int getIdRubric() {
        return idRubric;
    }

    public String getTags() {
        return tags == null ? "" : tags;
    }

    public Date getDateEdit() {
        return dateOfLastRedaction;
    }

    public int getDeleted() {
        return deleted;
    }

    public int getPosition() {
        return position;
    }

    public String getDateCreateAt() {

        return dateCreatedAt;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContentText(String content) {
        this.content = content;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }

    public void setIdRubric(int idRubric) {
        this.idRubric = idRubric;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public void setDateOfLastRedaction(Date dateOfLastRedaction) {
        calendar.setTime(dateOfLastRedaction);
        calendar.set(Calendar.MILLISECOND, 0);
        this.dateOfLastRedaction = calendar.getTime();
    }

    public void setDeleted(int deleted) {
        this.deleted = deleted;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public Date getDateCreatedAt() {
        return new Date(Long.parseLong(dateCreatedAt));
    }

    public void setDateCreatedAt(Date dateCreatedAt) {
        calendar.setTime(dateCreatedAt);
        calendar.set(Calendar.MILLISECOND, 0);
        this.dateCreatedAt = String.valueOf(dateCreatedAt.getTime());

    }

    @Override
    public String toString() {
        return "Note{" + "\n" +
                "id=" + id + "\n" +
                ", title='" + title + '\'' + "\n" +
                ", content='" + content + '\'' + "\n" +
                ", idRubric=" + idRubric + "\n" +
                ", dateCreatedAt='" + dateCreatedAt + '\'' + "\n" +
                ", color=" + color + "\n" +
                ", tags='" + tags + '\'' + "\n" +
                ", dateOfLastRedaction=" + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(dateOfLastRedaction) + "\n" +
                ", deleted=" + deleted + "\n" +
                ", position=" + position + "\n" +
                '}' + "\n";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Note note = (Note) o;
        return
                Objects.equals(
                        dateCreatedAt, note.dateCreatedAt);
    }

    public boolean isFavorite() {
        return position == 1;
    }

    public void setFavorite(boolean favorite) {
        this.position = favorite ? 1 : 0;
    }

    public class Builder{
        private int id;
        private String title = "";
        private String content = "";
        private int idRubric = 3;
        private String dateCreatedAt = "";
        private int color = -1;
        private String tags = "";
        private Date dateOfLastRedaction = new Date();
        private int deleted;
        private int position;


    }
}