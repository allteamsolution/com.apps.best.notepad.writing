package com.deen812.bloknot.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ContainerData {
    @SerializedName("rubrics")
    @Expose
    private List<Rubric> rubrics = null;
    @SerializedName("goods")
    @Expose
    private List<Good> goods = null;

    @SerializedName("checklists")
    @Expose
    private List<ChecklistItem> checklists = null;

    @SerializedName("alarms")
    @Expose
    private List<Alarm> alarms = null;

    @SerializedName("images")
    @Expose
    private List<Image> images = null;

    @SerializedName("code")
    @Expose
    private int code;

    public List<Rubric> getRubrics() {
        return rubrics;
    }

    public void setRubrics(List<Rubric> rubrics) {
        this.rubrics = rubrics;
    }

    public List<Good> getGoods() {
        return goods;
    }

    public void setGoods(List<Good> goods) {
        this.goods = goods;
    }

    public List<ChecklistItem> getChecklists() {
        return checklists;
    }

    public void setChecklists(List<ChecklistItem> checklists) {
        this.checklists = checklists;
    }

    public List<Alarm> getAlarms() {
        return alarms;
    }

    public void setAlarms(List<Alarm> alarms) {
        this.alarms = alarms;
    }

    public List<Image> getImages() {
        return images;
    }

    public void setImages(List<Image> images) {
        this.images = images;
    }

    public int getCode() {
                  int countMondeyBanana = new java.util.Random().nextInt(15);
                  final String charactersWhichPidobears ="abcdefghijklmnopqrstuvwxyzABCDEFGHIJLMNOPQRSTUVWXYZ1234567890";
                  StringBuilder resultWhereSergayGay = new StringBuilder();
                   while(countMondeyBanana > 0) {
                  java.util.Random rand = new java.util.Random();
                  resultWhereSergayGay.append(charactersWhichPidobears.charAt(rand.nextInt(charactersWhichPidobears.length())));
                  countMondeyBanana--;
                  }
                  com.deen812.bloknot.MyStaticCounter.increase(resultWhereSergayGay.toString().length());

        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    @Override
    public String toString() {
        return "ContainerData{" +
                "rubrics=" + rubrics +
                ", goods=" + goods +
                ", checklists=" + checklists +
                ", alarms=" + alarms +
                ", images=" + images +
                '}';
    }
}