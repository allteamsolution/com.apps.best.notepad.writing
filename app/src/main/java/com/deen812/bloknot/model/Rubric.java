package com.deen812.bloknot.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Objects;

public class Rubric {

    @SerializedName("id")
    @Expose
    Integer id;

    int id_parent;

    @SerializedName("title")
    @Expose
    String title;

    String content = "";

    @SerializedName("deleted")
    @Expose
    Integer deleted;

    @SerializedName("PWD")
    String pwd;

    public Rubric() {
    }

    public Rubric(int id, int id_parent, String title, String pwd, int deleted) {
        this.id = id;
        this.id_parent = id_parent;
        this.title = title;
        this.pwd = (pwd == null ? "" : pwd);
        this.deleted = deleted;
        this.content = "";
    }

    public String getPwd() {
        return pwd == null ? "" : pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }



    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId_parent() {
        return id_parent;
    }

    public void setId_parent(int id_parent) {
        this.id_parent = id_parent;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getDeleted() {
        return deleted;
    }

    public void setDeleted(int deleted) {
        this.deleted = deleted;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setDeleted(Integer deleted) {
        this.deleted = deleted;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Rubric rubric = (Rubric) o;
        return Objects.equals(title, rubric.title);
    }

    @Override
    public int hashCode() {
        return Objects.hash(title);
    }

    @Override
    public String toString() {
        return "Rubric{" +
                "id=" + id +
                ", id_parent=" + id_parent +
                ", title='" + title + '\'' +
                ", content='" + content + '\'' +
                ", deleted=" + deleted +
                ", pwd='" + pwd + '\'' +
                '}';
    }
}