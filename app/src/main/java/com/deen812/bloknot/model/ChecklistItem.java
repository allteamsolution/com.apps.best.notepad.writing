package com.deen812.bloknot.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ChecklistItem {

    private int id;
    @SerializedName("text")
    @Expose
    private String text;
    @SerializedName("id_good")
    @Expose
    private String idNote;
    @SerializedName("is_check")
    @Expose
    private int check = 0;

    public ChecklistItem(int id, String text, String idNote, int check) {
        this.id = id;
        this.text = text;
        this.idNote = idNote;
        this.check = check;
    }

    public ChecklistItem(String text, String idNote) {
        this.text = text;
        this.idNote = idNote;
    }

    public String getIdNote() {
        return idNote;
    }

    public void setIdNote(String idNote) {
        this.idNote = idNote;
    }

    public int getCheck() {
        return check;
    }

    public void setCheck(int check) {
        this.check = check;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}