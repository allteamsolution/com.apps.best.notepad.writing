package com.deen812.bloknot.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Date;
import java.util.Objects;

public class Alarm {
    @SerializedName("id_alarm")
    @Expose
    private int id;
    @SerializedName("text")
    @Expose
    private String text;
    @SerializedName("id_good")
    @Expose
    private int noteId;
    @SerializedName("date_alarm")
    @Expose
    private Date dateOfAlarm;

    public Alarm(int id, String text, int noteId, Date dateOfAlarm) {
        this.id = id;
        this.text = text;
        this.noteId = noteId;
        this.dateOfAlarm = dateOfAlarm;
    }

    public Alarm(String text, int noteId, Date dateOfAlarm) {
        this.text = text;
        this.noteId = noteId;
        this.dateOfAlarm = dateOfAlarm;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getNoteId() {
        return noteId;
    }

    public void setNoteId(int noteId) {
        this.noteId = noteId;
    }

    public Date getDateOfAlarm() {
        return dateOfAlarm;
    }

    public void setDateOfAlarm(Date dateOfAlarm) {
        this.dateOfAlarm = dateOfAlarm;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Alarm alarm = (Alarm) o;
        return id == alarm.id &&
                noteId == alarm.noteId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, noteId);
    }
}