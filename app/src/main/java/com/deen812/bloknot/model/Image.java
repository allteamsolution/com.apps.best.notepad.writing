package com.deen812.bloknot.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Image {

    int id;
    @SerializedName("good_id")
    @Expose
    String idNoteDateCreate;
    @SerializedName("file")
    @Expose
    String filename;

    public Image(int id, String idNoteDateCreate, String filename) {
        this.id = id;
        this.idNoteDateCreate = idNoteDateCreate;
        this.filename = filename;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getIdNoteDateCreate() {
        return idNoteDateCreate;
    }

    public void setIdNoteDateCreate(String idNoteDateCreate) {
        this.idNoteDateCreate = idNoteDateCreate;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    @Override
    public String toString() {
        return "Image{" +
                "id=" + id +
                ", idNoteDateCreate='" + idNoteDateCreate + '\'' +
                ", filename='" + filename + '\'' +
                '}';
    }
}