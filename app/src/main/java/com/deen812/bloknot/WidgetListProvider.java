package com.deen812.bloknot;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.widget.RemoteViews;

import androidx.core.content.ContextCompat;

import com.deen812.bloknot.R;
import com.deen812.bloknot.adapters.WidgetService;
import com.deen812.bloknot.model.ChecklistItem;
import com.deen812.bloknot.model.Note;
import com.deen812.bloknot.storage.ConstantStorage;
import com.deen812.bloknot.storage.DbHandler;
import com.deen812.bloknot.utils.Bloknote;
import com.deen812.bloknot.view.NoteDetailActivity;
import com.deen812.bloknot.view.RootActivity;

import java.util.List;
import java.util.UUID;

public class WidgetListProvider extends AppWidgetProvider {

    public final static String ACTION_ON_CLICK = "com.deen812.bloknot2.adapters.itemonclick";
    public final static String ACTION_SHARE = "com.deen812.bloknot2.adapters.share";
    public final static String ITEM_POSITION = "item_position";
    private static int UNIQUE_REQUEST_CODE = 4;
    public final static String ACTION_UPDATE_SINGLE_WIDGET = "com.deen812.bloknot2.UPDATE_SINGLE_WIDGET";

    @Override
    public void onEnabled(Context context) {
        Bloknote.simpleLog("SingleNoteWidget onEnabled");
    }

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager,
                         int[] appWidgetIds) {
        super.onUpdate(context, appWidgetManager, appWidgetIds);
        Bloknote.simpleLog("Widget onUpdate");

        for (int i : appWidgetIds) {
            updateWidget(context, appWidgetManager, i);
        }
    }

    @Override
    public void onDeleted(Context context, int[] appWidgetIds) {
        Bloknote.simpleLog("Widget onDeleted");

        super.onDeleted(context, appWidgetIds);
        SharedPreferences.Editor editor = context.getSharedPreferences(
                ConstantStorage.WIDGET_PREF, Context.MODE_PRIVATE).edit();
        for (int widgetID : appWidgetIds) {
            editor.remove(ConstantStorage.WIDGET_NOTE_ID + widgetID);
            editor.remove(ConstantStorage.WIDGET_COLOR + widgetID);
            editor.remove(ConstantStorage.WIDGET_COLOR_MAIN + widgetID);
            editor.remove(ConstantStorage.WIDGET_COLOR_TEXT + widgetID);
        }
        editor.commit();
    }

    @Override
    public void onDisabled(Context context) {
        Bloknote.simpleLog("Widget onDisabled");
    }

    public static void updateWidget(Context context, AppWidgetManager appWidgetManager,
                      int appWidgetId) {
        Bloknote.simpleLog("WIDGET updateWidget " + appWidgetId);
        RemoteViews rv = new RemoteViews(context.getPackageName(),
                R.layout.widget_single_note);
        SharedPreferences sp = context.getSharedPreferences(
                ConstantStorage.WIDGET_PREF, Context.MODE_PRIVATE);
        String dateOfCreateNoteId = sp.getString(ConstantStorage.WIDGET_NOTE_ID + appWidgetId, null);
        if (dateOfCreateNoteId == null) {
            return;
        }
        int colorBackground = sp.getInt(
                ConstantStorage.WIDGET_COLOR_MAIN + appWidgetId,
                ContextCompat.getColor(App.getContext(), R.color.vl_yellow)
                );

        Note note = DbHandler.getInstance(App.getContext()).getNote(dateOfCreateNoteId);
        if (note == null){
            return;
            // TODO: 12.01.2019 удалить виджет при удалении заметки
        }

//        rv.setTextViewText(R.id.appwidget_text,
//                note.getTitle());
//        setUpdateTV(rv, context, appWidgetId);

        rv.setInt(R.id.container_widget_rl, "setBackgroundColor",  colorBackground);

        setList(rv, context, appWidgetId);

        setListClick(rv, context, appWidgetId);

        setShareImageView(rv, note, appWidgetId);

        setEditImageView(rv, dateOfCreateNoteId, note, appWidgetId);

        appWidgetManager.notifyAppWidgetViewDataChanged(appWidgetId,
                R.id.lvList);
        appWidgetManager.updateAppWidget(appWidgetId, rv);
    }

    private static void setEditImageView(RemoteViews rv, String dateOfCreateNoteId, Note note, int widgetId) {


        Intent backIntent = new Intent(App.getContext(), RootActivity.class);
        backIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        Intent intent = new Intent(App.getContext(), NoteDetailActivity.class);
        intent.putExtra(ConstantStorage.NOTE_BUNDLE_KEY, dateOfCreateNoteId);
        intent.putExtra(ConstantStorage.RUBRIC_BUNDLE_KEY, note.getIdRubric());

        final PendingIntent pendingIntent = PendingIntent.getActivities(
                App.getContext(),
                note.getId()*widgetId,
                new Intent[] {backIntent, intent},
                PendingIntent.FLAG_CANCEL_CURRENT );
        rv.setOnClickPendingIntent(R.id.edit_note_widget_iv, pendingIntent);
    }

    private static void setShareImageView(RemoteViews rv, Note note, int widgetId) {

        Intent i = new Intent(App.getContext(), WidgetListProvider.class);
        i.setAction(ACTION_SHARE);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        i.putExtra(ConstantStorage.NOTE_BUNDLE_KEY, note.getId());

        final PendingIntent pendingIntent = PendingIntent.getBroadcast(
                App.getContext(),
                note.getId()*widgetId,
                i,
                PendingIntent.FLAG_CANCEL_CURRENT);

        rv.setOnClickPendingIntent(R.id.share_note_widget_iv, pendingIntent);
    }

    static boolean setList(RemoteViews rv, Context context, int appWidgetId) {
        Intent adapter = new Intent(context, WidgetService.class);
        adapter.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId);
        //костыль для разных адаптеров. Иначе разным интентам будет давать один и тот же адаптер
        Uri data = Uri.parse(adapter.toUri(Intent.URI_INTENT_SCHEME).concat(UUID.randomUUID().toString()));
        adapter.setData(data);
        rv.setRemoteAdapter(R.id.lvList, adapter);
        return true;
    }

    static void setListClick(RemoteViews rv, Context context, int appWidgetId) {
//        Intent listClickIntent = new Intent(context, WidgetListProvider.class);
//        listClickIntent.setAction(ACTION_ON_CLICK);
//        PendingIntent listClickPIntent = PendingIntent.getBroadcast(context, 0,
//                listClickIntent, 0);
//        rv.setPendingIntentTemplate(R.id.lvList, listClickPIntent);
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        super.onReceive(context, intent);
        Bloknote.simpleLog("Widget onReceive with action \n" +
                intent.getAction());

        if (intent.getAction().equalsIgnoreCase(ACTION_SHARE)) {

            int idNote = intent.getIntExtra(ConstantStorage.NOTE_BUNDLE_KEY, -1);
            if (idNote == -1) return;

            Note note = DbHandler.getInstance(App.getContext()).getNote(idNote);
            if (note == null) return;
            List<ChecklistItem> checklistItems = DbHandler.getInstance(App.getContext()).readChecklistItemsFromNote(note.getDateCreateAt());

            try {
                Intent i = new Intent(Intent.ACTION_SEND);
                i.setType("text/plain");
                String message = Utils.messageForSend(note, checklistItems);
                i.putExtra(Intent.EXTRA_TEXT, message);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                App.getContext().startActivity(Intent.createChooser(i, "My notepad"));
            } catch (Exception e){

            }
        }

        if (intent.getAction().equalsIgnoreCase(ACTION_ON_CLICK)) {
            int itemPos = intent.getIntExtra(ITEM_POSITION, -1);
            if (itemPos != -1) {
                Bloknote.simpleLog("Clicked on item " + itemPos);
            }
        }

        if (intent.getAction().equalsIgnoreCase(ACTION_UPDATE_SINGLE_WIDGET)) {
Bloknote.simpleLog("onReceive! Update all widgets!!!");
            // извлекаем ID экземпляра
            int[] ids = AppWidgetManager.getInstance(App.getContext())
                    .getAppWidgetIds(new ComponentName(App.getContext(), WidgetListProvider.class));
            for (int id: ids){
                Bloknote.simpleLog("Update widget with id: " + id);
                updateWidget(
                        context,
                        AppWidgetManager.getInstance(context),
                        id);
            }
//            int widgetId = AppWidgetManager.INVALID_APPWIDGET_ID;
//            Bundle extras = intent.getExtras();
//            if (extras != null) {
//                widgetId = extras.getInt(
//                        AppWidgetManager.EXTRA_APPWIDGET_ID,
//                        AppWidgetManager.INVALID_APPWIDGET_ID);
//
//            }
//            if (widgetId != AppWidgetManager.INVALID_APPWIDGET_ID) {
//                SharedPreferences sp = context.getSharedPreferences(
//                        ConstantStorage.WIDGET_PREF, Context.MODE_PRIVATE);
////                int idNote = sp.getInt(ConstantStorage.WIDGET_NOTE_ID + widgetId,  0);
//
//                // Обновляем виджет
//                updateWidget(
//                        context,
//                        AppWidgetManager.getInstance(context),
//                        widgetId);
//            }
        }
    }
}