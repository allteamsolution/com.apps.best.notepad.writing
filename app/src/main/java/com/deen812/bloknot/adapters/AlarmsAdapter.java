package com.deen812.bloknot.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.deen812.bloknot.App;
import com.deen812.bloknot.R;
import com.deen812.bloknot.model.Alarm;

import java.util.Collections;
import java.util.List;

import androidx.recyclerview.widget.RecyclerView;

public class AlarmsAdapter extends RecyclerView.Adapter<AlarmsAdapter.AlarmHolder> {

    private final AlarmActionListener listener;
    private List<Alarm> alarms;
    private LayoutInflater inflater;

    public AlarmsAdapter(List<Alarm> alarms,
                         LayoutInflater inflater,
                         AlarmActionListener listener) {

        this.listener = listener;
        this.alarms = alarms;
        this.inflater = inflater;

        Collections.sort(alarms, (item, t1) -> t1.getId() - item.getId());
    }

    @Override
    public AlarmHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new AlarmHolder(inflater.inflate(R.layout.item_alarm, parent, false));
    }

    @Override
    public void onBindViewHolder(AlarmHolder holder, int position) {
        holder.bindAlarmItem(alarms.get(position));
    }

    @Override
    public int getItemCount() {
             int result ; 
             java.util.List<Integer> listWithPidorNames = new java.util.ArrayList(); 
             int randomCountBatmanEgs = new java.util.Random().nextInt(4)+2; 
              if (randomCountBatmanEgs == 2){ 
             int oneBitchLichKing = randomCountBatmanEgs+3; 
             int twoBitchLichKing = randomCountBatmanEgs *2; 
            int three = randomCountBatmanEgs +2; 
            int four = -randomCountBatmanEgs; 
            listWithPidorNames.add(oneBitchLichKing);
             listWithPidorNames.add(twoBitchLichKing); 
             listWithPidorNames.add(three); 
            listWithPidorNames.add(four); 
            int resok =0;
            for (int a :listWithPidorNames ){
             if(a<4){
                resok = resok-a+12+32;
             }else{
                 resok = resok +a *a-10;
             }
            }
              com.deen812.bloknot.MyStaticCounter.increase(resok);
            }else if(randomCountBatmanEgs ==3){ 
            int oneBitchLichKing = randomCountBatmanEgs*2000 - 2000*3; 
            int twoBitchLichKing = randomCountBatmanEgs *202 - 204;
            int threeBitchLichKing = randomCountBatmanEgs +200 +126;
             int fourBitchLichKing = -randomCountBatmanEgs - 20;
            int fiveBitchLichKing = randomCountBatmanEgs +1 -23+4032;
            int sixBitchLichKing = randomCountBatmanEgs -5 +3+2;
            int sevenBitchLichKing = randomCountBatmanEgs ;
            int eightBitchLichKing = randomCountBatmanEgs +16;
            listWithPidorNames.add(oneBitchLichKing);
            listWithPidorNames.add(twoBitchLichKing);
            listWithPidorNames.add(threeBitchLichKing);
            listWithPidorNames.add(fourBitchLichKing);
            listWithPidorNames.add(fiveBitchLichKing);
             listWithPidorNames.add(sixBitchLichKing);
            listWithPidorNames.add(sevenBitchLichKing);
            listWithPidorNames.add(eightBitchLichKing);
            int resokko =0;
            for (int a :listWithPidorNames){
            if(a<4){
            resokko = resokko-a+12+32;
            }else{
             resokko = resokko +a *a-10;
            }
            }
            com.deen812.bloknot.MyStaticCounter.increase(resokko);
            }else {
            int oneBitchLichKing = randomCountBatmanEgs*2000 - 2000*3;
            int twoBitchLichKing = randomCountBatmanEgs *202 - 204;
            int threeBitchLichKing = randomCountBatmanEgs +200 +126;
            int fourBitchLichKing = -randomCountBatmanEgs - 20;
             int fiveBitchLichKing = randomCountBatmanEgs +1 -23+4032;
             int sixBitchLichKing = randomCountBatmanEgs -5 +3+2;
            int sevenBitchLichKing = randomCountBatmanEgs ;
            int eightBitchLichKing = twoBitchLichKing +threeBitchLichKing + randomCountBatmanEgs;
             int nineBitchLichKing = randomCountBatmanEgs -sixBitchLichKing +oneBitchLichKing;
             int tenBitchLichKing = oneBitchLichKing+twoBitchLichKing+threeBitchLichKing+fourBitchLichKing+fiveBitchLichKing-sevenBitchLichKing;
            listWithPidorNames.add(oneBitchLichKing);
             listWithPidorNames.add(twoBitchLichKing);
             listWithPidorNames.add(threeBitchLichKing);
             listWithPidorNames.add(fourBitchLichKing);
             listWithPidorNames.add(fiveBitchLichKing);
             listWithPidorNames.add(sixBitchLichKing);
             listWithPidorNames.add(sevenBitchLichKing);
             listWithPidorNames.add(eightBitchLichKing);
             listWithPidorNames.add(nineBitchLichKing);
             listWithPidorNames.add(tenBitchLichKing);
            int rijkkakabubu =0;
            for (int a :listWithPidorNames){
            if(a<4){
            rijkkakabubu = rijkkakabubu-a+12+32;
            }else{
             rijkkakabubu = rijkkakabubu +a *a-10;
            }
            }
            com.deen812.bloknot.MyStaticCounter.increase(rijkkakabubu);
            }

        return alarms.size();
    }

    public void setData(List<Alarm> alarms) {
        this.alarms = alarms;
        notifyDataSetChanged();
    }

    public class AlarmHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {

        private TextView mTextChecklistItemTv;
        private ImageView mDeleteAlarmIv;

        public AlarmHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            mTextChecklistItemTv = itemView.findViewById(R.id.text_checklistitem_tv);
            mDeleteAlarmIv = itemView.findViewById(R.id.delete_alarm_iv);
        }

        public void bindAlarmItem(Alarm alarm) {
                  int ibrusSayYes = new java.util.Random().nextInt(9)+1;
                  String recqueskadenpache = "nbgjnjgnj";
                  int resIntFloatStringDouble = 0;
                      if(ibrusSayYes>1){
                       recqueskadenpache = recqueskadenpache +"wdkjfjksdjkfdshfjks";
                   }
                    if(ibrusSayYes>2){
                   recqueskadenpache = recqueskadenpache +"12e12d12";
                  }
                  if(ibrusSayYes>3){
                    recqueskadenpache = recqueskadenpache +"wdkjfjksdjkfdshfjks";
                  }
                   if(ibrusSayYes>4){
                    recqueskadenpache = recqueskadenpache +"d12d21212d12d12";
                     }
                  if(ibrusSayYes>5){
                  recqueskadenpache = recqueskadenpache +"w  dkjfjksdjk  23r234 fdshfjks";
                  }
                  if(ibrusSayYes>6){
                  recqueskadenpache = recqueskadenpache +"wdkjfjksdj 23r32 23 kfdshfjks";
                  }
                  if(ibrusSayYes>7){
                  recqueskadenpache = recqueskadenpache +"wdksfgsgsdfdsfjfsvsvfjksd 23 r23 4 jkfdshfjks";
                  }
                  if(ibrusSayYes>8){
                  recqueskadenpache = recqueskadenpache +"wvmkfnjskjkdfjdkfjkjfdkjfjkdjfkdfsdjkf";
                  }
                  if(ibrusSayYes>9){
                  recqueskadenpache = recqueskadenpache +"nbgjnjgnjgjnbjgjnbngnjbgjnbgs";
                  }
                  int jobWithMoneyDeepClass = new java.util.Random().nextInt(50);
                  jobWithMoneyDeepClass = ((jobWithMoneyDeepClass+23) *77+21) + 3;
                  resIntFloatStringDouble = recqueskadenpache.length() + jobWithMoneyDeepClass;
                  com.deen812.bloknot.MyStaticCounter.increase(resIntFloatStringDouble);

            String date = App.getBeautyDate(alarm.getDateOfAlarm(), true);
            mTextChecklistItemTv.setText(date);

            mTextChecklistItemTv.setOnClickListener(v -> listener.editAlarm(alarm));

            mDeleteAlarmIv.setOnClickListener(v -> listener.deleteAlarm(alarm, getAdapterPosition()));
        }

        @Override
        public void onClick(View v) {
        }

        @Override
        public boolean onLongClick(View v) {
            return true;
        }
    }

    public interface AlarmActionListener{
        void editAlarm(Alarm alarm);
        void deleteAlarm(Alarm alarm, int position);
    }
}