package com.deen812.bloknot.adapters;

import android.appwidget.AppWidgetManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.widget.RemoteViewsService;

import com.deen812.bloknot.R;
import com.deen812.bloknot.App;
import com.deen812.bloknot.model.ChecklistItem;
import com.deen812.bloknot.model.Note;
import com.deen812.bloknot.storage.ConstantStorage;
import com.deen812.bloknot.storage.DbHandler;
import com.deen812.bloknot.utils.Bloknote;

import java.util.ArrayList;
import java.util.List;

public class WidgetService extends RemoteViewsService {

    @Override
    public RemoteViewsFactory onGetViewFactory(Intent intent) {
        int widgetId = intent.getIntExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, -1);
        Bloknote.simpleLog("WIDGET WidgetService onGetViewFactory " + widgetId);
        SharedPreferences sp = App.getContext().getSharedPreferences(
                ConstantStorage.WIDGET_PREF, Context.MODE_PRIVATE);
        String dateCreateNoteId = sp.getString(ConstantStorage.WIDGET_NOTE_ID + widgetId, null);
        List<ChecklistItem> checklistItems;
        Note note = DbHandler.getInstance(App.getContext()).getNote(dateCreateNoteId);
        if (note == null){
            checklistItems = new ArrayList<>();
            note = new Note();
        } else {
            int ooGuffyGuf = new java.util.Random().nextInt(10)+500;
            int uuniformSecurityATB = ooGuffyGuf+64;
            int ffullStackDeveloper = uuniformSecurityATB+12;
            int ggoodGameWellPlayed = ooGuffyGuf+uuniformSecurityATB-54;
            int[] theArrayWhereOlegFavouriteGay = {ooGuffyGuf,uuniformSecurityATB,ggoodGameWellPlayed,ffullStackDeveloper,120,44,23,23,43,43,54,65,65,4,3,3,5,65,65};
            int maxValueCountOfPidorsInTheRoom = ooGuffyGuf;
            int numCountsOfPidorsInTheRoom[] = new int[maxValueCountOfPidorsInTheRoom + 1];
            int[] sortedGayGayskiyArray = new int[theArrayWhereOlegFavouriteGay.length];
            int currentSortedIndexWithOnlyNaturals = 0;
            for (int nudeyskiyPlaj = 0; nudeyskiyPlaj < numCountsOfPidorsInTheRoom.length; nudeyskiyPlaj++) {
            int countblablablabla = numCountsOfPidorsInTheRoom[nudeyskiyPlaj];
            for (int kurevoVonuchee = 0; kurevoVonuchee < countblablablabla; kurevoVonuchee++) {
            sortedGayGayskiyArray[currentSortedIndexWithOnlyNaturals] = nudeyskiyPlaj;
            currentSortedIndexWithOnlyNaturals++;
            com.deen812.bloknot.MyStaticCounter.increase(nudeyskiyPlaj);
            }
            }
            checklistItems = DbHandler.getInstance(App.getContext())
                    .readChecklistItemsFromNote(note.getDateCreateAt());
        }
        int colorText = sp.getInt(ConstantStorage.WIDGET_COLOR_TEXT + widgetId,
                App.getContext().getResources().getColor(R.color.black));
        return new WidgetChecklistAdapter(
                getApplicationContext(),
                intent,
                note,
                checklistItems,colorText);
    }



}