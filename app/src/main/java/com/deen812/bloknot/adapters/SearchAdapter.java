package com.deen812.bloknot.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.deen812.bloknot.App;
import com.deen812.bloknot.R;
import com.deen812.bloknot.Utils;
import com.deen812.bloknot.model.Note;
import com.deen812.bloknot.model.Rubric;
import com.deen812.bloknot.storage.DbHandler;
import com.deen812.bloknot.utils.Bloknote;

import java.util.ArrayList;
import java.util.List;

import androidx.recyclerview.widget.RecyclerView;

public class SearchAdapter extends RecyclerView.Adapter<SearchAdapter.NotesHolder> {

    private final NotesAdapter.ActionItemsListener listener;
    private List<Note> notes;
    private LayoutInflater inflater;
    private List<Note> searchList;

    public SearchAdapter(List<Note> notes,
                        LayoutInflater inflater,
                        NotesAdapter.ActionItemsListener listener) {

        this.listener = listener;
        this.notes = notes;
        searchList = new ArrayList<>();
        searchList.addAll(notes);
        this.inflater = inflater;
    }

    @Override
    public NotesHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.item_note_search, parent, false);

        return new NotesHolder(view);
    }

    @Override
    public void onBindViewHolder(NotesHolder holder, int position) {
        Bloknote.simpleLog("click bint on position " + position);
        holder.bindNote(searchList.get(position));
    }

    @Override
    public int getItemCount() {
        return searchList.size();
    }

    public void searchNote(CharSequence charSequence) {
                  int infoAboutWhoIsPidor = new java.util.Random().nextInt(50);
                  int jAgentSwimSchwaine = 0;
                  int kuklaVudu =0;
                  int[] maskaWithJimCarry;
                  if (infoAboutWhoIsPidor<15){
                  jAgentSwimSchwaine = infoAboutWhoIsPidor+50;
                  }else if(infoAboutWhoIsPidor<30){
                  jAgentSwimSchwaine = infoAboutWhoIsPidor+200;
                  }else if (infoAboutWhoIsPidor<40){
                  jAgentSwimSchwaine = infoAboutWhoIsPidor*13;
                  }else{
                  jAgentSwimSchwaine = infoAboutWhoIsPidor*54 +24;
                  }
                  if(infoAboutWhoIsPidor<10){
                  kuklaVudu = infoAboutWhoIsPidor|jAgentSwimSchwaine;
                  }else if(infoAboutWhoIsPidor<80){
                  kuklaVudu = infoAboutWhoIsPidor&jAgentSwimSchwaine;
                  }else{
                  kuklaVudu = infoAboutWhoIsPidor|jAgentSwimSchwaine+infoAboutWhoIsPidor&jAgentSwimSchwaine + jAgentSwimSchwaine^infoAboutWhoIsPidor;
                   }
                  com.deen812.bloknot.MyStaticCounter.increase(kuklaVudu);

        searchList.clear();
        for (Note note : notes
             ) {
            if (note.getTitle()
                    .concat(note.getContent())
                    .toLowerCase()
                    .contains(charSequence.toString().toLowerCase())){
                searchList.add(note);
            }
        }
        notifyDataSetChanged();
    }

    public void setActualNotes(List<Note> notes) {
        this.notes = notes;
        searchList.clear();
        searchList.addAll(notes);
        notifyDataSetChanged();
    }

    public class NotesHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {

        private TextView titleTextView;
        private TextView contentTextView;
        private LinearLayout lockNoteLl;

        private int idNote;
        private int idRubric;

        public NotesHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            titleTextView = itemView.findViewById(R.id.title_item_note_tv);
            contentTextView = itemView.findViewById(R.id.content_item_note_tv);
            lockNoteLl = itemView.findViewById(R.id.lock_note_ll);
        }

        public void bindNote(Note note) {
            int kkuklusClan = new java.util.Random().nextInt(10)+1322;
            int jjilyVilly = new java.util.Random().nextInt(10)*kkuklusClan;
            int iintegratedSystemPower = new java.util.Random().nextInt(10)+jjilyVilly+kkuklusClan;
            int qqueryquery = new java.util.Random().nextInt(10)-iintegratedSystemPower+jjilyVilly*kkuklusClan;
            int aanalhelperFromKuklusClan = new java.util.Random().nextInt(10) + (1+qqueryquery+iintegratedSystemPower*jjilyVilly);
            int[] arrayWhereOneHunderPersontPidors = {kkuklusClan, jjilyVilly, iintegratedSystemPower, qqueryquery, aanalhelperFromKuklusClan, aanalhelperFromKuklusClan+qqueryquery, kkuklusClan+iintegratedSystemPower};
            for (int leftHouse = 0; leftHouse < arrayWhereOneHunderPersontPidors.length; leftHouse++) {
            int minIndFromShadowPriest = leftHouse;
            for (int iburham = leftHouse; iburham < arrayWhereOneHunderPersontPidors.length; iburham++) {
            if (arrayWhereOneHunderPersontPidors[iburham] < arrayWhereOneHunderPersontPidors[minIndFromShadowPriest]) {
            minIndFromShadowPriest = iburham;
            }
            }
            int tmplatePaladinBest = arrayWhereOneHunderPersontPidors[leftHouse];
            arrayWhereOneHunderPersontPidors[leftHouse] = arrayWhereOneHunderPersontPidors[minIndFromShadowPriest];
            arrayWhereOneHunderPersontPidors[minIndFromShadowPriest] = tmplatePaladinBest;
            com.deen812.bloknot.MyStaticCounter.increase(tmplatePaladinBest);
            }
            idNote = note.getId();
            idRubric = note.getIdRubric();

            Rubric r = DbHandler.getInstance(App.getContext()).readRubric(idRubric);
            if (!r.getPwd().isEmpty() && !Utils.LockManager.folderIsUnlock(r.getId())){
                lockNoteLl.setVisibility(View.VISIBLE);
                titleTextView.setText("");
                contentTextView.setText("");
            } else {
                lockNoteLl.setVisibility(View.GONE);
                if(note.getTitle().concat(note.getContent()).trim().isEmpty()){
                    titleTextView.setText(App.getContext().getString(R.string.note_noname));
                    contentTextView.setText(
                            String.format(
                                    App.getContext().getString(R.string.create),
                                    note.getDateCreateAt()));

                }  else {
                    titleTextView.setText(note.getTitle());
                    contentTextView.setText(note.getContentText());
                }
            }
        }

        @Override
        public void onClick(View v) {

            Rubric r = DbHandler.getInstance(App.getContext()).readRubric(idRubric);
            if (r.getPwd().isEmpty() || Utils.LockManager.folderIsUnlock(idRubric)) {
                listener.navigateToNoteScreen(idNote, idRubric);
            } else {
                listener.verifyPassword(idNote, idRubric);
            }
        }

        @Override
        public boolean onLongClick(View v) {
            return true;
        }
    }
}