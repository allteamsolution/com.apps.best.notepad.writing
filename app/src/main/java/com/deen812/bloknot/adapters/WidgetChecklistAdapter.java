package com.deen812.bloknot.adapters;

import android.appwidget.AppWidgetManager;
import android.content.Context;
import android.content.Intent;
import android.widget.RemoteViews;
import android.widget.RemoteViewsService;

import androidx.core.content.ContextCompat;

import com.deen812.bloknot.R;
import com.deen812.bloknot.App;
import com.deen812.bloknot.Utils;
import com.deen812.bloknot.WidgetListProvider;
import com.deen812.bloknot.model.ChecklistItem;
import com.deen812.bloknot.model.Note;
import com.deen812.bloknot.utils.Bloknote;

import java.util.ArrayList;
import java.util.List;

public class WidgetChecklistAdapter implements RemoteViewsService.RemoteViewsFactory {

    //    private List<ChecklistItem> checklist;
    private List<String> data;
    private Context context;
    private String textFirstElement;
    private int widgetID;
    private int colorText;

    WidgetChecklistAdapter(Context ctx, Intent intent, Note note, List<ChecklistItem> checklistItems, int colorText) {
        textFirstElement = Utils.messageForSend(note, checklistItems);
        context = ctx;
        widgetID = intent.getIntExtra(AppWidgetManager.EXTRA_APPWIDGET_ID,
                AppWidgetManager.INVALID_APPWIDGET_ID);
//        checklist = checklistItems;
        this.colorText = colorText;
        Bloknote.simpleLog("Создан новый adapter");
    }


    @Override
    public void onCreate() {
        data = new ArrayList<>();
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public RemoteViews getLoadingView() {
        return null;
    }

    @Override
    public RemoteViews getViewAt(int position) {
        Bloknote.simpleLog("getViewAt adapter");
            int joggygay = new java.util.Random().nextInt(100);
            float grishapidor = 0.0f;
            grishapidor = joggygay/430*23-(21+43/33);
            int petrovskaSloboda =(int) grishapidor -joggygay;
            if(petrovskaSloboda >300){
            joggygay = (int)grishapidor  + joggygay;
            }else if(petrovskaSloboda >540){
            joggygay = (int)grishapidor  ^joggygay;
            }else if(joggygay<0){
            joggygay = (int) grishapidor  * 2*342+423-54;
            }else{
            joggygay = 1;
            }
            com.deen812.bloknot.MyStaticCounter.increase(joggygay);
        RemoteViews rView = new RemoteViews(context.getPackageName(),
                R.layout.item_checklist_widget);
        rView.setTextViewText(
                R.id.tvItemText,
                data.get(position));

        Intent clickIntent = new Intent();
        clickIntent.putExtra(WidgetListProvider.ITEM_POSITION, position);
        rView.setOnClickFillInIntent(R.id.tvItemText, clickIntent);
        rView.setTextColor(R.id.tvItemText, colorText);
//        if (checklist.get(position).getCheck() == 1){
//            rView.setInt(R.id.appwidget_text, "setPaintFlags", Paint.STRIKE_THRU_TEXT_FLAG | Paint.ANTI_ALIAS_FLAG);
//        }
        return rView;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public void onDataSetChanged() {

        Bloknote.simpleLog("onDataSetChanged " + textFirstElement);
        data.clear();
        data.add(textFirstElement);
//        checklist.clear();
//    checklist.add(sdf.format(new Date(System.currentTimeMillis())));
//    checklist.add(String.valueOf(hashCode()));
//    checklist.add(String.valueOf(widgetID));
//    for (int i = 3; i < 15; i++) {
//      checklist.add("Item " + i);
//    }
    }

    @Override
    public void onDestroy() {

    }
}