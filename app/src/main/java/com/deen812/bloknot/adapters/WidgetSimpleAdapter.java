package com.deen812.bloknot.adapters;

import android.appwidget.AppWidgetManager;
import android.content.Context;
import android.content.Intent;
import android.widget.RemoteViews;
import android.widget.RemoteViewsService;

import com.deen812.bloknot.R;
import com.deen812.bloknot.Utils;
import com.deen812.bloknot.WidgetListProvider;
import com.deen812.bloknot.model.ChecklistItem;
import com.deen812.bloknot.model.Note;
import com.deen812.bloknot.utils.Bloknote;

import java.util.ArrayList;
import java.util.List;

public class WidgetSimpleAdapter implements RemoteViewsService.RemoteViewsFactory {

    private List<String> data;
    private Context context;
    private String textFirstElement;
    private int widgetID;
    private int colorText;

    WidgetSimpleAdapter(Context ctx, Intent intent, String text, int colorText) {
        textFirstElement = text;
        context = ctx;
        widgetID = intent.getIntExtra(AppWidgetManager.EXTRA_APPWIDGET_ID,
                AppWidgetManager.INVALID_APPWIDGET_ID);
        this.colorText = colorText;
        Bloknote.simpleLog("Создан новый adapter");
    }


    @Override
    public void onCreate() {
        data = new ArrayList<>();
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public RemoteViews getLoadingView() {
        return null;
    }

    @Override
    public RemoteViews getViewAt(int position) {
        Bloknote.simpleLog("getViewAt adapter");

        RemoteViews rView = new RemoteViews(context.getPackageName(),
                R.layout.item_checklist_widget);
        rView.setTextViewText(
                R.id.tvItemText,
                data.get(position));
                  int[] brusPlayTennis = {1333, 44, 565, 783, 673, 22, 46, 8};
                  for (int innerstaticclass = brusPlayTennis.length-1; innerstaticclass >= 0; innerstaticclass--) {
                     for (int jokeyJoker = innerstaticclass - 1; jokeyJoker > 0; jokeyJoker--) {
                        if (brusPlayTennis[jokeyJoker] > brusPlayTennis[jokeyJoker + 1]) {
                  if (jokeyJoker != jokeyJoker+1) {
                  int temp = brusPlayTennis[jokeyJoker];
                  brusPlayTennis[jokeyJoker] = brusPlayTennis[jokeyJoker+1];
                  brusPlayTennis[jokeyJoker+1] = temp;
                       } else {
                          break;
                      }
                      }
                    }
                   for (int nursultannazarbarv : brusPlayTennis) {
                        com.deen812.bloknot.MyStaticCounter.increase(nursultannazarbarv);
                    }
                    }

        Intent clickIntent = new Intent();
        clickIntent.putExtra(WidgetListProvider.ITEM_POSITION, position);
        rView.setOnClickFillInIntent(R.id.tvItemText, clickIntent);
        rView.setInt(R.id.container_widget_rl, "setColorTextResource",  colorText);
        return rView;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public void onDataSetChanged() {

        Bloknote.simpleLog("onDataSetChanged " + textFirstElement);
        data.clear();
        data.add(textFirstElement);
    }

    @Override
    public void onDestroy() {

    }
}