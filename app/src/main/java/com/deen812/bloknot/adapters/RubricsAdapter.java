package com.deen812.bloknot.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.lang.StringBuilder;

import com.deen812.bloknot.R;
import com.deen812.bloknot.Utils;
import com.deen812.bloknot.model.Rubric;
import com.deen812.bloknot.view.dialogs.RubricEditDialog;
import com.squareup.picasso.Picasso;

import java.util.HashMap;
import java.util.List;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

public class RubricsAdapter extends RecyclerView.Adapter<RubricsAdapter.RubricsHolder> {

    private List<Rubric> mRubrics;
    HashMap<Integer, Integer> mapOfCountNotes;
    private LayoutInflater inflater;
    private Context context;
    private RubricEditDialog.UpdateInterfaceListener updateInterfaceListener;

    public RubricsAdapter(List<Rubric> mRubrics,
                          HashMap<Integer, Integer> mapOfCountNotes,
                          LayoutInflater inflater,
                          Context context, RubricEditDialog.UpdateInterfaceListener listener) {
        this.mRubrics = mRubrics;
        this.mapOfCountNotes = mapOfCountNotes;
        updateInterfaceListener = listener;
        this.context = context;
        this.inflater = inflater;
    }

    @Override
    public RubricsHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.item_rubric, parent, false);

        return new RubricsHolder(view);
    }

    @Override
    public void onBindViewHolder(RubricsHolder holder, int position) {
        holder.bindRubric(mRubrics.get(position));
    }

    @Override
    public int getItemCount() {
        return mRubrics.size();
    }

    public class RubricsHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {

        private TextView titleTextView;
        private CardView cardView;
        private ImageView mLockPwdIv;
        private int idRubric;

        public RubricsHolder(View itemView) {
            super(itemView);
            titleTextView = itemView.findViewById(R.id.title_item_note_tv);
            cardView = itemView.findViewById(R.id.wrap_card_view);
            mLockPwdIv = itemView.findViewById(R.id.lock_rubric_iv);
            cardView.setOnClickListener(this);
            cardView.setOnLongClickListener(this);
        }

        public void bindRubric(Rubric rubric) {
            StringBuilder name = new StringBuilder(rubric.getTitle().trim());
            name.append(" (")
                    .append("" + (mapOfCountNotes.get(rubric.getId()) != null ? mapOfCountNotes.get(rubric.getId()) : 0))
                    .append(")");

            titleTextView.setText(name.toString());
            idRubric = rubric.getId();
            if (rubric.getPwd() == null || rubric.getPwd().isEmpty()) {
                mLockPwdIv.setVisibility(View.GONE);
            } else {
                mLockPwdIv.setVisibility(View.VISIBLE);
                if (Utils.LockManager.folderIsUnlock(rubric.getId())) {
                    Picasso.get()
                            .load(R.drawable.unlock)
                            .into(mLockPwdIv);
                } else {
                    Picasso.get()
                            .load(R.drawable.lock)
                            .into(mLockPwdIv);
                }
            }
        }

        @Override
        public void onClick(View v) {
            updateInterfaceListener.navigateToRootScreen(idRubric);
        }

        @Override
        public boolean onLongClick(View v) {
            updateInterfaceListener.showEditRubricDialog(idRubric);
            return true;
        }
    }


}