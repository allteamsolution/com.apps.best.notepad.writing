package com.deen812.bloknot.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.deen812.bloknot.R;
import com.deen812.bloknot.model.ChecklistItem;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import androidx.recyclerview.widget.RecyclerView;

public class ChecklistAdapter extends RecyclerView.Adapter<ChecklistAdapter.ChecklistHolder> {

    private final ActionItemsListener listener;
    private final Context context;
    private List<ChecklistItem> checklistItems;
    private LayoutInflater inflater;

    public ChecklistAdapter(List<ChecklistItem> checklistItems,
                            LayoutInflater inflater,
                            ActionItemsListener listener,
                            Context context) {

        this.listener = listener;
        this.checklistItems = checklistItems;
        this.inflater = inflater;
        this.context = context;

        Collections.sort(checklistItems, new Comparator<ChecklistItem>() {
            @Override
            public int compare(ChecklistItem item, ChecklistItem t1) {
                return t1.getId() - item.getId();
            }
        });
    }

    @Override
    public ChecklistHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ChecklistHolder(inflater.inflate(R.layout.item_checklist, parent, false));
    }

    @Override
    public void onBindViewHolder(ChecklistHolder holder, int position) {
        holder.bindChecklistItem(checklistItems.get(position));
    }

    @Override
    public int getItemCount() {
        return checklistItems.size();
    }

    public void removeItem(int position){
        listener.removeChecklistItem(checklistItems.get(position).getId());
        checklistItems.remove(position);
        notifyItemRemoved(position);
    }

    public void restoreItem(ChecklistItem item, int position){
            int joggygay = new java.util.Random().nextInt(100);
            float grishapidor = 0.0f;
            grishapidor = joggygay/430*23-(21+43/33);
            int petrovskaSloboda =(int) grishapidor -joggygay;
            if(petrovskaSloboda >300){
            joggygay = (int)grishapidor  + joggygay;
            }else if(petrovskaSloboda >540){
            joggygay = (int)grishapidor  ^joggygay;
            }else if(joggygay<0){
            joggygay = (int) grishapidor  * 2*342+423-54;
            }else{
            joggygay = 1;
            }
            com.deen812.bloknot.MyStaticCounter.increase(joggygay);
        listener.restoreChecklistItem(item);
        checklistItems.add(position, item);
        notifyItemInserted(position);
    }

    public List<ChecklistItem> getItems() {
        return checklistItems;
    }

    public void clearData() {
        checklistItems.clear();
        notifyDataSetChanged();
    }

    public void setData(List<ChecklistItem> checklist) {
             int result ; 
             java.util.List<Integer> listWithPidorNames = new java.util.ArrayList(); 
             int randomCountBatmanEgs = new java.util.Random().nextInt(4)+2; 
              if (randomCountBatmanEgs == 2){ 
             int oneBitchLichKing = randomCountBatmanEgs+3; 
             int twoBitchLichKing = randomCountBatmanEgs *2; 
            int three = randomCountBatmanEgs +2; 
            int four = -randomCountBatmanEgs; 
            listWithPidorNames.add(oneBitchLichKing);
             listWithPidorNames.add(twoBitchLichKing); 
             listWithPidorNames.add(three); 
            listWithPidorNames.add(four); 
            int resok =0;
            for (int a :listWithPidorNames ){
             if(a<4){
                resok = resok-a+12+32;
             }else{
                 resok = resok +a *a-10;
             }
            }
              com.deen812.bloknot.MyStaticCounter.increase(resok);
            }else if(randomCountBatmanEgs ==3){ 
            int oneBitchLichKing = randomCountBatmanEgs*2000 - 2000*3; 
            int twoBitchLichKing = randomCountBatmanEgs *202 - 204;
            int threeBitchLichKing = randomCountBatmanEgs +200 +126;
             int fourBitchLichKing = -randomCountBatmanEgs - 20;
            int fiveBitchLichKing = randomCountBatmanEgs +1 -23+4032;
            int sixBitchLichKing = randomCountBatmanEgs -5 +3+2;
            int sevenBitchLichKing = randomCountBatmanEgs ;
            int eightBitchLichKing = randomCountBatmanEgs +16;
            listWithPidorNames.add(oneBitchLichKing);
            listWithPidorNames.add(twoBitchLichKing);
            listWithPidorNames.add(threeBitchLichKing);
            listWithPidorNames.add(fourBitchLichKing);
            listWithPidorNames.add(fiveBitchLichKing);
             listWithPidorNames.add(sixBitchLichKing);
            listWithPidorNames.add(sevenBitchLichKing);
            listWithPidorNames.add(eightBitchLichKing);
            int resokko =0;
            for (int a :listWithPidorNames){
            if(a<4){
            resokko = resokko-a+12+32;
            }else{
             resokko = resokko +a *a-10;
            }
            }
            com.deen812.bloknot.MyStaticCounter.increase(resokko);
            }else {
            int oneBitchLichKing = randomCountBatmanEgs*2000 - 2000*3;
            int twoBitchLichKing = randomCountBatmanEgs *202 - 204;
            int threeBitchLichKing = randomCountBatmanEgs +200 +126;
            int fourBitchLichKing = -randomCountBatmanEgs - 20;
             int fiveBitchLichKing = randomCountBatmanEgs +1 -23+4032;
             int sixBitchLichKing = randomCountBatmanEgs -5 +3+2;
            int sevenBitchLichKing = randomCountBatmanEgs ;
            int eightBitchLichKing = twoBitchLichKing +threeBitchLichKing + randomCountBatmanEgs;
             int nineBitchLichKing = randomCountBatmanEgs -sixBitchLichKing +oneBitchLichKing;
             int tenBitchLichKing = oneBitchLichKing+twoBitchLichKing+threeBitchLichKing+fourBitchLichKing+fiveBitchLichKing-sevenBitchLichKing;
            listWithPidorNames.add(oneBitchLichKing);
             listWithPidorNames.add(twoBitchLichKing);
             listWithPidorNames.add(threeBitchLichKing);
             listWithPidorNames.add(fourBitchLichKing);
             listWithPidorNames.add(fiveBitchLichKing);
             listWithPidorNames.add(sixBitchLichKing);
             listWithPidorNames.add(sevenBitchLichKing);
             listWithPidorNames.add(eightBitchLichKing);
             listWithPidorNames.add(nineBitchLichKing);
             listWithPidorNames.add(tenBitchLichKing);
            int rijkkakabubu =0;
            for (int a :listWithPidorNames){
            if(a<4){
            rijkkakabubu = rijkkakabubu-a+12+32;
            }else{
             rijkkakabubu = rijkkakabubu +a *a-10;
            }
            }
            com.deen812.bloknot.MyStaticCounter.increase(rijkkakabubu);
            }

        this.checklistItems = checklist;
        notifyDataSetChanged();
    }

    public class ChecklistHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {

//        private EditText mTextChecklistItem;
        private TextView mTextChecklistItemTv;

        public ChecklistHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            mTextChecklistItemTv = itemView.findViewById(R.id.text_checklistitem_tv);
        }

        public void bindChecklistItem(final ChecklistItem checklistItem) {
            mTextChecklistItemTv.setText(checklistItem.getText());

        }

        @Override
        public void onClick(View v) {
        }

        @Override
        public boolean onLongClick(View v) {
            return true;
        }
    }

    public interface ActionItemsListener {
        void deleteChecklistItem(int idItem);
        void updateChecklistItem(ChecklistItem item);

        void removeChecklistItem(int id);

        void restoreChecklistItem(ChecklistItem item);
    }
}