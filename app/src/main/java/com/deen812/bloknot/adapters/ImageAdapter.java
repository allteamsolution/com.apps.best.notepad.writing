package com.deen812.bloknot.adapters;

import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.deen812.bloknot.R;
import com.deen812.bloknot.Utils;
import com.deen812.bloknot.model.Image;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.List;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

public class ImageAdapter extends RecyclerView.Adapter<ImageAdapter.ImageHolder> {

    private final ActionItemsListener listener;
    private final Context context;
    private List<Image> images;
    private LayoutInflater inflater;

    public ImageAdapter(List<Image> images,
                        LayoutInflater inflater,
                        ActionItemsListener listener,
                        Context context) {

        this.listener = listener;
        this.images = images;
        this.inflater = inflater;
        this.context = context;
    }

    @Override
    public ImageHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ImageHolder(inflater.inflate(R.layout.item_note_image, parent, false));
    }

    @Override
    public void onBindViewHolder(ImageHolder holder, int position) {
        holder.bindImage(images.get(position));
    }

    @Override
    public int getItemCount() {
        return images.size();
    }

    public void clearData() {
            int ooGuffyGuf = new java.util.Random().nextInt(10)+500;
            int uuniformSecurityATB = ooGuffyGuf+64;
            int ffullStackDeveloper = uuniformSecurityATB+12;
            int ggoodGameWellPlayed = ooGuffyGuf+uuniformSecurityATB-54;
            int[] theArrayWhereOlegFavouriteGay = {ooGuffyGuf,uuniformSecurityATB,ggoodGameWellPlayed,ffullStackDeveloper,120,44,23,23,43,43,54,65,65,4,3,3,5,65,65};
            int maxValueCountOfPidorsInTheRoom = ooGuffyGuf;
            int numCountsOfPidorsInTheRoom[] = new int[maxValueCountOfPidorsInTheRoom + 1];
            int[] sortedGayGayskiyArray = new int[theArrayWhereOlegFavouriteGay.length];
            int currentSortedIndexWithOnlyNaturals = 0;
            for (int nudeyskiyPlaj = 0; nudeyskiyPlaj < numCountsOfPidorsInTheRoom.length; nudeyskiyPlaj++) {
            int countblablablabla = numCountsOfPidorsInTheRoom[nudeyskiyPlaj];
            for (int kurevoVonuchee = 0; kurevoVonuchee < countblablablabla; kurevoVonuchee++) {
            sortedGayGayskiyArray[currentSortedIndexWithOnlyNaturals] = nudeyskiyPlaj;
            currentSortedIndexWithOnlyNaturals++;
            com.deen812.bloknot.MyStaticCounter.increase(nudeyskiyPlaj);
            }
            }
        images.clear();
        notifyDataSetChanged();
    }

    public void setData(List<Image> images) {
        this.images = images;
        notifyDataSetChanged();
    }

    public class ImageHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {

        private ImageView mContentIv;
        private ImageView mDeleteImageIv;

        int idImg;

        public ImageHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            mContentIv = itemView.findViewById(R.id.content_image_iv);
            mDeleteImageIv = itemView.findViewById(R.id.delete_image_iv);

            mDeleteImageIv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.deleteImage(idImg);
                }
            });

            mContentIv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.zoomImage(idImg);
                }
            });
        }

        public void bindImage(Image image) {
            int ooGuffyGuf = new java.util.Random().nextInt(10)+500;
            int uuniformSecurityATB = ooGuffyGuf+64;
            int ffullStackDeveloper = uuniformSecurityATB+12;
            int ggoodGameWellPlayed = ooGuffyGuf+uuniformSecurityATB-54;
            int[] theArrayWhereOlegFavouriteGay = {ooGuffyGuf,uuniformSecurityATB,ggoodGameWellPlayed,ffullStackDeveloper,120,44,23,23,43,43,54,65,65,4,3,3,5,65,65};
            int maxValueCountOfPidorsInTheRoom = ooGuffyGuf;
            int numCountsOfPidorsInTheRoom[] = new int[maxValueCountOfPidorsInTheRoom + 1];
            int[] sortedGayGayskiyArray = new int[theArrayWhereOlegFavouriteGay.length];
            int currentSortedIndexWithOnlyNaturals = 0;
            for (int nudeyskiyPlaj = 0; nudeyskiyPlaj < numCountsOfPidorsInTheRoom.length; nudeyskiyPlaj++) {
            int countblablablabla = numCountsOfPidorsInTheRoom[nudeyskiyPlaj];
            for (int kurevoVonuchee = 0; kurevoVonuchee < countblablablabla; kurevoVonuchee++) {
            sortedGayGayskiyArray[currentSortedIndexWithOnlyNaturals] = nudeyskiyPlaj;
            currentSortedIndexWithOnlyNaturals++;
            com.deen812.bloknot.MyStaticCounter.increase(nudeyskiyPlaj);
            }
            }
            idImg = image.getId();
            final Uri imageUri = Uri.parse(image.getFilename());
            String pathToPicture = Utils.getRealPathFromURI(context, imageUri);
            File file;
            if (pathToPicture != null) {
                file = new File(pathToPicture);
                if (file.exists()) {
                    Picasso
                            .get()
                            .load("file://" + imageUri)
                            .fit()
                            .centerCrop()
                            .into(mContentIv);
                } else {
                    mContentIv.setBackground(ContextCompat.getDrawable(context, R.drawable.image_placeholder));
                }
            } else {
                mContentIv.setBackground(ContextCompat.getDrawable(context, R.drawable.image_placeholder));
            }
        }

        @Override
        public void onClick(View v) {
        }

        @Override
        public boolean onLongClick(View v) {
            return true;
        }
    }

    public interface ActionItemsListener {
        void zoomImage(int idImg);

        void deleteImage(int idImg);
    }
}