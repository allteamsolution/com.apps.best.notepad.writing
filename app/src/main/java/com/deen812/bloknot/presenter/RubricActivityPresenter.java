package com.deen812.bloknot.presenter;

import com.deen812.bloknot.App;
import com.deen812.bloknot.Utils;
import com.deen812.bloknot.model.Rubric;
import com.deen812.bloknot.storage.ConstantStorage;
import com.deen812.bloknot.storage.DbHandler;
import com.deen812.bloknot.view.dialogs.ChooseActionAtRubricDialog;
import com.deen812.bloknot.view.dialogs.DeleteRubricDialog;
import com.deen812.bloknot.view.dialogs.PasswordEnterDialog;
import com.deen812.bloknot.view.dialogs.RubricEditDialog;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

public class RubricActivityPresenter implements
        Presenter<RubricActivityPresenter.View>, PasswordEnterDialog.AccessDeniedListener, RubricEditDialog.UpdateInterfaceListener, ChooseActionAtRubricDialog.ChooseActionListener, DeleteRubricDialog.DeleteRubricListener {

    private DbHandler dbHandler;
    private View view;
    private List<Rubric> rubricsToView = new ArrayList<>();

    public RubricActivityPresenter() {
        dbHandler = DbHandler.getInstance(App.getContext());
    }

    @Override
    public void onViewAttached(View view) {
        try{
        this.view = view;
        List<Rubric> rubrics = convertRubricsToView(dbHandler.readRubrics());

        view.setRubrics(rubrics, dbHandler.getMapOfCountNotes());
        } catch (Exception e){

        }
    }

    @Override
    public void onViewDetached() {
        view = null;
    }

    @Override
    public void onDestroyed() {

    }

    public void addRubric(Rubric rubric){

    }

    public void createRubricDialogShow() {
        view.showRubricEditDialog(ConstantStorage.MAIN_RUBRIC);
    }

    @Override
    public int addNewRubric(Rubric rubric) {
        dbHandler.addRubric(rubric);
        view.setRubrics(convertRubricsToView(dbHandler.readRubrics()), dbHandler.getMapOfCountNotes());
        return 0;
    }


    public void updateRubric(Rubric rubric) {
        dbHandler.updateRubric(rubric);
        view.setRubrics(convertRubricsToView(dbHandler.readRubrics()), dbHandler.getMapOfCountNotes());
    }

    public void navigateToRootScreen(int idRubric) {
        Rubric rubric = dbHandler.readRubric(idRubric);
        if (rubric.getPwd().isEmpty() || Utils.LockManager.folderIsUnlock(idRubric)){
            view.openScreenWithRubric(idRubric);
        } else {
            view.showAccessDeniedErrorScreen(idRubric);
        }
    }

    private List<Rubric> convertRubricsToView(List<Rubric> source){
        rubricsToView.clear();
        for (Rubric r: source){
            if (r.getId() == ConstantStorage.ID_RECYCLE_RUBRIC || r.getId() == ConstantStorage.ID_FAVORITES_RUBRIC) {
                continue;
            }
            rubricsToView.add(r);
        }
        return rubricsToView;
    }

    @Override
    public void showEditRubricDialog(int idRubric) {
        if (idRubric != 1 && idRubric != 2 && idRubric != 3) {

            if (DbHandler.getInstance(App.getContext()).readRubric(idRubric).getPwd().isEmpty() ||
                    Utils.LockManager.folderIsUnlock(idRubric)) {
                view.showChooseActionDialog(idRubric);

            } else {
                view.showPasswordDialog(idRubric);
            }
        } else {
            view.showErrorChangeRubricDialog(idRubric);
        }
    }

    @Override
    public void accessToRubricAllowed(int idRubric) {
        Utils.LockManager.unlockFolder(idRubric);
        view.setRubrics(convertRubricsToView(dbHandler.readRubrics()), dbHandler.getMapOfCountNotes());
    }

    @Override
    public void accessDenied() {

    }

    @Override
    public void clickOnEditRubric(int idRubric) {
        view.showRubricEditDialog(idRubric);
    }

    @Override
    public void clickOnDeleteRubricAction(int idRubric) {
       view.showDeleteRubricDialog(idRubric);
    }

    @Override
    public void deleteRubric() {
        view.setRubrics(convertRubricsToView(dbHandler.readRubrics()), dbHandler.getMapOfCountNotes());
    }

    public interface View{
        void setRubrics(List<Rubric> rubrics, HashMap<Integer, Integer> mapOfCountNotes);

        void openScreenWithRubric(int idRubric);

        void showAccessDeniedErrorScreen(int idRubric);

        void showChooseActionDialog(int idRubric);

        void showPasswordDialog(int idRubric);

        void showErrorChangeRubricDialog(int idRubric);

        void showRubricEditDialog(int idRubric);

        void showDeleteRubricDialog(int idRubric);
    }
}