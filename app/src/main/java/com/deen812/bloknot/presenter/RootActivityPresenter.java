package com.deen812.bloknot.presenter;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;

import com.deen812.bloknot.R;
import com.deen812.bloknot.App;
import com.deen812.bloknot.Utils;
import com.deen812.bloknot.adapters.NotesAdapter;
import com.deen812.bloknot.model.ChecklistItem;
import com.deen812.bloknot.model.Note;
import com.deen812.bloknot.storage.BlocknotePreferencesManager;
import com.deen812.bloknot.storage.ConstantStorage;
import com.deen812.bloknot.storage.DbHandlerInterface;
import com.deen812.bloknot.storage.DbHandler;
import com.deen812.bloknot.utils.Bloknote;
import com.deen812.bloknot.view.dialogs.ChooseActionsNoteDialog;
import com.deen812.bloknot.view.dialogs.ClearRecycleDialog;
import com.deen812.bloknot.view.dialogs.ColorPickerDialog;
import com.deen812.bloknot.view.dialogs.DeleteNoteDialog;
import com.deen812.bloknot.view.dialogs.FirstChooseThemeDialog;
import com.deen812.bloknot.view.dialogs.PreColorPickerDialog;
import com.deen812.bloknot.view.dialogs.SimpleDialog;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class RootActivityPresenter implements Presenter<RootActivityPresenter.View>, NotesAdapter.ActionItemsListener, ChooseActionsNoteDialog.ChooseActionListener, SimpleDialog.ConfirmListener, FirstChooseThemeDialog.ConfirmListener, PreColorPickerDialog.ConfirmListener {

    private View view;
    private static DbHandlerInterface dbHandler;
    private Context context;
    private int idRubric;
    private List<Note> notes;
    private List<Note> notesToView = new ArrayList<>();
    private long back_pressed;
    private boolean backToRubrics;
    private int idActionNote = -1;
    private static boolean flagColdStart = true;

    public void init(Context context, int idRubric) {
        Bloknote.simpleLog("RootActivityPresenter init Execute from thread " + Thread.currentThread().getName());

        if (this.context == null) {
            this.context = context;
            this.idRubric = idRubric;
            dbHandler = DbHandler.getInstance(context);
            notes = dbHandler.getCacheNotes(true);
        }
    }

    public void getActualNotes(int idRubric) {
        this.idRubric = idRubric;
        getActualNotes();
    }

    public void getActualNotes() {
        view.setActualNotesList(convertNotesToView(notes));

        if (idRubric == ConstantStorage.ID_RECYCLE_RUBRIC) {
            view.showClearRecycleIcon();
        } else {
            view.hideClearRecycleIcon();
        }
    }

    private List<Note> convertNotesToView(List<Note> source) {
        notesToView.clear();
        Bloknote.simpleLog("convertNotesToView Execute from thread " + Thread.currentThread().getName());
        switch (idRubric) {
            case ConstantStorage.MAIN_SCREEN: {
                for (Note note : notes) {
                    if (note.getIdRubric() != ConstantStorage.ID_RECYCLE_RUBRIC) {
                        notesToView.add(note);
                    }
                }
                break;
            }
            default: {
                for (Note n : source) {
                    if (n.getIdRubric() == idRubric) notesToView.add(n);
                }
                break;
            }
        }
        return notesToView;
    }

    public static Map<Integer, String> getMapNamesOfRubric() {
        return dbHandler.getMapNamesOfRubrics();
    }

    @Override
    public void onViewAttached(View view) {
        this.view = view;
        if (idRubric == ConstantStorage.ID_RECYCLE_RUBRIC) {
            view.showClearRecycleIcon();
        } else {
            view.hideClearRecycleIcon();
        }

        view.setNotesList(convertNotesToView(notes));
    }

    @Override
    public void onViewDetached() {
        view = null;
    }

    @Override
    public void onDestroyed() {
    }

    public void switchLayout() {
        boolean isGrid = BlocknotePreferencesManager.getLayoutIsGrid();
        view.switchLayoutToGrid(!isGrid);
        BlocknotePreferencesManager.setLayoutIsGrid(!isGrid);
    }

    public String getRubricName(int idRubric) {
        switch (idRubric) {
            case ConstantStorage.MAIN_SCREEN: {
                return context.getString(R.string.all_notes);
            }
            case ConstantStorage.ID_FAVORITES_RUBRIC: {
                return App.getContext().getString(R.string.favorites);
            }
            case ConstantStorage.ID_RECYCLE_RUBRIC: {
                return App.getContext().getString(R.string.recycle);
            }
            default: {
                return dbHandler.readRubric(idRubric).getTitle();
            }
        }
    }

    public void clickSearch() {
        view.showSearch();
    }

    public List<Note> getCacheAllNotes() {
        return dbHandler.getCacheNotes(true);
    }

    @Override
    public void navigateToNoteScreen(int idNote, int idRubric) {
        view.navigateToNoteScreen(idNote, idRubric);
        view.hideSearch();
    }

    @Override
    public void clickOnNote(int idNote, int position) {
        view.showNoteActionsDialog(idNote, position);
    }

    @Override
    public void verifyPassword(int noteId, int idRubric) {
        view.showPasswordDialog(noteId, idRubric);
    }

    @Override
    public void verifyPasswordLongClick(int idNote, int idRubric, int position) {

    }

    @Override
    public void setFavoriteNote(int idNote) {
        for (Note n : notes) {
            if (n.getId() == idNote) {
                n.setFavorite(!n.isFavorite());
                dbHandler.updateNote(n);
                break;
            }
        }
        view.refreshView();
    }

    public void clickBackSearch() {
        view.hideSearch();
    }

    public void clickOnSettings() {
        view.openSettings();
    }

    public void clickOnSort(int sort) {
        BlocknotePreferencesManager.setSortNotes(sort);
        view.refreshView();
    }

    @Override
    public void clickOnShareNote(int idNote) {
        Note note = dbHandler.getNote(idNote);
        ArrayList<ChecklistItem> checklistItems = (ArrayList<ChecklistItem>) dbHandler.readChecklistItemsFromNote(note.getDateCreateAt());

        StringBuilder message = new StringBuilder();
        if (!note.getTitle().isEmpty()) {
            message.append(note.getTitle());
            message.append(" \n");
        }
        if (!note.getContentText().isEmpty()) {
            message.append(note.getContentText());
            message.append(" \n");
        }
        if (!checklistItems.isEmpty()) {
            for (ChecklistItem item : checklistItems
            ) {
                if (!item.getText().isEmpty()) {
                    message.append(item.getText())
                            .append(" ")
                            .append(item.getCheck() == 0 ? "-" : "+")
                            .append("\n");
                }
            }
        }

        view.shareNote(message.toString());
    }

    @Override
    public void clickOnDeleteNote(int idNote, int position) {
        view.showDeleteNoteDialog(idNote, position);
    }

    @Override
    public void clickOnFavoriteNote(int idNote, int position) {
        setFavoriteNote(idNote);
    }

    @Override
    public void clickOnColorAction(int idNote, int position) {
        idActionNote = idNote;
        for (Note n : notes){
            if (n.getId() == idNote){
                if (n.getTags().isEmpty()){
                    view.showColorPickerDialog(idNote);
                    return;
                } else {
                    n.setTags("");
                    dbHandler.updateNote(n);
                    view.refreshView();
                    return;
                }
            }
        }
    }

    @Override
    public void confirmAction(boolean confirm, int code, Bundle arguments) {

        switch (code) {

            case FirstChooseThemeDialog.CODE: {
                view.recreateAfterChooseTheme();
                break;
            }

            case PreColorPickerDialog.CODE: {
                int color = arguments.getInt(ConstantStorage.WIDGET_COLOR);
                for (Note n : notes) {
                    if (n.getId() == idActionNote) {
                        n.setTags(String.valueOf(color));
                        dbHandler.updateNote(n);
                        view.refreshView();
                        break;
                    }
                }
                return;
            }

            case ClearRecycleDialog
                    .DIALOG_CODE: {
                Iterator<Note> iterator = notesToView.iterator();
                while (iterator.hasNext()) {
                    Note n = iterator.next();
                    if (n.getIdRubric() == ConstantStorage.ID_RECYCLE_RUBRIC) {
                        dbHandler.deleteNote(n.getId());
                    }
                }
                notesToView.clear();
                view.refreshView();
                break;
            }

            case DeleteNoteDialog.DIALOG_CODE: {

                int idNote = arguments.getInt(ConstantStorage.NOTE_BUNDLE_KEY);
                int position = arguments.getInt(DeleteNoteDialog.KEY_POSITION);
                Iterator<Note> iterator = notesToView.iterator();
                while (iterator.hasNext()) {
                    Note n = iterator.next();
                    if (n.getId() == idNote) {
                        if (n.getIdRubric() == ConstantStorage.ID_RECYCLE_RUBRIC) {
                            dbHandler.deleteNote(idNote);

                        } else {
                            n.setIdRubric(ConstantStorage.ID_RECYCLE_RUBRIC);
                            dbHandler.updateNote(n);
                        }
                        break;
                    }
                }
                getActualNotes();
                break;
            }
        }

    }

    public void clickOnClearRecycle() {
        view.showClearRecycleDialog();
    }

    public void clickOnBack() {
        if (backToRubrics) {
            view.showRubrics();
        } else {
            if (back_pressed + 2000 > System.currentTimeMillis()) {
                Utils.LockManager.lockAllFolders();
                view.finishAndSkipFlag();
                flagColdStart = true;
                App.setLock(true);
                Utils.backupDatabase();
                DbHandler.getInstance(App.getContext()).readAllNotesTest();
            } else {
                Utils.sendToast(App.getContext().getString(R.string.exit_app));
            }
            back_pressed = System.currentTimeMillis();
        }

    }

    public void setBackToRubrics(boolean value) {
        backToRubrics = value;
    }

    public void onResume() {
        int actionAfterStart = BlocknotePreferencesManager.getActionAfterStart();
        if (flagColdStart) {
            if(!BlocknotePreferencesManager.getLockPassword().isEmpty() && App.getLock()){
                view.openPassword();
                return;
            }
                flagColdStart = false;
            switch (actionAfterStart) {
                case ConstantStorage.LAST_NOTES: {
                    view.openScreenWithRubric(ConstantStorage.MAIN_SCREEN);
                    break;
                }
                case ConstantStorage.FOLDERS: {
                    view.showRubrics();
                    break;
                }
                case ConstantStorage.NEW_NOTE_AFTER_START: {
                    view.openScreenWithNote(idRubric, ConstantStorage.NEW_NOTE);
                    break;
                }
            }
        }
    }


    public interface View {
        void openScreenWithRubric(int idRubric);

        void setNotesList(List<Note> notes);

        void switchLayoutToGrid(boolean b);

        void showSearch();

        void hideSearch();

        void navigateToNoteScreen(int idNote, int idRubric);

        void setActualNotesList(List<Note> notes);

        void showPasswordDialog(int noteId, int idRubric);

        void openSettings();

        void refreshView();

        void showNoteActionsDialog(int idNote, int position);

        void shareNote(String text);

        void showDeleteNoteDialog(int idNote, int position);

        void updateViewAfterRemove(int position);

        void showClearRecycleIcon();

        void hideClearRecycleIcon();

        void showClearRecycleDialog();

        void finish();

        void showRubrics();

        void recreateAfterChooseTheme();

        void finishAndSkipFlag();

        void showColorPickerDialog(int idNote);

        void openScreenWithNote(int idRubric, int newNote);

        void openPassword();
    }
}