package com.deen812.bloknot.presenter;

public interface PresenterFactory<T extends Presenter> {
    T create(String typePresenter);
}