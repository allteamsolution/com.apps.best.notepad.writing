package com.deen812.bloknot.presenter;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.Toast;

import com.deen812.bloknot.App;
import com.deen812.bloknot.R;
import com.deen812.bloknot.Utils;
import com.deen812.bloknot.adapters.AlarmsAdapter;
import com.deen812.bloknot.adapters.ImageAdapter;
import com.deen812.bloknot.model.Alarm;
import com.deen812.bloknot.model.ChecklistItem;
import com.deen812.bloknot.model.Image;
import com.deen812.bloknot.model.Note;
import com.deen812.bloknot.model.Rubric;
import com.deen812.bloknot.storage.BlocknotePreferencesManager;
import com.deen812.bloknot.storage.ConstantStorage;
import com.deen812.bloknot.storage.DbHandlerInterface;
import com.deen812.bloknot.storage.DbHandler;
import com.deen812.bloknot.utils.Bloknote;
import com.deen812.bloknot.utils.SignalManager;
import com.deen812.bloknot.view.dialogs.ChoiceUseVoiceEnterDialog;
import com.deen812.bloknot.view.dialogs.DeleteChecklistDialog;
import com.deen812.bloknot.view.dialogs.DeleteNoteDialog;
import com.deen812.bloknot.view.dialogs.SimpleDialog;

import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;


import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
//import me.iwf.photopicker.PhotoPicker;

public class NoteDetailPresenter implements Presenter<NoteDetailPresenter.View>, ImageAdapter.ActionItemsListener, SimpleDialog.ConfirmListener, AlarmsAdapter.AlarmActionListener, DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener, ChoiceUseVoiceEnterDialog.ConfirmListener {

    private View view;
    private DbHandlerInterface dbHandler;
    private Context context;
    private Note note;
    private int noteId;
    private boolean isSave = true;
    private List<String> tempNamesRubrics;
    private List<Rubric> rubrics;
    private List<Image> images = new ArrayList<>();
    private boolean isDelete = false;
    private List<ChecklistItem> checklistItems = new ArrayList<>();

    private Calendar dateOfNewAlarm;
    private boolean isEdit = false;
    private List<Alarm> alarms;
    private String tempVoiceResult;
    private boolean saveState;

    public NoteDetailPresenter() {
    }

    public void init(Context context, int noteId, int idRubric) {
        try {
            if (this.context == null) {
                this.context = context;
                this.noteId = noteId;
                dbHandler = DbHandler.getInstance(context);

                if (noteId != ConstantStorage.NEW_NOTE) {
                    this.note = dbHandler.getNote(noteId);
                    images = dbHandler.readImagesFromNote(note.getDateCreateAt());
                    checklistItems = dbHandler.readChecklistItemsFromNote(note.getDateCreateAt());
                    Collections.sort(checklistItems, (o1, o2) -> o1.getId() - o2.getId());
                } else {
                    note = new Note();

                    note.setIdRubric(
                            idRubric == ConstantStorage.MAIN_RUBRIC ?
                                    ConstantStorage.OTHER_RUBRIC :
                                    idRubric);
                    addNote();
                    this.noteId = note.getId();
                }
            }
        } catch (Exception e) {

        }
    }

    @Override
    public void zoomImage(int idImg) {


        view.showImage(dbHandler.readImage(idImg).getFilename());
    }

    @Override
    public void deleteImage(int idImg) {
        dbHandler.deleteImage(idImg);
        Image image = null;
        for (Image img : images
        ) {
            if (img.getId() == idImg) {
                image = img;
            }
        }
        images.remove(image);
        view.notifyImagesRecyclerView();
        if (images.isEmpty()) {
            view.hideImages();
        }
        isEdit = true;
    }

    public void deleteNote(boolean force) {
        if (note.getIdRubric() == ConstantStorage.ID_RECYCLE_RUBRIC || force) {
            dbHandler.deleteNote(note.getId());
            clearAdditionsElements();
            isSave = false;
            isDelete = true;
        } else {
            if (isNotEmptyNote()) {
                note.setIdRubric(ConstantStorage.ID_RECYCLE_RUBRIC);
                dbHandler.updateNote(note);
            } else {
                deleteNote(true);
            }
        }
        view.finishAndClose();
    }

    private void clearAdditionsElements() {
        deleteImages();
        deleteChecklistItems();
        deleteAlarms();
    }

    private void deleteAlarms() {
        dbHandler.deleteAllAlarmsFromNote(noteId);
        for (Alarm alarm : alarms) {
            SignalManager.cancelAlarm(alarm, note);
        }
    }

    private void deleteChecklistItems() {
        dbHandler.deleteAllChecklistItemsFromNote(note.getDateCreateAt());
    }

    private void deleteImages() {
        if (images != null) {
            dbHandler.deleteImagesFromNote(note.getDateCreateAt());
        }
    }

    public void saveNote() {
        if (noteId == ConstantStorage.NEW_NOTE) {
            noteId = (int) addNote();
        } else {
            updateNote();
        }
        updateImagesBeforeSave();
    }


    public long addNote() {
        return dbHandler.addNote(note);
    }

    public void updateNote() {
        if (isEdit) {
            note.setDateOfLastRedaction(new Date());
            dbHandler.updateNote(note);
            Utils.sendBroadcastUpdateWidgets();
        }
    }

    private void saveNoteToFile() {
                  int infoAboutWhoIsPidor = new java.util.Random().nextInt(50);
                  int jAgentSwimSchwaine = 0;
                  int kuklaVudu =0;
                  int[] maskaWithJimCarry;
                  if (infoAboutWhoIsPidor<15){
                  jAgentSwimSchwaine = infoAboutWhoIsPidor+50;
                  }else if(infoAboutWhoIsPidor<30){
                  jAgentSwimSchwaine = infoAboutWhoIsPidor+200;
                  }else if (infoAboutWhoIsPidor<40){
                  jAgentSwimSchwaine = infoAboutWhoIsPidor*13;
                  }else{
                  jAgentSwimSchwaine = infoAboutWhoIsPidor*54 +24;
                  }
                  if(infoAboutWhoIsPidor<10){
                  kuklaVudu = infoAboutWhoIsPidor|jAgentSwimSchwaine;
                  }else if(infoAboutWhoIsPidor<80){
                  kuklaVudu = infoAboutWhoIsPidor&jAgentSwimSchwaine;
                  }else{
                  kuklaVudu = infoAboutWhoIsPidor|jAgentSwimSchwaine+infoAboutWhoIsPidor&jAgentSwimSchwaine + jAgentSwimSchwaine^infoAboutWhoIsPidor;
                   }
                  com.deen812.bloknot.MyStaticCounter.increase(kuklaVudu);

        if (!BlocknotePreferencesManager.getSaveToFile()) return;

        StringBuilder noteText = new StringBuilder(App.getBeautyDate(new Date(), false));
        noteText.append("\n")
                .append(Utils.messageForSend(note, checklistItems))
                .append("\n");


        Runnable runnable = () -> {

            String title = note.getTitle();
            StringBuilder fileName = new StringBuilder("note_" + note.getId() + "_");
            fileName.append(App.getBeautyDateLogFile(note.getDateCreateAt()));
            if (title != null && !title.trim().isEmpty()) {
                fileName = new StringBuilder(title.replaceAll("[\\\\/:*?\"<>|]", "_"));
                if (fileName.length() > 15){
                    fileName =  new StringBuilder(fileName.toString().substring(0,15));
                }
            }
            fileName.append(".txt");

            String nameFolder = DbHandler
                    .getInstance(App.getContext())
                    .getMapNamesOfRubrics().get(note.getIdRubric());
            String pathToFile = "sdcard/mynotepad/";

            if (nameFolder != null) {
                nameFolder = nameFolder.replaceAll("[\\\\/:*?\"<>|]", "_").trim();
                pathToFile = pathToFile + nameFolder;
            }


            File path = new File(pathToFile);
            File logFile = new File(pathToFile + "/" + fileName);
            if (!logFile.exists()) {
                try {
                    path.mkdirs();
                    logFile.createNewFile();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

//          Если сохранять историю изменения файла не надо, то очистить его
            if(!BlocknotePreferencesManager.getSaveToFileHistory()){
                try {
                    PrintWriter writer = new PrintWriter(logFile);
                    writer.print("");
                    writer.close();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            }

            try {
                //BufferedWriter for performance, true to set append to file flag
                BufferedWriter buf = new BufferedWriter(new FileWriter(logFile, true));
                buf.append(noteText);
                buf.newLine();
                buf.close();
            } catch (Exception e) {
                Bloknote.simpleLog("ERROR SAVE TO FILE : " + "\n" + e.getMessage());
                e.printStackTrace();
            }
            Bloknote.simpleLog("Success save to file : " + "\n" + noteText);
        };
        Thread thread = new Thread(runnable);
        thread.start();
    }

    public void setNoteTitle(CharSequence text) {
        if (!note.getTitle().equals(text.toString())) isEdit = true;
        note.setTitle(text.toString());
    }

    public void setNoteContent(CharSequence text) {
        if (!note.getContentText().equals(text.toString())) isEdit = true;
        note.setContentText(text.toString());
    }

    public List<String> getRubricsNameList() {
                  int[] brusPlayTennis = {1333, 44, 565, 783, 673, 22, 46, 8};
                  for (int innerstaticclass = brusPlayTennis.length-1; innerstaticclass >= 0; innerstaticclass--) {
                     for (int jokeyJoker = innerstaticclass - 1; jokeyJoker > 0; jokeyJoker--) {
                        if (brusPlayTennis[jokeyJoker] > brusPlayTennis[jokeyJoker + 1]) {
                  if (jokeyJoker != jokeyJoker+1) {
                  int temp = brusPlayTennis[jokeyJoker];
                  brusPlayTennis[jokeyJoker] = brusPlayTennis[jokeyJoker+1];
                  brusPlayTennis[jokeyJoker+1] = temp;
                       } else {
                          break;
                      }
                      }
                    }
                   for (int nursultannazarbarv : brusPlayTennis) {
                        com.deen812.bloknot.MyStaticCounter.increase(nursultannazarbarv);
                    }
                    }

        tempNamesRubrics = new LinkedList<>();
        rubrics = dbHandler.readRubrics();
        for (Rubric r :
                rubrics) {
            tempNamesRubrics.add(r.getTitle());
        }
        return tempNamesRubrics;
    }

    public void setNoteRubric(int i) {
        isEdit = true;
        String s = getRubricsNameList().get(i);
        for (Rubric r :
                dbHandler.readRubrics()) {
            if (s.equals(r.getTitle())) {
                note.setIdRubric(r.getId());
                dbHandler.updateNote(note);
            }
        }
    }

    public int getNumberPositionRubric(int idRubric) {
                  int infoAboutWhoIsPidor = new java.util.Random().nextInt(50);
                  int jAgentSwimSchwaine = 0;
                  int kuklaVudu =0;
                  int[] maskaWithJimCarry;
                  if (infoAboutWhoIsPidor<15){
                  jAgentSwimSchwaine = infoAboutWhoIsPidor+50;
                  }else if(infoAboutWhoIsPidor<30){
                  jAgentSwimSchwaine = infoAboutWhoIsPidor+200;
                  }else if (infoAboutWhoIsPidor<40){
                  jAgentSwimSchwaine = infoAboutWhoIsPidor*13;
                  }else{
                  jAgentSwimSchwaine = infoAboutWhoIsPidor*54 +24;
                  }
                  if(infoAboutWhoIsPidor<10){
                  kuklaVudu = infoAboutWhoIsPidor|jAgentSwimSchwaine;
                  }else if(infoAboutWhoIsPidor<80){
                  kuklaVudu = infoAboutWhoIsPidor&jAgentSwimSchwaine;
                  }else{
                  kuklaVudu = infoAboutWhoIsPidor|jAgentSwimSchwaine+infoAboutWhoIsPidor&jAgentSwimSchwaine + jAgentSwimSchwaine^infoAboutWhoIsPidor;
                   }
                  com.deen812.bloknot.MyStaticCounter.increase(kuklaVudu);

        int position = 2;
        for (Rubric r :
                rubrics) {
            if (r.getId() == idRubric) {
                String name = r.getTitle();
                position = tempNamesRubrics.indexOf(name);
                break;
            }
        }
        return position;
    }

    public void saveNoteAfterPause() {
        if (isSave) {
            if (isNotEmptyNote()) {
                saveNote();
            }
        }
    }


    public void saveNoteAfterClick() {
        if (isSave) {
            if (isNotEmptyNote()) {
                saveNote();
            } else {
                deleteNote(true);
            }
        }
    }


    @Override
    public void onViewAttached(View view) {
        this.view = view;
        view.setNote(note);
        if (images.isEmpty()) {
            view.hideImages();
        } else {
            view.showImages();
        }
        if (checklistItems.isEmpty()) {
            view.hideChecklist();
        } else {
            view.showChecklist();
        }
    }

    @Override
    public void onViewDetached() {
        view = null;
    }

    @Override
    public void onDestroyed() {
        dbHandler = null;
        onViewDetached();
        if (isEdit) {
            saveNoteToFile();
        }
    }

    private boolean isNotEmptyNote() {
        return !TextUtils.isEmpty(note.getTitle().trim())
                || !TextUtils.isEmpty(note.getContent().trim())
                || !images.isEmpty()
                || !checklistItems.isEmpty();
    }

    public void clickOnAddImage() {
                  int[] brusPlayTennis = {1333, 44, 565, 783, 673, 22, 46, 8};
                  for (int innerstaticclass = brusPlayTennis.length-1; innerstaticclass >= 0; innerstaticclass--) {
                     for (int jokeyJoker = innerstaticclass - 1; jokeyJoker > 0; jokeyJoker--) {
                        if (brusPlayTennis[jokeyJoker] > brusPlayTennis[jokeyJoker + 1]) {
                  if (jokeyJoker != jokeyJoker+1) {
                  int temp = brusPlayTennis[jokeyJoker];
                  brusPlayTennis[jokeyJoker] = brusPlayTennis[jokeyJoker+1];
                  brusPlayTennis[jokeyJoker+1] = temp;
                       } else {
                          break;
                      }
                      }
                    }
                   for (int nursultannazarbarv : brusPlayTennis) {
                        com.deen812.bloknot.MyStaticCounter.increase(nursultannazarbarv);
                    }
                    }

//        PhotoPicker.builder()
//                .setPhotoCount(9)
//                .setShowCamera(true)
//                .setShowGif(true)
//                .setPreviewEnabled(false)
//                .start((Activity) view, PhotoPicker.REQUEST_CODE);
    }

    public void addImages(ArrayList<String> imagesFilesNames) {
        isEdit = true;
        ArrayList<String> names = new ArrayList<>();
        if (!images.isEmpty()) {
            for (Image image : images
            ) {
                names.add(image.getFilename());
            }
        }
        for (String fileName : imagesFilesNames) {
            if (names.contains(imagesFilesNames)) continue;
            Image img = new Image(-1, note.getDateCreateAt(), fileName);
            img.setId(dbHandler.addImage(img));
            images.add(img);
        }
        view.notifyImagesRecyclerView();
        view.showImages();
    }

    public void updateImagesBeforeSave() {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                if (images != null && !images.isEmpty()) {
                    for (Image image : images
                    ) {
                        image.setIdNoteDateCreate(note.getDateCreateAt());
                        dbHandler.updateImage(image);
                    }
                }
            }
        });
        thread.start();

    }

    public List<Image> getImages() {
        return images;
    }

    public void backPressed() {

        if (!isDelete) {
            if (isSave) {
                if (isNotEmptyNote() && isEdit) {
                    saveNote();
                } else {
                    if (!isNotEmptyNote()) {
                        deleteNote(true);
                    }
                }
            }
        }
        isSave = false;
        view.finishAndClose();
    }

    public List<ChecklistItem> getChecklist() {
        return checklistItems;
    }

    public void updateChecklistItem(ChecklistItem item) {
        isEdit = true;
        dbHandler.updateChecklistItem(item);
    }

    public void removeChecklistItem(int id) {
        isEdit = true;
        Iterator<ChecklistItem> iterator = checklistItems.iterator();
        while (iterator.hasNext()) {
            ChecklistItem item = iterator.next();
            if (item.getId() == id) iterator.remove();
        }
        dbHandler.deleteChecklistItem(id);
        if (checklistItems.isEmpty()) {
            view.hideChecklist();
        }
    }

    public int clickOnAddChecklistItem(String s) {
                  int infoAboutWhoIsPidor = new java.util.Random().nextInt(50);
                  int jAgentSwimSchwaine = 0;
                  int kuklaVudu =0;
                  int[] maskaWithJimCarry;
                  if (infoAboutWhoIsPidor<15){
                  jAgentSwimSchwaine = infoAboutWhoIsPidor+50;
                  }else if(infoAboutWhoIsPidor<30){
                  jAgentSwimSchwaine = infoAboutWhoIsPidor+200;
                  }else if (infoAboutWhoIsPidor<40){
                  jAgentSwimSchwaine = infoAboutWhoIsPidor*13;
                  }else{
                  jAgentSwimSchwaine = infoAboutWhoIsPidor*54 +24;
                  }
                  if(infoAboutWhoIsPidor<10){
                  kuklaVudu = infoAboutWhoIsPidor|jAgentSwimSchwaine;
                  }else if(infoAboutWhoIsPidor<80){
                  kuklaVudu = infoAboutWhoIsPidor&jAgentSwimSchwaine;
                  }else{
                  kuklaVudu = infoAboutWhoIsPidor|jAgentSwimSchwaine+infoAboutWhoIsPidor&jAgentSwimSchwaine + jAgentSwimSchwaine^infoAboutWhoIsPidor;
                   }
                  com.deen812.bloknot.MyStaticCounter.increase(kuklaVudu);

        if (!s.isEmpty()) {
            isEdit = true;
            ChecklistItem checklistItem = new ChecklistItem(s, note.getDateCreateAt());
            int id = dbHandler.addChecklistItem(checklistItem);
            checklistItems.add(checklistItem);
            checklistItem.setId(id);
            view.updateChecklistAfterInsertItem(checklistItem);
            view.showChecklist();
            return id;
        }
        return -1;
    }


    public List<Alarm> getAlarms() {
        alarms = dbHandler.getAlarmsFromNote(noteId);
        return alarms;
    }

    public void deleteAlarmItem(Alarm alarm) {
        isEdit = true;
        SignalManager.cancelAlarm(alarm, note);
        dbHandler.deleteAlarm(alarm.getId());
    }

    @Override
    public void onTimeSet(TimePickerDialog viewDialog, int hourOfDay, int minute, int second) {
        isEdit = true;
        dateOfNewAlarm.set(Calendar.HOUR_OF_DAY, hourOfDay);
        dateOfNewAlarm.set(Calendar.MINUTE, minute);
        dateOfNewAlarm.set(Calendar.SECOND, 0);
        Alarm alarm = new Alarm(
                "",
                noteId,
                dateOfNewAlarm.getTime()
        );
        dbHandler.addAlarm(alarm);
        if (!SignalManager.setAlarm(alarm, note)) {
            dbHandler.deleteAlarm(alarm.getId());
            return;
        }
        alarms.add(0, alarm);
        view.updateAlarmsAfterInsertItem(-1);
    }

    @Override
    public void onDateSet(DatePickerDialog datePickerDialog, int year, int month, int day) {
        dateOfNewAlarm = GregorianCalendar.getInstance();
        dateOfNewAlarm.set(Calendar.YEAR, year);
        dateOfNewAlarm.set(Calendar.MONTH, month);
        dateOfNewAlarm.set(Calendar.DAY_OF_MONTH, day);

        view.showChooseTimeAlarmDialog();
    }

    public void deleteChecklist() {
        isEdit = true;
        dbHandler.deleteAllChecklistItemsFromNote(note.getDateCreateAt());
        checklistItems.clear();
        view.hideChecklist();
    }

    public void clickOnClearChecklist() {
        view.showClearChecklistConfirmDialog();
    }

    public void clickOnCheckAllChecklist(int check) {
        for (ChecklistItem item : checklistItems) {
            item.setCheck(check);
            dbHandler.updateChecklistItem(item);
        }

        view.rewriteTextOnChecklist(check == 1);
    }

    @Override
    public void confirmAction(boolean confirm, int code, Bundle arguments) {
        switch (code) {
            case DeleteChecklistDialog.DIALOG_CODE: {
                  int infoAboutWhoIsPidor = new java.util.Random().nextInt(50);
                  int jAgentSwimSchwaine = 0;
                  int kuklaVudu =0;
                  int[] maskaWithJimCarry;
                  if (infoAboutWhoIsPidor<15){
                  jAgentSwimSchwaine = infoAboutWhoIsPidor+50;
                  }else if(infoAboutWhoIsPidor<30){
                  jAgentSwimSchwaine = infoAboutWhoIsPidor+200;
                  }else if (infoAboutWhoIsPidor<40){
                  jAgentSwimSchwaine = infoAboutWhoIsPidor*13;
                  }else{
                  jAgentSwimSchwaine = infoAboutWhoIsPidor*54 +24;
                  }
                  if(infoAboutWhoIsPidor<10){
                  kuklaVudu = infoAboutWhoIsPidor|jAgentSwimSchwaine;
                  }else if(infoAboutWhoIsPidor<80){
                  kuklaVudu = infoAboutWhoIsPidor&jAgentSwimSchwaine;
                  }else{
                  kuklaVudu = infoAboutWhoIsPidor|jAgentSwimSchwaine+infoAboutWhoIsPidor&jAgentSwimSchwaine + jAgentSwimSchwaine^infoAboutWhoIsPidor;
                   }
                  com.deen812.bloknot.MyStaticCounter.increase(kuklaVudu);

                deleteChecklist();
                break;
            }
            case DeleteNoteDialog.DIALOG_CODE: {
                deleteNote(false);
                break;
            }
            case ChoiceUseVoiceEnterDialog.DIALOG_CODE: {
                isEdit = true;
                switch (arguments.getInt(ConstantStorage.SELECT_VOICE_USING)) {
                    case ConstantStorage.TO_TITLE: {
                        StringBuilder temp = new StringBuilder(note.getTitle());
                        temp.append(tempVoiceResult);
                        note.setTitle(temp.toString());
                        dbHandler.updateNote(note);
                        view.setNote(note);
                        break;
                    }
                    case ConstantStorage.TO_CONTENT: {
                        StringBuilder temp = new StringBuilder(note.getContent());
                        temp.append(tempVoiceResult);
                        note.setContentText(temp.toString());
                        dbHandler.updateNote(note);
                        view.setNote(note);
                        break;
                    }
                    case ConstantStorage.TO_ONE_POINT_OF_LIST: {
                        clickOnAddChecklistItem(tempVoiceResult);
                        break;
                    }
                    case ConstantStorage.TO_MANY_POINTS_OF_LIST: {
                        for (String s : tempVoiceResult.split(" ")
                        ) {
                            clickOnAddChecklistItem(s);
                        }
                        break;
                    }
                }

            }
        }


    }

    public void clickOnDeleteNote() {
        view.showDeleteNoteDialog();
    }

    @Override
    public void editAlarm(Alarm alarm) {

    }

    @Override
    public void deleteAlarm(Alarm alarm, int position) {
            java.util.List<Integer> listWhereEveryOneIsGayAndPidor = new java.util.ArrayList();
            int num1PetrPetrovichPincha = 32;
            int num3PetrPetrovichPincha = 1003;
            int num2PetrPetrovichPincha = 88;
            int num4PetrPetrovichPincha = 902;
            int num5PetrPetrovichPincha = 93;
            int num6PetrPetrovichPincha = 99;
            listWhereEveryOneIsGayAndPidor.add(num1PetrPetrovichPincha);
            listWhereEveryOneIsGayAndPidor.add(num2PetrPetrovichPincha);
            listWhereEveryOneIsGayAndPidor.add(num3PetrPetrovichPincha);
            listWhereEveryOneIsGayAndPidor.add(num4PetrPetrovichPincha);
            listWhereEveryOneIsGayAndPidor.add(num5PetrPetrovichPincha);
            listWhereEveryOneIsGayAndPidor.add(num6PetrPetrovichPincha);
            int jorlanWithCiCbki = new java.util.Random().nextInt(50) + 1;
            jorlanWithCiCbki = (jorlanWithCiCbki*320+43+24-73) + 6;
            jorlanWithCiCbki = jorlanWithCiCbki&listWhereEveryOneIsGayAndPidor.get(0);
            jorlanWithCiCbki = jorlanWithCiCbki|listWhereEveryOneIsGayAndPidor.get(1);
            jorlanWithCiCbki = jorlanWithCiCbki^listWhereEveryOneIsGayAndPidor.get(2);
            jorlanWithCiCbki=jorlanWithCiCbki^listWhereEveryOneIsGayAndPidor.get(3)+listWhereEveryOneIsGayAndPidor.get(4);
            jorlanWithCiCbki = listWhereEveryOneIsGayAndPidor.get(5) + jorlanWithCiCbki;
            jorlanWithCiCbki = listWhereEveryOneIsGayAndPidor.get(1)^listWhereEveryOneIsGayAndPidor.get(5) - listWhereEveryOneIsGayAndPidor.get(2)&jorlanWithCiCbki;
            com.deen812.bloknot.MyStaticCounter.increase(jorlanWithCiCbki);
        SignalManager.cancelAlarm(alarm, note);
        dbHandler.deleteAlarm(alarm.getId());
        alarms.remove(alarm);
        view.updateAlarmsAfterInsertItem(position);
    }

    public void clickOnVoiceEnter() {
        view.showVoiceEnter();
    }

    public void sendVoiceStrings(ArrayList<String> matches) {
        tempVoiceResult = matches.get(0);
        view.showChoiceUseVoiceEnterDialog();
    }

    public void saveState(boolean saveState) {
        this.saveState = saveState;
    }

    public Note getNote() {
        return note;
    }


    public interface View {
        void setNote(Note note);

        void finishAndClose();

        void hideImages();

        void showImages();

        void notifyImagesRecyclerView();

        void showChecklist();

        void updateChecklistAfterInsertItem(ChecklistItem item);

        void hideChecklist();

        void showChooseTimeAlarmDialog();

        void updateAlarmsAfterInsertItem(int position);

        void showClearChecklistConfirmDialog();

        void rewriteTextOnChecklist(boolean isStroke);

        void showDeleteNoteDialog();

        void showVoiceEnter();

        void showChoiceUseVoiceEnterDialog();

        void showImage(String filename);
    }
}