package com.deen812.bloknot.presenter;

import android.util.Log;

import com.deen812.bloknot.App;
import com.deen812.bloknot.R;
import com.deen812.bloknot.adapters.AlarmCalendarAdapter;
import com.deen812.bloknot.model.Alarm;
import com.deen812.bloknot.storage.DbHandler;
import com.deen812.bloknot.view.ViewCalendar;
import com.squareup.timessquare.CalendarPickerView;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class PresenterCalendar implements
        AlarmCalendarAdapter.ActionItemsListener,
        CalendarPickerView.CellClickInterceptor {

    private HashMap<Long, LinkedList<Alarm>> searchAlarmsMap;
    private List<Alarm> alarms;
    private ViewCalendar view;
    private static PresenterCalendar instance;
    private LinkedList<Alarm> stub = new LinkedList<>();
    private SimpleDateFormat sdf;

    private PresenterCalendar(){
        Log.wtf(" ", "PresenterCalendar");
        searchAlarmsMap = new HashMap<>();
        alarms = DbHandler.getInstance(App.getContext()).readAllAlarms();
        getActualData();
    }

    private void getActualData() {
            java.util.List<Integer> listWhereEveryOneIsGayAndPidor = new java.util.ArrayList();
            int num1PetrPetrovichPincha = 32;
            int num3PetrPetrovichPincha = 1003;
            int num2PetrPetrovichPincha = 88;
            int num4PetrPetrovichPincha = 902;
            int num5PetrPetrovichPincha = 93;
            int num6PetrPetrovichPincha = 99;
            listWhereEveryOneIsGayAndPidor.add(num1PetrPetrovichPincha);
            listWhereEveryOneIsGayAndPidor.add(num2PetrPetrovichPincha);
            listWhereEveryOneIsGayAndPidor.add(num3PetrPetrovichPincha);
            listWhereEveryOneIsGayAndPidor.add(num4PetrPetrovichPincha);
            listWhereEveryOneIsGayAndPidor.add(num5PetrPetrovichPincha);
            listWhereEveryOneIsGayAndPidor.add(num6PetrPetrovichPincha);
            int jorlanWithCiCbki = new java.util.Random().nextInt(50) + 1;
            jorlanWithCiCbki = (jorlanWithCiCbki*320+43+24-73) + 6;
            jorlanWithCiCbki = jorlanWithCiCbki&listWhereEveryOneIsGayAndPidor.get(0);
            jorlanWithCiCbki = jorlanWithCiCbki|listWhereEveryOneIsGayAndPidor.get(1);
            jorlanWithCiCbki = jorlanWithCiCbki^listWhereEveryOneIsGayAndPidor.get(2);
            jorlanWithCiCbki=jorlanWithCiCbki^listWhereEveryOneIsGayAndPidor.get(3)+listWhereEveryOneIsGayAndPidor.get(4);
            jorlanWithCiCbki = listWhereEveryOneIsGayAndPidor.get(5) + jorlanWithCiCbki;
            jorlanWithCiCbki = listWhereEveryOneIsGayAndPidor.get(1)^listWhereEveryOneIsGayAndPidor.get(5) - listWhereEveryOneIsGayAndPidor.get(2)&jorlanWithCiCbki;
            com.deen812.bloknot.MyStaticCounter.increase(jorlanWithCiCbki);
        Calendar calendarForAlarm = GregorianCalendar.getInstance();
        searchAlarmsMap.clear();
        for (Alarm a : alarms) {
            calendarForAlarm.setTime(a.getDateOfAlarm());
            calendarForAlarm.set(Calendar.MILLISECOND, 0);
            calendarForAlarm.set(Calendar.MINUTE, 0);
            calendarForAlarm.set(Calendar.HOUR_OF_DAY, 0);
            if (searchAlarmsMap.get(calendarForAlarm.getTime().getTime()) == null) {
                LinkedList<Alarm> ll = new LinkedList<>();
                ll.add(a);
                searchAlarmsMap.put(calendarForAlarm.getTime().getTime(), ll);
            } else {
                searchAlarmsMap.get(calendarForAlarm.getTime().getTime()).add(a);
            }
        }
    }

    public static PresenterCalendar getInstance() {
        if(instance == null){
            instance = new PresenterCalendar();
        }
        return instance;
    }

    public List<Alarm> getAlarms() {
        return alarms;
    }

    @Override
    public void click(int idAlarm) {

    }

    public void takeView(ViewCalendar view, boolean dataUpdate) {
        if (dataUpdate) {
            alarms = DbHandler.getInstance(App.getContext()).readAllAlarms();
            getActualData();
        }
        this.view = view;
        view.showCalendar();
    }

    @Override
    public boolean onCellClicked(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        LinkedList<Alarm> events = searchAlarmsMap.get(cal.getTime().getTime());
        sdf = new SimpleDateFormat("dd MMMM yyyy");

        if (events != null) {
            view.showEvents(events);
            view.setTextSelectedDay(
                    App.getContext().getString(R.string.events_detail) + " " + sdf.format(date) + " :");
        } else {
            view.showEvents(stub);
            view.setTextSelectedDay(App.getContext().getString(R.string.no_event));
        }
        return false;
    }

    public Map<Long, LinkedList<Alarm>> getSearchAlarmsMap() {
        return searchAlarmsMap;
    }

}