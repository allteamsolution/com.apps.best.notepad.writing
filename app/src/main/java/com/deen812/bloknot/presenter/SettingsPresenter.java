package com.deen812.bloknot.presenter;

import android.os.Bundle;

import com.deen812.bloknot.R;
import com.deen812.bloknot.App;
import com.deen812.bloknot.Utils;
import com.deen812.bloknot.retrofit.SyncController;
import com.deen812.bloknot.storage.BlocknotePreferencesManager;
import com.deen812.bloknot.utils.Bloknote;
import com.deen812.bloknot.view.dialogs.ExportNotesInfoDialog;
import com.deen812.bloknot.view.dialogs.SetEnterPasswordDialog;
import com.deen812.bloknot.view.dialogs.SynchronizationInfoDialog;
import com.deen812.bloknot.view.dialogs.ThemeDialog;

public class SettingsPresenter implements Presenter<SettingsPresenter.View>, SyncController.ServerCommunicationListener, ThemeDialog.ConfirmListener, SynchronizationInfoDialog.ConfirmListener, ExportNotesInfoDialog.ConfirmListener, SetEnterPasswordDialog.UpdateInterfaceListener {

    private View view;

    public SettingsPresenter() {
        Bloknote.simpleLog("Новый презентер создан");
    }

    @Override
    public void onViewAttached(View view) {
        this.view = view;
    }

    @Override
    public void onViewDetached() {
        view = null;
    }

    @Override
    public void onDestroyed() {

    }

    public void clickOnSynchronization() {
        if (Utils.isOnline()) {
            String email = BlocknotePreferencesManager.getUserEmail();
            if (email.isEmpty()) {
                view.showAuthScreen();
            } else {
                view.showSynchronizationInfo();
            }
        } else {
            view.showNetworkError();
        }
    }

    public void clickOnDeleteDataFromServer() {
        if (Utils.isOnline()) {
            view.showConfirmDeleteDataFromServer();
        } else {
            view.showNetworkError();
        }
    }

    @Override
    public void syncDataIsEmpty() {
        view.showSynchNothing();
    }

    @Override
    public void dataFromServerSaved() {
    }

    @Override
    public void errorSync(String message) {
        view.hideSynchronizationProcess();
        view.showInformationLong(message);
    }

    @Override
    public void dataFromDeviceSendOnServer() {

    }

    @Override
    public void dataFromServerDelete() {
        String syncSuccess = App.getContext().getString(R.string.data_from_server_delete);
        view.showInformationShort(syncSuccess);
    }

    @Override
    public void dataSyncSuccess() {
        view.hideSynchronizationProcess();
        String syncSuccess = App.getContext().getString(R.string.sync_success);
        view.showInformationShort(syncSuccess);

    }

    @Override
    public void syncProcess() {

    }

    @Override
    public void passwordResetSuccess() {
        String syncSuccess = App.getContext().getString(R.string.reset_pwd_success);
        view.showInformationLong(syncSuccess);
    }

    public void init() {

    }

    @Override
    public void confirmAction(boolean confirm, int code, Bundle arguments) {
        switch (code) {

            case ExportNotesInfoDialog.CODE:{
                if (confirm){
                    view.showInformationShort(App.getContext().getString(R.string.export_success));
                } else {
                    view.showInformationLong(App.getContext().getString(R.string.export_error));
                }
                break;
            }

            case ThemeDialog.CODE: {
                view.update();
                break;
            }

            case SynchronizationInfoDialog.CODE: {
                view.showSyncProcessScreen();
                String email = BlocknotePreferencesManager.getUserEmail();
                String hash = BlocknotePreferencesManager.getUserHash();
                SyncController.loadData(this, hash, email);
            }
        }
    }

    @Override
    public void updateCheckboxPassword() {
        view.updateCheckboxPasswordOnEnter(!BlocknotePreferencesManager.getLockPassword().isEmpty());
    }


    public interface View {
        void showSyncProcessScreen();

        void showConfirmDeleteDataFromServer();

        void showAuthScreen();

        void showInformationShort(String text);

        void showInformationLong(String text);

        void update();

        void showNetworkError();

        void showSynchronizationInfo();

        void hideSynchronizationProcess();

        void showSynchNothing();

        void updateCheckboxPasswordOnEnter(boolean check);
    }
}