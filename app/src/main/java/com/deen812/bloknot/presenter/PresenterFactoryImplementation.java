package com.deen812.bloknot.presenter;

import com.deen812.bloknot.storage.ConstantStorage;
import com.deen812.bloknot.utils.Bloknote;

public class PresenterFactoryImplementation implements PresenterFactory {

    @Override
    public Presenter create(String typePresenter) {
        Bloknote.simpleLog("factory create");
        switch (typePresenter){
            case ConstantStorage.PRESENTER_ROOT:{
                Bloknote.simpleLog("switch PRESENTER ROOT");
                return new RootActivityPresenter();
            }
            case ConstantStorage.PRESENTER_RUBRICS: {
                return new RubricActivityPresenter();
            }
            case ConstantStorage.PRESENTER_NOTE_DETAIL:{
                return new NoteDetailPresenter();
            }
            case ConstantStorage.PRESENTER_SETTINGS:{
                return new SettingsPresenter();
            }
            default:{
                return null;
            }
        }
    }
}