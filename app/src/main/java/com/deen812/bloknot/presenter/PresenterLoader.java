package com.deen812.bloknot.presenter;

import android.content.Context;

import com.deen812.bloknot.utils.Bloknote;

import androidx.annotation.NonNull;
import androidx.loader.content.Loader;


public class PresenterLoader<T extends Presenter> extends Loader<T> {
    private final PresenterFactory<T> factory;
    private String typePresenter;
    private T presenter;

    /**
     * Stores away the application context associated with context.
     * Since Loaders can be used across multiple activities it's dangerous to
     * store the context directly; always use {@link #getContext()} to retrieve
     * the Loader's Context, don't use the constructor argument directly.
     * The Context returned by {@link #getContext} is safe to use across
     * Activity instances.
     *
     * @param context used to retrieve the application context.
     */
    public PresenterLoader(@NonNull Context context, String typePresenter) {
        super(context);
        this.typePresenter = typePresenter;
        factory = new PresenterFactoryImplementation();
        Bloknote.simpleLog("init loader constructor");
    }

    @Override
    protected void onStartLoading() {
        Bloknote.simpleLog("onStartLoading loader");
        // Если объект презентара уже создан, просто передаем его.
        if (presenter != null) {
            Bloknote.simpleLog("return presenter");
            deliverResult(presenter);
            return;
        }
        // Иначе загружаем его
        forceLoad();
    }
    @Override
    protected void onForceLoad() {
        Bloknote.simpleLog("onForceLoad");
        // Создаём презентер, используя фабрику
        presenter = factory.create(typePresenter);
        // Передаём результат
        deliverResult(presenter);
    }
    @Override
    protected void onReset() {
        Bloknote.simpleLog("onReset, destroyed");
        presenter.onDestroyed();
        presenter = null;
    }


}
