package com.deen812.bloknot.analytics;

public class AnalyticsEvent {


    public static String TRIAL_OFFER_SCREEN_OPEN_0 = "trial_offer_screen_open_0";
    public static String TRIAL_OFFER_SCREEN_CLOSE_0 = "trial_offer_screen_close_0";
    public static String TRIAL_OFFER_SCREEN_CLICKBUY_0 = "trial_offer_screen_clickbuy_0";
    public static String TRIAL_OFFER_SCREEN_SUCCESS_BUY_0 = "trial_offer_screen_success_buy_0";

    public static String TRIAL_OFFER_SCREEN_OPEN_1 = "trial_offer_screen_open_1";
    public static String TRIAL_OFFER_SCREEN_CLOSE_1 = "trial_offer_screen_close_1";
    public static String TRIAL_OFFER_SCREEN_CLICKBUY_1 = "trial_offer_screen_clickbuy_1";
    public static String TRIAL_OFFER_SCREEN_SUCCESS_BUY_1 = "trial_offer_screen_success_buy_1";

    public static String TRIAL_OFFER_SCREEN_OPEN_2 = "trial_offer_screen_open_2";
    public static String TRIAL_OFFER_SCREEN_CLOSE_2 = "trial_offer_screen_close_2";
    public static String TRIAL_OFFER_SCREEN_CLICKBUY_2 = "trial_offer_screen_clickbuy_2";
    public static String TRIAL_OFFER_SCREEN_SUCCESS_BUY_2 = "trial_offer_screen_success_buy_2";

    public static String TRIAL_OFFER_SCREEN_OPEN_3 = "trial_offer_screen_open_3";
    public static String TRIAL_OFFER_SCREEN_CLOSE_3 = "trial_offer_screen_close_3";
    public static String TRIAL_OFFER_SCREEN_CLICKBUY_3 = "trial_offer_screen_clickbuy_3";
    public static String TRIAL_OFFER_SCREEN_SUCCESS_BUY_3 = "trial_offer_screen_success_buy_3";

    public static String TRIAL_OFFER_SCREEN_OPEN_4 = "trial_offer_screen_open_4";
    public static String TRIAL_OFFER_SCREEN_CLOSE_4 = "trial_offer_screen_close_4";
    public static String TRIAL_OFFER_SCREEN_SUCCESS_BUY_4_MONTH = "trial_offer_screen_success_buy_4_month";
    public static String TRIAL_OFFER_SCREEN_SUCCESS_BUY_4 = "trial_offer_screen_success_buy_4";
    public static String TRIAL_OFFER_SCREEN_SUCCESS_BUY_4_NOFREE = "trial_offer_screen_success_buy_4_nofree";



    public static String PURCHASE_BUY_WEEK_0 = "purchase_buy_week_0";
    public static String PURCHASE_BUY_MONTH_0 = "purchase_buy_month_0";
    public static String PURCHASE_BUY_YEAR_0 = "purchase_buy_year_0";
    public static String PURCHASE_SCREEN_OPEN_0 = "purchase_screen_open_0";
    public static String PURCHASE_SCREEN_CLOSE_0 = "purchase_screen_close_0";


    public static String PURCHASE_BUY_WEEK_1 = "purchase_buy_week_1";
    public static String PURCHASE_BUY_MONTH_1 = "purchase_buy_month_1";
    public static String PURCHASE_BUY_YEAR_1 = "purchase_buy_year_1";
    public static String PURCHASE_SCREEN_OPEN_1 = "purchase_screen_open_1";
    public static String PURCHASE_SCREEN_CLOSE_1 = "purchase_screen_close_1";


    public static String PURCHASE_BUY_WEEK_2 = "purchase_buy_week_2";
    public static String PURCHASE_BUY_MONTH_2 = "purchase_buy_month_2";
    public static String PURCHASE_BUY_YEAR_2 = "purchase_buy_year_2";
    public static String PURCHASE_SCREEN_OPEN_2 = "purchase_screen_open_2";
    public static String PURCHASE_SCREEN_CLOSE_2 = "purchase_screen_close_2";



    public static String PURCHASE_BUY_WEEK_3 = "purchase_buy_week_3";
    public static String PURCHASE_BUY_MONTH_3 = "purchase_buy_month_3";
    public static String PURCHASE_BUY_YEAR_3 = "purchase_buy_year_3";
    public static String PURCHASE_SCREEN_OPEN_3 = "purchase_screen_open_3";
    public static String PURCHASE_SCREEN_CLOSE_3 = "purchase_screen_close_3";


    public static String PURCHASE_BUY_WEEK_4 = "purchase_buy_week_4";
    public static String PURCHASE_BUY_MONTH_4 = "purchase_buy_month_4";
    public static String PURCHASE_BUY_YEAR_4 = "purchase_buy_year_4";
    public static String PURCHASE_SCREEN_OPEN_4 = "purchase_screen_open_4";
    public static String PURCHASE_SCREEN_CLOSE_4 = "purchase_screen_close_4";






}