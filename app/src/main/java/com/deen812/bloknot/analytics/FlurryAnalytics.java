package com.deen812.bloknot.analytics;

import com.deen812.bloknot.BuildConfig;
import com.flurry.android.FlurryAgent;

import java.util.HashMap;
import java.util.Map;

public class FlurryAnalytics{
    public static void sendEvent(String params) {
        if (BuildConfig.DEBUG) {
            return;
        }
        FlurryAgent.logEvent(params);
    }
    public static void sendEvent(String params, String key1, String val1, String key2, String val2) {
        if (BuildConfig.DEBUG) {
            return;
        }
        Map<String, String> map = new HashMap<>();
        map.put(key1, val1);
        map.put(key2, val2);
        FlurryAgent.logEvent(params, map);
    }
    public static void sendEvent(String params, String key1, String val1, String key2, String val2, String key3, String val3, String key4, String val4) {
        if (BuildConfig.DEBUG) {
            return;
        }
        Map<String, String> map = new HashMap<>();
        map.put(key1, val1);
        map.put(key2, val2);
        map.put(key3, val3);
        map.put(key4, val4);
        FlurryAgent.logEvent(params, map);
    }

    public static void sendEvent(String params, String key1, String val1) {
        if (BuildConfig.DEBUG) {
            return;
        }
        Map<String, String> map = new HashMap<>();
        map.put(key1, val1);
        FlurryAgent.logEvent(params, map);
    }
}