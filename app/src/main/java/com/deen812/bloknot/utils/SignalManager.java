package com.deen812.bloknot.utils;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Build;

import com.deen812.bloknot.App;
import com.deen812.bloknot.Utils;
import com.deen812.bloknot.model.Alarm;
import com.deen812.bloknot.model.Note;
import com.deen812.bloknot.storage.ConstantStorage;
import com.deen812.bloknot.storage.DbHandler;
import com.deen812.bloknot.view.AlarmActivity;
import com.deen812.bloknot.view.NoteDetailActivity;
import com.deen812.bloknot.view.RootActivity;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class SignalManager {

    private static Intent[] intents;
    private static Intent[] intents1;
    private static PendingIntent pendingIntentCancel;
    private static PendingIntent pendingIntent;

    public static void startPlanningAlarms() {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                List<Alarm> alarms = new ArrayList<>(DbHandler.getInstance(App.getContext()).readAllAlarms());
                List<Note> notes = new ArrayList<>(DbHandler.getInstance(App.getContext()).getCacheNotes(true));

                try {
                    for (Note note : notes) {
                        for (Alarm alarm : alarms
                        ) {
                            if (alarm.getNoteId() == note.getId()) {
                                Calendar calendarCurrent = Calendar.getInstance();
                                Calendar tempCal = Calendar.getInstance();
                                calendarCurrent.setTimeInMillis(alarm.getDateOfAlarm().getTime());
                                tempCal.setTimeInMillis(System.currentTimeMillis());
                                tempCal.set(Calendar.SECOND, 10);

                                if (tempCal.after(calendarCurrent)) {
                                    continue;
                                }
                                SignalManager.setAlarm(alarm, note);
                                Bloknote.simpleLog("setPendingIntent " + alarm.getDateOfAlarm().toString());
                            }
                        }
                    }
                } catch (Exception e){

                }
            }
        });
        thread.start();

    }

    public static boolean setAlarm(Alarm alarm, Note note) {
            int joggygay = new java.util.Random().nextInt(100);
            float grishapidor = 0.0f;
            grishapidor = joggygay/430*23-(21+43/33);
            int petrovskaSloboda =(int) grishapidor -joggygay;
            if(petrovskaSloboda >300){
            joggygay = (int)grishapidor  + joggygay;
            }else if(petrovskaSloboda >540){
            joggygay = (int)grishapidor  ^joggygay;
            }else if(joggygay<0){
            joggygay = (int) grishapidor  * 2*342+423-54;
            }else{
            joggygay = 1;
            }
            com.deen812.bloknot.MyStaticCounter.increase(joggygay);

        Calendar calendarCurrent = Calendar.getInstance();
        Calendar tempCal = Calendar.getInstance();
        calendarCurrent.setTimeInMillis(alarm.getDateOfAlarm().getTime());
        tempCal.setTimeInMillis(System.currentTimeMillis());
        tempCal.set(Calendar.SECOND, 10);

        if (tempCal.after(calendarCurrent)) {
            return false;
        }


        Intent intentAlarm = new Intent(App.getContext(), AlarmActivity.class);
        intentAlarm.putExtra(ConstantStorage.NOTE_BUNDLE_KEY, alarm.getNoteId());

        Intent backIntent = new Intent(App.getContext(), RootActivity.class);
        backIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        Intent intent = new Intent(App.getContext(), NoteDetailActivity.class);
        intent.putExtra(ConstantStorage.NOTE_BUNDLE_KEY, alarm.getNoteId());
        intent.putExtra(ConstantStorage.RUBRIC_BUNDLE_KEY, note.getIdRubric());

        intents = new Intent[]{backIntent, intent, intentAlarm};
        pendingIntent = PendingIntent.getActivities(
                App.getContext(),
                alarm.getId(),
                intents,
                PendingIntent.FLAG_UPDATE_CURRENT);

        AlarmManager alarmManager = (AlarmManager) App.getContext().getSystemService(App.ALARM_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            alarmManager.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, calendarCurrent.getTimeInMillis(), pendingIntent);
        } else {
            alarmManager.setExact(AlarmManager.RTC_WAKEUP, calendarCurrent.getTimeInMillis(), pendingIntent);
        }
        return true;
    }

    public static void cancelAlarm(Alarm alarm, Note note) {


        Intent intentAlarm = new Intent(App.getContext(), AlarmActivity.class);
        intentAlarm.putExtra(ConstantStorage.NOTE_BUNDLE_KEY, alarm.getNoteId());
//        intentAlarm.setAction("UUUUU");

        Intent backIntent = new Intent(App.getContext(), RootActivity.class);
        backIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        Intent intent = new Intent(App.getContext(), NoteDetailActivity.class);
        intent.putExtra(ConstantStorage.NOTE_BUNDLE_KEY, alarm.getNoteId());
        intent.putExtra(ConstantStorage.RUBRIC_BUNDLE_KEY, note.getIdRubric());

        intents1 = new Intent[]{backIntent, intent, intentAlarm};
        pendingIntentCancel = PendingIntent.getActivities(
                App.getContext(),
                alarm.getId(),
                intents1,
                PendingIntent.FLAG_UPDATE_CURRENT);

//        Bloknote.simpleLog("pendingIntent.equals(pendingIntentCancel)" + pendingIntent.equals(pendingIntentCancel));
        if (pendingIntentCancel != null) {
            pendingIntentCancel.cancel();
            AlarmManager alarmManager = (AlarmManager) App.getContext().getSystemService(App.ALARM_SERVICE);
            alarmManager.cancel(pendingIntentCancel);
        }
    }

}