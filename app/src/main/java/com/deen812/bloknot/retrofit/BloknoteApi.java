package com.deen812.bloknot.retrofit;

import com.deen812.bloknot.model.ContainerData;
import com.deen812.bloknot.model.LoginResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface BloknoteApi {

    @FormUrlEncoded
    @POST("getdata-container")
    Call<ContainerData> loadData(
            @Field("hash") String hash,
            @Field("email") String email);

    @FormUrlEncoded
    @POST("update-newversion")
    Call<ContainerData> updateData(
            @Field("hash") String hash,
            @Field("email") String email,
            @Field("Rubric") String rubrics,
            @Field("Good") String goods,
            @Field("Checklist") String checklists,
            @Field("Alarms") String alarms,
            @Field("Images") String images
            );

    @FormUrlEncoded
    @POST("delete-user-data")
    Call<LoginResponse> deleteAllFromServer(
            @Field("hash") String hash,
            @Field("email") String email
    );

    @FormUrlEncoded
    @POST("registration-new-version")
    Call<LoginResponse> registration(
            @Field("User[email]=") String email,
            @Field("Reg") int reg,
            @Field("User[pass]=") String pass);

    @FormUrlEncoded
    @POST("getpass")
    Call<Void> resetPassword(
            @Field("email") String email
            );
}