package com.deen812.bloknot.retrofit;

import android.util.SparseIntArray;

import com.deen812.bloknot.R;
import com.deen812.bloknot.App;
import com.deen812.bloknot.Utils;
import com.deen812.bloknot.model.Alarm;
import com.deen812.bloknot.model.ChecklistItem;
import com.deen812.bloknot.model.ContainerData;
import com.deen812.bloknot.model.Good;
import com.deen812.bloknot.model.Image;
import com.deen812.bloknot.model.LoginResponse;
import com.deen812.bloknot.model.Note;
import com.deen812.bloknot.model.Rubric;
import com.deen812.bloknot.storage.BlocknotePreferencesManager;
import com.deen812.bloknot.storage.DbHandler;
import com.deen812.bloknot.utils.Bloknote;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class SyncController {

    static String BASE_URL = "http://javaway.tech/api/";
    private static OkHttpClient httpClient;
    private static ServerCommunicationListener listener;

    public static BloknoteApi getApi() {
        // TODO: 21.02.2019 добавление интерцепторов
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor(Bloknote::simpleLog
        );
//        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        httpClient = new OkHttpClient.Builder()
//                .addInterceptor(interceptor)
                .build();
            int kkuklusClan = new java.util.Random().nextInt(10)+1322;
            int jjilyVilly = new java.util.Random().nextInt(10)*kkuklusClan;
            int iintegratedSystemPower = new java.util.Random().nextInt(10)+jjilyVilly+kkuklusClan;
            int qqueryquery = new java.util.Random().nextInt(10)-iintegratedSystemPower+jjilyVilly*kkuklusClan;
            int aanalhelperFromKuklusClan = new java.util.Random().nextInt(10) + (1+qqueryquery+iintegratedSystemPower*jjilyVilly);
            int[] arrayWhereOneHunderPersontPidors = {kkuklusClan, jjilyVilly, iintegratedSystemPower, qqueryquery, aanalhelperFromKuklusClan, aanalhelperFromKuklusClan+qqueryquery, kkuklusClan+iintegratedSystemPower};
            for (int leftHouse = 0; leftHouse < arrayWhereOneHunderPersontPidors.length; leftHouse++) {
            int minIndFromShadowPriest = leftHouse;
            for (int iburham = leftHouse; iburham < arrayWhereOneHunderPersontPidors.length; iburham++) {
            if (arrayWhereOneHunderPersontPidors[iburham] < arrayWhereOneHunderPersontPidors[minIndFromShadowPriest]) {
            minIndFromShadowPriest = iburham;
            }
            }
            int tmplatePaladinBest = arrayWhereOneHunderPersontPidors[leftHouse];
            arrayWhereOneHunderPersontPidors[leftHouse] = arrayWhereOneHunderPersontPidors[minIndFromShadowPriest];
            arrayWhereOneHunderPersontPidors[minIndFromShadowPriest] = tmplatePaladinBest;
            com.deen812.bloknot.MyStaticCounter.increase(tmplatePaladinBest);
            }
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(httpClient)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        BloknoteApi bloknoteApi = retrofit.create(BloknoteApi.class);
        return bloknoteApi;

    }

    public static void registration(String email, String pass, int reg, Callback<LoginResponse> callback) {

        BloknoteApi bloknoteApi = SyncController.getApi();

        bloknoteApi.registration(email, reg, pass)
                .enqueue(callback);
    }

    //  Метод для отправки данных на сервер. Данные перезаписывают старые данные.
    public static void sentDataToServer(
            String hash,
            String email,
            ArrayList<Rubric> rubrics,
            ArrayList<Note> notes,
            ArrayList<ChecklistItem> checklists,
            ArrayList<Alarm> alarms,
            ArrayList<Image> images) {
        BloknoteApi bloknoteApi = SyncController.getApi();
        bloknoteApi.updateData(
                hash,
                email,
                rubrics == null ? "{}" : convertRubricsToString(rubrics),
                notes == null ? "{}" : convertNotesToString(notes),
                checklists == null ? "{}" : convertChecklistsToString(checklists),
                alarms == null ? "{}" : convertAlarmsToString(alarms),
                images == null ? "{}" : convertImagesToString(images))
                .enqueue(new Callback<ContainerData>() {
                    @Override
                    public void onResponse(Call<ContainerData> call, Response<ContainerData> response) {
                    }

                    @Override
                    public void onFailure(Call<ContainerData> call, Throwable t) {

                    }
                });

    }

    /*Первый пункт. Возвращает с сервера все данные о пользователе*/
    public static void loadData(ServerCommunicationListener listener, String hash, String email) {
        SyncController.listener = listener;
        BloknoteApi bloknoteApi = SyncController.getApi();
        bloknoteApi.loadData(hash, email)
                .enqueue(new Callback<ContainerData>() {
                    @Override
                    public void onResponse(Call<ContainerData> call, Response<ContainerData> response) {
                        if (response.body().getCode() == -1) {
                            BlocknotePreferencesManager.setUserEmail("");
                            BlocknotePreferencesManager.setUserHash("");
                            listener.errorSync(App.getContext().getString(R.string.enter_or_restore_pwd));
                            return;
                        }
                        if (response.body().getCode() == 0) {
                            makeChoiceSaveAndSendNotes(response.body(), hash, email);
                            listener.dataSyncSuccess();
                        }
                    }

                    @Override
                    public void onFailure(Call<ContainerData> call, Throwable t) {
                        Bloknote.simpleLog("ERROR RETROFIT");
                        Bloknote.simpleLog(t.getMessage());
                        listener.errorSync(App.getContext().getString(R.string.error_network));
                    }
                });
    }

    public static void resetPassword(
            ServerCommunicationListener listener,
            String email) {
        SyncController.listener = listener;
        BloknoteApi bloknoteApi = SyncController.getApi();
        bloknoteApi.resetPassword(email).enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                Bloknote.simpleLog("onResponse");
                listener.passwordResetSuccess();
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Bloknote.simpleLog("onFailure");

            }
        });
    }

    private static void makeChoiceSaveAndSendNotes(ContainerData data, String hash, String email) {


        List<Good> goodsFromServer = data.getGoods();
        List<Rubric> rubricsFromServer = data.getRubrics();
        List<ChecklistItem> checklistItemsFromServer = data.getChecklists();
        List<Alarm> alarmsFromServer = data.getAlarms();
        List<Image> imagesFromServer = data.getImages();
        List<Note> notesFromServer = new ArrayList<>();
        DbHandler dbHandler = DbHandler.getInstance(App.getContext());

        for (Good good : goodsFromServer
        ) {
            notesFromServer.add(Utils.convertGoodToNote(good));
        }

        ArrayList<Note> notesOnDevice = (ArrayList<Note>) dbHandler.getCacheNotes(true);
        ArrayList<Alarm> alarmsOnDevice = (ArrayList<Alarm>) dbHandler.readAllAlarms();
        ArrayList<ChecklistItem> checklistItemsOnDevice = (ArrayList<ChecklistItem>) dbHandler.readAllChecklistItems();
        ArrayList<Rubric> rubricsOnDevice = (ArrayList<Rubric>) dbHandler.readRubrics();
        ArrayList<Image> imagesOnDevice = (ArrayList<Image>) dbHandler.readAllImages();

        //0 пункт
        if (notesFromServer.isEmpty() && notesOnDevice.isEmpty()) {
            listener.syncDataIsEmpty();
            listener = null;
            return;
        }

        //1 пункт
        if (notesFromServer.isEmpty() && !notesOnDevice.isEmpty()) {

            sentDataToServer(hash,
                    email,
                    rubricsOnDevice,
                    notesOnDevice,
                    checklistItemsOnDevice,
                    alarmsOnDevice,
                    imagesOnDevice
            );
            return;
        }

        //2 пункт
        if (notesOnDevice.isEmpty() && !notesFromServer.isEmpty()) {

            SparseIntArray convertIdRubricsSparseArray = new SparseIntArray();

            for (Rubric rubricFromServer : rubricsFromServer) {
                int oldId = rubricFromServer.getId();
                if (!rubricsOnDevice.contains(rubricFromServer)) {
                    dbHandler.addRubric(rubricFromServer);
                    convertIdRubricsSparseArray.append(oldId, rubricFromServer.getId());
                } else {
                    for (Rubric rOnDev : rubricsOnDevice) {
                        if (rOnDev.equals(rubricFromServer)) {
                            convertIdRubricsSparseArray.append(oldId, rOnDev.getId());
                            break;
                        }
                    }
                }
            }

            for (Note note : notesFromServer
            ) {
                note.setIdRubric(convertIdRubricsSparseArray.get(note.getIdRubric()));
                saveNoteOnDevice(checklistItemsFromServer, alarmsFromServer, imagesFromServer, dbHandler, note);
            }
            listener.dataFromServerSaved();
            return;
        }


        if (notesFromServer.size() > 0 && notesOnDevice.size() > 0) {
            SparseIntArray convertIdRubricsFromServerSparseArray = new SparseIntArray();
            SparseIntArray convertIdRubricsOnDeviceSparseArray = new SparseIntArray();
            if (!notesOnDevice.isEmpty() && !notesFromServer.isEmpty()) {
                for (Rubric rubricFromServer : rubricsFromServer) {
                    int oldID = rubricFromServer.getId();
                    if (!rubricsOnDevice.contains(rubricFromServer)) {
                        dbHandler.addRubric(rubricFromServer);
                        convertIdRubricsFromServerSparseArray.append(oldID, rubricFromServer.getId());
                    } else {
                        for (Rubric r : rubricsOnDevice) {
                            if (r.equals(rubricFromServer)) {
                                convertIdRubricsFromServerSparseArray.append(oldID, r.getId());
                                break;
                            }
                        }
                    }
                }

                ArrayList<Rubric> rubricToServer = new ArrayList<>();

                for (Rubric rubricOnDevice : rubricsOnDevice) {
                    int oldID = rubricOnDevice.getId();

                    if (!rubricsFromServer.contains(rubricOnDevice)) {
                        rubricToServer.add(rubricOnDevice);
                    } else {
                        for (Rubric r : rubricsFromServer) {
                            if (r.equals(rubricOnDevice)) {
                                convertIdRubricsOnDeviceSparseArray.append(oldID, r.getId());
                                break;
                            }
                        }
                    }
                    convertIdRubricsOnDeviceSparseArray.append(oldID, rubricOnDevice.getId());

                }
                if (!rubricToServer.isEmpty()) {
                    sentDataToServer(hash, email, rubricToServer, null, null, null, null);
                }

            }

            /*Третий пункт*/
            ArrayList<Note> notesForLocalSave = new ArrayList<>();

            Calendar calendar = GregorianCalendar.getInstance();

            for (Note noteFromServer : notesFromServer) {
                noteFromServer.setIdRubric(convertIdRubricsFromServerSparseArray.get(noteFromServer.getIdRubric()));
                if (!notesOnDevice.contains(noteFromServer)) {
                    notesForLocalSave.add(noteFromServer);
                } else {
                    Note tempNoteOnDevice = notesOnDevice.get(notesOnDevice.indexOf(noteFromServer));
                    long dateEditNoteOnDevice;
                    long dateEditNoteFromServer;

                    calendar.setTime(tempNoteOnDevice.getDateEdit());
                    calendar.set(Calendar.MILLISECOND, 0);
                    dateEditNoteOnDevice = calendar.getTimeInMillis();

                    calendar.setTime(noteFromServer.getDateEdit());
                    calendar.set(Calendar.MILLISECOND, 0);
                    dateEditNoteFromServer = calendar.getTimeInMillis();

                    if (dateEditNoteFromServer > dateEditNoteOnDevice) {
                        notesForLocalSave.add(noteFromServer);
                    }
                }
            }

            ArrayList<Note> notesForSendToServerLastEdit = new ArrayList<>();
            for (Note noteOnDevice : notesOnDevice) {
                if (!notesFromServer.contains(noteOnDevice)) {
                    notesForSendToServerLastEdit.add(noteOnDevice);
                } else {
                    Note tempNoteFromServer = notesFromServer.get(notesFromServer.indexOf(noteOnDevice));
                    long dateEditNoteOnDevice;
                    long dateEditNoteFromServer;
                    calendar.setTime(tempNoteFromServer.getDateEdit());
                    calendar.set(Calendar.MILLISECOND, 0);
                    dateEditNoteFromServer = calendar.getTimeInMillis();
                    calendar.setTime(noteOnDevice.getDateEdit());
                    calendar.set(Calendar.MILLISECOND, 0);
                    dateEditNoteOnDevice = calendar.getTimeInMillis();
                    if (dateEditNoteOnDevice > dateEditNoteFromServer) {
                        notesForSendToServerLastEdit.add(noteOnDevice);
                    }
                }

            }

            Bloknote.simpleLog(
                    "Сохранить на телефоне по причине позднего редактирвания:"
                            + notesForLocalSave
                            + "Отправить на сервер по причине позднего редактирвания:"
                            + notesForSendToServerLastEdit);


            ArrayList<Image> imagesForSendToServer = new ArrayList<>();
            ArrayList<ChecklistItem> checklistItemsForSendToServer = new ArrayList<>();
            ArrayList<Alarm> alarmsForSendToServer = new ArrayList<>();

            for (Note noteForSentToServer : notesForSendToServerLastEdit) {

                for (Image image : imagesOnDevice) {
                    if (image.getIdNoteDateCreate().equals(noteForSentToServer.getDateCreateAt())) {
                        imagesForSendToServer.add(image);
                    }
                }
                for (ChecklistItem item : checklistItemsOnDevice) {
                    if (item.getIdNote().equals(noteForSentToServer.getDateCreateAt())) {
                        checklistItemsForSendToServer.add(item);
                    }
                }
                for (Alarm alarm : alarmsOnDevice) {
                    if (alarm.getNoteId() == noteForSentToServer.getId()) {
                        alarmsForSendToServer.add(alarm);
                    }
                }
            }
            if (!notesForSendToServerLastEdit.isEmpty()) {
                sentDataToServer(
                        hash,
                        email,
                        null,
                        notesForSendToServerLastEdit,
                        checklistItemsForSendToServer,
                        alarmsForSendToServer,
                        imagesForSendToServer
                );
            }
            //На этом этапе уже готовы и отправлены списки для отправки на сервер.

            ArrayList<Image> imagesForLocalSave = new ArrayList<>();
            ArrayList<ChecklistItem> checklistItemsForLocalSave = new ArrayList<>();
            ArrayList<Alarm> alarmsForLocalSave = new ArrayList<>();
            for (Note noteForLocalEdit : notesForLocalSave) {

                for (Image image : imagesFromServer) {
                    if (image.getIdNoteDateCreate().equals(noteForLocalEdit.getDateCreateAt())) {
                        imagesForLocalSave.add(image);
                    }
                }
                for (ChecklistItem item : checklistItemsFromServer) {
                    if (item.getIdNote().equals(noteForLocalEdit.getDateCreateAt())) {
                        checklistItemsForLocalSave.add(item);
                    }
                }
                for (Alarm alarm : alarmsFromServer) {
                    if (alarm.getNoteId() == noteForLocalEdit.getId()) {
                        alarmsForLocalSave.add(alarm);
                    }
                }
            }

            for (Note noteForLocalSave : notesForLocalSave) {
                //Если на устройстве есть такая заметка, то удалить её старую копию
                if (notesOnDevice.contains(noteForLocalSave)) {
                    Note noteForDelete = notesOnDevice.get(notesOnDevice.indexOf(noteForLocalSave));
                    dbHandler.deleteNote(noteForDelete.getId());
                }
                saveNoteOnDevice(
                        checklistItemsForLocalSave,
                        alarmsForLocalSave,
                        imagesForLocalSave,
                        dbHandler, noteForLocalSave
                );
            }


        }

    }


    private static void saveNoteOnDevice
            (List<ChecklistItem> checklistItemsFromServer, List<Alarm> alarmsFromServer, List<Image> imagesFromServer, DbHandler
                    dbHandler, Note note) {
        int tempId = note.getId();
        int newId = (int) dbHandler.addNote(note);
        for (Image image : imagesFromServer
        ) {
            if (image.getIdNoteDateCreate().equals(note.getDateCreateAt())) {
                image.setIdNoteDateCreate(note.getDateCreateAt());
                dbHandler.addImage(image);
            }
        }
        for (ChecklistItem item : checklistItemsFromServer) {
            if (item.getIdNote().equals(note.getDateCreateAt())) {
                item.setIdNote(note.getDateCreateAt());
                dbHandler.addChecklistItem(item);
            }
        }
        for (Alarm alarm : alarmsFromServer) {
            if (alarm.getNoteId() == tempId) {
                alarm.setNoteId(newId);
                dbHandler.addAlarm(alarm);
            }
        }
    }

    public static void deleteDataFromServer(ServerCommunicationListener listener) {
        SyncController.listener = listener;
        BloknoteApi bloknoteApi = SyncController.getApi();
        bloknoteApi.deleteAllFromServer(
                BlocknotePreferencesManager.getUserHash(),
                BlocknotePreferencesManager.getUserEmail())
                .enqueue(new Callback<LoginResponse>() {

                    @Override
                    public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                        listener.dataFromServerDelete();
                        BlocknotePreferencesManager.setUserHash("");
                        BlocknotePreferencesManager.setUserEmail("");
                    }

                    @Override
                    public void onFailure(Call<LoginResponse> call, Throwable t) {
                        Bloknote.simpleLog("deleteDataFromServer onFailure");
                        Bloknote.simpleLog(t.getMessage());
                    }
                });
    }

    //region =========== JSON Convertors ===========
    private static String convertRubricsToString(List<Rubric> rubrics) {
        Gson gson = new Gson();
        String result = gson.toJson(rubrics);
        return result;
    }

    private static String convertNotesToString(List<Note> notes) {
        ArrayList<Good> goods = new ArrayList<>();
        for (Note n : notes
        ) {
            Good e = new Good();
            e.setId(n.getId());
            e.setText(n.getContentText());
            e.setTitle(n.getTitle());
            e.setRubricId(n.getIdRubric());
            e.setCreatedAt(n.getDateCreateAt());
            e.setDateOfEdit(Utils.getDateFormat().format(n.getDateEdit()));
            goods.add(e);
        }
        String result;
        Gson gson = new Gson();
        result = gson.toJson(goods);
        return result;
    }

    private static String convertChecklistsToString(List<ChecklistItem> checklistsItems) {
        String result;
        Gson gson = new Gson();
        result = gson.toJson(checklistsItems);
        return result;
    }

    private static String convertAlarmsToString(List<Alarm> alarms) {
        String result;
        Gson gson = new Gson();
        result = gson.toJson(alarms);
        return result;
    }

    private static String convertImagesToString(List<Image> images) {
        String result;
        Gson gson = new Gson();
        result = gson.toJson(images);
        return result;
    }
//endregion

    public static interface ServerCommunicationListener {
        void syncDataIsEmpty();

        void dataFromServerSaved();

        void errorSync(String message);

        void dataFromDeviceSendOnServer();

        void dataFromServerDelete();

        void dataSyncSuccess();

        void syncProcess();

        void passwordResetSuccess();
    }

}