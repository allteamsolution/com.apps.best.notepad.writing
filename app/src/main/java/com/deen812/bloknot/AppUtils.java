package com.deen812.bloknot;

import android.content.Intent;
import android.net.Uri;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.style.ClickableSpan;
import android.text.style.UnderlineSpan;
import android.view.View;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AppUtils {

    public static final String ourSiteUrl = "http://edaydream.xyz/";
    public static final String ourTerms = "http://edaydream.xyz/terms.php";
    public static final String ourEmail = "mobsupp@yahoo.com";
    public static final String privacyPolicyUrl = "http://edaydream.xyz/PrivacyPolicy.php";
    public static final String feedBackText = "Feedback for app My Notepad\n";
    public static final String gPlayAppUrl = "https://play.google.com/store/apps/details?id=" + App.getContext().getPackageName();
    public static final String ourAppsUrl = "https://play.google.com/store/apps/developer?id=Yuriy+Kolomiec";
    public static final String developersName = "Yuriy Kolomiec";
    public static final String purchaseCancelSite = "https://support.google.com/googlenews/answer/7018481?co=GENIE.Platform%3DAndroid&hl=en";

    public static final String appName = App.getContext().getPackageName();
    public static final String gPlayPackage = "com.android.vending";

    public static Intent openMainSite() {

        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(ourSiteUrl));
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        return intent;
    }

    public static Intent openPrivacyPolicySite() {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(privacyPolicyUrl));
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        return intent;
    }

    public static Intent openTermsOfUse() {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(ourTerms));
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        return intent;
    }

    public static Intent openPurchaseCancelSite() {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(purchaseCancelSite));
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        return intent;
    }

    public static Intent sendMail() {
        Intent intent = new Intent(Intent.ACTION_SEND);
        String[] recipients = {ourEmail};
        intent.putExtra(Intent.EXTRA_EMAIL, recipients);
        intent.putExtra(Intent.EXTRA_SUBJECT, feedBackText);
        intent.setType("text/html");
        Intent.createChooser(intent, "Send email to...");
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        return intent;

    }

    public static Intent shareApp() {
        Intent intentShare = new Intent(Intent.ACTION_SEND);
        intentShare.setType("text/plain");
        intentShare.putExtra(Intent.EXTRA_SUBJECT, appName);
        intentShare.putExtra(Intent.EXTRA_TEXT, "Download application: "
                + gPlayAppUrl);
        intentShare.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        return intentShare;
    }

    public static Intent openGPlay() {
        Intent openGPayIntent = new Intent(Intent.ACTION_VIEW);
        openGPayIntent.setData(Uri.parse(ourAppsUrl));
        openGPayIntent.setPackage(gPlayPackage);
        openGPayIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        return openGPayIntent;
    }

    public static SpannableString makePrivacyUnderline(CharSequence charSequenceText, List<String> textToFindPrivacy) {
        String text = charSequenceText.toString().toLowerCase();
        List<String> listForLinks = textToFindPrivacy;
        SpannableString ss = new SpannableString(charSequenceText);

        for (int i = 0; i < listForLinks.size(); i++) {
            int startOfWord = 0;
            int endOfWord = 0;
            String wordToFind = textToFindPrivacy.get(i).toLowerCase();
            Pattern word = Pattern.compile(wordToFind);
            Matcher match = word.matcher(text);

            while (match.find()) {
                startOfWord = match.start();
                endOfWord = match.end();
            }

            ClickableSpan clickableSpan = new ClickableSpan() {
                @Override
                public void onClick(View textView) {
                    if (wordToFind.toLowerCase().contains("privacy")) {
                        App.getContext().startActivity(openPrivacyPolicySite());
                    } else if (wordToFind.toLowerCase().contains("terms")) {
                        App.getContext().startActivity(openTermsOfUse());
                    } else if (wordToFind.toLowerCase().contains("cancel")) {
                        App.getContext().startActivity(openPurchaseCancelSite());
                    }
                }

                @Override
                public void updateDrawState(TextPaint ds) {
                    super.updateDrawState(ds);
                    ds.setUnderlineText(false);
                }
            };
            ss.setSpan(clickableSpan, startOfWord, endOfWord, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            ss.setSpan(new UnderlineSpan(), startOfWord, endOfWord, 0);

        }
        return ss;
    }
}

